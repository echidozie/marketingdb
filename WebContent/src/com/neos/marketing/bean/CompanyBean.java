package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.exception.*;

public class CompanyBean implements java.io.Serializable {

	private Long companyId  = null;
	private String name  = null;
	private Collection entityCompanyFkList  = null;
	private Collection companyFkList  = null;
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;

	public CompanyBean(){}
	
	public CompanyBean(CompanyBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(CompanyBO theBO){
		this.companyId = theBO.getCompanyId();  
		this.name = theBO.getName();  
		Collection tempEntityCompanyFkListList = theBO.getEntityCompanyFkList();  		
		entityCompanyFkList = new ArrayList();
		if(tempEntityCompanyFkListList != null){
			for (Iterator i = tempEntityCompanyFkListList.iterator(); i.hasNext();){
				EntityBO entityBO = (EntityBO) i.next();
				EntityBean entityBean = new EntityBean(entityBO);
				entityCompanyFkList.add(entityBean);
			}		
		}
		Collection tempCompanyFkListList = theBO.getCompanyFkList();  		
		companyFkList = new ArrayList();
		if(tempCompanyFkListList != null){
			for (Iterator i = tempCompanyFkListList.iterator(); i.hasNext();){
				PersonBO personBO = (PersonBO) i.next();
				PersonBean personBean = new PersonBean(personBO);
				companyFkList.add(personBean);
			}		
		}
	}


	public String deleteAction(){
		ArrayList results = ((CompanyList)JSFUtil.getSessionBean("CompanyList")).getCompanyList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		CompanyActionManager manager = new CompanyActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		CompanyActionManager manager = new CompanyActionManager();
		CompanyBO createBO = new CompanyBO();
		CompanyList company = new CompanyList();
		CompanyDAO dao = new CompanyDAO();
		if(name != "" && name != null){
			createBO.setCompanyId(companyId);
			createBO.setName(name);
			createBO.setEntityCompanyFkList(entityCompanyFkList);
			createBO.setCompanyFkList(companyFkList);
			String returnStr = manager.createAction(createBO);
			if(returnStr.equalsIgnoreCase("success") && dao.getIsDuplicate() == false){
				addNotetext="";
				return manager.listNavigate(maxRows, pageNum);
			}else{
				addNotetext="Error: Duplicate Entry";
				dao.setIsDuplicate(false);
				return "";
			}
		}
		else{
			addNotetext="Please Enter all Credentials";
			return "";
		}
	}
	
	public String editAction(){
		CompanyActionManager manager = new CompanyActionManager();
		CompanyBO editBO = new CompanyBO();
		editBO.setCompanyId(companyId);
		editBO.setName(name);
		CompanyBO pkBO = new CompanyBO();
		pkBO.setCompanyId(this.getCompanyId());
		String returnStr = manager.editAction(editBO, pkBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}

	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, companyId, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), companyId);
	}
	
	public String searchAction(){
		CompanyActionManager manager = new CompanyActionManager();
		CompanyBO searchBO = new CompanyBO();

		if(companyId!=null){		
			searchBO.setCompanyId(companyId);
		}

		if(name!=null){		
			searchBO.setName(name);
		}

		if(entityCompanyFkList!=null){		
			searchBO.setEntityCompanyFkList(entityCompanyFkList);
		}

		if(companyFkList!=null){		
			searchBO.setCompanyFkList(companyFkList);
		}
		JSFUtil.addToSession("CompanySearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("CompanySearchBO");
		companyId=null;
		name=null;
		entityCompanyFkList=null;
		companyFkList=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		CompanyActionManager manager = new CompanyActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
	}
	
	public String createNavigate(){
		CompanyActionManager manager = new CompanyActionManager();
		String theReturn = manager.populateCompanyList();			// Populates the Company List to check if input is duplicate
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		CompanyActionManager manager = new CompanyActionManager();
		String action = manager.editNavigate();
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), companyId); // populates notes list with company type and object
		populateDetailLists();
		return action;
	}
	
	public String listNavigate(){
		CompanyActionManager manager = new CompanyActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		CompanyDAO dao = new CompanyDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		CompanyActionManager manager = new CompanyActionManager();
		String action = manager.searchNavigate();
		clearBean();
		return action;	
	}
	
	public Collection getResults(){
		CompanyActionManager manager = new CompanyActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		CompanyActionManager manager = new CompanyActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		CompanyActionManager manager = new CompanyActionManager();
	    CompanyBean selBean = new CompanyBean();
		selBean.setCompanyId(companyId);
		selBean.setName(name);
		selBean.setEntityCompanyFkList(entityCompanyFkList);
		selBean.setCompanyFkList(companyFkList);
		ArrayList results = ((CompanyList)JSFUtil.getSessionBean("CompanyList")).getCompanyList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		CompanyBean temp = (CompanyBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((CompanyList)JSFUtil.getSessionBean("CompanyList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((CompanyList)JSFUtil.getSessionBean("CompanyList")).setEditDisabled(false);
     			}
     			else{
     				((CompanyList)JSFUtil.getSessionBean("CompanyList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		CompanyActionManager manager = new CompanyActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		CompanyActionManager manager = new CompanyActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		CompanyList list = (CompanyList)JSFUtil.getSessionBean("CompanyList");
		int listSize = list.getCompanyList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		CompanyActionManager manager = new CompanyActionManager();
		this.companyId  = null;
		this.name  = null;
		this.entityCompanyFkList  = null;
		this.companyFkList  = null;
	}

	
	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}

	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a |Long|
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
    	this.companyId = new Long(companyId);
	}

	/**
	 * get the Name attribute
	 * @return the value of the Name attribute
	 */	
	public String getName() {
    	return this.name;
   	}

	/**
	 * set the Name attribute to a value defined by a String
	 * @param name the long value for the attribute
	 */
	public void setName(String name) {
    	this.name = name;
	}

	/**
	 * get the Collection of entityCompanyFkLists
	 * @return the Collection of entityCompanyFkLists
	 */
	public Collection getEntityCompanyFkList() {
		return this.entityCompanyFkList;
	}
	
	/**
	 * get boolean rendered value for list entityCompanyFkList
	 * @return the boolean rendered value for list entityCompanyFkList
	 */
	public boolean isEntityCompanyFkListRendered() {
		if(this.entityCompanyFkList != null){
			return !this.entityCompanyFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the EntityCompanyFkList to a Collection of EntityCompanyFkListBOs
	 * @param list the collection to set the EntityCompanyFkList to
	 */ 	
	public void setEntityCompanyFkList(Collection list) {
		this.entityCompanyFkList = list;
	}

	/**
	 * get the Collection of companyFkLists
	 * @return the Collection of companyFkLists
	 */
	public Collection getCompanyFkList() {
		return this.companyFkList;
	}
	
	/**
	 * get boolean rendered value for list companyFkList
	 * @return the boolean rendered value for list companyFkList
	 */
	public boolean isCompanyFkListRendered() {
		if(this.companyFkList != null){
			return !this.companyFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the CompanyFkList to a Collection of CompanyFkListBOs
	 * @param list the collection to set the CompanyFkList to
	 */ 	
	public void setCompanyFkList(Collection list) {
		this.companyFkList = list;
	}



	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(CompanyBean temp){
    	boolean isEqual = false;
		if(this.getCompanyId().equals(temp.getCompanyId())){
			isEqual = true;
		}
		return isEqual;
	}


    

}