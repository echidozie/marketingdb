package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.exception.*;

public class DetailBean implements java.io.Serializable {

	private Long detailId  = null;
	private Long sourceId  = null;
	private String detail  = null;
	private String source  = null;
	private Collection touchDetailFkList  = null;
	private Collection touchbackDetailFkList  = null;
	private SourceBO detailSourceFkBO = null;
    private ArrayList sourceIdResults = new ArrayList();  
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;

	public DetailBean(){}
	
	public DetailBean(DetailBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(DetailBO theBO){
		this.detailId = theBO.getDetailId();  
		this.sourceId = theBO.getSourceId();  
		this.detail = theBO.getDetail();  
		Collection tempTouchDetailFkListList = theBO.getTouchDetailFkList();  		
		touchDetailFkList = new ArrayList();
		if(tempTouchDetailFkListList != null){
			for (Iterator i = tempTouchDetailFkListList.iterator(); i.hasNext();){
				TouchBO touchBO = (TouchBO) i.next();
				TouchBean touchBean = new TouchBean(touchBO);
				touchDetailFkList.add(touchBean);
			}		
		}
		Collection tempTouchbackDetailFkListList = theBO.getTouchbackDetailFkList();  		
		touchbackDetailFkList = new ArrayList();
		if(tempTouchbackDetailFkListList != null){
			for (Iterator i = tempTouchbackDetailFkListList.iterator(); i.hasNext();){
				TouchbackBO touchbackBO = (TouchbackBO) i.next();
				TouchbackBean touchbackBean = new TouchbackBean(touchbackBO);
				touchbackDetailFkList.add(touchbackBean);
			}		
		}
	}


	public String deleteAction(){
		ArrayList results = ((DetailList)JSFUtil.getSessionBean("DetailList")).getDetailList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		DetailActionManager manager = new DetailActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		DetailActionManager manager = new DetailActionManager();
		DetailBO createBO = new DetailBO();
		if(detail != "" && detail != null){
			createBO.setDetailId(detailId);
			createBO.setSourceId(sourceId);
			createBO.setDetail(detail);
			createBO.setTouchDetailFkList(touchDetailFkList);
			createBO.setTouchbackDetailFkList(touchbackDetailFkList);
			String returnStr = manager.createAction(createBO);
			if(returnStr.equalsIgnoreCase("success")){
				addNotetext="";
				return manager.listNavigate(maxRows, pageNum);
			}else{
				addNotetext="";
				return "";
			}
		}
		else{
			addNotetext = "Please enter detail";
			return "";
		}
	}
	
	public String editAction(){
		DetailActionManager manager = new DetailActionManager();
		DetailBO editBO = new DetailBO();
		editBO.setDetailId(detailId);
		editBO.setSourceId(sourceId);
		editBO.setDetail(detail);
		DetailBO pkBO = new DetailBO();
		pkBO.setDetailId(this.getDetailId());
		String returnStr = manager.editAction(editBO, pkBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, detailId, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), detailId);
	}
	
	public String searchAction(){
		DetailActionManager manager = new DetailActionManager();
		DetailBO searchBO = new DetailBO();

		if(detailId!=null){		
			searchBO.setDetailId(detailId);
		}

		if(sourceId > 0){		
			searchBO.setSourceId(sourceId);
		}

		if(detail.length() > 0){		
			searchBO.setDetail(detail);
		}

		if(touchDetailFkList!=null){		
			searchBO.setTouchDetailFkList(touchDetailFkList);
		}

		if(touchbackDetailFkList!=null){		
			searchBO.setTouchbackDetailFkList(touchbackDetailFkList);
		}
		JSFUtil.addToSession("DetailSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("DetailSearchBO");
		detailId=null;
		sourceId=null;
		detail=null;
		touchDetailFkList=null;
		touchbackDetailFkList=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		DetailActionManager manager = new DetailActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
		try{
			SourceBO aSourceIdBO = new SourceBO();		
			SourceFacade aSourceIdController = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
			Collection sourceIdCol =	aSourceIdController.retrieve(aSourceIdBO);
			sourceIdResults = new ArrayList();
			for (Iterator i = sourceIdCol.iterator(); i.hasNext();) {
				SourceBO temp = (SourceBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getSourceId());
				sourceIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
	}
	
	public String createNavigate(){
		DetailActionManager manager = new DetailActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		DetailActionManager manager = new DetailActionManager();
		String action = manager.editNavigate();
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), detailId); // populates notes list with detail type and object
		populateDetailLists();
		return action;
	}



	public String listNavigate(){
		DetailActionManager manager = new DetailActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		DetailDAO dao = new DetailDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		DetailActionManager manager = new DetailActionManager();
		String action = manager.searchNavigate();
		clearBean();
		try{
			SourceBO aSourceIdBO = new SourceBO();		
			SourceFacade aSourceIdController = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
			Collection sourceIdCol =	aSourceIdController.retrieve(aSourceIdBO);
			sourceIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			sourceIdResults.add(blankItem);
			for (Iterator i = sourceIdCol.iterator(); i.hasNext();) {
				SourceBO temp = (SourceBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getSourceId());
				sourceIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		return action;	
	}
	
	public Collection getResults(){
		DetailActionManager manager = new DetailActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		DetailActionManager manager = new DetailActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		DetailActionManager manager = new DetailActionManager();
	    DetailBean selBean = new DetailBean();
		selBean.setDetailId(detailId);
		selBean.setSourceId(sourceId);
		selBean.setDetail(detail);
		selBean.setTouchDetailFkList(touchDetailFkList);
		selBean.setTouchbackDetailFkList(touchbackDetailFkList);
		ArrayList results = ((DetailList)JSFUtil.getSessionBean("DetailList")).getDetailList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		DetailBean temp = (DetailBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((DetailList)JSFUtil.getSessionBean("DetailList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((DetailList)JSFUtil.getSessionBean("DetailList")).setEditDisabled(false);
     			}
     			else{
     				((DetailList)JSFUtil.getSessionBean("DetailList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		DetailActionManager manager = new DetailActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		DetailActionManager manager = new DetailActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		DetailList list = (DetailList)JSFUtil.getSessionBean("DetailList");
		int listSize = list.getDetailList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		DetailActionManager manager = new DetailActionManager();
		this.detailId  = null;
		this.sourceId  = null;
		this.detail  = null;
		this.touchDetailFkList  = null;
		this.touchbackDetailFkList  = null;
	}
	
	private void getSourceString(Long id) {
		if(this.source != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.sourceIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.sourceIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.source = ((SelectItem) this.sourceIdResults.get(i)).getLabel();
				break;
			}
		}
	}

	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}
		
	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a |Long|
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
    	this.detailId = new Long(detailId);
	}

	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a |Long|
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
    	this.sourceId = new Long(sourceId);
	}

	/**
	 * get the Detail attribute
	 * @return the value of the Detail attribute
	 */	
	public String getDetail() {
    	return this.detail;
   	}

	/**
	 * set the Detail attribute to a value defined by a String
	 * @param detail the long value for the attribute
	 */
	public void setDetail(String detail) {
    	this.detail = detail;
	}

	/**
	 * get the Collection of touchDetailFkLists
	 * @return the Collection of touchDetailFkLists
	 */
	public Collection getTouchDetailFkList() {
		return this.touchDetailFkList;
	}
	
	/**
	 * get boolean rendered value for list touchDetailFkList
	 * @return the boolean rendered value for list touchDetailFkList
	 */
	public boolean isTouchDetailFkListRendered() {
		if(this.touchDetailFkList != null){
			return !this.touchDetailFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the TouchDetailFkList to a Collection of TouchDetailFkListBOs
	 * @param list the collection to set the TouchDetailFkList to
	 */ 	
	public void setTouchDetailFkList(Collection list) {
		this.touchDetailFkList = list;
	}

	/**
	 * get the Collection of touchbackDetailFkLists
	 * @return the Collection of touchbackDetailFkLists
	 */
	public Collection getTouchbackDetailFkList() {
		return this.touchbackDetailFkList;
	}
	
	/**
	 * get boolean rendered value for list touchbackDetailFkList
	 * @return the boolean rendered value for list touchbackDetailFkList
	 */
	public boolean isTouchbackDetailFkListRendered() {
		if(this.touchbackDetailFkList != null){
			return !this.touchbackDetailFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the TouchbackDetailFkList to a Collection of TouchbackDetailFkListBOs
	 * @param list the collection to set the TouchbackDetailFkList to
	 */ 	
	public void setTouchbackDetailFkList(Collection list) {
		this.touchbackDetailFkList = list;
	}

	public ArrayList getSourceIdResults(){
		return this.sourceIdResults;
	}
	
	public void setSourceIdResults(ArrayList results) {
		this.sourceIdResults = results;
	}
	
	public void setSourceId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setSourceId(vce.getNewValue().toString());
		}else{
			this.setSourceId("");
		}
	}

	public String getSource() {
		this.getSourceString(this.sourceId);
		return this.source;
	}
	
	public void setSource(String name) {
		this.source = name;
		this.getSourceString(this.sourceId);
	}

	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(DetailBean temp){
    	boolean isEqual = false;
		if(this.getDetailId().equals(temp.getDetailId())){
			isEqual = true;
		}
		return isEqual;
	}

	public SourceBO getDetailSourceFkBO() {
		return detailSourceFkBO;
	}

	public void setDetailSourceFkBO(SourceBO detailSourceFkBO) {
		this.detailSourceFkBO = detailSourceFkBO;
	}

    

}