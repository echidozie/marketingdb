package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.DateConverter;
import com.neos.marketing.util.exception.*;

public class DownloadsBean implements java.io.Serializable {

	private Long id  = null;
	private String name  = null;
	private String email  = null;
	private String downloadId  = null;
	private String itemTitle  = null;
	private java.util.Date timestamp  = null;
	private String timestampString = null;
	private String timeRequested  = null;
	private String ipAddress  = null;
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;
	
	public DownloadsBean(){}
	
	public DownloadsBean(DownloadsBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(DownloadsBO theBO){
		this.id = theBO.getId();  
		this.name = theBO.getName();  
		this.email = theBO.getEmail();  
		this.downloadId = theBO.getDownloadId();  
		this.itemTitle = theBO.getItemTitle();  
		this.timestamp = theBO.getTimestamp();  
		this.timeRequested = theBO.getTimeRequested();  
		this.ipAddress = theBO.getIpAddress();  
	}


	public String deleteAction(){
		ArrayList results = ((DownloadsList)JSFUtil.getSessionBean("DownloadsList")).getDownloadsList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		DownloadsActionManager manager = new DownloadsActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		DownloadsActionManager manager = new DownloadsActionManager();
		DownloadsBO createBO = new DownloadsBO();
		createBO.setId(id);
		createBO.setName(name);
		createBO.setEmail(email);
		createBO.setDownloadId(downloadId);
		createBO.setItemTitle(itemTitle);
		createBO.setTimestamp(timestamp);
		createBO.setTimeRequested(timeRequested);
		createBO.setIpAddress(ipAddress);
		String returnStr = manager.createAction(createBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String editAction(){
		DownloadsActionManager manager = new DownloadsActionManager();
		DownloadsBO editBO = new DownloadsBO();
		editBO.setId(id);
		editBO.setName(name);
		editBO.setEmail(email);
		editBO.setDownloadId(downloadId);
		editBO.setItemTitle(itemTitle);
		editBO.setTimestamp(timestamp);
		editBO.setTimeRequested(timeRequested);
		editBO.setIpAddress(ipAddress);
		DownloadsBO pkBO = new DownloadsBO();
		pkBO.setId(this.getId());
		String returnStr = manager.editAction(editBO, pkBO);
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), id); // populates notes list deownloads notes

		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, id, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), id);
	}
	
	public String searchAction(){
		DownloadsActionManager manager = new DownloadsActionManager();
		DownloadsBO searchBO = new DownloadsBO();

		if(id!=null){		
			searchBO.setId(id);
		}

		if(name!=null){		
			searchBO.setName(name);
		}

		if(email!=null){		
			searchBO.setEmail(email);
		}

		if(downloadId!=null){		
			searchBO.setDownloadId(downloadId);
		}

		if(itemTitle!=null){		
			searchBO.setItemTitle(itemTitle);
		}

		if(timestamp!=null){		
			searchBO.setTimestamp(timestamp);
		}

		if(timeRequested!=null){		
			searchBO.setTimeRequested(timeRequested);
		}

		if(ipAddress!=null){		
			searchBO.setIpAddress(ipAddress);
		}
		JSFUtil.addToSession("DownloadsSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("DownloadsSearchBO");
		id=null;
		name=null;
		email=null;
		downloadId=null;
		itemTitle=null;
		timestamp=null;
		timeRequested=null;
		ipAddress=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		DownloadsActionManager manager = new DownloadsActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
	}
	
	public String createNavigate(){
		DownloadsActionManager manager = new DownloadsActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		DownloadsActionManager manager = new DownloadsActionManager();
		String action = manager.editNavigate();
		populateDetailLists();
		return action;
	}
	
	public String listNavigate(){
		DownloadsActionManager manager = new DownloadsActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		DownloadsDAO dao = new DownloadsDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		DownloadsActionManager manager = new DownloadsActionManager();
		String action = manager.searchNavigate();
		clearBean();
		return action;	
	}
	
	public Collection getResults(){
		DownloadsActionManager manager = new DownloadsActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		DownloadsActionManager manager = new DownloadsActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		DownloadsActionManager manager = new DownloadsActionManager();
	    DownloadsBean selBean = new DownloadsBean();
		selBean.setId(id);
		selBean.setName(name);
		selBean.setEmail(email);
		selBean.setDownloadId(downloadId);
		selBean.setItemTitle(itemTitle);
		selBean.setTimestamp(timestamp);
		selBean.setTimeRequested(timeRequested);
		selBean.setIpAddress(ipAddress);
		ArrayList results = ((DownloadsList)JSFUtil.getSessionBean("DownloadsList")).getDownloadsList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		DownloadsBean temp = (DownloadsBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((DownloadsList)JSFUtil.getSessionBean("DownloadsList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((DownloadsList)JSFUtil.getSessionBean("DownloadsList")).setEditDisabled(false);
     			}
     			else{
     				((DownloadsList)JSFUtil.getSessionBean("DownloadsList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		DownloadsActionManager manager = new DownloadsActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		DownloadsActionManager manager = new DownloadsActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		DownloadsList list = (DownloadsList)JSFUtil.getSessionBean("DownloadsList");
		int listSize = list.getDownloadsList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		DownloadsActionManager manager = new DownloadsActionManager();
		this.id  = null;
		this.name  = null;
		this.email  = null;
		this.downloadId  = null;
		this.itemTitle  = null;
		this.timestamp  = null;
		this.timestampString  = null;
		this.timeRequested  = null;
		this.ipAddress  = null;
	}

	
	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}

	/**
	 * get the Id attribute
	 * @return the value of the Id attribute
	 */	
	public Long getId() {
    	return this.id;
   	}

	/**
	 * set the Id attribute to a value defined by a Long
	 * @param id the long value for the attribute
	 */
	public void setId(Long id) {
    	this.id = id;
	}
	/**
	 * overloaded method to set the Id attribute to a value defined by a |Long|
	 * @param id the String value for the attribute
	 */
	public void setId(String id) {
    	this.id = new Long(id);
	}

	/**
	 * get the Name attribute
	 * @return the value of the Name attribute
	 */	
	public String getName() {
    	return this.name;
   	}

	/**
	 * set the Name attribute to a value defined by a String
	 * @param name the long value for the attribute
	 */
	public void setName(String name) {
    	this.name = name;
	}

	/**
	 * get the Email attribute
	 * @return the value of the Email attribute
	 */	
	public String getEmail() {
    	return this.email;
   	}

	/**
	 * set the Email attribute to a value defined by a String
	 * @param email the long value for the attribute
	 */
	public void setEmail(String email) {
    	this.email = email;
	}

	/**
	 * get the DownloadId attribute
	 * @return the value of the DownloadId attribute
	 */	
	public String getDownloadId() {
    	return this.downloadId;
   	}

	/**
	 * set the DownloadId attribute to a value defined by a String
	 * @param downloadId the long value for the attribute
	 */
	public void setDownloadId(String downloadId) {
    	this.downloadId = downloadId;
	}

	/**
	 * get the ItemTitle attribute
	 * @return the value of the ItemTitle attribute
	 */	
	public String getItemTitle() {
    	return this.itemTitle;
   	}

	/**
	 * set the ItemTitle attribute to a value defined by a String
	 * @param itemTitle the long value for the attribute
	 */
	public void setItemTitle(String itemTitle) {
    	this.itemTitle = itemTitle;
	}

	/**
	 * get the Timestamp attribute
	 * @return the value of the Timestamp attribute
	 */	
	public java.util.Date getTimestamp() {
    	return this.timestamp;
   	}

	/**
	 * set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the long value for the attribute
	 */
	public void setTimestamp(java.util.Date timestamp) {
    	this.timestamp = timestamp;
    	this.timestampString = DateConverter.convertDateToString(timestamp);
	}
	
	/**
	 * get the TimestampString attribute
	 * @return the value of the Timestamp attribute
	 */	
	public String getTimestampString() {
    	return this.timestampString;
   	}

	/**
	 * set the TimestampString attribute to a value defined by a String
	 * @param timestamp the long value for the attribute
	 */
	public void setTimestampString(String timestamp) {
    	this.timestampString = timestamp;
    	if(timestamp.length() > 0){
			this.timestamp = DateConverter.convertStringToDate(timestamp);    	
		}
	}	
	/**
	 * overloaded method to set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the String value for the attribute
	 */
	public void setTimestamp(String timestamp) {
    	if (timestamp != null && !timestamp.equals("")) {
    		this.timestamp = DateConverter.convertStringToDate(timestamp);
		}
	}		


	/**
	 * get the TimeRequested attribute
	 * @return the value of the TimeRequested attribute
	 */	
	public String getTimeRequested() {
    	return this.timeRequested;
   	}

	/**
	 * set the TimeRequested attribute to a value defined by a String
	 * @param timeRequested the long value for the attribute
	 */
	public void setTimeRequested(String timeRequested) {
    	this.timeRequested = timeRequested;
	}

	/**
	 * get the IpAddress attribute
	 * @return the value of the IpAddress attribute
	 */	
	public String getIpAddress() {
    	return this.ipAddress;
   	}

	/**
	 * set the IpAddress attribute to a value defined by a String
	 * @param ipAddress the long value for the attribute
	 */
	public void setIpAddress(String ipAddress) {
    	this.ipAddress = ipAddress;
	}



	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(DownloadsBean temp){
    	boolean isEqual = false;
		if(this.getId().equals(temp.getId())){
			isEqual = true;
		}
		return isEqual;
	}


    

}