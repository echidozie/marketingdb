package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.exception.*;

public class EmailBean implements java.io.Serializable {

	private Long emailId  = null;
	private String emailAddress  = null;
	private Long personId  = null;
	private PersonBO emailPersonFkBO = null;
    private ArrayList personIdResults = new ArrayList();
    private String person = null;
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;
	
	public EmailBean(){}
	
	public EmailBean(EmailBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(EmailBO theBO){
		this.emailId = theBO.getEmailId();  
		this.emailAddress = theBO.getEmailAddress();  
		this.personId = theBO.getPersonId();  
	}


	public String deleteAction(){
		ArrayList results = ((EmailList)JSFUtil.getSessionBean("EmailList")).getEmailList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		EmailActionManager manager = new EmailActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		EmailActionManager manager = new EmailActionManager();
		EmailBO createBO = new EmailBO();
		EmailDAO dao = new EmailDAO();
		if(emailAddress != "" && emailAddress != null){
			createBO.setEmailId(emailId);
			createBO.setEmailAddress(emailAddress);
			createBO.setPersonId(personId);
			String returnStr = manager.createAction(createBO);
			if(returnStr.equalsIgnoreCase("success")&& dao.getIsDuplicate() == false){
				addNotetext="";
				return manager.listNavigate(maxRows, pageNum);
			}else{
				addNotetext="Error: Duplicate Entry";
				dao.setIsDuplicate(false);
				return "";
			}
		}
		else{
			addNotetext="Please Enter Email";
			return "";
		}
	}
	
	public String editAction(){
		EmailActionManager manager = new EmailActionManager();
		EmailBO editBO = new EmailBO();
		editBO.setEmailId(emailId);
		editBO.setEmailAddress(emailAddress);
		editBO.setPersonId(personId);
		EmailBO pkBO = new EmailBO();
		pkBO.setEmailId(this.getEmailId());
		String returnStr = manager.editAction(editBO, pkBO);
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), emailId);	// populates the notebean with the notes for this id
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, emailId, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), emailId);
	}
	
	public String searchAction(){
		EmailActionManager manager = new EmailActionManager();
		EmailBO searchBO = new EmailBO();

		if(emailId!=null){		
			searchBO.setEmailId(emailId);
		}

		if(emailAddress.length() > 0){		
			searchBO.setEmailAddress(emailAddress);
		}

		if(personId > 0){		
			searchBO.setPersonId(personId);
		}
		JSFUtil.addToSession("EmailSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("EmailSearchBO");
		emailId=null;
		emailAddress=null;
		personId=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		EmailActionManager manager = new EmailActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
		try{
			PersonBO aPersonIdBO = new PersonBO();		
			PersonFacade aPersonIdController = (PersonFacade) FacadeFactory.getFacade("PersonFacade");
			Collection personIdCol =	aPersonIdController.retrieve(aPersonIdBO);
			personIdResults = new ArrayList();
			for (Iterator i = personIdCol.iterator(); i.hasNext();) {
				PersonBO temp = (PersonBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getPersonId());
				personIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
	}
	
	public String createNavigate(){
		EmailActionManager manager = new EmailActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		EmailActionManager manager = new EmailActionManager();
		String action = manager.editNavigate();
		populateDetailLists();
		return action;
	}
	
	public String listNavigate(){
		EmailActionManager manager = new EmailActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		EmailDAO dao = new EmailDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		EmailActionManager manager = new EmailActionManager();
		String action = manager.searchNavigate();
		clearBean();
		try{
			PersonBO aPersonIdBO = new PersonBO();		
			PersonFacade aPersonIdController = (PersonFacade) FacadeFactory.getFacade("PersonFacade");
			Collection personIdCol =	aPersonIdController.retrieve(aPersonIdBO);
			personIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			personIdResults.add(blankItem);
			for (Iterator i = personIdCol.iterator(); i.hasNext();) {
				PersonBO temp = (PersonBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getPersonId());
				personIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		return action;	
	}
	
	public Collection getResults(){
		EmailActionManager manager = new EmailActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		EmailActionManager manager = new EmailActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		EmailActionManager manager = new EmailActionManager();
	    EmailBean selBean = new EmailBean();
		selBean.setEmailId(emailId);
		selBean.setEmailAddress(emailAddress);
		selBean.setPersonId(personId);
		ArrayList results = ((EmailList)JSFUtil.getSessionBean("EmailList")).getEmailList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		EmailBean temp = (EmailBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((EmailList)JSFUtil.getSessionBean("EmailList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((EmailList)JSFUtil.getSessionBean("EmailList")).setEditDisabled(false);
     			}
     			else{
     				((EmailList)JSFUtil.getSessionBean("EmailList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		EmailActionManager manager = new EmailActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		EmailActionManager manager = new EmailActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		EmailList list = (EmailList)JSFUtil.getSessionBean("EmailList");
		int listSize = list.getEmailList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		EmailActionManager manager = new EmailActionManager();
		this.emailId  = null;
		this.emailAddress  = null;
		this.personId  = null;
	}
	
	private void getPersonString(Long id) {
		if(this.person != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.personIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.personIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.person = ((SelectItem) this.personIdResults.get(i)).getLabel();
				break;
			}
		}
	}

	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}
	
	/**
	 * get the EmailId attribute
	 * @return the value of the EmailId attribute
	 */	
	public Long getEmailId() {
    	return this.emailId;
   	}

	/**
	 * set the EmailId attribute to a value defined by a Long
	 * @param emailId the long value for the attribute
	 */
	public void setEmailId(Long emailId) {
    	this.emailId = emailId;
	}
	/**
	 * overloaded method to set the EmailId attribute to a value defined by a |Long|
	 * @param emailId the String value for the attribute
	 */
	public void setEmailId(String emailId) {
    	this.emailId = new Long(emailId);
	}

	/**
	 * get the EmailAddress attribute
	 * @return the value of the EmailAddress attribute
	 */	
	public String getEmailAddress() {
    	return this.emailAddress;
   	}

	/**
	 * set the EmailAddress attribute to a value defined by a String
	 * @param emailAddress the long value for the attribute
	 */
	public void setEmailAddress(String emailAddress) {
    	this.emailAddress = emailAddress;
	}

	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a |Long|
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
    	this.personId = new Long(personId);
	}

	public ArrayList getPersonIdResults(){
		return this.personIdResults;
	}
	
	public void setPersonIdResults(ArrayList results) {
		this.personIdResults = results;
	}
	
	public void setPersonId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setPersonId(vce.getNewValue().toString());
		}else{
			this.setPersonId("");
		}
	}

	public void setPerson(String name){
		this.person = name;
		this.getPersonString(this.personId);
	}
	
	public String getPerson(){
		this.getPersonString(this.personId);
		return this.person;
	}

	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(EmailBean temp){
    	boolean isEqual = false;
		if(this.getEmailId().equals(temp.getEmailId())){
			isEqual = true;
		}
		return isEqual;
	}

	public PersonBO getEmailPersonFkBO() {
		return emailPersonFkBO;
	}

	public void setEmailPersonFkBO(PersonBO emailPersonFkBO) {
		this.emailPersonFkBO = emailPersonFkBO;
	}

    

}