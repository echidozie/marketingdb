package com.neos.marketing.bean;

import java.util.ArrayList; 
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.exception.*;

public class EntityBean implements java.io.Serializable {

	private Long entityId  = null;
	private Long companyId  = null;
	private Long personId  = null;
	private String companyName = null;
	private String firstName = null;
	private String lastName = null;
	private String company = null;
	private String person = null;
	private Collection touchEntityFkList  = null;
	private Collection touchbackEntityFkList  = null;
	private CompanyBO entityCompanyFkBO = null;
	private PersonBO entityPersonFkBO = null;
    private ArrayList companyIdResults = new ArrayList();  
    private ArrayList personIdResults = new ArrayList();  
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	
	private String addNotetext = null;
	
	public EntityBean(){}
	
	public EntityBean(EntityBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(EntityBO theBO){
		this.entityId = theBO.getEntityId();  
		this.companyId = theBO.getCompanyId();  
		this.personId = theBO.getPersonId();  
		this.companyName = theBO.getCompanyName();
		this.firstName = theBO.getFirstName();
		this.lastName = theBO.getLastName();
		Collection tempTouchEntityFkListList = theBO.getTouchEntityFkList();  		
		touchEntityFkList = new ArrayList();
		if(tempTouchEntityFkListList != null){
			for (Iterator i = tempTouchEntityFkListList.iterator(); i.hasNext();){
				TouchBO touchBO = (TouchBO) i.next();
				TouchBean touchBean = new TouchBean(touchBO);
				touchEntityFkList.add(touchBean);
			}		
		}
		Collection tempTouchbackEntityFkListList = theBO.getTouchbackEntityFkList();  		
		touchbackEntityFkList = new ArrayList();
		if(tempTouchbackEntityFkListList != null){
			for (Iterator i = tempTouchbackEntityFkListList.iterator(); i.hasNext();){
				TouchbackBO touchbackBO = (TouchbackBO) i.next();
				TouchbackBean touchbackBean = new TouchbackBean(touchbackBO);
				touchbackEntityFkList.add(touchbackBean);
			}		
		}
	}


	public String deleteAction(){
		ArrayList results = ((EntityList)JSFUtil.getSessionBean("EntityList")).getEntityList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		EntityActionManager manager = new EntityActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		EntityActionManager manager = new EntityActionManager();
		EntityBO createBO = new EntityBO();
		createBO.setEntityId(entityId);
		createBO.setCompanyId(companyId);
		createBO.setPersonId(personId);
		createBO.setTouchEntityFkList(touchEntityFkList);
		createBO.setTouchbackEntityFkList(touchbackEntityFkList);
		String returnStr = manager.createAction(createBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String editAction(){
		EntityActionManager manager = new EntityActionManager();
		EntityBO editBO = new EntityBO();
		editBO.setEntityId(entityId);
		editBO.setCompanyId(companyId);
		editBO.setPersonId(personId);
		EntityBO pkBO = new EntityBO();
		pkBO.setEntityId(this.getEntityId());
		String returnStr = manager.editAction(editBO, pkBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String searchAction(){
		EntityActionManager manager = new EntityActionManager();
		EntityBO searchBO = new EntityBO();

		if(entityId!=null){		
			searchBO.setEntityId(entityId);
		}

		/*if(companyId!=null){		
			searchBO.setCompanyId(companyId);
		}*/

		if(personId > 0){		
			searchBO.setPersonId(personId);
		}

		if(touchEntityFkList!=null){		
			searchBO.setTouchEntityFkList(touchEntityFkList);
		}

		if(touchbackEntityFkList!=null){		
			searchBO.setTouchbackEntityFkList(touchbackEntityFkList);
		}
		JSFUtil.addToSession("EntitySearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("EntitySearchBO");
		entityId=null;
		companyId=null;
		personId=null;
		touchEntityFkList=null;
		touchbackEntityFkList=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		EntityActionManager manager = new EntityActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
		try{
			CompanyBO aCompanyIdBO = new CompanyBO();		
			CompanyFacade aCompanyIdController = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");
			Collection companyIdCol =	aCompanyIdController.retrieve(aCompanyIdBO);
			companyIdResults = new ArrayList();
			for (Iterator i = companyIdCol.iterator(); i.hasNext();) {
				CompanyBO temp = (CompanyBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getCompanyId());
				companyIdResults.add(item);
			}
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
		try{
			PersonBO aPersonIdBO = new PersonBO();		
			PersonFacade aPersonIdController = (PersonFacade) FacadeFactory.getFacade("PersonFacade");
			Collection personIdCol =	aPersonIdController.retrieve(aPersonIdBO);
			personIdResults = new ArrayList();
			for (Iterator i = personIdCol.iterator(); i.hasNext();) {
				PersonBO temp = (PersonBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getPersonId());
				personIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
	}
	
	public String createNavigate(){
		EntityActionManager manager = new EntityActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
	
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, entityId, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), entityId);
	}
	
	public String editNavigate(){
		EntityActionManager manager = new EntityActionManager();
		String action = manager.editNavigate();
		populateDetailLists();
		
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), entityId);
		return action;
	}
	
	public String listNavigate(){
		EntityActionManager manager = new EntityActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		EntityDAO dao = new EntityDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		EntityActionManager manager = new EntityActionManager();
		String action = manager.searchNavigate();
		clearBean();
		try{
			CompanyBO aCompanyIdBO = new CompanyBO();		
			CompanyFacade aCompanyIdController = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");
			Collection companyIdCol =	aCompanyIdController.retrieve(aCompanyIdBO);
			companyIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			companyIdResults.add(blankItem);
			for (Iterator i = companyIdCol.iterator(); i.hasNext();) {
				CompanyBO temp = (CompanyBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getCompanyId());
				companyIdResults.add(item);
			}
			this.getCompanyString(companyId);
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		try{
			PersonBO aPersonIdBO = new PersonBO();		
			PersonFacade aPersonIdController = (PersonFacade) FacadeFactory.getFacade("PersonFacade");
			Collection personIdCol =	aPersonIdController.retrieve(aPersonIdBO);
			personIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			personIdResults.add(blankItem);
			for (Iterator i = personIdCol.iterator(); i.hasNext();) {
				PersonBO temp = (PersonBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getPersonId());
				personIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		return action;	
	}
	
	public Collection getResults(){
		EntityActionManager manager = new EntityActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		EntityActionManager manager = new EntityActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		EntityActionManager manager = new EntityActionManager();
	    EntityBean selBean = new EntityBean();
		selBean.setEntityId(entityId);
		selBean.setCompanyId(companyId);
		selBean.setPersonId(personId);
		selBean.setTouchEntityFkList(touchEntityFkList);
		selBean.setTouchbackEntityFkList(touchbackEntityFkList);
		ArrayList results = ((EntityList)JSFUtil.getSessionBean("EntityList")).getEntityList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		EntityBean temp = (EntityBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((EntityList)JSFUtil.getSessionBean("EntityList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((EntityList)JSFUtil.getSessionBean("EntityList")).setEditDisabled(false);
     			}
     			else{
     				((EntityList)JSFUtil.getSessionBean("EntityList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		EntityActionManager manager = new EntityActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		EntityActionManager manager = new EntityActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		EntityList list = (EntityList)JSFUtil.getSessionBean("EntityList");
		int listSize = list.getEntityList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		EntityActionManager manager = new EntityActionManager();
		this.entityId  = null;
		this.companyId  = null;
		this.personId  = null;
		this.touchEntityFkList  = null;
		this.touchbackEntityFkList  = null;
	}
	
	private void getCompanyString(Long id) {
		if(this.company != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.companyIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.companyIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.company = ((SelectItem) this.companyIdResults.get(i)).getLabel();
				break;
			}
		}
	}
	
	private void getPersonString(Long id) {
		if(this.person != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.personIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.personIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.person = ((SelectItem) this.personIdResults.get(i)).getLabel();
				break;
			}
		}
	}

	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}
	
	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a |Long|
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
    	this.entityId = new Long(entityId);
	}

	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
		this.getCompanyString(this.companyId);
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
    	this.getCompanyString(companyId);
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a |Long|
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
    	this.companyId = new Long(companyId);
	}

	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
		this.getPersonString(this.personId);
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
    	this.getPersonString(this.personId);
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a |Long|
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
    	this.personId = new Long(personId);
	}

	public String getCompany() {
		return this.company;
	}
	
	public void setCompany(String name) {
		this.company = name;
	}
	
	public String getPerson() {
		return this.person;
	}
	
	public void setPerson(String name) {
		this.person = name;
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * get the Collection of touchEntityFkLists
	 * @return the Collection of touchEntityFkLists
	 */
	public Collection getTouchEntityFkList() {
		return this.touchEntityFkList;
	}
	
	/**
	 * get boolean rendered value for list touchEntityFkList
	 * @return the boolean rendered value for list touchEntityFkList
	 */
	public boolean isTouchEntityFkListRendered() {
		if(this.touchEntityFkList != null){
			return !this.touchEntityFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the TouchEntityFkList to a Collection of TouchEntityFkListBOs
	 * @param list the collection to set the TouchEntityFkList to
	 */ 	
	public void setTouchEntityFkList(Collection list) {
		this.touchEntityFkList = list;
	}

	/**
	 * get the Collection of touchbackEntityFkLists
	 * @return the Collection of touchbackEntityFkLists
	 */
	public Collection getTouchbackEntityFkList() {
		return this.touchbackEntityFkList;
	}
	
	/**
	 * get boolean rendered value for list touchbackEntityFkList
	 * @return the boolean rendered value for list touchbackEntityFkList
	 */
	public boolean isTouchbackEntityFkListRendered() {
		if(this.touchbackEntityFkList != null){
			return !this.touchbackEntityFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the TouchbackEntityFkList to a Collection of TouchbackEntityFkListBOs
	 * @param list the collection to set the TouchbackEntityFkList to
	 */ 	
	public void setTouchbackEntityFkList(Collection list) {
		this.touchbackEntityFkList = list;
	}

	public ArrayList getCompanyIdResults(){
		return this.companyIdResults;
	}
	
	public void setCompanyIdResults(ArrayList results) {
		this.companyIdResults = results;
	}
	
	public void setCompanyId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setCompanyId(vce.getNewValue().toString());
		}else{
			this.setCompanyId("");
		}
	}
	public ArrayList getPersonIdResults(){
		return this.personIdResults;
	}
	
	public void setPersonIdResults(ArrayList results) {
		this.personIdResults = results;
	}
	
	public void setPersonId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setPersonId(vce.getNewValue().toString());
		}else{
			this.setPersonId("");
		}
	}


	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(EntityBean temp){
    	boolean isEqual = false;
		if(this.getEntityId().equals(temp.getEntityId())){
			isEqual = true;
		}
		return isEqual;
	}

	public CompanyBO getEntityCompanyFkBO() {
		return entityCompanyFkBO;
	}

	public void setEntityCompanyFkBO(CompanyBO entityCompanyFkBO) {
		this.entityCompanyFkBO = entityCompanyFkBO;
	}
	public PersonBO getEntityPersonFkBO() {
		return entityPersonFkBO;
	}

	public void setEntityPersonFkBO(PersonBO entityPersonFkBO) {
		this.entityPersonFkBO = entityPersonFkBO;
	}

    

}