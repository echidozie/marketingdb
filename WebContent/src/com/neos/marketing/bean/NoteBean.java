package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class NoteBean implements java.io.Serializable {

	private Long noteId  = null;
	private String objectType  = null;
	private Long objectId  = null;
	private String note  = null;
	private int pageNum = 0;
	private int maxRows = 15;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	
	public NoteBean(){}
	
	public NoteBean(NoteBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(NoteBO theBO){
		this.noteId = theBO.getNoteId();  
		this.objectType = theBO.getObjectType();  
		this.objectId = theBO.getObjectId();  
		this.note = theBO.getNote();  
	}


	public String deleteAction(){
		ArrayList results = ((NoteList)JSFUtil.getSessionBean("NoteList")).getNoteList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		NoteActionManager manager = new NoteActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		NoteActionManager manager = new NoteActionManager();
		NoteBO createBO = new NoteBO();
		createBO.setNoteId(noteId);
		createBO.setObjectType(objectType);
		createBO.setObjectId(objectId);
		createBO.setNote(note);
		String returnStr = manager.createAction(createBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	/* function to create Note called in other beans */
	public void createNoteAction(String createNoteType, Long createObjectId, String createNote){
		NoteActionManager manager = new NoteActionManager();
		NoteBO createBO = new NoteBO();
		this.noteId = null;
		createBO.setNoteId(noteId);
		createBO.setObjectType(createNoteType);
		createBO.setObjectId(createObjectId);
		createBO.setNote(createNote);
		manager.createAction(createBO);
	}
	
	public String editAction(){
		NoteActionManager manager = new NoteActionManager();
		NoteBO editBO = new NoteBO();
		editBO.setNoteId(noteId);
		editBO.setObjectType(objectType);
		editBO.setObjectId(objectId);
		editBO.setNote(note);
		NoteBO pkBO = new NoteBO();
		pkBO.setNoteId(this.getNoteId());
		String returnStr = manager.editAction(editBO, pkBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String searchAction(){
		NoteActionManager manager = new NoteActionManager();
		NoteBO searchBO = new NoteBO();

		if(noteId!=null){		
			searchBO.setNoteId(noteId);
		}

		if(objectType!=null){		
			searchBO.setObjectType(objectType);
		}

		if(objectId!=null){		
			searchBO.setObjectId(objectId);
		}

		if(note!=null){		
			searchBO.setNote(note);
		}
		JSFUtil.addToSession("NoteSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("NoteSearchBO");
		noteId=null;
		objectType=null;
		objectId=null;
		note=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		NoteActionManager manager = new NoteActionManager();
		return manager.deleteNavigate();
	}
	
	/* Function called from another object's edit page to delete specified note*/
	public void deleteNoteAction(){
		NoteActionManager manager = new NoteActionManager();
		manager.deleteNoteAction(noteId, objectType, objectId, note);
	}

	private void populateDetailLists() {
	}
	
	public String createNavigate(){
		NoteActionManager manager = new NoteActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		NoteActionManager manager = new NoteActionManager();
		String action = manager.editNavigate();
		populateDetailLists();
		return action;
	}
	
	/* Function called from another object's edit page to delete specified note*/
	public void editNoteAction(){
		NoteActionManager manager = new NoteActionManager();
		manager.editNoteAction(noteId, objectType, objectId, note);
	}
	
	public String listNavigate(){
		NoteActionManager manager = new NoteActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}

	/* Populates the notes List with the object type and objectId */
	public void populateFilter(String objectType, Long objectId){
		NoteActionManager manager = new NoteActionManager();
		manager.populateFilter(objectType, objectId);
		return;
	}
	
	
	public String searchNavigate(){
		NoteActionManager manager = new NoteActionManager();
		String action = manager.searchNavigate();
		clearBean();
		return action;	
	}
	
	public Collection getResults(){
		NoteActionManager manager = new NoteActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		NoteActionManager manager = new NoteActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		NoteActionManager manager = new NoteActionManager();
	    NoteBean selBean = new NoteBean();
		selBean.setNoteId(noteId);
		selBean.setObjectType(objectType);
		selBean.setObjectId(objectId);
		selBean.setNote(note);
		ArrayList results = ((NoteList)JSFUtil.getSessionBean("NoteList")).getNoteList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		NoteBean temp = (NoteBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((NoteList)JSFUtil.getSessionBean("NoteList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((NoteList)JSFUtil.getSessionBean("NoteList")).setEditDisabled(false);
     			}
     			else{
     				((NoteList)JSFUtil.getSessionBean("NoteList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		NoteActionManager manager = new NoteActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		NoteActionManager manager = new NoteActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		NoteList list = (NoteList)JSFUtil.getSessionBean("NoteList");
		int listSize = list.getNoteList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		NoteActionManager manager = new NoteActionManager();
		this.noteId  = null;
		this.objectType  = null;
		this.objectId  = null;
		this.note  = null;
	}


	/**
	 * get the NoteId attribute
	 * @return the value of the NoteId attribute
	 */	
	public Long getNoteId() {
    	return this.noteId;
   	}

	/**
	 * set the NoteId attribute to a value defined by a Long
	 * @param noteId the long value for the attribute
	 */
	public void setNoteId(Long noteId) {
    	this.noteId = noteId;
	}
	/**
	 * overloaded method to set the NoteId attribute to a value defined by a |Long|
	 * @param noteId the String value for the attribute
	 */
	public void setNoteId(String noteId) {
    	this.noteId = new Long(noteId);
	}

	/**
	 * get the ObjectType attribute
	 * @return the value of the ObjectType attribute
	 */	
	public String getObjectType() {
    	return this.objectType;
   	}

	/**
	 * set the ObjectType attribute to a value defined by a String
	 * @param objectType the long value for the attribute
	 */
	public void setObjectType(String objectType) {
    	this.objectType = objectType;
	}

	/**
	 * get the ObjectId attribute
	 * @return the value of the ObjectId attribute
	 */	
	public Long getObjectId() {
    	return this.objectId;
   	}

	/**
	 * set the ObjectId attribute to a value defined by a Long
	 * @param objectId the long value for the attribute
	 */
	public void setObjectId(Long objectId) {
    	this.objectId = objectId;
	}
	/**
	 * overloaded method to set the ObjectId attribute to a value defined by a |Long|
	 * @param objectId the String value for the attribute
	 */
	public void setObjectId(String objectId) {
    	this.objectId = new Long(objectId);
	}

	/**
	 * get the Note attribute
	 * @return the value of the Note attribute
	 */	
	public String getNote() {
    	return this.note;
   	}

	/**
	 * set the Note attribute to a value defined by a String
	 * @param note the long value for the attribute
	 */
	public void setNote(String note) {
    	this.note = note;
	}



	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(NoteBean temp){
    	boolean isEqual = false;
		if(this.getNoteId().equals(temp.getNoteId())){
			isEqual = true;
		}
		return isEqual;
	}


    

}