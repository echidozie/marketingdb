package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.exception.*;

public class PersonBean implements java.io.Serializable {

	private Long personId  = null;
	private Long entityId  = null;
	private Long emailId = null;
	private String firstName  = null;
	private String lastName  = null;
	private String title  = null;
	private String emailAddress = null;
	private Long companyId  = null;
	private String company = null;
	private Collection emailPersonFkList  = null;
	private Collection entityPersonFkList  = null;
	private Collection touchEntityFkList  = null;
	private Collection touchbackEntityFkList  = null;
	private CompanyBO companyFkBO = null;
    private ArrayList companyIdResults = new ArrayList();  
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;
	
	public PersonBean(){}
	
	public PersonBean(PersonBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(PersonBO theBO){
		this.personId = theBO.getPersonId();  
		this.firstName = theBO.getFirstName();  
		this.lastName = theBO.getLastName();  
		this.title = theBO.getTitle();  
		this.emailAddress = theBO.getemailAddress();
		this.companyId = theBO.getCompanyId();  
		Collection tempEmailPersonFkListList = theBO.getEmailPersonFkList();  		
		emailPersonFkList = new ArrayList();
		if(tempEmailPersonFkListList != null){
			for (Iterator i = tempEmailPersonFkListList.iterator(); i.hasNext();){
				EmailBO emailBO = (EmailBO) i.next();
				EmailBean emailBean = new EmailBean(emailBO);
				emailPersonFkList.add(emailBean);
			}		
		}
		Collection tempEntityPersonFkListList = theBO.getEntityPersonFkList();  		
		entityPersonFkList = new ArrayList();
		if(tempEntityPersonFkListList != null){
			for (Iterator i = tempEntityPersonFkListList.iterator(); i.hasNext();){
				EntityBO entityBO = (EntityBO) i.next();
				EntityBean entityBean = new EntityBean(entityBO);
				entityPersonFkList.add(entityBean);
			}		
		}
	}


	public String deleteAction(){
		ArrayList results = ((PersonList)JSFUtil.getSessionBean("PersonList")).getPersonList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		PersonActionManager manager = new PersonActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		PersonActionManager manager = new PersonActionManager();
		PersonBO createBO = new PersonBO();

		if(firstName!= null && firstName != "" && lastName != "" && lastName!= null){
			createBO.setPersonId(personId);
			createBO.setFirstName(firstName);
			createBO.setLastName(lastName);
			createBO.setTitle(title);
			createBO.setCompanyId(companyId);
			createBO.setEmailPersonFkList(emailPersonFkList);
			createBO.setEntityPersonFkList(entityPersonFkList);
			String returnStr = manager.createAction(createBO);
			if(returnStr.equalsIgnoreCase("success")){
				addNotetext ="";
				PersonDAO dao = new PersonDAO();
				createEntity(dao.personID);
				createEmail(dao.personID);
				return manager.listNavigate(maxRows, pageNum);
			}else{
				addNotetext="";
				return returnStr;
			}
		}
		else{
			addNotetext="Please Enter All Credential";
			return "";
		}
	}
	
	public String createEntity(Long PersonID){	
		EntityActionManager entitymanager = new EntityActionManager();
		EntityBO entitycreateBO = new EntityBO();
		entitycreateBO.setEntityId(entityId);
		entitycreateBO.setCompanyId(companyId);
		entitycreateBO.setPersonId(PersonID);
		entitycreateBO.setTouchEntityFkList(touchEntityFkList);
		entitycreateBO.setTouchbackEntityFkList(touchbackEntityFkList);
		String returnStr = entitymanager.createAction(entitycreateBO);
		if(returnStr.equalsIgnoreCase("success")){
			return entitymanager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createEmail(Long PersonID){
		EmailActionManager manager = new EmailActionManager();
		EmailBO createBO = new EmailBO();
		EmailDAO dao = new EmailDAO();
		if(emailAddress != "" && emailAddress != null){
			createBO.setEmailId(emailId);
			createBO.setEmailAddress(emailAddress);
			createBO.setPersonId(PersonID);
			String returnStr = manager.createAction(createBO);
			if(returnStr.equalsIgnoreCase("success")){
				addNotetext="";
				return "";
			}else{
				return "";
			}
		}
		else{
			return "";
		}
	}
	
	public String editAction(){
		PersonActionManager manager = new PersonActionManager();
		PersonBO editBO = new PersonBO();
		editBO.setPersonId(personId);
		editBO.setFirstName(firstName);
		editBO.setLastName(lastName);
		editBO.setTitle(title);
		editBO.setCompanyId(companyId);
		PersonBO pkBO = new PersonBO();
		pkBO.setPersonId(this.getPersonId());
		String returnStr = manager.editAction(editBO, pkBO);
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), personId);	// populates the notebean with the notes for this id
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, personId, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), personId);
	}
	
	public String searchAction(){
		PersonActionManager manager = new PersonActionManager();
		PersonBO searchBO = new PersonBO();

		if(personId!=null){		
			searchBO.setPersonId(personId);
		}

		if(firstName.length()>0){		
			searchBO.setFirstName(firstName);
		}

		if(lastName.length()>0){		
			searchBO.setLastName(lastName);
		}

		if(title.length()>0){		
			searchBO.setTitle(title);
		}

		if(companyId >0){		
			searchBO.setCompanyId(companyId);
		}

		if(emailPersonFkList!=null){		
			searchBO.setEmailPersonFkList(emailPersonFkList);
		}

		if(entityPersonFkList!=null){		
			searchBO.setEntityPersonFkList(entityPersonFkList);
		}
		JSFUtil.addToSession("PersonSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("PersonSearchBO");
		personId=null;
		firstName=null;
		lastName=null;
		title=null;
		companyId=null;
		emailPersonFkList=null;
		entityPersonFkList=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		PersonActionManager manager = new PersonActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
		try{
			CompanyBO aCompanyIdBO = new CompanyBO();		
			CompanyFacade aCompanyIdController = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");
			Collection companyIdCol =	aCompanyIdController.retrieve(aCompanyIdBO);
			companyIdResults = new ArrayList();
			for (Iterator i = companyIdCol.iterator(); i.hasNext();) {
				CompanyBO temp = (CompanyBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getCompanyId());
				companyIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
	}
	
	public String createNavigate(){
		PersonActionManager manager = new PersonActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		PersonActionManager manager = new PersonActionManager();
		String action = manager.editNavigate();
		populateDetailLists();
		return action;
	}
	
	public String listNavigate(){
		PersonActionManager manager = new PersonActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	
	public void exporttoCSV() throws ApplicationException{
		PersonDAO dao = new PersonDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		PersonActionManager manager = new PersonActionManager();
		String action = manager.searchNavigate();
		clearBean();
		try{
			CompanyBO aCompanyIdBO = new CompanyBO();		
			CompanyFacade aCompanyIdController = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");
			Collection companyIdCol =	aCompanyIdController.retrieve(aCompanyIdBO);
			companyIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			companyIdResults.add(blankItem);
			for (Iterator i = companyIdCol.iterator(); i.hasNext();) {
				CompanyBO temp = (CompanyBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getCompanyId());
				companyIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		return action;	
	}
	
	public Collection getResults(){
		PersonActionManager manager = new PersonActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		PersonActionManager manager = new PersonActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		PersonActionManager manager = new PersonActionManager();
	    PersonBean selBean = new PersonBean();
		selBean.setPersonId(personId);
		selBean.setFirstName(firstName);
		selBean.setLastName(lastName);
		selBean.setTitle(title);
		selBean.setCompanyId(companyId);
		selBean.setEmailPersonFkList(emailPersonFkList);
		selBean.setEntityPersonFkList(entityPersonFkList);
		ArrayList results = ((PersonList)JSFUtil.getSessionBean("PersonList")).getPersonList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		PersonBean temp = (PersonBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((PersonList)JSFUtil.getSessionBean("PersonList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((PersonList)JSFUtil.getSessionBean("PersonList")).setEditDisabled(false);
     			}
     			else{
     				((PersonList)JSFUtil.getSessionBean("PersonList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		PersonActionManager manager = new PersonActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		PersonActionManager manager = new PersonActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		PersonList list = (PersonList)JSFUtil.getSessionBean("PersonList");
		int listSize = list.getPersonList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		PersonActionManager manager = new PersonActionManager();
		this.personId  = null;
		this.firstName  = null;
		this.lastName  = null;
		this.title  = null;
		this.companyId  = null;
		this.emailPersonFkList  = null;
		this.entityPersonFkList  = null;
	}
	
	private void getCompanyString(Long id) {
		if(this.company != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.companyIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.companyIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.company = ((SelectItem) this.companyIdResults.get(i)).getLabel();
				break;
			}
		}
	}


	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setemailAddress(String emailAddress) {
    	this.emailAddress = emailAddress;
   	}
	
	
	public String getemailAddress() {
    	return this.emailAddress;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}
	
	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a |Long|
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
    	this.personId = new Long(personId);
	}

	/**
	 * get the FirstName attribute
	 * @return the value of the FirstName attribute
	 */	
	public String getFirstName() {
    	return this.firstName;
   	}

	/**
	 * set the FirstName attribute to a value defined by a String
	 * @param firstName the long value for the attribute
	 */
	public void setFirstName(String firstName) {
    	this.firstName = firstName;
	}

	/**
	 * get the LastName attribute
	 * @return the value of the LastName attribute
	 */	
	public String getLastName() {
    	return this.lastName;
   	}

	/**
	 * set the LastName attribute to a value defined by a String
	 * @param lastName the long value for the attribute
	 */
	public void setLastName(String lastName) {
    	this.lastName = lastName;
	}

	/**
	 * get the Title attribute
	 * @return the value of the Title attribute
	 */	
	public String getTitle() {
    	return this.title;
   	}

	/**
	 * set the Title attribute to a value defined by a String
	 * @param title the long value for the attribute
	 */
	public void setTitle(String title) {
    	this.title = title;
	}

	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a |Long|
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
    	this.companyId = new Long(companyId);
	}

	/**
	 * get the Collection of emailPersonFkLists
	 * @return the Collection of emailPersonFkLists
	 */
	public Collection getEmailPersonFkList() {
		return this.emailPersonFkList;
	}
	
	/**
	 * get boolean rendered value for list emailPersonFkList
	 * @return the boolean rendered value for list emailPersonFkList
	 */
	public boolean isEmailPersonFkListRendered() {
		if(this.emailPersonFkList != null){
			return !this.emailPersonFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the EmailPersonFkList to a Collection of EmailPersonFkListBOs
	 * @param list the collection to set the EmailPersonFkList to
	 */ 	
	public void setEmailPersonFkList(Collection list) {
		this.emailPersonFkList = list;
	}

	/**
	 * get the Collection of entityPersonFkLists
	 * @return the Collection of entityPersonFkLists
	 */
	public Collection getEntityPersonFkList() {
		return this.entityPersonFkList;
	}
	
	/**
	 * get boolean rendered value for list entityPersonFkList
	 * @return the boolean rendered value for list entityPersonFkList
	 */
	public boolean isEntityPersonFkListRendered() {
		if(this.entityPersonFkList != null){
			return !this.entityPersonFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the EntityPersonFkList to a Collection of EntityPersonFkListBOs
	 * @param list the collection to set the EntityPersonFkList to
	 */ 	
	public void setEntityPersonFkList(Collection list) {
		this.entityPersonFkList = list;
	}

	public ArrayList getCompanyIdResults(){
		return this.companyIdResults;
	}
	
	public void setCompanyIdResults(ArrayList results) {
		this.companyIdResults = results;
	}
	
	public String getCompany(){
		this.getCompanyString(this.companyId);
		return this.company;
	}
	
	public void setCompany(String name){
		this.company = name;
		this.getCompanyString(this.companyId);
	}
	
	public void setCompanyId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setCompanyId(vce.getNewValue().toString());
		}else{
			this.setCompanyId("");
		}
	}


	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(PersonBean temp){
    	boolean isEqual = false;
		if(this.getPersonId().equals(temp.getPersonId())){
			isEqual = true;
		}
		return isEqual;
	}

	public CompanyBO getCompanyFkBO() {
		return companyFkBO;
	}

	public void setCompanyFkBO(CompanyBO companyFkBO) {
		this.companyFkBO = companyFkBO;
	}

    

}