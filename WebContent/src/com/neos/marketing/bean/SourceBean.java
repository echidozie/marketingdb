package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.exception.*;

public class SourceBean implements java.io.Serializable {

	private Long sourceId  = null;
	private String source  = null;
	private Collection detailSourceFkList  = null;
	private Collection touchSourceFkList  = null;
	private Collection touchbackSourceFkList  = null;
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;
	
	public SourceBean(){}
	
	public SourceBean(SourceBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(SourceBO theBO){
		this.sourceId = theBO.getSourceId();  
		this.source = theBO.getSource();  
		Collection tempDetailSourceFkListList = theBO.getDetailSourceFkList();  		
		detailSourceFkList = new ArrayList();
		if(tempDetailSourceFkListList != null){
			for (Iterator i = tempDetailSourceFkListList.iterator(); i.hasNext();){
				DetailBO detailBO = (DetailBO) i.next();
				DetailBean detailBean = new DetailBean(detailBO);
				detailSourceFkList.add(detailBean);
			}		
		}
		Collection tempTouchSourceFkListList = theBO.getTouchSourceFkList();  		
		touchSourceFkList = new ArrayList();
		if(tempTouchSourceFkListList != null){
			for (Iterator i = tempTouchSourceFkListList.iterator(); i.hasNext();){
				TouchBO touchBO = (TouchBO) i.next();
				TouchBean touchBean = new TouchBean(touchBO);
				touchSourceFkList.add(touchBean);
			}		
		}
		Collection tempTouchbackSourceFkListList = theBO.getTouchbackSourceFkList();  		
		touchbackSourceFkList = new ArrayList();
		if(tempTouchbackSourceFkListList != null){
			for (Iterator i = tempTouchbackSourceFkListList.iterator(); i.hasNext();){
				TouchbackBO touchbackBO = (TouchbackBO) i.next();
				TouchbackBean touchbackBean = new TouchbackBean(touchbackBO);
				touchbackSourceFkList.add(touchbackBean);
			}		
		}
	}


	public String deleteAction(){
		ArrayList results = ((SourceList)JSFUtil.getSessionBean("SourceList")).getSourceList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		SourceActionManager manager = new SourceActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		SourceActionManager manager = new SourceActionManager();
		SourceBO createBO = new SourceBO();
		SourceDAO dao = new SourceDAO();
		if(source != "" && source != null){
			createBO.setSourceId(sourceId);
			createBO.setSource(source);
			createBO.setDetailSourceFkList(detailSourceFkList);
			createBO.setTouchSourceFkList(touchSourceFkList);
			createBO.setTouchbackSourceFkList(touchbackSourceFkList);
			String returnStr = manager.createAction(createBO);
			if(returnStr.equalsIgnoreCase("success") && dao.getIsDuplicate() == false){
				addNotetext="";
				return manager.listNavigate(maxRows, pageNum);
			}else{
				addNotetext="Error: Duplicate Entry";
				dao.setIsDuplicate(false);
				return "";
			}
		}
		else{
			addNotetext="Please Enter a Source";
			return "";
		}
	}
	
	public String editAction(){
		SourceActionManager manager = new SourceActionManager();
		SourceBO editBO = new SourceBO();
		editBO.setSourceId(sourceId);
		editBO.setSource(source);
		SourceBO pkBO = new SourceBO();
		pkBO.setSourceId(this.getSourceId());
		String returnStr = manager.editAction(editBO, pkBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, sourceId, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), sourceId);
	}
	
	public String searchAction(){
		SourceActionManager manager = new SourceActionManager();
		SourceBO searchBO = new SourceBO();

		if(sourceId!=null){		
			searchBO.setSourceId(sourceId);
		}

		if(source!=null){		
			searchBO.setSource(source);
		}

		if(detailSourceFkList!=null){		
			searchBO.setDetailSourceFkList(detailSourceFkList);
		}

		if(touchSourceFkList!=null){		
			searchBO.setTouchSourceFkList(touchSourceFkList);
		}

		if(touchbackSourceFkList!=null){		
			searchBO.setTouchbackSourceFkList(touchbackSourceFkList);
		}
		JSFUtil.addToSession("SourceSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("SourceSearchBO");
		sourceId=null;
		source=null;
		detailSourceFkList=null;
		touchSourceFkList=null;
		touchbackSourceFkList=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		SourceActionManager manager = new SourceActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
	}
	
	public String createNavigate(){
		SourceActionManager manager = new SourceActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		SourceActionManager manager = new SourceActionManager();
		String action = manager.editNavigate();
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), sourceId);	// populates the notebean with the notes for this id
		populateDetailLists();
		return action;
	}
	
	public String listNavigate(){
		SourceActionManager manager = new SourceActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		SourceDAO dao = new SourceDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		SourceActionManager manager = new SourceActionManager();
		String action = manager.searchNavigate();
		clearBean();
		return action;	
	}
	
	public Collection getResults(){
		SourceActionManager manager = new SourceActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		SourceActionManager manager = new SourceActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		SourceActionManager manager = new SourceActionManager();
	    SourceBean selBean = new SourceBean();
		selBean.setSourceId(sourceId);
		selBean.setSource(source);
		selBean.setDetailSourceFkList(detailSourceFkList);
		selBean.setTouchSourceFkList(touchSourceFkList);
		selBean.setTouchbackSourceFkList(touchbackSourceFkList);
		ArrayList results = ((SourceList)JSFUtil.getSessionBean("SourceList")).getSourceList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		SourceBean temp = (SourceBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((SourceList)JSFUtil.getSessionBean("SourceList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((SourceList)JSFUtil.getSessionBean("SourceList")).setEditDisabled(false);
     			}
     			else{
     				((SourceList)JSFUtil.getSessionBean("SourceList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		SourceActionManager manager = new SourceActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		SourceActionManager manager = new SourceActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		SourceList list = (SourceList)JSFUtil.getSessionBean("SourceList");
		int listSize = list.getSourceList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		SourceActionManager manager = new SourceActionManager();
		this.sourceId  = null;
		this.source  = null;
		this.detailSourceFkList  = null;
		this.touchSourceFkList  = null;
		this.touchbackSourceFkList  = null;
	}

	
	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}

	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a |Long|
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
    	this.sourceId = new Long(sourceId);
	}

	/**
	 * get the Source attribute
	 * @return the value of the Source attribute
	 */	
	public String getSource() {
    	return this.source;
   	}

	/**
	 * set the Source attribute to a value defined by a String
	 * @param source the long value for the attribute
	 */
	public void setSource(String source) {
    	this.source = source;
	}

	/**
	 * get the Collection of detailSourceFkLists
	 * @return the Collection of detailSourceFkLists
	 */
	public Collection getDetailSourceFkList() {
		return this.detailSourceFkList;
	}
	
	/**
	 * get boolean rendered value for list detailSourceFkList
	 * @return the boolean rendered value for list detailSourceFkList
	 */
	public boolean isDetailSourceFkListRendered() {
		if(this.detailSourceFkList != null){
			return !this.detailSourceFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the DetailSourceFkList to a Collection of DetailSourceFkListBOs
	 * @param list the collection to set the DetailSourceFkList to
	 */ 	
	public void setDetailSourceFkList(Collection list) {
		this.detailSourceFkList = list;
	}

	/**
	 * get the Collection of touchSourceFkLists
	 * @return the Collection of touchSourceFkLists
	 */
	public Collection getTouchSourceFkList() {
		return this.touchSourceFkList;
	}
	
	/**
	 * get boolean rendered value for list touchSourceFkList
	 * @return the boolean rendered value for list touchSourceFkList
	 */
	public boolean isTouchSourceFkListRendered() {
		if(this.touchSourceFkList != null){
			return !this.touchSourceFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the TouchSourceFkList to a Collection of TouchSourceFkListBOs
	 * @param list the collection to set the TouchSourceFkList to
	 */ 	
	public void setTouchSourceFkList(Collection list) {
		this.touchSourceFkList = list;
	}

	/**
	 * get the Collection of touchbackSourceFkLists
	 * @return the Collection of touchbackSourceFkLists
	 */
	public Collection getTouchbackSourceFkList() {
		return this.touchbackSourceFkList;
	}
	
	/**
	 * get boolean rendered value for list touchbackSourceFkList
	 * @return the boolean rendered value for list touchbackSourceFkList
	 */
	public boolean isTouchbackSourceFkListRendered() {
		if(this.touchbackSourceFkList != null){
			return !this.touchbackSourceFkList.isEmpty();
		}else{
			return false;
		}
	}
	
	/**
	 * set the TouchbackSourceFkList to a Collection of TouchbackSourceFkListBOs
	 * @param list the collection to set the TouchbackSourceFkList to
	 */ 	
	public void setTouchbackSourceFkList(Collection list) {
		this.touchbackSourceFkList = list;
	}



	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(SourceBean temp){
    	boolean isEqual = false;
		if(this.getSourceId().equals(temp.getSourceId())){
			isEqual = true;
		}
		return isEqual;
	}


    

}