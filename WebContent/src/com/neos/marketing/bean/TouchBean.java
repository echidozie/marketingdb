package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.DateConverter;
import com.neos.marketing.util.exception.*;

public class TouchBean implements java.io.Serializable {

	private Long touchId  = null;
	private Long entityId  = null;
	private java.util.Date touchDate  = null;
	private String touchDateString = null;
	private Long sourceId  = null;
	private Long detailId  = null;
	private String entity = null;
	private String source = null;
	private String detail = null;
	private SourceBO touchSourceFkBO = null;
	private EntityBO touchEntityFkBO = null;
	private DetailBO touchDetailFkBO = null;
    private ArrayList entityIdResults = new ArrayList();  
    private ArrayList sourceIdResults = new ArrayList();  
    private ArrayList detailIdResults = new ArrayList();  
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;
	
	public TouchBean(){}
	
	public TouchBean(TouchBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(TouchBO theBO){
		this.touchId = theBO.getTouchId();  
		this.entityId = theBO.getEntityId();  
		this.touchDate = theBO.getTouchDate();  
		this.sourceId = theBO.getSourceId();  
		this.detailId = theBO.getDetailId();  
	}


	public String deleteAction(){
		ArrayList results = ((TouchList)JSFUtil.getSessionBean("TouchList")).getTouchList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		TouchActionManager manager = new TouchActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		TouchActionManager manager = new TouchActionManager();
		TouchBO createBO = new TouchBO();
		createBO.setTouchId(touchId);
		createBO.setEntityId(entityId);
		createBO.setTouchDate(touchDate);
		createBO.setSourceId(sourceId);
		createBO.setDetailId(detailId);
		String returnStr = manager.createAction(createBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String editAction(){
		TouchActionManager manager = new TouchActionManager();
		TouchBO editBO = new TouchBO();
		editBO.setTouchId(touchId);
		editBO.setEntityId(entityId);
		editBO.setTouchDate(touchDate);
		editBO.setSourceId(sourceId);
		editBO.setDetailId(detailId);
		TouchBO pkBO = new TouchBO();
		pkBO.setTouchId(this.getTouchId());
		String returnStr = manager.editAction(editBO, pkBO);
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), touchId);	// populates the notebean with the notes for this id
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, touchId, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), touchId);
	}
	
	public String searchAction(){
		TouchActionManager manager = new TouchActionManager();
		TouchBO searchBO = new TouchBO();

		if(touchId > 0){		
			searchBO.setTouchId(touchId);
		}

		if(entityId > 0){		
			searchBO.setEntityId(entityId);
		}

		/*if(touchDate!=null){		
			searchBO.setTouchDate(touchDate);
		}*/

		if(sourceId > 0){		
			searchBO.setSourceId(sourceId);
		}

		if(detailId > 0){		
			searchBO.setDetailId(detailId);
		}
		JSFUtil.addToSession("TouchSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("TouchSearchBO");
		touchId=null;
		entityId=null;
		touchDate=null;
		sourceId=null;
		detailId=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		TouchActionManager manager = new TouchActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
		try{
			EntityBO aEntityIdBO = new EntityBO();		
			EntityFacade aEntityIdController = (EntityFacade) FacadeFactory.getFacade("EntityFacade");
			Collection entityIdCol =	aEntityIdController.retrieve(aEntityIdBO);
			entityIdResults = new ArrayList();
			for (Iterator i = entityIdCol.iterator(); i.hasNext();) {
				EntityBO temp = (EntityBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getEntityId());
				entityIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
		try{
			SourceBO aSourceIdBO = new SourceBO();		
			SourceFacade aSourceIdController = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
			Collection sourceIdCol =	aSourceIdController.retrieve(aSourceIdBO);
			sourceIdResults = new ArrayList();
			for (Iterator i = sourceIdCol.iterator(); i.hasNext();) {
				SourceBO temp = (SourceBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getSourceId());
				sourceIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
		refreshDetailList();		
	}

	public void changeSourceId(ValueChangeEvent vce) {
		System.out.println("refreshing list");
		if(vce.getNewValue()!=null){
			this.setSourceId(vce.getNewValue().toString());
		}else{
			this.setSourceId("");
		}
		this.refreshDetailList();
	}
	
	private void refreshDetailList() {
		try{
			DetailBO aDetailIdBO = new DetailBO();		
			aDetailIdBO.setSourceId(this.getSourceId());
			DetailFacade aDetailIdController = (DetailFacade) FacadeFactory.getFacade("DetailFacade");
			Collection detailIdCol =	aDetailIdController.retrieve(aDetailIdBO);
			detailIdResults = new ArrayList();
			SelectItem item = new SelectItem();
			item.setLabel("--None Selected--");
			item.setValue("");
			detailIdResults.add(item);			
			for (Iterator i = detailIdCol.iterator(); i.hasNext();) {
				DetailBO temp = (DetailBO) i.next();      	
	 			item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getDetailId());
				detailIdResults.add(item);
			}	

		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}
	}
	
	public String createNavigate(){
		TouchActionManager manager = new TouchActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		TouchActionManager manager = new TouchActionManager();
		String action = manager.editNavigate();
		populateDetailLists();
		return action;
	}
	
	public String listNavigate(){
		TouchActionManager manager = new TouchActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		this.setSourceId((Long) null);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		TouchDAO dao = new TouchDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		TouchActionManager manager = new TouchActionManager();
		String action = manager.searchNavigate();
		clearBean();
		try{
			EntityBO aEntityIdBO = new EntityBO();		
			EntityFacade aEntityIdController = (EntityFacade) FacadeFactory.getFacade("EntityFacade");
			Collection entityIdCol =	aEntityIdController.retrieve(aEntityIdBO);
			entityIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			entityIdResults.add(blankItem);
			for (Iterator i = entityIdCol.iterator(); i.hasNext();) {
				EntityBO temp = (EntityBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getEntityId());
				entityIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		try{
			SourceBO aSourceIdBO = new SourceBO();		
			SourceFacade aSourceIdController = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
			Collection sourceIdCol =	aSourceIdController.retrieve(aSourceIdBO);
			sourceIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			sourceIdResults.add(blankItem);
			for (Iterator i = sourceIdCol.iterator(); i.hasNext();) {
				SourceBO temp = (SourceBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getSourceId());
				sourceIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		try{
			DetailBO aDetailIdBO = new DetailBO();		
			DetailFacade aDetailIdController = (DetailFacade) FacadeFactory.getFacade("DetailFacade");
			Collection detailIdCol =	aDetailIdController.retrieve(aDetailIdBO);
			detailIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			detailIdResults.add(blankItem);
			for (Iterator i = detailIdCol.iterator(); i.hasNext();) {
				DetailBO temp = (DetailBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getDetailId());
				detailIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		return action;	
	}
	
	public Collection getResults(){
		TouchActionManager manager = new TouchActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		TouchActionManager manager = new TouchActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		TouchActionManager manager = new TouchActionManager();
	    TouchBean selBean = new TouchBean();
		selBean.setTouchId(touchId);
		selBean.setEntityId(entityId);
		selBean.setTouchDate(touchDate);
		selBean.setSourceId(sourceId);
		selBean.setDetailId(detailId);
		ArrayList results = ((TouchList)JSFUtil.getSessionBean("TouchList")).getTouchList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		TouchBean temp = (TouchBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((TouchList)JSFUtil.getSessionBean("TouchList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((TouchList)JSFUtil.getSessionBean("TouchList")).setEditDisabled(false);
     			}
     			else{
     				((TouchList)JSFUtil.getSessionBean("TouchList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		TouchActionManager manager = new TouchActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		TouchActionManager manager = new TouchActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		TouchList list = (TouchList)JSFUtil.getSessionBean("TouchList");
		int listSize = list.getTouchList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		TouchActionManager manager = new TouchActionManager();
		this.touchId  = null;
		this.entityId  = null;
		this.touchDate  = null;
		this.touchDateString  = null;
		this.sourceId  = null;
		this.detailId  = null;
	}

	private void getEntityString(Long id) {
		if(this.entity != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.entityIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.entityIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.entity = ((SelectItem) this.entityIdResults.get(i)).getLabel();
				break;
			}
		}
	}
	
	private void getSourceString(Long id) {
		if(this.source != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.sourceIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.sourceIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.source = ((SelectItem) this.sourceIdResults.get(i)).getLabel();
				break;
			}
		}
	}
	
	private void getDetailString(Long id) {
		if(this.detail != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.detailIdResults.size(); i++){
			try {
				Long result = new Long((long) ((SelectItem) this.detailIdResults.get(i)).getValue());
				if(result.equals(id)){
					this.detail = ((SelectItem) this.detailIdResults.get(i)).getLabel();
					break;
				}
			}
			catch(ClassCastException e){
				continue;
			}
		}
	}

	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}

	/**
	 * get the TouchId attribute
	 * @return the value of the TouchId attribute
	 */	
	public Long getTouchId() {
    	return this.touchId;
   	}

	/**
	 * set the TouchId attribute to a value defined by a Long
	 * @param touchId the long value for the attribute
	 */
	public void setTouchId(Long touchId) {
    	this.touchId = touchId;
	}
	/**
	 * overloaded method to set the TouchId attribute to a value defined by a |Long|
	 * @param touchId the String value for the attribute
	 */
	public void setTouchId(String touchId) {
    	this.touchId = new Long(touchId);
	}

	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a |Long|
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
    	this.entityId = new Long(entityId);
	}

	/**
	 * get the TouchDate attribute
	 * @return the value of the TouchDate attribute
	 */	
	public java.util.Date getTouchDate() {
    	return this.touchDate;
   	}

	/**
	 * set the TouchDate attribute to a value defined by a java.util.Date
	 * @param touchDate the long value for the attribute
	 */
	public void setTouchDate(java.util.Date touchDate) {
    	this.touchDate = touchDate;
    	this.touchDateString = DateConverter.convertDateToString(touchDate);
	}
	
	/**
	 * get the TouchDateString attribute
	 * @return the value of the TouchDate attribute
	 */	
	public String getTouchDateString() {
    	return this.touchDateString;
   	}

	/**
	 * set the TouchDateString attribute to a value defined by a String
	 * @param touchDate the long value for the attribute
	 */
	public void setTouchDateString(String touchDate) {
    	this.touchDateString = touchDate;
    	if(touchDate.length() > 0){
			this.touchDate = DateConverter.convertStringToDate(touchDate);    	
		}
	}	
	/**
	 * overloaded method to set the TouchDate attribute to a value defined by a java.util.Date
	 * @param touchDate the String value for the attribute
	 */
	public void setTouchDate(String touchDate) {
    	if (touchDate != null && !touchDate.equals("")) {
    		this.touchDate = DateConverter.convertStringToDate(touchDate);
		}
	}		


	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a |Long|
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
    	this.sourceId = new Long(sourceId);
	}

	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a |Long|
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
    	this.detailId = new Long(detailId);
	}

	public ArrayList getEntityIdResults(){
		return this.entityIdResults;
	}
	
	public void setEntityIdResults(ArrayList results) {
		this.entityIdResults = results;
	}
	
	public void setEntityId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setEntityId(vce.getNewValue().toString());
		}else{
			this.setEntityId("");
		}
	}
	public ArrayList getSourceIdResults(){
		return this.sourceIdResults;
	}
	
	public void setSourceIdResults(ArrayList results) {
		this.sourceIdResults = results;
	}
	
	public void setSourceId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setSourceId(vce.getNewValue().toString());
		}else{
			this.setSourceId("");
		}
	}
	public ArrayList getDetailIdResults(){
		return this.detailIdResults;
	}
	
	public void setDetailIdResults(ArrayList results) {
		this.detailIdResults = results;
	}
	
	public void setDetailId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setDetailId(vce.getNewValue().toString());
		}else{
			this.setDetailId("");
		}
	}

	public String getEntity(){
		this.getEntityString(this.entityId);
		return this.entity;
	}
	
	public void setEntity(String name){
		this.entity = name;
		this.getEntityString(this.entityId);
	}
	
	public String getSource(){
		this.getSourceString(this.sourceId);
		return this.source;
	}
	
	public void setSource(String name){
		this.source = name;
		this.getSourceString(this.sourceId);
	}
	
	public String getDetail(){
		this.getDetailString(this.detailId);
		return this.detail;
	}
	
	public void setDetail(String name){
		this.detail = name;
		this.getDetailString(this.detailId);
	}

	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(TouchBean temp){
    	boolean isEqual = false;
		if(this.getTouchId().equals(temp.getTouchId())){
			isEqual = true;
		}
		return isEqual;
	}

	public SourceBO getTouchSourceFkBO() {
		return touchSourceFkBO;
	}

	public void setTouchSourceFkBO(SourceBO touchSourceFkBO) {
		this.touchSourceFkBO = touchSourceFkBO;
	}
	public EntityBO getTouchEntityFkBO() {
		return touchEntityFkBO;
	}

	public void setTouchEntityFkBO(EntityBO touchEntityFkBO) {
		this.touchEntityFkBO = touchEntityFkBO;
	}
	public DetailBO getTouchDetailFkBO() {
		return touchDetailFkBO;
	}

	public void setTouchDetailFkBO(DetailBO touchDetailFkBO) {
		this.touchDetailFkBO = touchDetailFkBO;
	}

    

}