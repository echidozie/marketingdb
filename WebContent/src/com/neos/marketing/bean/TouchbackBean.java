package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.DateConverter;
import com.neos.marketing.util.exception.*;

public class TouchbackBean implements java.io.Serializable {

	private Long touchbackId  = null;
	private Long entityId  = null;
	private java.util.Date touchbackDate  = null;
	private String touchbackDateString = null;
	private Long sourceId  = null;
	private Long detailId  = null;
	private String entity = null;
	private String source = null;
	private String detail = null;
	private DetailBO touchbackDetailFkBO = null;
	private EntityBO touchbackEntityFkBO = null;
	private SourceBO touchbackSourceFkBO = null;
    private ArrayList entityIdResults = new ArrayList();  
    private ArrayList sourceIdResults = new ArrayList();  
    private ArrayList detailIdResults = new ArrayList();  
	private int pageNum = 0;
	private int maxRows = 15;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;
	private String errorCheck = null;
	
	public TouchbackBean(){}
	
	public TouchbackBean(TouchbackBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(TouchbackBO theBO){
		this.touchbackId = theBO.getTouchbackId();  
		this.entityId = theBO.getEntityId();  
		this.touchbackDate = theBO.getTouchbackDate();  
		this.sourceId = theBO.getSourceId();  
		this.detailId = theBO.getDetailId();  
	}


	public String deleteAction(){
		ArrayList results = ((TouchbackList)JSFUtil.getSessionBean("TouchbackList")).getTouchbackList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		TouchbackActionManager manager = new TouchbackActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		TouchbackActionManager manager = new TouchbackActionManager();
		TouchbackBO createBO = new TouchbackBO();
		if(touchbackDate != null){
			createBO.setTouchbackId(touchbackId);
			createBO.setEntityId(entityId);
			createBO.setTouchbackDate(touchbackDate);
			createBO.setSourceId(sourceId);
			createBO.setDetailId(detailId);
			String returnStr = manager.createAction(createBO);
			if(returnStr.equalsIgnoreCase("success")){
				entity="";
				return manager.listNavigate(maxRows, pageNum);
			}else{
				entity="";
				return returnStr;
			}
		}
		else{
			entity = "Please Enter All Credentials";
			return "";			
		}
	}
	
	public String editAction(){
		TouchbackActionManager manager = new TouchbackActionManager();
		TouchbackBO editBO = new TouchbackBO();
		editBO.setTouchbackId(touchbackId);
		editBO.setEntityId(entityId);
		editBO.setTouchbackDate(touchbackDate);
		editBO.setSourceId(sourceId);
		editBO.setDetailId(detailId);
		TouchbackBO pkBO = new TouchbackBO();
		pkBO.setTouchbackId(this.getTouchbackId());
		String returnStr = manager.editAction(editBO, pkBO);
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), touchbackId);	// populates the notebean with the notes for this id
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
		
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, touchbackId, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), touchbackId);
	}
	

	public String searchAction(){
		TouchbackActionManager manager = new TouchbackActionManager();
		TouchbackBO searchBO = new TouchbackBO();

		if(touchbackId >0){		
			searchBO.setTouchbackId(touchbackId);
		}

		if(entityId > 0){		
			searchBO.setEntityId(entityId);
		}

		/*if(touchbackDate!=null){		
			searchBO.setTouchbackDate(touchbackDate);
		}*/

		if(sourceId > 0){		
			searchBO.setSourceId(sourceId);
		}

		if(detailId > 0){		
			searchBO.setDetailId(detailId);
		}
		JSFUtil.addToSession("TouchbackSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("TouchbackSearchBO");
		touchbackId=null;
		entityId=null;
		touchbackDate=null;
		sourceId=null;
		detailId=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		TouchbackActionManager manager = new TouchbackActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
		try{
			EntityBO aEntityIdBO = new EntityBO();		
			EntityFacade aEntityIdController = (EntityFacade) FacadeFactory.getFacade("EntityFacade");
			Collection entityIdCol =	aEntityIdController.retrieve(aEntityIdBO);
			entityIdResults = new ArrayList();
			for (Iterator i = entityIdCol.iterator(); i.hasNext();) {
				EntityBO temp = (EntityBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getEntityId());
				entityIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
		try{
			SourceBO aSourceIdBO = new SourceBO();		
			SourceFacade aSourceIdController = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
			Collection sourceIdCol =	aSourceIdController.retrieve(aSourceIdBO);
			sourceIdResults = new ArrayList();
			for (Iterator i = sourceIdCol.iterator(); i.hasNext();) {
				SourceBO temp = (SourceBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getSourceId());
				sourceIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
		try{
			DetailBO aDetailIdBO = new DetailBO();		
			DetailFacade aDetailIdController = (DetailFacade) FacadeFactory.getFacade("DetailFacade");
			Collection detailIdCol =	aDetailIdController.retrieve(aDetailIdBO);
			detailIdResults = new ArrayList();
			for (Iterator i = detailIdCol.iterator(); i.hasNext();) {
				DetailBO temp = (DetailBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getDetailId());
				detailIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
		}		
	}
	
	public String createNavigate(){
		TouchbackActionManager manager = new TouchbackActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		TouchbackActionManager manager = new TouchbackActionManager();
		String action = manager.editNavigate();
		populateDetailLists();
		return action;
	}
	
	public String listNavigate(){
		TouchbackActionManager manager = new TouchbackActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		TouchbackDAO dao = new TouchbackDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		TouchbackActionManager manager = new TouchbackActionManager();
		String action = manager.searchNavigate();
		clearBean();
		try{
			EntityBO aEntityIdBO = new EntityBO();		
			EntityFacade aEntityIdController = (EntityFacade) FacadeFactory.getFacade("EntityFacade");
			Collection entityIdCol =	aEntityIdController.retrieve(aEntityIdBO);
			entityIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			entityIdResults.add(blankItem);
			for (Iterator i = entityIdCol.iterator(); i.hasNext();) {
				EntityBO temp = (EntityBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getEntityId());
				entityIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		try{
			SourceBO aSourceIdBO = new SourceBO();		
			SourceFacade aSourceIdController = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
			Collection sourceIdCol =	aSourceIdController.retrieve(aSourceIdBO);
			sourceIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			sourceIdResults.add(blankItem);
			for (Iterator i = sourceIdCol.iterator(); i.hasNext();) {
				SourceBO temp = (SourceBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getSourceId());
				sourceIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		try{
			DetailBO aDetailIdBO = new DetailBO();		
			DetailFacade aDetailIdController = (DetailFacade) FacadeFactory.getFacade("DetailFacade");
			Collection detailIdCol =	aDetailIdController.retrieve(aDetailIdBO);
			detailIdResults = new ArrayList();
			SelectItem blankItem = new SelectItem();
     		blankItem.setLabel("Any");
     		blankItem.setValue("");
			detailIdResults.add(blankItem);
			for (Iterator i = detailIdCol.iterator(); i.hasNext();) {
				DetailBO temp = (DetailBO) i.next();      	
	 			SelectItem item = new SelectItem();
	     		item.setLabel(temp.get_DisplayDescription());
	     		item.setValue(temp.getDetailId());
				detailIdResults.add(item);
			}	
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			action = "error";
		}		
		return action;	
	}
	
	public Collection getResults(){
		TouchbackActionManager manager = new TouchbackActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		TouchbackActionManager manager = new TouchbackActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		TouchbackActionManager manager = new TouchbackActionManager();
	    TouchbackBean selBean = new TouchbackBean();
		selBean.setTouchbackId(touchbackId);
		selBean.setEntityId(entityId);
		selBean.setTouchbackDate(touchbackDate);
		selBean.setSourceId(sourceId);
		selBean.setDetailId(detailId);
		ArrayList results = ((TouchbackList)JSFUtil.getSessionBean("TouchbackList")).getTouchbackList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		TouchbackBean temp = (TouchbackBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((TouchbackList)JSFUtil.getSessionBean("TouchbackList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((TouchbackList)JSFUtil.getSessionBean("TouchbackList")).setEditDisabled(false);
     			}
     			else{
     				((TouchbackList)JSFUtil.getSessionBean("TouchbackList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		TouchbackActionManager manager = new TouchbackActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		TouchbackActionManager manager = new TouchbackActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		TouchbackList list = (TouchbackList)JSFUtil.getSessionBean("TouchbackList");
		int listSize = list.getTouchbackList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		TouchbackActionManager manager = new TouchbackActionManager();
		this.touchbackId  = null;
		this.entityId  = null;
		this.touchbackDate  = null;
		this.touchbackDateString  = null;
		this.sourceId  = null;
		this.detailId  = null;
	}
	
	private void getEntityString(Long id) {
		if(this.entity != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.entityIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.entityIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.entity = ((SelectItem) this.entityIdResults.get(i)).getLabel();
				break;
			}
		}
	}
	
	private void getSourceString(Long id) {
		if(this.source != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.sourceIdResults.size(); i++){
			Long result = new Long((long) ((SelectItem) this.sourceIdResults.get(i)).getValue());
			if(result.equals(id)){
				this.source = ((SelectItem) this.sourceIdResults.get(i)).getLabel();
				break;
			}
		}
	}
	
	private void getDetailString(Long id) {
		if(this.detail != null){
			return;
		}
		this.populateDetailLists();
		for(int i = 0; i < this.detailIdResults.size(); i++){
			try {
				Long result = new Long((long) ((SelectItem) this.detailIdResults.get(i)).getValue());
				if(result.equals(id)){
					this.detail = ((SelectItem) this.detailIdResults.get(i)).getLabel();
					break;
				}
			}
			catch(ClassCastException e){
				continue;
			}
		}
	}

	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}
	
	/**
	 * get the TouchbackId attribute
	 * @return the value of the TouchbackId attribute
	 */	
	public Long getTouchbackId() {
    	return this.touchbackId;
   	}

	/**
	 * set the TouchbackId attribute to a value defined by a Long
	 * @param touchbackId the long value for the attribute
	 */
	public void setTouchbackId(Long touchbackId) {
    	this.touchbackId = touchbackId;
	}
	/**
	 * overloaded method to set the TouchbackId attribute to a value defined by a |Long|
	 * @param touchbackId the String value for the attribute
	 */
	public void setTouchbackId(String touchbackId) {
    	this.touchbackId = new Long(touchbackId);
	}

	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a |Long|
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
    	this.entityId = new Long(entityId);
	}

	/**
	 * get the TouchbackDate attribute
	 * @return the value of the TouchbackDate attribute
	 */	
	public java.util.Date getTouchbackDate() {
    	return this.touchbackDate;
   	}

	/**
	 * set the TouchbackDate attribute to a value defined by a java.util.Date
	 * @param touchbackDate the long value for the attribute
	 */
	public void setTouchbackDate(java.util.Date touchbackDate) {
    	this.touchbackDate = touchbackDate;
    	this.touchbackDateString = DateConverter.convertDateToString(touchbackDate);
	}
	
	/**
	 * get the TouchbackDateString attribute
	 * @return the value of the TouchbackDate attribute
	 */	
	public String getTouchbackDateString() {
    	return this.touchbackDateString;
   	}

	/**
	 * set the TouchbackDateString attribute to a value defined by a String
	 * @param touchbackDate the long value for the attribute
	 */
	public void setTouchbackDateString(String touchbackDate) {
    	this.touchbackDateString = touchbackDate;
    	if(touchbackDate.length() > 0){
			this.touchbackDate = DateConverter.convertStringToDate(touchbackDate);    	
		}
	}	
	/**
	 * overloaded method to set the TouchbackDate attribute to a value defined by a java.util.Date
	 * @param touchbackDate the String value for the attribute
	 */
	public void setTouchbackDate(String touchbackDate) {
    	if (touchbackDate != null && !touchbackDate.equals("")) {
    		this.touchbackDate = DateConverter.convertStringToDate(touchbackDate);
		}
	}		


	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a |Long|
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
    	this.sourceId = new Long(sourceId);
	}

	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a |Long|
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
    	this.detailId = new Long(detailId);
	}

	public ArrayList getEntityIdResults(){
		return this.entityIdResults;
	}
	
	public void setEntityIdResults(ArrayList results) {
		this.entityIdResults = results;
	}
	
	public void setEntityId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setEntityId(vce.getNewValue().toString());
		}else{
			this.setEntityId("");
		}
	}
	public ArrayList getSourceIdResults(){
		return this.sourceIdResults;
	}
	
	public void setSourceIdResults(ArrayList results) {
		this.sourceIdResults = results;
	}
	
	public void setSourceId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setSourceId(vce.getNewValue().toString());
		}else{
			this.setSourceId("");
		}
	}
	public ArrayList getDetailIdResults(){
		return this.detailIdResults;
	}
	
	public void setDetailIdResults(ArrayList results) {
		this.detailIdResults = results;
	}
	
	public void setDetailId(ValueChangeEvent vce){
		if(vce.getNewValue()!=null){
			this.setDetailId(vce.getNewValue().toString());
		}else{
			this.setDetailId("");
		}
	}
	
	public String getEntity(){
		this.getEntityString(this.entityId);
		return this.entity;
	}
	
	public void setEntity(String name){
		this.entity = name;
		this.getEntityString(this.entityId);
	}
	
	public String getSource(){
		this.getSourceString(this.sourceId);
		return this.source;
	}
	
	public void setSource(String name){
		this.source = name;
		this.getSourceString(this.sourceId);
	}
	
	public String getDetail(){
		this.getDetailString(this.detailId);
		return this.detail;
	}
	
	public void setDetail(String name){
		this.detail = name;
		this.getDetailString(this.detailId);
	}

	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(TouchbackBean temp){
    	boolean isEqual = false;
		if(this.getTouchbackId().equals(temp.getTouchbackId())){
			isEqual = true;
		}
		return isEqual;
	}

	public DetailBO getTouchbackDetailFkBO() {
		return touchbackDetailFkBO;
	}

	public void setTouchbackDetailFkBO(DetailBO touchbackDetailFkBO) {
		this.touchbackDetailFkBO = touchbackDetailFkBO;
	}
	public EntityBO getTouchbackEntityFkBO() {
		return touchbackEntityFkBO;
	}

	public void setTouchbackEntityFkBO(EntityBO touchbackEntityFkBO) {
		this.touchbackEntityFkBO = touchbackEntityFkBO;
	}
	public SourceBO getTouchbackSourceFkBO() {
		return touchbackSourceFkBO;
	}

	public void setTouchbackSourceFkBO(SourceBO touchbackSourceFkBO) {
		this.touchbackSourceFkBO = touchbackSourceFkBO;
	}

    

}