package com.neos.marketing.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.DateConverter;
import com.neos.marketing.util.exception.*;

public class VisitorsBean implements java.io.Serializable {

	private Long id  = null;
	private String ipAddress  = null;
	private String hostname  = null;
	private String pageVisited  = null;
	private String referrer  = null;
	private String timeRequested  = null;
	private java.util.Date timestamp  = null;
	private String timestampString = null;
	private String os  = null;
	private String browser  = null;
	private int pageNum = 0;
	private int maxRows = 50;
	private boolean isLastPage = false;
	private boolean isFirstPage = true;
	private boolean selected = false;
	private String addNotetext = null;
	public VisitorsBean(){}
	
	public VisitorsBean(VisitorsBO theBO){
		this.mapToBO(theBO);
	}
	
	public void mapToBO(VisitorsBO theBO){
		this.id = theBO.getId();  
		this.ipAddress = theBO.getIpAddress();  
		this.hostname = theBO.getHostname();  
		this.pageVisited = theBO.getPageVisited();  
		this.referrer = theBO.getReferrer();  
		this.timeRequested = theBO.getTimeRequested();  
		this.timestamp = theBO.getTimestamp();  
		this.os = theBO.getOs();  
		this.browser = theBO.getBrowser();  
	}


	public String deleteAction(){
		ArrayList results = ((VisitorsList)JSFUtil.getSessionBean("VisitorsList")).getVisitorsList();
		if(results.size() <= 1){
	    	this.pageNum--;
	    }
		VisitorsActionManager manager = new VisitorsActionManager();
		String returnStr = manager.deleteAction();
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String createAction(){
		VisitorsActionManager manager = new VisitorsActionManager();
		VisitorsBO createBO = new VisitorsBO();
		createBO.setId(id);
		createBO.setIpAddress(ipAddress);
		createBO.setHostname(hostname);
		createBO.setPageVisited(pageVisited);
		createBO.setReferrer(referrer);
		createBO.setTimeRequested(timeRequested);
		createBO.setTimestamp(timestamp);
		createBO.setOs(os);
		createBO.setBrowser(browser);
		String returnStr = manager.createAction(createBO);
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public String editAction(){
		VisitorsActionManager manager = new VisitorsActionManager();
		VisitorsBO editBO = new VisitorsBO();
		editBO.setId(id);
		editBO.setIpAddress(ipAddress);
		editBO.setHostname(hostname);
		editBO.setPageVisited(pageVisited);
		editBO.setReferrer(referrer);
		editBO.setTimeRequested(timeRequested);
		editBO.setTimestamp(timestamp);
		editBO.setOs(os);
		editBO.setBrowser(browser);
		VisitorsBO pkBO = new VisitorsBO();
		pkBO.setId(this.getId());
		String returnStr = manager.editAction(editBO, pkBO);
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), id);	// populates the notebean with the notes for this id
		if(returnStr.equalsIgnoreCase("success")){
			return manager.listNavigate(maxRows, pageNum);
		}else{
			return returnStr;
		}
	}
	
	public void addNote(){
		if(addNotetext != "" ){
			NoteBean note = new NoteBean();
			String className = (this.getClass()).getSimpleName();
			note.createNoteAction(className, id, addNotetext);
			addNotetext = "";
		}
	}
	
	public void refreshNotesList(){
		((NoteBean)JSFUtil.getSessionBean("NoteBean")).populateFilter((this.getClass()).getSimpleName(), id);
	}
	
	public String searchAction(){
		VisitorsActionManager manager = new VisitorsActionManager();
		VisitorsBO searchBO = new VisitorsBO();

		if(id!=null){		
			searchBO.setId(id);
		}

		if(ipAddress!=null){		
			searchBO.setIpAddress(ipAddress);
		}

		if(hostname!=null){		
			searchBO.setHostname(hostname);
		}

		if(pageVisited!=null){		
			searchBO.setPageVisited(pageVisited);
		}

		if(referrer!=null){		
			searchBO.setReferrer(referrer);
		}

		if(timeRequested!=null){		
			searchBO.setTimeRequested(timeRequested);
		}

		if(timestamp!=null){		
			searchBO.setTimestamp(timestamp);
		}

		if(os!=null){		
			searchBO.setOs(os);
		}

		if(browser!=null){		
			searchBO.setBrowser(browser);
		}
		JSFUtil.addToSession("VisitorsSearchBO", searchBO);
		return this.listNavigate();
	}
	
	public String clearSearchAction(){
		JSFUtil.removeFromSession("VisitorsSearchBO");
		id=null;
		ipAddress=null;
		hostname=null;
		pageVisited=null;
		referrer=null;
		timeRequested=null;
		timestamp=null;
		os=null;
		browser=null;
		return this.listNavigate();
	}
	
	public String deleteNavigate(){
		VisitorsActionManager manager = new VisitorsActionManager();
		return manager.deleteNavigate();
	}

	private void populateDetailLists() {
	}
	
	public String createNavigate(){
		VisitorsActionManager manager = new VisitorsActionManager();
		clearBean();
		String action = null;
		action = manager.createNavigate();
		populateDetailLists();    		
		return action;
	}
		
	public String editNavigate(){
		VisitorsActionManager manager = new VisitorsActionManager();
		String action = manager.editNavigate();
		populateDetailLists();
		return action;
	}
	
	public String listNavigate(){
		VisitorsActionManager manager = new VisitorsActionManager();
		String theReturn = manager.listNavigate(maxRows, pageNum);
		populateDetailLists();
		checkFirstLastPage();
		return theReturn;
	}
	public void exporttoCSV() throws ApplicationException{
		VisitorsDAO dao = new VisitorsDAO();
		dao.exporttoCSV();
	}
	
	public String searchNavigate(){
		VisitorsActionManager manager = new VisitorsActionManager();
		String action = manager.searchNavigate();
		clearBean();
		return action;	
	}
	
	public Collection getResults(){
		VisitorsActionManager manager = new VisitorsActionManager();
		return manager.getResults();
	}
	
	public void setResults(ArrayList results){
		VisitorsActionManager manager = new VisitorsActionManager();
		manager.setResults(results);
	}
	
	public void selected(){
		VisitorsActionManager manager = new VisitorsActionManager();
	    VisitorsBean selBean = new VisitorsBean();
		selBean.setId(id);
		selBean.setIpAddress(ipAddress);
		selBean.setHostname(hostname);
		selBean.setPageVisited(pageVisited);
		selBean.setReferrer(referrer);
		selBean.setTimeRequested(timeRequested);
		selBean.setTimestamp(timestamp);
		selBean.setOs(os);
		selBean.setBrowser(browser);
		ArrayList results = ((VisitorsList)JSFUtil.getSessionBean("VisitorsList")).getVisitorsList();
     	for (Iterator i = results.iterator(); i.hasNext();) {
     		VisitorsBean temp = (VisitorsBean) i.next();
     		if(temp.isSelected()) {
     			// Ensures only one is selected at a time
     			temp.setSelected(false);
     			((VisitorsList)JSFUtil.getSessionBean("VisitorsList")).setEditDisabled(true);
     		} else if(temp.equals(selBean)){
     			temp.setSelected(!temp.isSelected());
     			if(temp.isSelected() == true){
     				((VisitorsList)JSFUtil.getSessionBean("VisitorsList")).setEditDisabled(false);
     			}
     			else{
     				((VisitorsList)JSFUtil.getSessionBean("VisitorsList")).setEditDisabled(true);
     			}
     		} 
     	}
	}
	
	public void prevResults(){
		VisitorsActionManager manager = new VisitorsActionManager();
    	pageNum--;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void nextResults(){
		VisitorsActionManager manager = new VisitorsActionManager();
    	pageNum++;
		manager.listNavigate(maxRows, pageNum);
		checkFirstLastPage();
	}
	
	public void checkFirstLastPage(){
		VisitorsList list = (VisitorsList)JSFUtil.getSessionBean("VisitorsList");
		int listSize = list.getVisitorsList().size();
		int startingPoint = maxRows * pageNum;
        if(pageNum == 0){
     		this.setFirstPage(true);
     	}else{
     		this.setFirstPage(false);
     	}
		if(listSize < maxRows ||
     			(listSize == maxRows &&
     					startingPoint + listSize == list.getTotalSize())){
     		this.setLastPage(true);
     	}else{
     		this.setLastPage(false);
     	}
    }	

	private void clearBean(){
		VisitorsActionManager manager = new VisitorsActionManager();
		this.id  = null;
		this.ipAddress  = null;
		this.hostname  = null;
		this.pageVisited  = null;
		this.referrer  = null;
		this.timeRequested  = null;
		this.timestamp  = null;
		this.timestampString  = null;
		this.os  = null;
		this.browser  = null;
	}


	/**
	 * get the addNotetex attribute
	 * @return the value of the addNotetext attribute
	 */	
	public String getAddNotetext() {
    	return this.addNotetext;
   	}
	
	/**
	 * set the addNotetext attribute to a value defined by a String
	 * @param addNotetext the string value for the attribute
	 */	
	public void setAddNotetext(String addNotetext) {
    	this.addNotetext = addNotetext;
   	}
	
	/**
	 * get the Id attribute
	 * @return the value of the Id attribute
	 */	
	public Long getId() {
    	return this.id;
   	}

	/**
	 * set the Id attribute to a value defined by a Long
	 * @param id the long value for the attribute
	 */
	public void setId(Long id) {
    	this.id = id;
	}
	/**
	 * overloaded method to set the Id attribute to a value defined by a |Long|
	 * @param id the String value for the attribute
	 */
	public void setId(String id) {
    	this.id = new Long(id);
	}

	/**
	 * get the IpAddress attribute
	 * @return the value of the IpAddress attribute
	 */	
	public String getIpAddress() {
    	return this.ipAddress;
   	}

	/**
	 * set the IpAddress attribute to a value defined by a String
	 * @param ipAddress the long value for the attribute
	 */
	public void setIpAddress(String ipAddress) {
    	this.ipAddress = ipAddress;
	}

	/**
	 * get the Hostname attribute
	 * @return the value of the Hostname attribute
	 */	
	public String getHostname() {
    	return this.hostname;
   	}

	/**
	 * set the Hostname attribute to a value defined by a String
	 * @param hostname the long value for the attribute
	 */
	public void setHostname(String hostname) {
    	this.hostname = hostname;
	}

	/**
	 * get the PageVisited attribute
	 * @return the value of the PageVisited attribute
	 */	
	public String getPageVisited() {
    	return this.pageVisited;
   	}

	/**
	 * set the PageVisited attribute to a value defined by a String
	 * @param pageVisited the long value for the attribute
	 */
	public void setPageVisited(String pageVisited) {
    	this.pageVisited = pageVisited;
	}

	/**
	 * get the Referrer attribute
	 * @return the value of the Referrer attribute
	 */	
	public String getReferrer() {
    	return this.referrer;
   	}

	/**
	 * set the Referrer attribute to a value defined by a String
	 * @param referrer the long value for the attribute
	 */
	public void setReferrer(String referrer) {
    	this.referrer = referrer;
	}

	/**
	 * get the TimeRequested attribute
	 * @return the value of the TimeRequested attribute
	 */	
	public String getTimeRequested() {
    	return this.timeRequested;
   	}

	/**
	 * set the TimeRequested attribute to a value defined by a String
	 * @param timeRequested the long value for the attribute
	 */
	public void setTimeRequested(String timeRequested) {
    	this.timeRequested = timeRequested;
	}

	/**
	 * get the Timestamp attribute
	 * @return the value of the Timestamp attribute
	 */	
	public java.util.Date getTimestamp() {
    	return this.timestamp;
   	}

	/**
	 * set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the long value for the attribute
	 */
	public void setTimestamp(java.util.Date timestamp) {
    	this.timestamp = timestamp;
    	this.timestampString = DateConverter.convertDateToString(timestamp);
	}
	
	/**
	 * get the TimestampString attribute
	 * @return the value of the Timestamp attribute
	 */	
	public String getTimestampString() {
    	return this.timestampString;
   	}

	/**
	 * set the TimestampString attribute to a value defined by a String
	 * @param timestamp the long value for the attribute
	 */
	public void setTimestampString(String timestamp) {
    	this.timestampString = timestamp;
    	if(timestamp.length() > 0){
			this.timestamp = DateConverter.convertStringToDate(timestamp);    	
		}
	}	
	/**
	 * overloaded method to set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the String value for the attribute
	 */
	public void setTimestamp(String timestamp) {
    	if (timestamp != null && !timestamp.equals("")) {
    		this.timestamp = DateConverter.convertStringToDate(timestamp);
		}
	}		


	/**
	 * get the Os attribute
	 * @return the value of the Os attribute
	 */	
	public String getOs() {
    	return this.os;
   	}

	/**
	 * set the Os attribute to a value defined by a String
	 * @param os the long value for the attribute
	 */
	public void setOs(String os) {
    	this.os = os;
	}

	/**
	 * get the Browser attribute
	 * @return the value of the Browser attribute
	 */	
	public String getBrowser() {
    	return this.browser;
   	}

	/**
	 * set the Browser attribute to a value defined by a String
	 * @param browser the long value for the attribute
	 */
	public void setBrowser(String browser) {
    	this.browser = browser;
	}



	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean isFirstPage) {
		this.isFirstPage = isFirstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
    
    public boolean equals(VisitorsBean temp){
    	boolean isEqual = false;
		if(this.getId().equals(temp.getId())){
			isEqual = true;
		}
		return isEqual;
	}


    

}