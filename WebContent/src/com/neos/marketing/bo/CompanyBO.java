package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import java.util.Collection;

/**
 * Business Object for Company data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class CompanyBO implements java.io.Serializable {


	private Long companyId  = null;
	private String name  = null;
	private Collection entityCompanyFkList  = null;
	private Collection companyFkList  = null;

	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public CompanyBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public CompanyBO(CompanyDTO dto) {
		super();
		this.setCompanyId(dto.getCompanyId());
		this.setName(dto.getName());
	}	


	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a Long
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
		try {
			this.companyId = new Long(companyId);
		} catch (NumberFormatException nfe) {
			this.companyId = null;
		}
	}		

	/**
	 * get the Name attribute
	 * @return the value of the Name attribute
	 */	
	public String getName() {
    	return this.name;
   	}

	/**
	 * set the Name attribute to a value defined by a String
	 * @param name the long value for the attribute
	 */
	public void setName(String name) {
    	this.name = name;
	}

	/**
	 * get the EntityCompanyFkList attribute
	 * @return the value of the EntityCompanyFkList attribute
	 */	
	public Collection getEntityCompanyFkList() {
    	return this.entityCompanyFkList;
   	}

	/**
	 * set the EntityCompanyFkList attribute to a value defined by a Collection
	 * @param entityCompanyFkList the long value for the attribute
	 */
	public void setEntityCompanyFkList(Collection entityCompanyFkList) {
    	this.entityCompanyFkList = entityCompanyFkList;
	}


	/**
	 * get the CompanyFkList attribute
	 * @return the value of the CompanyFkList attribute
	 */	
	public Collection getCompanyFkList() {
    	return this.companyFkList;
   	}

	/**
	 * set the CompanyFkList attribute to a value defined by a Collection
	 * @param companyFkList the long value for the attribute
	 */
	public void setCompanyFkList(Collection companyFkList) {
    	this.companyFkList = companyFkList;
	}


	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a CompanyBO with only the Primary Key Values set
	 */ 	
   public CompanyBO getPKBO() {
		CompanyBO pkBO = new CompanyBO();
		pkBO.setCompanyId(this.getCompanyId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a CompanyDTO with only the Primary Key Values set
	 */ 	
   public CompanyDTO getPKDTO() {
		CompanyDTO pkDTO = new CompanyDTO();
		pkDTO.setCompanyId(this.getCompanyId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a CompanyDTO with only the Primary Key Values set
	 */ 	
   public CompanyDTO getDTO() {
		CompanyDTO dto = new CompanyDTO();
		dto.setCompanyId(this.getCompanyId());
		dto.setName(this.getName());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this CompanyDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this CompanyDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tCompanyId = "+this.getCompanyId()+", ");
		description.append(" \n\tName = "+this.getName()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this CompanyDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this CompanyDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tCompanyId = "+this.getCompanyId()+", ");
		description.append(" \n\tName = "+this.getName()+", ");
	
	
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(CompanyBO bo){
		if(		this.companyId  == bo.getCompanyId() &&
		this.name  == bo.getName() &&
		this.entityCompanyFkList  == bo.getEntityCompanyFkList() &&
		this.companyFkList  == bo.getCompanyFkList() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the CompanyForm.  This description is what appears in
	 * lists and dropdowns containing CompanyForms.  The default is just a
	 * concatenation of every value in the CompanyForm.
	 * 
	 * @return decription of the CompanyForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getCompanyId()==null)? "" : (this.getCompanyId()) +" ");
		display.append((this.getName()==null)? "" : (this.getName()) +" ");
		return display.toString();
	}

}


