package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import java.util.Collection;

/**
 * Business Object for Detail data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class DetailBO implements java.io.Serializable {


	private Long detailId  = null;
	private Long sourceId  = null;
	private String detail  = null;
	private Collection touchDetailFkList  = null;
	private Collection touchbackDetailFkList  = null;
	private SourceBO detailSourceFkBO = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public DetailBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public DetailBO(DetailDTO dto) {
		super();
		this.setDetailId(dto.getDetailId());
		this.setSourceId(dto.getSourceId());
		this.setDetail(dto.getDetail());
	}	


	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a Long
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
		try {
			this.detailId = new Long(detailId);
		} catch (NumberFormatException nfe) {
			this.detailId = null;
		}
	}		



	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a Long
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
		try {
			this.sourceId = new Long(sourceId);
		} catch (NumberFormatException nfe) {
			this.sourceId = null;
		}
	}		



	/**
	 * get the Detail attribute
	 * @return the value of the Detail attribute
	 */	
	public String getDetail() {
    	return this.detail;
   	}

	/**
	 * set the Detail attribute to a value defined by a String
	 * @param detail the long value for the attribute
	 */
	public void setDetail(String detail) {
    	this.detail = detail;
	}


	/**
	 * get the TouchDetailFkList attribute
	 * @return the value of the TouchDetailFkList attribute
	 */	
	public Collection getTouchDetailFkList() {
    	return this.touchDetailFkList;
   	}

	/**
	 * set the TouchDetailFkList attribute to a value defined by a Collection
	 * @param touchDetailFkList the long value for the attribute
	 */
	public void setTouchDetailFkList(Collection touchDetailFkList) {
    	this.touchDetailFkList = touchDetailFkList;
	}


	/**
	 * get the TouchbackDetailFkList attribute
	 * @return the value of the TouchbackDetailFkList attribute
	 */	
	public Collection getTouchbackDetailFkList() {
    	return this.touchbackDetailFkList;
   	}

	/**
	 * set the TouchbackDetailFkList attribute to a value defined by a Collection
	 * @param touchbackDetailFkList the long value for the attribute
	 */
	public void setTouchbackDetailFkList(Collection touchbackDetailFkList) {
    	this.touchbackDetailFkList = touchbackDetailFkList;
	}


	/**
	 * get the detailSourceFkBO attribute
	 * @return the value of the detailSourceFkBO attribute
	 */	
	public SourceBO getDetailSourceFkBO() {
		return this.detailSourceFkBO;
	}
	
	/**
	 * set the detailSourceFkBO attribute to a value defined by a Collection
	 * @param detailSourceFkBO the SourceBO value for the attribute
	 */ 	
	public void setDetailSourceFkBO(SourceBO detailSourceFkBO) {
		this.detailSourceFkBO = detailSourceFkBO;
	}

	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a DetailBO with only the Primary Key Values set
	 */ 	
   public DetailBO getPKBO() {
		DetailBO pkBO = new DetailBO();
		pkBO.setDetailId(this.getDetailId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a DetailDTO with only the Primary Key Values set
	 */ 	
   public DetailDTO getPKDTO() {
		DetailDTO pkDTO = new DetailDTO();
		pkDTO.setDetailId(this.getDetailId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a DetailDTO with only the Primary Key Values set
	 */ 	
   public DetailDTO getDTO() {
		DetailDTO dto = new DetailDTO();
		dto.setDetailId(this.getDetailId());
		dto.setSourceId(this.getSourceId());
		dto.setDetail(this.getDetail());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this DetailDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this DetailDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tDetailId = "+this.getDetailId()+", ");
		description.append(" \n\tSourceId = "+this.getSourceId()+", ");
		description.append(" \n\tDetail = "+this.getDetail()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this DetailDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this DetailDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tDetailId = "+this.getDetailId()+", ");
		description.append(" \n\tSourceId = "+this.getSourceId()+", ");
		description.append(" \n\tDetail = "+this.getDetail()+", ");
	
	
		description.append(" \n\tDetailSourceFkBO = "+((this.getDetailSourceFkBO()==null)? "" : ("\n\t ["+this.getDetailSourceFkBO().getTableDescription()))+"], ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(DetailBO bo){
		if(		this.detailId  == bo.getDetailId() &&
		this.sourceId  == bo.getSourceId() &&
		this.detail  == bo.getDetail() &&
		this.touchDetailFkList  == bo.getTouchDetailFkList() &&
		this.touchbackDetailFkList  == bo.getTouchbackDetailFkList() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the DetailForm.  This description is what appears in
	 * lists and dropdowns containing DetailForms.  The default is just a
	 * concatenation of every value in the DetailForm.
	 * 
	 * @return decription of the DetailForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getDetailId()==null)? "" : (this.getDetailId()) +" ");
		display.append((this.getSourceId()==null)? "" : (this.getSourceId()) +" ");
		display.append((this.getDetail()==null)? "" : (this.getDetail()) +" ");
		return display.toString();
	}

}


