package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import com.neos.marketing.util.DateConverter;

import java.util.Date;

/**
 * Business Object for Downloads data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class DownloadsBO implements java.io.Serializable {


	private Long id  = null;
	private String name  = null;
	private String email  = null;
	private String downloadId  = null;
	private String itemTitle  = null;
	private java.util.Date timestamp  = null;
	private String timeRequested  = null;
	private String ipAddress  = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public DownloadsBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public DownloadsBO(DownloadsDTO dto) {
		super();
		this.setId(dto.getId());
		this.setName(dto.getName());
		this.setEmail(dto.getEmail());
		this.setDownloadId(dto.getDownloadId());
		this.setItemTitle(dto.getItemTitle());
		this.setTimestamp(dto.getTimestamp());
		this.setTimeRequested(dto.getTimeRequested());
		this.setIpAddress(dto.getIpAddress());
	}	


	/**
	 * get the Id attribute
	 * @return the value of the Id attribute
	 */	
	public Long getId() {
    	return this.id;
   	}

	/**
	 * set the Id attribute to a value defined by a Long
	 * @param id the long value for the attribute
	 */
	public void setId(Long id) {
    	this.id = id;
	}
	/**
	 * overloaded method to set the Id attribute to a value defined by a Long
	 * @param id the String value for the attribute
	 */
	public void setId(String id) {
		try {
			this.id = new Long(id);
		} catch (NumberFormatException nfe) {
			this.id = null;
		}
	}		



	/**
	 * get the Name attribute
	 * @return the value of the Name attribute
	 */	
	public String getName() {
    	return this.name;
   	}

	/**
	 * set the Name attribute to a value defined by a String
	 * @param name the long value for the attribute
	 */
	public void setName(String name) {
    	this.name = name;
	}


	/**
	 * get the Email attribute
	 * @return the value of the Email attribute
	 */	
	public String getEmail() {
    	return this.email;
   	}

	/**
	 * set the Email attribute to a value defined by a String
	 * @param email the long value for the attribute
	 */
	public void setEmail(String email) {
    	this.email = email;
	}


	/**
	 * get the DownloadId attribute
	 * @return the value of the DownloadId attribute
	 */	
	public String getDownloadId() {
    	return this.downloadId;
   	}

	/**
	 * set the DownloadId attribute to a value defined by a String
	 * @param downloadId the long value for the attribute
	 */
	public void setDownloadId(String downloadId) {
    	this.downloadId = downloadId;
	}


	/**
	 * get the ItemTitle attribute
	 * @return the value of the ItemTitle attribute
	 */	
	public String getItemTitle() {
    	return this.itemTitle;
   	}

	/**
	 * set the ItemTitle attribute to a value defined by a String
	 * @param itemTitle the long value for the attribute
	 */
	public void setItemTitle(String itemTitle) {
    	this.itemTitle = itemTitle;
	}


	/**
	 * get the Timestamp attribute
	 * @return the value of the Timestamp attribute
	 */	
	public java.util.Date getTimestamp() {
    	return this.timestamp;
   	}

	/**
	 * set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the long value for the attribute
	 */
	public void setTimestamp(java.util.Date timestamp) {
    	this.timestamp = timestamp;
	}
	/**
	 * overloaded method to set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the String value for the attribute
	 */
	public void setTimestamp(String timestamp) {
    	if (timestamp != null && !timestamp.equals("")) {
    		this.timestamp = DateConverter.convertStringToDate(timestamp);
		}
	}		



	/**
	 * get the TimeRequested attribute
	 * @return the value of the TimeRequested attribute
	 */	
	public String getTimeRequested() {
    	return this.timeRequested;
   	}

	/**
	 * set the TimeRequested attribute to a value defined by a String
	 * @param timeRequested the long value for the attribute
	 */
	public void setTimeRequested(String timeRequested) {
    	this.timeRequested = timeRequested;
	}


	/**
	 * get the IpAddress attribute
	 * @return the value of the IpAddress attribute
	 */	
	public String getIpAddress() {
    	return this.ipAddress;
   	}

	/**
	 * set the IpAddress attribute to a value defined by a String
	 * @param ipAddress the long value for the attribute
	 */
	public void setIpAddress(String ipAddress) {
    	this.ipAddress = ipAddress;
	}


	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a DownloadsBO with only the Primary Key Values set
	 */ 	
   public DownloadsBO getPKBO() {
		DownloadsBO pkBO = new DownloadsBO();
		pkBO.setId(this.getId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a DownloadsDTO with only the Primary Key Values set
	 */ 	
   public DownloadsDTO getPKDTO() {
		DownloadsDTO pkDTO = new DownloadsDTO();
		pkDTO.setId(this.getId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a DownloadsDTO with only the Primary Key Values set
	 */ 	
   public DownloadsDTO getDTO() {
		DownloadsDTO dto = new DownloadsDTO();
		dto.setId(this.getId());
		dto.setName(this.getName());
		dto.setEmail(this.getEmail());
		dto.setDownloadId(this.getDownloadId());
		dto.setItemTitle(this.getItemTitle());
		dto.setTimestamp(this.getTimestamp());
		dto.setTimeRequested(this.getTimeRequested());
		dto.setIpAddress(this.getIpAddress());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this DownloadsDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this DownloadsDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tId = "+this.getId()+", ");
		description.append(" \n\tName = "+this.getName()+", ");
		description.append(" \n\tEmail = "+this.getEmail()+", ");
		description.append(" \n\tDownloadId = "+this.getDownloadId()+", ");
		description.append(" \n\tItemTitle = "+this.getItemTitle()+", ");
		description.append(" \n\tTimestamp = "+DateConverter.convertDateToString(this.getTimestamp())+", ");
		description.append(" \n\tTimeRequested = "+this.getTimeRequested()+", ");
		description.append(" \n\tIpAddress = "+this.getIpAddress()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this DownloadsDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this DownloadsDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tId = "+this.getId()+", ");
		description.append(" \n\tName = "+this.getName()+", ");
		description.append(" \n\tEmail = "+this.getEmail()+", ");
		description.append(" \n\tDownloadId = "+this.getDownloadId()+", ");
		description.append(" \n\tItemTitle = "+this.getItemTitle()+", ");
		description.append(" \n\tTimestamp = "+DateConverter.convertDateToString(this.getTimestamp())+", ");
		description.append(" \n\tTimeRequested = "+this.getTimeRequested()+", ");
		description.append(" \n\tIpAddress = "+this.getIpAddress()+", ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(DownloadsBO bo){
		if(		this.id  == bo.getId() &&
		this.name  == bo.getName() &&
		this.email  == bo.getEmail() &&
		this.downloadId  == bo.getDownloadId() &&
		this.itemTitle  == bo.getItemTitle() &&
		this.timestamp  == bo.getTimestamp() &&
		this.timeRequested  == bo.getTimeRequested() &&
		this.ipAddress  == bo.getIpAddress() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the DownloadsForm.  This description is what appears in
	 * lists and dropdowns containing DownloadsForms.  The default is just a
	 * concatenation of every value in the DownloadsForm.
	 * 
	 * @return decription of the DownloadsForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getId()==null)? "" : (this.getId()) +" ");
		display.append((this.getName()==null)? "" : (this.getName()) +" ");
		display.append((this.getEmail()==null)? "" : (this.getEmail()) +" ");
		display.append((this.getDownloadId()==null)? "" : (this.getDownloadId()) +" ");
		display.append((this.getItemTitle()==null)? "" : (this.getItemTitle()) +" ");
		display.append((this.getTimestamp()==null)? "" : (this.getTimestamp()) +" ");
		display.append((this.getTimeRequested()==null)? "" : (this.getTimeRequested()) +" ");
		display.append((this.getIpAddress()==null)? "" : (this.getIpAddress()) +" ");
		return display.toString();
	}

}


