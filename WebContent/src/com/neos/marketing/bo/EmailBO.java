package com.neos.marketing.bo;

import com.neos.marketing.dto.*;


/**
 * Business Object for Email data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class EmailBO implements java.io.Serializable {


	private Long emailId  = null;
	private String emailAddress  = null;
	private Long personId  = null;
	private PersonBO emailPersonFkBO = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public EmailBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public EmailBO(EmailDTO dto) {
		super();
		this.setEmailId(dto.getEmailId());
		this.setEmailAddress(dto.getEmailAddress());
		this.setPersonId(dto.getPersonId());
	}	


	/**
	 * get the EmailId attribute
	 * @return the value of the EmailId attribute
	 */	
	public Long getEmailId() {
    	return this.emailId;
   	}

	/**
	 * set the EmailId attribute to a value defined by a Long
	 * @param emailId the long value for the attribute
	 */
	public void setEmailId(Long emailId) {
    	this.emailId = emailId;
	}
	/**
	 * overloaded method to set the EmailId attribute to a value defined by a Long
	 * @param emailId the String value for the attribute
	 */
	public void setEmailId(String emailId) {
		try {
			this.emailId = new Long(emailId);
		} catch (NumberFormatException nfe) {
			this.emailId = null;
		}
	}		



	/**
	 * get the EmailAddress attribute
	 * @return the value of the EmailAddress attribute
	 */	
	public String getEmailAddress() {
    	return this.emailAddress;
   	}

	/**
	 * set the EmailAddress attribute to a value defined by a String
	 * @param emailAddress the long value for the attribute
	 */
	public void setEmailAddress(String emailAddress) {
    	this.emailAddress = emailAddress;
	}


	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a Long
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
		try {
			this.personId = new Long(personId);
		} catch (NumberFormatException nfe) {
			this.personId = null;
		}
	}		



	/**
	 * get the emailPersonFkBO attribute
	 * @return the value of the emailPersonFkBO attribute
	 */	
	public PersonBO getEmailPersonFkBO() {
		return this.emailPersonFkBO;
	}
	
	/**
	 * set the emailPersonFkBO attribute to a value defined by a Long
	 * @param emailPersonFkBO the PersonBO value for the attribute
	 */ 	
	public void setEmailPersonFkBO(PersonBO emailPersonFkBO) {
		this.emailPersonFkBO = emailPersonFkBO;
	}

	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a EmailBO with only the Primary Key Values set
	 */ 	
   public EmailBO getPKBO() {
		EmailBO pkBO = new EmailBO();
		pkBO.setEmailId(this.getEmailId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a EmailDTO with only the Primary Key Values set
	 */ 	
   public EmailDTO getPKDTO() {
		EmailDTO pkDTO = new EmailDTO();
		pkDTO.setEmailId(this.getEmailId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a EmailDTO with only the Primary Key Values set
	 */ 	
   public EmailDTO getDTO() {
		EmailDTO dto = new EmailDTO();
		dto.setEmailId(this.getEmailId());
		dto.setEmailAddress(this.getEmailAddress());
		dto.setPersonId(this.getPersonId());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this EmailDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this EmailDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tEmailId = "+this.getEmailId()+", ");
		description.append(" \n\tEmailAddress = "+this.getEmailAddress()+", ");
		description.append(" \n\tPersonId = "+this.getPersonId()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this EmailDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this EmailDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tEmailId = "+this.getEmailId()+", ");
		description.append(" \n\tEmailAddress = "+this.getEmailAddress()+", ");
		description.append(" \n\tPersonId = "+this.getPersonId()+", ");
		description.append(" \n\tEmailPersonFkBO = "+((this.getEmailPersonFkBO()==null)? "" : ("\n\t ["+this.getEmailPersonFkBO().getTableDescription()))+"], ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(EmailBO bo){
		if(		this.emailId  == bo.getEmailId() &&
		this.emailAddress  == bo.getEmailAddress() &&
		this.personId  == bo.getPersonId() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the EmailForm.  This description is what appears in
	 * lists and dropdowns containing EmailForms.  The default is just a
	 * concatenation of every value in the EmailForm.
	 * 
	 * @return decription of the EmailForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getEmailId()==null)? "" : (this.getEmailId()) +" ");
		display.append((this.getEmailAddress()==null)? "" : (this.getEmailAddress()) +" ");
		display.append((this.getPersonId()==null)? "" : (this.getPersonId()) +" ");
		return display.toString();
	}

}


