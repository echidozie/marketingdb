package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import java.util.Collection;

/**
 * Business Object for Entity data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class EntityBO implements java.io.Serializable {


	private Long entityId  = null;
	private Long companyId  = null;
	private Long personId  = null;
	private String companyName = null;
	private String firstName = null;
	private String lastName = null;	
	private Collection touchEntityFkList  = null;
	private Collection touchbackEntityFkList  = null;
	private CompanyBO entityCompanyFkBO = null;
	private PersonBO entityPersonFkBO = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public EntityBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public EntityBO(EntityDTO dto) {
		super();
		this.setEntityId(dto.getEntityId());
		this.setCompanyId(dto.getCompanyId());
		this.setPersonId(dto.getPersonId());
		this.setCompanyName(dto.getCompanyName());
		this.setFirstName(dto.getFirstName());
		this.setLastName(dto.getLastName());
	}	


	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a Long
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
		try {
			this.entityId = new Long(entityId);
		} catch (NumberFormatException nfe) {
			this.entityId = null;
		}
	}		



	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a Long
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
		try {
			this.companyId = new Long(companyId);
		} catch (NumberFormatException nfe) {
			this.companyId = null;
		}
	}		

	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a Long
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
		try {
			this.personId = new Long(personId);
		} catch (NumberFormatException nfe) {
			this.personId = null;
		}
	}		

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * get the TouchEntityFkList attribute
	 * @return the value of the TouchEntityFkList attribute
	 */	
	public Collection getTouchEntityFkList() {
    	return this.touchEntityFkList;
   	}

	/**
	 * set the TouchEntityFkList attribute to a value defined by a Collection
	 * @param touchEntityFkList the long value for the attribute
	 */
	public void setTouchEntityFkList(Collection touchEntityFkList) {
    	this.touchEntityFkList = touchEntityFkList;
	}


	/**
	 * get the TouchbackEntityFkList attribute
	 * @return the value of the TouchbackEntityFkList attribute
	 */	
	public Collection getTouchbackEntityFkList() {
    	return this.touchbackEntityFkList;
   	}

	/**
	 * set the TouchbackEntityFkList attribute to a value defined by a Collection
	 * @param touchbackEntityFkList the long value for the attribute
	 */
	public void setTouchbackEntityFkList(Collection touchbackEntityFkList) {
    	this.touchbackEntityFkList = touchbackEntityFkList;
	}


	/**
	 * get the entityCompanyFkBO attribute
	 * @return the value of the entityCompanyFkBO attribute
	 */	
	public CompanyBO getEntityCompanyFkBO() {
		return this.entityCompanyFkBO;
	}
	
	/**
	 * set the entityCompanyFkBO attribute to a value defined by a Collection
	 * @param entityCompanyFkBO the CompanyBO value for the attribute
	 */ 	
	public void setEntityCompanyFkBO(CompanyBO entityCompanyFkBO) {
		this.entityCompanyFkBO = entityCompanyFkBO;
	}

	/**
	 * get the entityPersonFkBO attribute
	 * @return the value of the entityPersonFkBO attribute
	 */	
	public PersonBO getEntityPersonFkBO() {
		return this.entityPersonFkBO;
	}
	
	/**
	 * set the entityPersonFkBO attribute to a value defined by a Collection
	 * @param entityPersonFkBO the PersonBO value for the attribute
	 */ 	
	public void setEntityPersonFkBO(PersonBO entityPersonFkBO) {
		this.entityPersonFkBO = entityPersonFkBO;
	}

	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a EntityBO with only the Primary Key Values set
	 */ 	
   public EntityBO getPKBO() {
		EntityBO pkBO = new EntityBO();
		pkBO.setEntityId(this.getEntityId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a EntityDTO with only the Primary Key Values set
	 */ 	
   public EntityDTO getPKDTO() {
		EntityDTO pkDTO = new EntityDTO();
		pkDTO.setEntityId(this.getEntityId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a EntityDTO with only the Primary Key Values set
	 */ 	
   public EntityDTO getDTO() {
		EntityDTO dto = new EntityDTO();
		dto.setEntityId(this.getEntityId());
		dto.setCompanyId(this.getCompanyId());
		dto.setPersonId(this.getPersonId());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this EntityDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this EntityDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tEntityId = "+this.getEntityId()+", ");
		description.append(" \n\tCompanyId = "+this.getCompanyId()+", ");
		description.append(" \n\tPersonId = "+this.getPersonId()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this EntityDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this EntityDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tEntityId = "+this.getEntityId()+", ");
		description.append(" \n\tCompanyId = "+this.getCompanyId()+", ");
		description.append(" \n\tPersonId = "+this.getPersonId()+", ");
	
	
		description.append(" \n\tEntityCompanyFkBO = "+((this.getEntityCompanyFkBO()==null)? "" : ("\n\t ["+this.getEntityCompanyFkBO().getTableDescription()))+"], ");
		description.append(" \n\tEntityPersonFkBO = "+((this.getEntityPersonFkBO()==null)? "" : ("\n\t ["+this.getEntityPersonFkBO().getTableDescription()))+"], ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(EntityBO bo){
		if(		this.entityId  == bo.getEntityId() &&
		this.companyId  == bo.getCompanyId() &&
		this.personId  == bo.getPersonId() &&
		this.touchEntityFkList  == bo.getTouchEntityFkList() &&
		this.touchbackEntityFkList  == bo.getTouchbackEntityFkList() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the EntityForm.  This description is what appears in
	 * lists and dropdowns containing EntityForms.  The default is just a
	 * concatenation of every value in the EntityForm.
	 * 
	 * @return decription of the EntityForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getEntityId()==null)? "" : (this.getEntityId()) +" ");
		display.append((this.getCompanyName()==null)? "" : (this.getCompanyName()) +" - ");
		display.append((this.getFirstName()==null)? "" : (this.getFirstName()) +" ");
		display.append((this.getLastName()==null)? "" : (this.getLastName()) +" ");
		return display.toString();
	}

}


