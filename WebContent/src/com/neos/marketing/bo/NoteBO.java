package com.neos.marketing.bo;

import com.neos.marketing.dto.*;


/**
 * Business Object for Note data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class NoteBO implements java.io.Serializable {


	private Long noteId  = null;
	private String objectType  = null;
	private Long objectId  = null;
	private String note  = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public NoteBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public NoteBO(NoteDTO dto) {
		super();
		this.setNoteId(dto.getNoteId());
		this.setObjectType(dto.getObjectType());
		this.setObjectId(dto.getObjectId());
		this.setNote(dto.getNote());
	}	


	/**
	 * get the NoteId attribute
	 * @return the value of the NoteId attribute
	 */	
	public Long getNoteId() {
    	return this.noteId;
   	}

	/**
	 * set the NoteId attribute to a value defined by a Long
	 * @param noteId the long value for the attribute
	 */
	public void setNoteId(Long noteId) {
    	this.noteId = noteId;
	}
	/**
	 * overloaded method to set the NoteId attribute to a value defined by a Long
	 * @param noteId the String value for the attribute
	 */
	public void setNoteId(String noteId) {
		try {
			this.noteId = new Long(noteId);
		} catch (NumberFormatException nfe) {
			this.noteId = null;
		}
	}		



	/**
	 * get the ObjectType attribute
	 * @return the value of the ObjectType attribute
	 */	
	public String getObjectType() {
    	return this.objectType;
   	}

	/**
	 * set the ObjectType attribute to a value defined by a String
	 * @param objectType the long value for the attribute
	 */
	public void setObjectType(String objectType) {
    	this.objectType = objectType;
	}


	/**
	 * get the ObjectId attribute
	 * @return the value of the ObjectId attribute
	 */	
	public Long getObjectId() {
    	return this.objectId;
   	}

	/**
	 * set the ObjectId attribute to a value defined by a Long
	 * @param objectId the long value for the attribute
	 */
	public void setObjectId(Long objectId) {
    	this.objectId = objectId;
	}
	/**
	 * overloaded method to set the ObjectId attribute to a value defined by a Long
	 * @param objectId the String value for the attribute
	 */
	public void setObjectId(String objectId) {
		try {
			this.objectId = new Long(objectId);
		} catch (NumberFormatException nfe) {
			this.objectId = null;
		}
	}		



	/**
	 * get the Note attribute
	 * @return the value of the Note attribute
	 */	
	public String getNote() {
    	return this.note;
   	}

	/**
	 * set the Note attribute to a value defined by a String
	 * @param note the long value for the attribute
	 */
	public void setNote(String note) {
    	this.note = note;
	}


	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a NoteBO with only the Primary Key Values set
	 */ 	
   public NoteBO getPKBO() {
		NoteBO pkBO = new NoteBO();
		pkBO.setNoteId(this.getNoteId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a NoteDTO with only the Primary Key Values set
	 */ 	
   public NoteDTO getPKDTO() {
		NoteDTO pkDTO = new NoteDTO();
		pkDTO.setNoteId(this.getNoteId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a NoteDTO with only the Primary Key Values set
	 */ 	
   public NoteDTO getDTO() {
		NoteDTO dto = new NoteDTO();
		dto.setNoteId(this.getNoteId());
		dto.setObjectType(this.getObjectType());
		dto.setObjectId(this.getObjectId());
		dto.setNote(this.getNote());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this NoteDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this NoteDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tNoteId = "+this.getNoteId()+", ");
		description.append(" \n\tObjectType = "+this.getObjectType()+", ");
		description.append(" \n\tObjectId = "+this.getObjectId()+", ");
		description.append(" \n\tNote = "+this.getNote()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this NoteDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this NoteDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tNoteId = "+this.getNoteId()+", ");
		description.append(" \n\tObjectType = "+this.getObjectType()+", ");
		description.append(" \n\tObjectId = "+this.getObjectId()+", ");
		description.append(" \n\tNote = "+this.getNote()+", ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(NoteBO bo){
		if(		this.noteId  == bo.getNoteId() &&
		this.objectType  == bo.getObjectType() &&
		this.objectId  == bo.getObjectId() &&
		this.note  == bo.getNote() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the NoteForm.  This description is what appears in
	 * lists and dropdowns containing NoteForms.  The default is just a
	 * concatenation of every value in the NoteForm.
	 * 
	 * @return decription of the NoteForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getNoteId()==null)? "" : (this.getNoteId()) +" ");
		display.append((this.getObjectType()==null)? "" : (this.getObjectType()) +" ");
		display.append((this.getObjectId()==null)? "" : (this.getObjectId()) +" ");
		display.append((this.getNote()==null)? "" : (this.getNote()) +" ");
		return display.toString();
	}

}


