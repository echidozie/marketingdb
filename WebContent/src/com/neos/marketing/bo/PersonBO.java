package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import java.util.Collection;

/**
 * Business Object for Person data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class PersonBO implements java.io.Serializable {


	private Long personId  = null;
	private String firstName  = null;
	private String lastName  = null;
	private String title  = null;
	private String emailAddress=null;
	private Long companyId  = null;
	private Collection emailPersonFkList  = null;
	private Collection entityPersonFkList  = null;
	private CompanyBO companyFkBO = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public PersonBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public PersonBO(PersonDTO dto) {
		super();
		this.setPersonId(dto.getPersonId());
		this.setFirstName(dto.getFirstName());
		this.setLastName(dto.getLastName());
		this.setTitle(dto.getTitle());
		this.setCompanyId(dto.getCompanyId());
	}	

	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a Long
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
		try {
			this.personId = new Long(personId);
		} catch (NumberFormatException nfe) {
			this.personId = null;
		}
	}		



	/**
	 * get the FirstName attribute
	 * @return the value of the FirstName attribute
	 */	
	public String getFirstName() {
    	return this.firstName;
   	}

	/**
	 * set the FirstName attribute to a value defined by a String
	 * @param firstName the long value for the attribute
	 */
	public void setFirstName(String firstName) {
    	this.firstName = firstName;
	}


	/**
	 * get the LastName attribute
	 * @return the value of the LastName attribute
	 */	
	public String getLastName() {
    	return this.lastName;
   	}

	/**
	 * set the emailAddress attribute to a value defined by a String
	 * @param emailAddress the long value for the attribute
	 */
	public void setemailAddress(String emailAddress) {
    	this.emailAddress = emailAddress;
	}
	
	public String getemailAddress() {
    	return this.emailAddress;
   	}

	/**
	 * set the LastName attribute to a value defined by a String
	 * @param lastName the long value for the attribute
	 */
	public void setLastName(String lastName) {
    	this.lastName = lastName;
	}


	/**
	 * get the Title attribute
	 * @return the value of the Title attribute
	 */	
	public String getTitle() {
    	return this.title;
   	}

	/**
	 * set the Title attribute to a value defined by a String
	 * @param title the long value for the attribute
	 */
	public void setTitle(String title) {
    	this.title = title;
	}


	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a Long
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
		try {
			this.companyId = new Long(companyId);
		} catch (NumberFormatException nfe) {
			this.companyId = null;
		}
	}		



	/**
	 * get the EmailPersonFkList attribute
	 * @return the value of the EmailPersonFkList attribute
	 */	
	public Collection getEmailPersonFkList() {
    	return this.emailPersonFkList;
   	}

	/**
	 * set the EmailPersonFkList attribute to a value defined by a Collection
	 * @param emailPersonFkList the long value for the attribute
	 */
	public void setEmailPersonFkList(Collection emailPersonFkList) {
    	this.emailPersonFkList = emailPersonFkList;
	}


	/**
	 * get the EntityPersonFkList attribute
	 * @return the value of the EntityPersonFkList attribute
	 */	
	public Collection getEntityPersonFkList() {
    	return this.entityPersonFkList;
   	}

	/**
	 * set the EntityPersonFkList attribute to a value defined by a Collection
	 * @param entityPersonFkList the long value for the attribute
	 */
	public void setEntityPersonFkList(Collection entityPersonFkList) {
    	this.entityPersonFkList = entityPersonFkList;
	}


	/**
	 * get the companyFkBO attribute
	 * @return the value of the companyFkBO attribute
	 */	
	public CompanyBO getCompanyFkBO() {
		return this.companyFkBO;
	}
	
	/**
	 * set the companyFkBO attribute to a value defined by a Collection
	 * @param companyFkBO the CompanyBO value for the attribute
	 */ 	
	public void setCompanyFkBO(CompanyBO companyFkBO) {
		this.companyFkBO = companyFkBO;
	}

	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a PersonBO with only the Primary Key Values set
	 */ 	
   public PersonBO getPKBO() {
		PersonBO pkBO = new PersonBO();
		pkBO.setPersonId(this.getPersonId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a PersonDTO with only the Primary Key Values set
	 */ 	
   public PersonDTO getPKDTO() {
		PersonDTO pkDTO = new PersonDTO();
		pkDTO.setPersonId(this.getPersonId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a PersonDTO with only the Primary Key Values set
	 */ 	
   public PersonDTO getDTO() {
		PersonDTO dto = new PersonDTO();
		dto.setPersonId(this.getPersonId());
		dto.setFirstName(this.getFirstName());
		dto.setLastName(this.getLastName());
		dto.setTitle(this.getTitle());
		dto.setCompanyId(this.getCompanyId());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this PersonDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this PersonDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tPersonId = "+this.getPersonId()+", ");
		description.append(" \n\tFirstName = "+this.getFirstName()+", ");
		description.append(" \n\tLastName = "+this.getLastName()+", ");
		description.append(" \n\tTitle = "+this.getTitle()+", ");
		description.append(" \n\tCompanyId = "+this.getCompanyId()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this PersonDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this PersonDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tPersonId = "+this.getPersonId()+", ");
		description.append(" \n\tFirstName = "+this.getFirstName()+", ");
		description.append(" \n\tLastName = "+this.getLastName()+", ");
		description.append(" \n\tTitle = "+this.getTitle()+", ");
		description.append(" \n\tCompanyId = "+this.getCompanyId()+", ");
	
	
		description.append(" \n\tCompanyFkBO = "+((this.getCompanyFkBO()==null)? "" : ("\n\t ["+this.getCompanyFkBO().getTableDescription()))+"], ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(PersonBO bo){
		if(		this.personId  == bo.getPersonId() &&
		this.firstName  == bo.getFirstName() &&
		this.lastName  == bo.getLastName() &&
		this.title  == bo.getTitle() &&
		this.companyId  == bo.getCompanyId() &&
		this.emailAddress==bo.getemailAddress()&&
		this.emailPersonFkList  == bo.getEmailPersonFkList() &&
		this.entityPersonFkList  == bo.getEntityPersonFkList() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the PersonForm.  This description is what appears in
	 * lists and dropdowns containing PersonForms.  The default is just a
	 * concatenation of every value in the PersonForm.
	 * 
	 * @return decription of the PersonForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getPersonId()==null)? "" : (this.getPersonId()) +" ");
		display.append((this.getFirstName()==null)? "" : (this.getFirstName()) +" ");
		display.append((this.getLastName()==null)? "" : (this.getLastName()) +" ");
		display.append((this.getTitle()==null)? "" : (this.getTitle()) +" ");
		display.append((this.getCompanyId()==null)? "" : (this.getCompanyId()) +" ");
		return display.toString();
	}

}


