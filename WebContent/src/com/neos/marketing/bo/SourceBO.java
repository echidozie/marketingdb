package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import java.util.Collection;

/**
 * Business Object for Source data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class SourceBO implements java.io.Serializable {


	private Long sourceId  = null;
	private String source  = null;
	private Collection detailSourceFkList  = null;
	private Collection touchSourceFkList  = null;
	private Collection touchbackSourceFkList  = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public SourceBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public SourceBO(SourceDTO dto) {
		super();
		this.setSourceId(dto.getSourceId());
		this.setSource(dto.getSource());
	}	


	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a Long
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
		try {
			this.sourceId = new Long(sourceId);
		} catch (NumberFormatException nfe) {
			this.sourceId = null;
		}
	}		



	/**
	 * get the Source attribute
	 * @return the value of the Source attribute
	 */	
	public String getSource() {
    	return this.source;
   	}

	/**
	 * set the Source attribute to a value defined by a String
	 * @param source the long value for the attribute
	 */
	public void setSource(String source) {
    	this.source = source;
	}


	/**
	 * get the DetailSourceFkList attribute
	 * @return the value of the DetailSourceFkList attribute
	 */	
	public Collection getDetailSourceFkList() {
    	return this.detailSourceFkList;
   	}

	/**
	 * set the DetailSourceFkList attribute to a value defined by a Collection
	 * @param detailSourceFkList the long value for the attribute
	 */
	public void setDetailSourceFkList(Collection detailSourceFkList) {
    	this.detailSourceFkList = detailSourceFkList;
	}


	/**
	 * get the TouchSourceFkList attribute
	 * @return the value of the TouchSourceFkList attribute
	 */	
	public Collection getTouchSourceFkList() {
    	return this.touchSourceFkList;
   	}

	/**
	 * set the TouchSourceFkList attribute to a value defined by a Collection
	 * @param touchSourceFkList the long value for the attribute
	 */
	public void setTouchSourceFkList(Collection touchSourceFkList) {
    	this.touchSourceFkList = touchSourceFkList;
	}


	/**
	 * get the TouchbackSourceFkList attribute
	 * @return the value of the TouchbackSourceFkList attribute
	 */	
	public Collection getTouchbackSourceFkList() {
    	return this.touchbackSourceFkList;
   	}

	/**
	 * set the TouchbackSourceFkList attribute to a value defined by a Collection
	 * @param touchbackSourceFkList the long value for the attribute
	 */
	public void setTouchbackSourceFkList(Collection touchbackSourceFkList) {
    	this.touchbackSourceFkList = touchbackSourceFkList;
	}


	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a SourceBO with only the Primary Key Values set
	 */ 	
   public SourceBO getPKBO() {
		SourceBO pkBO = new SourceBO();
		pkBO.setSourceId(this.getSourceId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a SourceDTO with only the Primary Key Values set
	 */ 	
   public SourceDTO getPKDTO() {
		SourceDTO pkDTO = new SourceDTO();
		pkDTO.setSourceId(this.getSourceId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a SourceDTO with only the Primary Key Values set
	 */ 	
   public SourceDTO getDTO() {
		SourceDTO dto = new SourceDTO();
		dto.setSourceId(this.getSourceId());
		dto.setSource(this.getSource());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this SourceDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this SourceDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tSourceId = "+this.getSourceId()+", ");
		description.append(" \n\tSource = "+this.getSource()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this SourceDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this SourceDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tSourceId = "+this.getSourceId()+", ");
		description.append(" \n\tSource = "+this.getSource()+", ");
	
	
	
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(SourceBO bo){
		if(		this.sourceId  == bo.getSourceId() &&
		this.source  == bo.getSource() &&
		this.detailSourceFkList  == bo.getDetailSourceFkList() &&
		this.touchSourceFkList  == bo.getTouchSourceFkList() &&
		this.touchbackSourceFkList  == bo.getTouchbackSourceFkList() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the SourceForm.  This description is what appears in
	 * lists and dropdowns containing SourceForms.  The default is just a
	 * concatenation of every value in the SourceForm.
	 * 
	 * @return decription of the SourceForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getSourceId()==null)? "" : (this.getSourceId()) +" ");
		display.append((this.getSource()==null)? "" : (this.getSource()) +" ");
		return display.toString();
	}

}


