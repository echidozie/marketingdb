package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import com.neos.marketing.util.DateConverter;

import java.util.Date;

/**
 * Business Object for Touch data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class TouchBO implements java.io.Serializable {


	private Long touchId  = null;
	private Long entityId  = null;
	private java.util.Date touchDate  = null;
	private Long sourceId  = null;
	private Long detailId  = null;
	private SourceBO touchSourceFkBO = null;
	private EntityBO touchEntityFkBO = null;
	private DetailBO touchDetailFkBO = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public TouchBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public TouchBO(TouchDTO dto) {
		super();
		this.setTouchId(dto.getTouchId());
		this.setEntityId(dto.getEntityId());
		this.setTouchDate(dto.getTouchDate());
		this.setSourceId(dto.getSourceId());
		this.setDetailId(dto.getDetailId());
	}	


	/**
	 * get the TouchId attribute
	 * @return the value of the TouchId attribute
	 */	
	public Long getTouchId() {
    	return this.touchId;
   	}

	/**
	 * set the TouchId attribute to a value defined by a Long
	 * @param touchId the long value for the attribute
	 */
	public void setTouchId(Long touchId) {
    	this.touchId = touchId;
	}
	/**
	 * overloaded method to set the TouchId attribute to a value defined by a Long
	 * @param touchId the String value for the attribute
	 */
	public void setTouchId(String touchId) {
		try {
			this.touchId = new Long(touchId);
		} catch (NumberFormatException nfe) {
			this.touchId = null;
		}
	}		



	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a Long
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
		try {
			this.entityId = new Long(entityId);
		} catch (NumberFormatException nfe) {
			this.entityId = null;
		}
	}		



	/**
	 * get the TouchDate attribute
	 * @return the value of the TouchDate attribute
	 */	
	public java.util.Date getTouchDate() {
    	return this.touchDate;
   	}

	/**
	 * set the TouchDate attribute to a value defined by a java.util.Date
	 * @param touchDate the long value for the attribute
	 */
	public void setTouchDate(java.util.Date touchDate) {
    	this.touchDate = touchDate;
	}
	/**
	 * overloaded method to set the TouchDate attribute to a value defined by a java.util.Date
	 * @param touchDate the String value for the attribute
	 */
	public void setTouchDate(String touchDate) {
    	if (touchDate != null && !touchDate.equals("")) {
    		this.touchDate = DateConverter.convertStringToDate(touchDate);
		}
	}		



	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a Long
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
		try {
			this.sourceId = new Long(sourceId);
		} catch (NumberFormatException nfe) {
			this.sourceId = null;
		}
	}		



	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a Long
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
		try {
			this.detailId = new Long(detailId);
		} catch (NumberFormatException nfe) {
			this.detailId = null;
		}
	}		



	/**
	 * get the touchSourceFkBO attribute
	 * @return the value of the touchSourceFkBO attribute
	 */	
	public SourceBO getTouchSourceFkBO() {
		return this.touchSourceFkBO;
	}
	
	/**
	 * set the touchSourceFkBO attribute to a value defined by a Long
	 * @param touchSourceFkBO the SourceBO value for the attribute
	 */ 	
	public void setTouchSourceFkBO(SourceBO touchSourceFkBO) {
		this.touchSourceFkBO = touchSourceFkBO;
	}

	/**
	 * get the touchEntityFkBO attribute
	 * @return the value of the touchEntityFkBO attribute
	 */	
	public EntityBO getTouchEntityFkBO() {
		return this.touchEntityFkBO;
	}
	
	/**
	 * set the touchEntityFkBO attribute to a value defined by a Long
	 * @param touchEntityFkBO the EntityBO value for the attribute
	 */ 	
	public void setTouchEntityFkBO(EntityBO touchEntityFkBO) {
		this.touchEntityFkBO = touchEntityFkBO;
	}

	/**
	 * get the touchDetailFkBO attribute
	 * @return the value of the touchDetailFkBO attribute
	 */	
	public DetailBO getTouchDetailFkBO() {
		return this.touchDetailFkBO;
	}
	
	/**
	 * set the touchDetailFkBO attribute to a value defined by a Long
	 * @param touchDetailFkBO the DetailBO value for the attribute
	 */ 	
	public void setTouchDetailFkBO(DetailBO touchDetailFkBO) {
		this.touchDetailFkBO = touchDetailFkBO;
	}

	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a TouchBO with only the Primary Key Values set
	 */ 	
   public TouchBO getPKBO() {
		TouchBO pkBO = new TouchBO();
		pkBO.setTouchId(this.getTouchId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a TouchDTO with only the Primary Key Values set
	 */ 	
   public TouchDTO getPKDTO() {
		TouchDTO pkDTO = new TouchDTO();
		pkDTO.setTouchId(this.getTouchId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a TouchDTO with only the Primary Key Values set
	 */ 	
   public TouchDTO getDTO() {
		TouchDTO dto = new TouchDTO();
		dto.setTouchId(this.getTouchId());
		dto.setEntityId(this.getEntityId());
		dto.setTouchDate(this.getTouchDate());
		dto.setSourceId(this.getSourceId());
		dto.setDetailId(this.getDetailId());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this TouchDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this TouchDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tTouchId = "+this.getTouchId()+", ");
		description.append(" \n\tEntityId = "+this.getEntityId()+", ");
		description.append(" \n\tTouchDate = "+DateConverter.convertDateToString(this.getTouchDate())+", ");
		description.append(" \n\tSourceId = "+this.getSourceId()+", ");
		description.append(" \n\tDetailId = "+this.getDetailId()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this TouchDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this TouchDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tTouchId = "+this.getTouchId()+", ");
		description.append(" \n\tEntityId = "+this.getEntityId()+", ");
		description.append(" \n\tTouchDate = "+DateConverter.convertDateToString(this.getTouchDate())+", ");
		description.append(" \n\tSourceId = "+this.getSourceId()+", ");
		description.append(" \n\tDetailId = "+this.getDetailId()+", ");
		description.append(" \n\tTouchSourceFkBO = "+((this.getTouchSourceFkBO()==null)? "" : ("\n\t ["+this.getTouchSourceFkBO().getTableDescription()))+"], ");
		description.append(" \n\tTouchEntityFkBO = "+((this.getTouchEntityFkBO()==null)? "" : ("\n\t ["+this.getTouchEntityFkBO().getTableDescription()))+"], ");
		description.append(" \n\tTouchDetailFkBO = "+((this.getTouchDetailFkBO()==null)? "" : ("\n\t ["+this.getTouchDetailFkBO().getTableDescription()))+"], ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(TouchBO bo){
		if(		this.touchId  == bo.getTouchId() &&
		this.entityId  == bo.getEntityId() &&
		this.touchDate  == bo.getTouchDate() &&
		this.sourceId  == bo.getSourceId() &&
		this.detailId  == bo.getDetailId() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the TouchForm.  This description is what appears in
	 * lists and dropdowns containing TouchForms.  The default is just a
	 * concatenation of every value in the TouchForm.
	 * 
	 * @return decription of the TouchForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getTouchId()==null)? "" : (this.getTouchId()) +" ");
		display.append((this.getEntityId()==null)? "" : (this.getEntityId()) +" ");
		display.append((this.getTouchDate()==null)? "" : (this.getTouchDate()) +" ");
		display.append((this.getSourceId()==null)? "" : (this.getSourceId()) +" ");
		display.append((this.getDetailId()==null)? "" : (this.getDetailId()) +" ");
		return display.toString();
	}

}


