package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import com.neos.marketing.util.DateConverter;

import java.util.Date;

/**
 * Business Object for Touchback data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class TouchbackBO implements java.io.Serializable {


	private Long touchbackId  = null;
	private Long entityId  = null;
	private java.util.Date touchbackDate  = null;
	private Long sourceId  = null;
	private Long detailId  = null;
	private DetailBO touchbackDetailFkBO = null;
	private EntityBO touchbackEntityFkBO = null;
	private SourceBO touchbackSourceFkBO = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public TouchbackBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public TouchbackBO(TouchbackDTO dto) {
		super();
		this.setTouchbackId(dto.getTouchbackId());
		this.setEntityId(dto.getEntityId());
		this.setTouchbackDate(dto.getTouchbackDate());
		this.setSourceId(dto.getSourceId());
		this.setDetailId(dto.getDetailId());
	}	


	/**
	 * get the TouchbackId attribute
	 * @return the value of the TouchbackId attribute
	 */	
	public Long getTouchbackId() {
    	return this.touchbackId;
   	}

	/**
	 * set the TouchbackId attribute to a value defined by a Long
	 * @param touchbackId the long value for the attribute
	 */
	public void setTouchbackId(Long touchbackId) {
    	this.touchbackId = touchbackId;
	}
	/**
	 * overloaded method to set the TouchbackId attribute to a value defined by a Long
	 * @param touchbackId the String value for the attribute
	 */
	public void setTouchbackId(String touchbackId) {
		try {
			this.touchbackId = new Long(touchbackId);
		} catch (NumberFormatException nfe) {
			this.touchbackId = null;
		}
	}		



	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a Long
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
		try {
			this.entityId = new Long(entityId);
		} catch (NumberFormatException nfe) {
			this.entityId = null;
		}
	}		



	/**
	 * get the TouchbackDate attribute
	 * @return the value of the TouchbackDate attribute
	 */	
	public java.util.Date getTouchbackDate() {
    	return this.touchbackDate;
   	}

	/**
	 * set the TouchbackDate attribute to a value defined by a java.util.Date
	 * @param touchbackDate the long value for the attribute
	 */
	public void setTouchbackDate(java.util.Date touchbackDate) {
    	this.touchbackDate = touchbackDate;
	}
	/**
	 * overloaded method to set the TouchbackDate attribute to a value defined by a java.util.Date
	 * @param touchbackDate the String value for the attribute
	 */
	public void setTouchbackDate(String touchbackDate) {
    	if (touchbackDate != null && !touchbackDate.equals("")) {
    		this.touchbackDate = DateConverter.convertStringToDate(touchbackDate);
		}
	}		



	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a Long
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
		try {
			this.sourceId = new Long(sourceId);
		} catch (NumberFormatException nfe) {
			this.sourceId = null;
		}
	}		



	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a Long
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
		try {
			this.detailId = new Long(detailId);
		} catch (NumberFormatException nfe) {
			this.detailId = null;
		}
	}		



	/**
	 * get the touchbackDetailFkBO attribute
	 * @return the value of the touchbackDetailFkBO attribute
	 */	
	public DetailBO getTouchbackDetailFkBO() {
		return this.touchbackDetailFkBO;
	}
	
	/**
	 * set the touchbackDetailFkBO attribute to a value defined by a Long
	 * @param touchbackDetailFkBO the DetailBO value for the attribute
	 */ 	
	public void setTouchbackDetailFkBO(DetailBO touchbackDetailFkBO) {
		this.touchbackDetailFkBO = touchbackDetailFkBO;
	}

	/**
	 * get the touchbackEntityFkBO attribute
	 * @return the value of the touchbackEntityFkBO attribute
	 */	
	public EntityBO getTouchbackEntityFkBO() {
		return this.touchbackEntityFkBO;
	}
	
	/**
	 * set the touchbackEntityFkBO attribute to a value defined by a Long
	 * @param touchbackEntityFkBO the EntityBO value for the attribute
	 */ 	
	public void setTouchbackEntityFkBO(EntityBO touchbackEntityFkBO) {
		this.touchbackEntityFkBO = touchbackEntityFkBO;
	}

	/**
	 * get the touchbackSourceFkBO attribute
	 * @return the value of the touchbackSourceFkBO attribute
	 */	
	public SourceBO getTouchbackSourceFkBO() {
		return this.touchbackSourceFkBO;
	}
	
	/**
	 * set the touchbackSourceFkBO attribute to a value defined by a Long
	 * @param touchbackSourceFkBO the SourceBO value for the attribute
	 */ 	
	public void setTouchbackSourceFkBO(SourceBO touchbackSourceFkBO) {
		this.touchbackSourceFkBO = touchbackSourceFkBO;
	}

	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a TouchbackBO with only the Primary Key Values set
	 */ 	
   public TouchbackBO getPKBO() {
		TouchbackBO pkBO = new TouchbackBO();
		pkBO.setTouchbackId(this.getTouchbackId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a TouchbackDTO with only the Primary Key Values set
	 */ 	
   public TouchbackDTO getPKDTO() {
		TouchbackDTO pkDTO = new TouchbackDTO();
		pkDTO.setTouchbackId(this.getTouchbackId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a TouchbackDTO with only the Primary Key Values set
	 */ 	
   public TouchbackDTO getDTO() {
		TouchbackDTO dto = new TouchbackDTO();
		dto.setTouchbackId(this.getTouchbackId());
		dto.setEntityId(this.getEntityId());
		dto.setTouchbackDate(this.getTouchbackDate());
		dto.setSourceId(this.getSourceId());
		dto.setDetailId(this.getDetailId());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this TouchbackDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this TouchbackDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tTouchbackId = "+this.getTouchbackId()+", ");
		description.append(" \n\tEntityId = "+this.getEntityId()+", ");
		description.append(" \n\tTouchbackDate = "+DateConverter.convertDateToString(this.getTouchbackDate())+", ");
		description.append(" \n\tSourceId = "+this.getSourceId()+", ");
		description.append(" \n\tDetailId = "+this.getDetailId()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this TouchbackDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this TouchbackDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tTouchbackId = "+this.getTouchbackId()+", ");
		description.append(" \n\tEntityId = "+this.getEntityId()+", ");
		description.append(" \n\tTouchbackDate = "+DateConverter.convertDateToString(this.getTouchbackDate())+", ");
		description.append(" \n\tSourceId = "+this.getSourceId()+", ");
		description.append(" \n\tDetailId = "+this.getDetailId()+", ");
		description.append(" \n\tTouchbackDetailFkBO = "+((this.getTouchbackDetailFkBO()==null)? "" : ("\n\t ["+this.getTouchbackDetailFkBO().getTableDescription()))+"], ");
		description.append(" \n\tTouchbackEntityFkBO = "+((this.getTouchbackEntityFkBO()==null)? "" : ("\n\t ["+this.getTouchbackEntityFkBO().getTableDescription()))+"], ");
		description.append(" \n\tTouchbackSourceFkBO = "+((this.getTouchbackSourceFkBO()==null)? "" : ("\n\t ["+this.getTouchbackSourceFkBO().getTableDescription()))+"], ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(TouchbackBO bo){
		if(		this.touchbackId  == bo.getTouchbackId() &&
		this.entityId  == bo.getEntityId() &&
		this.touchbackDate  == bo.getTouchbackDate() &&
		this.sourceId  == bo.getSourceId() &&
		this.detailId  == bo.getDetailId() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the TouchbackForm.  This description is what appears in
	 * lists and dropdowns containing TouchbackForms.  The default is just a
	 * concatenation of every value in the TouchbackForm.
	 * 
	 * @return decription of the TouchbackForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getTouchbackId()==null)? "" : (this.getTouchbackId()) +" ");
		display.append((this.getEntityId()==null)? "" : (this.getEntityId()) +" ");
		display.append((this.getTouchbackDate()==null)? "" : (this.getTouchbackDate()) +" ");
		display.append((this.getSourceId()==null)? "" : (this.getSourceId()) +" ");
		display.append((this.getDetailId()==null)? "" : (this.getDetailId()) +" ");
		return display.toString();
	}

}


