package com.neos.marketing.bo;

import com.neos.marketing.dto.*;

import com.neos.marketing.util.DateConverter;

import java.util.Date;

/**
 * Business Object for Visitors data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.2
 */
public class VisitorsBO implements java.io.Serializable {


	private Long id  = null;
	private String ipAddress  = null;
	private String hostname  = null;
	private String pageVisited  = null;
	private String referrer  = null;
	private String timeRequested  = null;
	private java.util.Date timestamp  = null;
	private String os  = null;
	private String browser  = null;
	private boolean selected = false;
	/**
	 * a simple constructor
	 */
	public VisitorsBO() {
	}
	
	/**
	 * constructor that initalizes the BO based on attributes in a DTO.
	 */
	public VisitorsBO(VisitorsDTO dto) {
		super();
		this.setId(dto.getId());
		this.setIpAddress(dto.getIpAddress());
		this.setHostname(dto.getHostname());
		this.setPageVisited(dto.getPageVisited());
		this.setReferrer(dto.getReferrer());
		this.setTimeRequested(dto.getTimeRequested());
		this.setTimestamp(dto.getTimestamp());
		this.setOs(dto.getOs());
		this.setBrowser(dto.getBrowser());
	}	


	/**
	 * get the Id attribute
	 * @return the value of the Id attribute
	 */	
	public Long getId() {
    	return this.id;
   	}

	/**
	 * set the Id attribute to a value defined by a Long
	 * @param id the long value for the attribute
	 */
	public void setId(Long id) {
    	this.id = id;
	}
	/**
	 * overloaded method to set the Id attribute to a value defined by a Long
	 * @param id the String value for the attribute
	 */
	public void setId(String id) {
		try {
			this.id = new Long(id);
		} catch (NumberFormatException nfe) {
			this.id = null;
		}
	}		



	/**
	 * get the IpAddress attribute
	 * @return the value of the IpAddress attribute
	 */	
	public String getIpAddress() {
    	return this.ipAddress;
   	}

	/**
	 * set the IpAddress attribute to a value defined by a String
	 * @param ipAddress the long value for the attribute
	 */
	public void setIpAddress(String ipAddress) {
    	this.ipAddress = ipAddress;
	}


	/**
	 * get the Hostname attribute
	 * @return the value of the Hostname attribute
	 */	
	public String getHostname() {
    	return this.hostname;
   	}

	/**
	 * set the Hostname attribute to a value defined by a String
	 * @param hostname the long value for the attribute
	 */
	public void setHostname(String hostname) {
    	this.hostname = hostname;
	}


	/**
	 * get the PageVisited attribute
	 * @return the value of the PageVisited attribute
	 */	
	public String getPageVisited() {
    	return this.pageVisited;
   	}

	/**
	 * set the PageVisited attribute to a value defined by a String
	 * @param pageVisited the long value for the attribute
	 */
	public void setPageVisited(String pageVisited) {
    	this.pageVisited = pageVisited;
	}


	/**
	 * get the Referrer attribute
	 * @return the value of the Referrer attribute
	 */	
	public String getReferrer() {
    	return this.referrer;
   	}

	/**
	 * set the Referrer attribute to a value defined by a String
	 * @param referrer the long value for the attribute
	 */
	public void setReferrer(String referrer) {
    	this.referrer = referrer;
	}


	/**
	 * get the TimeRequested attribute
	 * @return the value of the TimeRequested attribute
	 */	
	public String getTimeRequested() {
    	return this.timeRequested;
   	}

	/**
	 * set the TimeRequested attribute to a value defined by a String
	 * @param timeRequested the long value for the attribute
	 */
	public void setTimeRequested(String timeRequested) {
    	this.timeRequested = timeRequested;
	}


	/**
	 * get the Timestamp attribute
	 * @return the value of the Timestamp attribute
	 */	
	public java.util.Date getTimestamp() {
    	return this.timestamp;
   	}

	/**
	 * set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the long value for the attribute
	 */
	public void setTimestamp(java.util.Date timestamp) {
    	this.timestamp = timestamp;
	}
	/**
	 * overloaded method to set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the String value for the attribute
	 */
	public void setTimestamp(String timestamp) {
    	if (timestamp != null && !timestamp.equals("")) {
    		this.timestamp = DateConverter.convertStringToDate(timestamp);
		}
	}		



	/**
	 * get the Os attribute
	 * @return the value of the Os attribute
	 */	
	public String getOs() {
    	return this.os;
   	}

	/**
	 * set the Os attribute to a value defined by a String
	 * @param os the long value for the attribute
	 */
	public void setOs(String os) {
    	this.os = os;
	}


	/**
	 * get the Browser attribute
	 * @return the value of the Browser attribute
	 */	
	public String getBrowser() {
    	return this.browser;
   	}

	/**
	 * set the Browser attribute to a value defined by a String
	 * @param browser the long value for the attribute
	 */
	public void setBrowser(String browser) {
    	this.browser = browser;
	}


	/**
	 * returns a BO object that is just the PrimaryKey values of this BO
	 * @return a VisitorsBO with only the Primary Key Values set
	 */ 	
   public VisitorsBO getPKBO() {
		VisitorsBO pkBO = new VisitorsBO();
		pkBO.setId(this.getId());
		return pkBO;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this BO
	 * @return a VisitorsDTO with only the Primary Key Values set
	 */ 	
   public VisitorsDTO getPKDTO() {
		VisitorsDTO pkDTO = new VisitorsDTO();
		pkDTO.setId(this.getId());
		return pkDTO;
	}
	
	/**
	 * returns a DTO object populated with the values of this BO
	 * @return a VisitorsDTO with only the Primary Key Values set
	 */ 	
   public VisitorsDTO getDTO() {
		VisitorsDTO dto = new VisitorsDTO();
		dto.setId(this.getId());
		dto.setIpAddress(this.getIpAddress());
		dto.setHostname(this.getHostname());
		dto.setPageVisited(this.getPageVisited());
		dto.setReferrer(this.getReferrer());
		dto.setTimeRequested(this.getTimeRequested());
		dto.setTimestamp(this.getTimestamp());
		dto.setOs(this.getOs());
		dto.setBrowser(this.getBrowser());
		return dto;
	}
		
	/**
	 * returns a String that is a description of an instance of this VisitorsDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this VisitorsDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
		description.append(" \n\tId = "+this.getId()+", ");
		description.append(" \n\tIpAddress = "+this.getIpAddress()+", ");
		description.append(" \n\tHostname = "+this.getHostname()+", ");
		description.append(" \n\tPageVisited = "+this.getPageVisited()+", ");
		description.append(" \n\tReferrer = "+this.getReferrer()+", ");
		description.append(" \n\tTimeRequested = "+this.getTimeRequested()+", ");
		description.append(" \n\tTimestamp = "+DateConverter.convertDateToString(this.getTimestamp())+", ");
		description.append(" \n\tOs = "+this.getOs()+", ");
		description.append(" \n\tBrowser = "+this.getBrowser()+", ");
      return description.toString();
   }
   
	/**
	 * returns a String that is a description of an instance of this VisitorsDTO object and it's
	 * related objects.  This is used only for debugging and logging but can be overwritten to change
	 * those views.
	 * @return a String that is a description of an instance of this VisitorsDTO object.
	 */   
	public String getNestedTableDescription() {
		//Use for debugging and logging
      	StringBuffer description = new StringBuffer("");
		description.append(" \n\tId = "+this.getId()+", ");
		description.append(" \n\tIpAddress = "+this.getIpAddress()+", ");
		description.append(" \n\tHostname = "+this.getHostname()+", ");
		description.append(" \n\tPageVisited = "+this.getPageVisited()+", ");
		description.append(" \n\tReferrer = "+this.getReferrer()+", ");
		description.append(" \n\tTimeRequested = "+this.getTimeRequested()+", ");
		description.append(" \n\tTimestamp = "+DateConverter.convertDateToString(this.getTimestamp())+", ");
		description.append(" \n\tOs = "+this.getOs()+", ");
		description.append(" \n\tBrowser = "+this.getBrowser()+", ");
  
      return description.toString();
   }
   	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean equals(VisitorsBO bo){
		if(		this.id  == bo.getId() &&
		this.ipAddress  == bo.getIpAddress() &&
		this.hostname  == bo.getHostname() &&
		this.pageVisited  == bo.getPageVisited() &&
		this.referrer  == bo.getReferrer() &&
		this.timeRequested  == bo.getTimeRequested() &&
		this.timestamp  == bo.getTimestamp() &&
		this.os  == bo.getOs() &&
		this.browser  == bo.getBrowser() &&
 1==1){
		return true;
		} else return false;
	}

	/**
	 * get the _DisplayDescription for the VisitorsForm.  This description is what appears in
	 * lists and dropdowns containing VisitorsForms.  The default is just a
	 * concatenation of every value in the VisitorsForm.
	 * 
	 * @return decription of the VisitorsForm used in drop downs and lists.
	 */	
	public String get_DisplayDescription() {
      	//You must define a table description or toString() else you'll end up with this:
		StringBuffer display = new StringBuffer("");
		//display.append((this.getId()==null)? "" : (this.getId()) +" ");
		display.append((this.getIpAddress()==null)? "" : (this.getIpAddress()) +" ");
		display.append((this.getHostname()==null)? "" : (this.getHostname()) +" ");
		display.append((this.getPageVisited()==null)? "" : (this.getPageVisited()) +" ");
		display.append((this.getReferrer()==null)? "" : (this.getReferrer()) +" ");
		display.append((this.getTimeRequested()==null)? "" : (this.getTimeRequested()) +" ");
		display.append((this.getTimestamp()==null)? "" : (this.getTimestamp()) +" ");
		display.append((this.getOs()==null)? "" : (this.getOs()) +" ");
		display.append((this.getBrowser()==null)? "" : (this.getBrowser()) +" ");
		return display.toString();
	}

}


