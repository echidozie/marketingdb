package com.neos.marketing.dao; 

import java.sql.Connection;
import java.sql.SQLException;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.util.db.*;
import com.neos.marketing.util.exception.ApplicationException;

/**
 * BaseDAO is a base database object.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    April 8, 2002
 * @version    1.0
 */
public class BaseDAO {
	private Log log = LogFactory.getLog(this.getClass().getName());
	
   /**
	* Close the connection.
	*/
	public void close(Connection connection) {  
    	try {	
			connection.close();
	  		log.trace("Closed a connection for " + this.getClass().getName());
	  	} catch (SQLException sqle) {
	  		log.warn("Error closing a connection for " + this.getClass().getName()+": "+sqle.getMessage());
	  	}
   }
            	
   /**
	* Return a connection to the database.
	*
	* @param user a String specifing the user identifier provided for compatibilty with
	* 	audit tracking version of DAOs.
	* @returns a connection to the datasource.
	* @throws ApplicationException specifying a database connection error.	
	*/
	protected Connection getConnection(String user) throws ApplicationException {
		DatabaseConnection tConnection = null;
		try {
			tConnection = DatabaseConnectionFactory.getDatbaseConnection();			
		}
		catch (Exception se) {
			log.error("Exception in getConnection() of BaseDAO" + se.getMessage());
		 	throw new ApplicationException("Exception in getConnection() of BaseDAO" + se.getMessage());
	  	}
		return tConnection.getConnection();
	}

   /*
	* Return a connection to the database.
	*
	* @returns a connection to the datasource.
	* @throws Exception specifying a database connection error.
	*/
	protected Connection getConnection() throws ApplicationException {
		DatabaseConnection tConnection = null;
		try {
			tConnection = DatabaseConnectionFactory.getDatbaseConnection();
			log.trace("Opened a connection for " + this.getClass().getName());
		}
		catch (Exception se) {
			se.printStackTrace();
			log.error("Exception in getConnection() of baseDAO" + se.getMessage());
		 	throw new ApplicationException("Exception in getConnection() of baseDAO" + se.getMessage());
	  }
		return tConnection.getConnection();
	}
	
}