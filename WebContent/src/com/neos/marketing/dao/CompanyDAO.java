package com.neos.marketing.dao;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.dto.*;
import com.neos.marketing.util.exception.*;


/**
 * Data access object for CompanyDTO.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */
public class CompanyDAO extends BaseDAO implements DataAccess {

 	private static final String TABLENAME = " company ";
	private Log log = LogFactory.getLog(this.getClass().getName());
	//declare variable to check for duplicate
    public static Boolean isDuplicate = false;
	//All Columns to select from the database.
	private static String ALLCOLUMNS = " company_id,  name ";
	private static String PKCOLUMNS = " company_id ";

	/** returns a count of the columsn **/
	public int count(CompanyDTO dto) throws ApplicationException {
		String sqlStatement = "SELECT COUNT(*) AS cnt FROM" + getTABLENAME();		

		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getCompanyId() != null) {
					whereBuffer.append("and company_id = ? ");
		}
		if (dto.getName() != null) {
					whereBuffer.append("and name like ? ");
		}

		int results = 0;
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			}
			
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	int count = 1;
         	
			if (dto.getCompanyId() != null) {	
				tStatement.setLong(count++, dto.getCompanyId().longValue());	 		
			}
			if (dto.getName() != null) {
				tStatement.setString(count++, dto.getName());
			}

        	tResultSet = tStatement.executeQuery();
        	tResultSet.next();
        	
         	results = tResultSet.getInt("cnt");


   		} catch (SQLException e) {
			log.error("Exception in select() UsersDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in count() UsersDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a collection of all persisted dtos matching the search criteria supplied in
	 * the given dto.
	 * @param the dto containing the fields to select on using an AND operation
	 * @throws Exception
	 */		
	public Collection select(CompanyDTO dto) throws ApplicationException {
		return (select(dto, 0, -1) ); 
	}

		
	/**
	 * returns a limited collection of persisted dtos matching the search criteria supplied in
	 * the given dto.  This is primarily used when the front end uses page breaks to display the result set.
	 * @param CompanyDTO the dto containing the fields to select on using an AND operation
	 * @param startRecord the first record in the result set to return - use 0 for the first record
	 * @param returnSetSize the maximum number of records to return - use -1 to return all records
	 * @throws Exception
	 */		
	public Collection select(CompanyDTO dto, int startRecord, int returnSetSize) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getCompanyId() != null) {
												whereBuffer.append("and company_id = ? ");
									}
		if (dto.getName() != null) {
												if (dto.getName().length() > 0) {
						whereBuffer.append("and name like ? ");
					}
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by name");
			} else {
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by name");
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getCompanyId() != null) {	
				tStatement.setLong(count++, dto.getCompanyId().longValue());	 		
			}
			if (dto.getName() != null && dto.getName().length() > 0) {
				tStatement.setString(count++, dto.getName());
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() CompanyDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() CompanyDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}
	
	public Collection select(CompanyDTO dto, int startRecord, int returnSetSize, String sort_col, String sort_dir) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getCompanyId() != null) {
												whereBuffer.append("and company_id = ? ");
									}
		if (dto.getName() != null) {
												whereBuffer.append("and name like ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3)+" order by name");
			} else {
			   if (sort_col != null) {
			      sqlStatement += ("order by " + sort_col + " " + sort_dir);
			   } else {
   			   	if (" ".equals(PKCOLUMNS))
   			      	sqlStatement += ("order by 1");
   			   	else
   			      	sqlStatement += (" order by name");
			   }
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getCompanyId() != null) {	
				tStatement.setLong(count++, dto.getCompanyId().longValue());	 		
			}
			if (dto.getName() != null) {
				tStatement.setString(count++, dto.getName());
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() CompanyDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() CompanyDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a String of the next available PrimaryKey 
	 * @throws Exception
	 */		
	public String getNewPK() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select (max( company_id )+1) as PK from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);	
			tResultSet = tStatement.executeQuery();
			tResultSet.next();
			returnVal = tResultSet.getString("PK");
			if (returnVal == null) {
				returnVal = "1";
			}						
   		} catch (SQLException e) {
			log.error("Exception in getNewPK() CompanyDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in getNewPK() CompanyDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
	     }
   		 return returnVal;				
	}
	
	//Export to CSV
	/*public void exporttoCSV() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select company_id as cd, name as nm from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);
			tResultSet = tStatement.executeQuery();
			try{
				JFrame frame = new JFrame();
				JFileChooser fc = new JFileChooser();
				File file = new File("Company.csv");
				fc.setSelectedFile(file);
				int r = fc.showSaveDialog(frame);   
				if(r == JFileChooser.APPROVE_OPTION){
					FileWriter fw = new FileWriter(fc.getSelectedFile());
					fw.append("COMPANY ID");
					fw.append(',');
					fw.append("COMPANY NAME");
					fw.append('\n');
					while(tResultSet.next()){
						fw.append(tResultSet.getString("cd"));
						fw.append(',');
						fw.append(tResultSet.getString("nm"));
						fw.append('\n');
					}
					fw.flush();
					fw.close();
				}
				else if(r == JFileChooser.CANCEL_OPTION) {
					System.out.println("Do nothing for CANCEL");
				}
			}
			catch(IOException ioe){
				System.out.println("Trouble writing to file: " + ioe.getMessage());
			}
					
   		} catch (SQLException e) {
			log.error("Exception in exporttoCSV: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in exporttoCSV: " + e.getMessage());
      	} finally {     	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {

   		 	}      
         	close(connection);
	     }			
	}*/
	public void exporttoCSV() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select company_id as cd, name as nm from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;
		
		HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("text/csv;charset=UTF-8");
		res.setHeader("Content-disposition",  "attachment; filename=Company.csv"); //attachment ensures it saved by browser
		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);
			tResultSet = tStatement.executeQuery();
			try{	
					PrintWriter fw = res.getWriter();
					fw.append("COMPANY ID");
					fw.append(',');
					fw.append("COMPANY NAME");
					fw.append('\n');
					while(tResultSet.next()){
						fw.append(tResultSet.getString("cd"));
						fw.append(',');
						fw.append(tResultSet.getString("nm"));
						fw.append('\n');
					}
					
					
					fw.flush();
					fw.close();
			}
			catch(IOException e){
				System.out.println("Trouble writing to file");
			}
					
   		} 
		catch (SQLException e) {
			log.error("Exception in exporttoCSV: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in exporttoCSV: " + e.getMessage());
      	} finally {     	
   			try {
   		    	tStatement.close();
			} 
			catch(Exception e) {
   		 	}      
         	close(connection);
	     }			
	}


	/**
	 * insert - Inserts a CompanyDTO into the database.
	 * @param dto a dto populated with the values to insert.
	 * @param user the id of the user doing the inserting. 
	 * @throws ApplicationException
	 */
	public void insert(CompanyDTO dto, String user) throws ApplicationException {		
		String sqlStatement = "insert into " + getTABLENAME() +" values (  ? ,  ?  )";
		String queryStatement = ("select name from " + getTABLENAME());
		String returnVal = "";
		Connection connection = null;
		PreparedStatement tStatement = null;
		PreparedStatement tStatement2 = null;
		ResultSet tResultSet = null;
		
		try {
			connection = this.getConnection(user);
            tStatement =  connection.prepareStatement(sqlStatement);
			tStatement2 =  connection.prepareStatement(queryStatement);	
			tResultSet = tStatement2.executeQuery();
			while(tResultSet.next()){				
				if (tResultSet.getString("name").equalsIgnoreCase(dto.getName())){
					isDuplicate = true;
					setIsDuplicate(true);
				}
			}
            /*
            * Set the attributes in the query.
            */
			if (isDuplicate == false){
				if (dto.getCompanyId() != null) {
					tStatement.setLong(1, dto.getCompanyId().longValue());	 
				} else {
				tStatement.setString(1, null);
				}
				if (dto.getName() != null) {
					tStatement.setString(2, dto.getName());
				} else {
					tStatement.setString(2, null);
				}
				tStatement.execute();
		    }
   		} catch (SQLException e) {
			log.error("Exception in insert() CompanyDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in insert() CompanyDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }   		
   		   close(connection);
   		}
	}

	public Boolean getIsDuplicate(){
		return isDuplicate;
	}
	public void setIsDuplicate(Boolean isDuplicate){
		this.isDuplicate = isDuplicate;
	}


	/**
	 * update - Updates a CompanyDTO in the database.
	 * @param dto the dto containing the updated information
	 * @param user the userid performing the update
	 * @throws ApplicationException
	 */
	public void update(CompanyDTO pkdto, CompanyDTO dto, String user) throws ApplicationException {
		String sqlStatement = "update " + getTABLENAME() +
			" SET  company_id = ?,  name = ? " +
			" where company_id = ?   ";

		Connection connection = null;
		PreparedStatement tStatement = null;		
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/
		 	if (dto.getCompanyId() != null) {
				tStatement.setLong(1, dto.getCompanyId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getName() != null) {
				tStatement.setString(2, dto.getName());
			} else {
				tStatement.setString(2, null);
			}

			/*
			* Set the attribute(s) of the primary key for this DAO
			*/ 		
		    tStatement.setLong(3, pkdto.getCompanyId().longValue());
        	   	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in update() CompanyDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in update() CompanyDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }
   		   close(connection);   		     
   		}
	} //end update



	/**
	 * delete - Deletes a CompanyDTO from the database.
	 * @param the dto to delete
	 * @param user the userid performing the delete
	 * @throws ApplicationException
	 */
	public void delete(CompanyDTO dto, String user) throws ApplicationException {
		String sqlStatement = "delete from" + getTABLENAME() + 
			" where  company_id = ? ";
		Connection connection =  null; 
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/


		   if (dto.getCompanyId() != null) {
				tStatement.setLong(1, dto.getCompanyId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}

        	tStatement.execute();
      } catch (SQLException e) {
	   log.error("Exception in delete() CompanyDAO" + e.getMessage(), e);
   	   throw new ApplicationException("Exception in delete() CompanyDAO" + e.getMessage());
      } finally {
   		 try {
   		    tStatement.close();
   		 } catch(Exception e) {
   		    //let it go.
   		 }      
         close(connection);
      }
      
	}


	/**
	 * create a dto representing the the current row in the result set.
	 * @param tResultSet the current result set
	 * @return a populated dto object
	 * @throws SQLException
	 */
  private CompanyDTO populateDTO(ResultSet tResultSet) throws SQLException {
		long l;
		CompanyDTO dto = new CompanyDTO();
		l = tResultSet.getLong("company_id");
		if (!tResultSet.wasNull()){
			dto.setCompanyId(new Long(l));
		}
		dto.setName(tResultSet.getString("name"));
		return dto;
	}	
	
	public static String getTABLENAME() {
		
		// account for some DBs that allow spaces in table names
		String tableName = TABLENAME;
		if (TABLENAME.trim().indexOf(" ") > -1) {
			String innerStr = TABLENAME.trim();
			tableName = (char)34 + innerStr + (char)34;
		} else {
			tableName = TABLENAME;
		}
		return tableName;
		
	}	
	
}	