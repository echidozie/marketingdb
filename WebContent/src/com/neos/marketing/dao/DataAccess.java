package com.neos.marketing.dao;

/**
 * DataAccess is an for interface for Data Access Objects.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    December 1, 2003
 * @version    1.0
 */
public interface DataAccess {
	
}