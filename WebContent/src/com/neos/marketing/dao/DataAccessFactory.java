package com.neos.marketing.dao;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * DataAccessFactory is a factory for DataAccess implementors.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    December 1, 2003
 * @version    1.0
 */
public class DataAccessFactory {
	private static Log log = LogFactory.getLog("com.neos.marketing.dao.DataAccessFactory");
	private static Map daoHash = new HashMap();
	
	static {
		try {
			daoHash.put("EntityDAO", "com.neos.marketing.dao.EntityDAO");
			daoHash.put("TouchbackDAO", "com.neos.marketing.dao.TouchbackDAO");
			daoHash.put("CompanyDAO", "com.neos.marketing.dao.CompanyDAO");
			daoHash.put("EmailDAO", "com.neos.marketing.dao.EmailDAO");
			daoHash.put("NoteDAO", "com.neos.marketing.dao.NoteDAO");
			daoHash.put("DownloadsDAO", "com.neos.marketing.dao.DownloadsDAO");
			daoHash.put("VisitorsDAO", "com.neos.marketing.dao.VisitorsDAO");
			daoHash.put("TouchDAO", "com.neos.marketing.dao.TouchDAO");
			daoHash.put("PersonDAO", "com.neos.marketing.dao.PersonDAO");
			daoHash.put("SourceDAO", "com.neos.marketing.dao.SourceDAO");
			daoHash.put("DetailDAO", "com.neos.marketing.dao.DetailDAO");
			java.util.ResourceBundle props = java.util.ResourceBundle.getBundle("com.neos.marketing.dao.dao");
			if (props != null) {
				for (Enumeration en = props.getKeys(); en.hasMoreElements();) {
					String key = (String) en.nextElement();
					daoHash.put(key, props.getString(key));
				}
			}
		} catch (Exception e) {
			//Don't care about exception
		}
	}
		
	/**
	 * return an instance of the request Data Access object.
	 * @param daoName the full name of the desired Data Access object.
	 * @return DataAccess the desired Data Access object.
	 */
	public static DataAccess getDAO(String daoKey) {
		DataAccess dao = null;
		try {		
			String daoName = (String) daoHash.get(daoKey);
			dao = (DataAccess) Class.forName(daoName).newInstance();
		} catch (InstantiationException ie) {
			log.error(ie.getMessage());
		} catch (IllegalAccessException iae) {
			log.error(iae.getMessage());
		} catch (ClassNotFoundException cnfe) {
			log.error(cnfe.getMessage());
		}
		return dao;
	}
	
}
