package com.neos.marketing.dao;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.dto.*;
import com.neos.marketing.util.DateConverter;
import com.neos.marketing.util.exception.*;


/**
 * Data access object for DownloadsDTO.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */
public class DownloadsDAO extends BaseDAO implements DataAccess {

 	private static final String TABLENAME = " downloads ";
	private Log log = LogFactory.getLog(this.getClass().getName());
    
	//All Columns to select from the database.
	private static String ALLCOLUMNS = " id,  name,  email,  download_id,  item_title,  timestamp,  time_requested,  ip_address ";
	private static String PKCOLUMNS = " id ";

	/** returns a count of the columsn **/
	public int count(DownloadsDTO dto) throws ApplicationException {
		String sqlStatement = "SELECT COUNT(*) AS cnt FROM" + getTABLENAME();		

		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getId() != null) {
					whereBuffer.append("and id = ? ");
		}
		if (dto.getName() != null) {
					whereBuffer.append("and name like ? ");
		}
		if (dto.getEmail() != null) {
					whereBuffer.append("and email like ? ");
		}
		if (dto.getDownloadId() != null) {
					whereBuffer.append("and download_id like ? ");
		}
		if (dto.getItemTitle() != null) {
					whereBuffer.append("and item_title like ? ");
		}
		if (dto.getTimestamp() != null) {
					whereBuffer.append("and timestamp = ? ");
		}
		if (dto.getTimeRequested() != null) {
					whereBuffer.append("and time_requested like ? ");
		}
		if (dto.getIpAddress() != null) {
					whereBuffer.append("and ip_address like ? ");
		}

		int results = 0;
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			}
			
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	int count = 1;
         	
			if (dto.getId() != null) {	
				tStatement.setLong(count++, dto.getId().longValue());	 		
			}
			if (dto.getName() != null) {
				tStatement.setString(count++, dto.getName());
			}
			if (dto.getEmail() != null) {
				tStatement.setString(count++, dto.getEmail());
			}
			if (dto.getDownloadId() != null) {
				tStatement.setString(count++, dto.getDownloadId());
			}
			if (dto.getItemTitle() != null) {
				tStatement.setString(count++, dto.getItemTitle());
			}
			if (dto.getTimestamp() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTimestamp()));	  				
			}
			if (dto.getTimeRequested() != null) {
				tStatement.setString(count++, dto.getTimeRequested());
			}
			if (dto.getIpAddress() != null) {
				tStatement.setString(count++, dto.getIpAddress());
			}

        	tResultSet = tStatement.executeQuery();
        	tResultSet.next();
        	
         	results = tResultSet.getInt("cnt");


   		} catch (SQLException e) {
			log.error("Exception in select() UsersDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in count() UsersDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a collection of all persisted dtos matching the search criteria supplied in
	 * the given dto.
	 * @param the dto containing the fields to select on using an AND operation
	 * @throws Exception
	 */		
	public Collection select(DownloadsDTO dto) throws ApplicationException {
		return (select(dto, 0, -1) ); 
	}

		
	/**
	 * returns a limited collection of persisted dtos matching the search criteria supplied in
	 * the given dto.  This is primarily used when the front end uses page breaks to display the result set.
	 * @param DownloadsDTO the dto containing the fields to select on using an AND operation
	 * @param startRecord the first record in the result set to return - use 0 for the first record
	 * @param returnSetSize the maximum number of records to return - use -1 to return all records
	 * @throws Exception
	 */		
	public Collection select(DownloadsDTO dto, int startRecord, int returnSetSize) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getId() != null) {
												whereBuffer.append("and id = ? ");
									}
		if (dto.getName() != null) {
												if (dto.getName().length() > 0) {
						whereBuffer.append("and name like ? ");
					}
									}
		if (dto.getEmail() != null) {
												if (dto.getEmail().length() > 0) {
						whereBuffer.append("and email like ? ");
					}
									}
		if (dto.getDownloadId() != null) {
												if (dto.getDownloadId().length() > 0) {
						whereBuffer.append("and download_id like ? ");
					}
									}
		if (dto.getItemTitle() != null) {
												if (dto.getItemTitle().length() > 0) {
						whereBuffer.append("and item_title like ? ");
					}
									}
		if (dto.getTimestamp() != null) {
												whereBuffer.append("and timestamp = ? ");
									}
		if (dto.getTimeRequested() != null) {
												if (dto.getTimeRequested().length() > 0) {
						whereBuffer.append("and time_requested like ? ");
					}
									}
		if (dto.getIpAddress() != null) {
												if (dto.getIpAddress().length() > 0) {
						whereBuffer.append("and ip_address like ? ");
					}
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by timestamp desc");
			} else {
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by timestamp desc");
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getId() != null) {	
				tStatement.setLong(count++, dto.getId().longValue());	 		
			}
			if (dto.getName() != null && dto.getName().length() > 0) {
				tStatement.setString(count++, dto.getName());
			}
			if (dto.getEmail() != null && dto.getEmail().length() > 0) {
				tStatement.setString(count++, dto.getEmail());
			}
			if (dto.getDownloadId() != null && dto.getDownloadId().length() > 0) {
				tStatement.setString(count++, dto.getDownloadId());
			}
			if (dto.getItemTitle() != null && dto.getItemTitle().length() > 0) {
				tStatement.setString(count++, dto.getItemTitle());
			}
			if (dto.getTimestamp() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTimestamp()));	  				
			}
			if (dto.getTimeRequested() != null && dto.getTimeRequested().length() > 0) {
				tStatement.setString(count++, dto.getTimeRequested());
			}
			if (dto.getIpAddress() != null && dto.getIpAddress().length() > 0) {
				tStatement.setString(count++, dto.getIpAddress());
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() DownloadsDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() DownloadsDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}
	
	public Collection select(DownloadsDTO dto, int startRecord, int returnSetSize, String sort_col, String sort_dir) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getId() != null) {
												whereBuffer.append("and id = ? ");
									}
		if (dto.getName() != null) {
												whereBuffer.append("and name like ? ");
									}
		if (dto.getEmail() != null) {
												whereBuffer.append("and email like ? ");
									}
		if (dto.getDownloadId() != null) {
												whereBuffer.append("and download_id like ? ");
									}
		if (dto.getItemTitle() != null) {
												whereBuffer.append("and item_title like ? ");
									}
		if (dto.getTimestamp() != null) {
												whereBuffer.append("and timestamp = ? ");
									}
		if (dto.getTimeRequested() != null) {
												whereBuffer.append("and time_requested like ? ");
									}
		if (dto.getIpAddress() != null) {
												whereBuffer.append("and ip_address like ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3)+" order by timestamp desc");
			} else {
			   if (sort_col != null) {
			      sqlStatement += ("order by " + sort_col + " " + sort_dir);
			   } else {
   			   	if (" ".equals(PKCOLUMNS))
   			      	sqlStatement += ("order by 1");
   			   	else
   			      	sqlStatement += (" order by timestamp desc");
			   }
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getId() != null) {	
				tStatement.setLong(count++, dto.getId().longValue());	 		
			}
			if (dto.getName() != null) {
				tStatement.setString(count++, dto.getName());
			}
			if (dto.getEmail() != null) {
				tStatement.setString(count++, dto.getEmail());
			}
			if (dto.getDownloadId() != null) {
				tStatement.setString(count++, dto.getDownloadId());
			}
			if (dto.getItemTitle() != null) {
				tStatement.setString(count++, dto.getItemTitle());
			}
			if (dto.getTimestamp() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTimestamp()));	  				
			}
			if (dto.getTimeRequested() != null) {
				tStatement.setString(count++, dto.getTimeRequested());
			}
			if (dto.getIpAddress() != null) {
				tStatement.setString(count++, dto.getIpAddress());
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() DownloadsDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() DownloadsDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a String of the next available PrimaryKey 
	 * @throws Exception
	 */		
	public String getNewPK() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select (max( id )+1) as PK from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);	
			tResultSet = tStatement.executeQuery();
			tResultSet.next();
			returnVal = tResultSet.getString("PK");
			if (returnVal == null) {
				returnVal = "1";
			}						
   		} catch (SQLException e) {
			log.error("Exception in getNewPK() DownloadsDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in getNewPK() DownloadsDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
	     }
   		 return returnVal;				
	}
	
	//Export to CSV	
	public void exporttoCSV() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select id as id, name as nm, email as em, company as cp, download_id as dd, item_title as it, timestamp as tp, ip_address as ip from " + getTABLENAME());		
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;
		
		HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("text/csv;charset=UTF-8");
		res.setHeader("Content-disposition",  "attachment; filename=Download.csv"); //attachment ensures it saved by browser
		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);
			tResultSet = tStatement.executeQuery();
			try{	
					PrintWriter fw = res.getWriter();
					fw.append("ID");
					fw.append(',');
					fw.append("NAME");
					fw.append(',');
					fw.append("EMAIL");
					fw.append(',');
					fw.append("COMPANY");
					fw.append(',');
					fw.append("DOWNLOAD ID");
					fw.append(',');
					fw.append("ITEM TITLE");
					fw.append(',');
					fw.append("TIMESTAMP");
					fw.append(',');
					fw.append("IP");
					fw.append('\n');
					while(tResultSet.next()){
						fw.append(tResultSet.getString("id"));
						fw.append(',');
						fw.append(tResultSet.getString("nm"));
						fw.append(',');
						fw.append(tResultSet.getString("em"));
						fw.append(',');
						fw.append(tResultSet.getString("cp"));
						fw.append(',');
						fw.append(tResultSet.getString("dd"));
						fw.append(',');
						fw.append(tResultSet.getString("it"));
						fw.append(',');
						fw.append(tResultSet.getString("tp"));
						fw.append(',');
						fw.append(tResultSet.getString("ip"));
						fw.append('\n');
					}
									
					fw.flush();
					fw.close();
			}
			catch(IOException e){
				System.out.println("Trouble writing to file");
			}
					
   		} 
		catch (SQLException e) {
			log.error("Exception in exporttoCSV: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in exporttoCSV: " + e.getMessage());
      	} finally {     	
   			try {
   		    	tStatement.close();
			} 
			catch(Exception e) {
   		 	}      
         	close(connection);
	     }			
	}


	/**
	 * insert - Inserts a DownloadsDTO into the database.
	 * @param dto a dto populated with the values to insert.
	 * @param user the id of the user doing the inserting. 
	 * @throws ApplicationException
	 */
	public void insert(DownloadsDTO dto, String user) throws ApplicationException {		
		String sqlStatement = "insert into " + getTABLENAME() +" values (  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ?  )";
 
		Connection connection = null;
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
            tStatement =  connection.prepareStatement(sqlStatement);
            /*
            * Set the attributes in the query.
            */
		 	if (dto.getId() != null) {
				tStatement.setLong(1, dto.getId().longValue());	 
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getName() != null) {
				tStatement.setString(2, dto.getName());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getEmail() != null) {
				tStatement.setString(3, dto.getEmail());
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getDownloadId() != null) {
				tStatement.setString(4, dto.getDownloadId());
			} else {
				tStatement.setString(4, null);
			}
		 	if (dto.getItemTitle() != null) {
				tStatement.setString(5, dto.getItemTitle());
			} else {
				tStatement.setString(5, null);
			}
		 	if (dto.getTimestamp() != null) {
				tStatement.setDate(6, DateConverter.convertDateToSqlDate(dto.getTimestamp()));
			} else {
				tStatement.setString(6, null);
			}
		 	if (dto.getTimeRequested() != null) {
				tStatement.setString(7, dto.getTimeRequested());
			} else {
				tStatement.setString(7, null);
			}
		 	if (dto.getIpAddress() != null) {
				tStatement.setString(8, dto.getIpAddress());
			} else {
				tStatement.setString(8, null);
			}
        	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in insert() DownloadsDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in insert() DownloadsDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }   		
   		   close(connection);
   		}
	}




	/**
	 * update - Updates a DownloadsDTO in the database.
	 * @param dto the dto containing the updated information
	 * @param user the userid performing the update
	 * @throws ApplicationException
	 */
	public void update(DownloadsDTO pkdto, DownloadsDTO dto, String user) throws ApplicationException {
		String sqlStatement = "update " + getTABLENAME() +
			" SET  id = ?,  name = ?,  email = ?,  download_id = ?,  item_title = ?,  timestamp = ?,  time_requested = ?,  ip_address = ? " +
			" where id = ?   ";

		Connection connection = null;
		PreparedStatement tStatement = null;		
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/
		 	if (dto.getId() != null) {
				tStatement.setLong(1, dto.getId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getName() != null) {
				tStatement.setString(2, dto.getName());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getEmail() != null) {
				tStatement.setString(3, dto.getEmail());
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getDownloadId() != null) {
				tStatement.setString(4, dto.getDownloadId());
			} else {
				tStatement.setString(4, null);
			}
		 	if (dto.getItemTitle() != null) {
				tStatement.setString(5, dto.getItemTitle());
			} else {
				tStatement.setString(5, null);
			}
		 	if (dto.getTimestamp() != null) {
				tStatement.setDate(6, DateConverter.convertDateToSqlDate(dto.getTimestamp()));
			} else {
				tStatement.setString(6, null);
			}
		 	if (dto.getTimeRequested() != null) {
				tStatement.setString(7, dto.getTimeRequested());
			} else {
				tStatement.setString(7, null);
			}
		 	if (dto.getIpAddress() != null) {
				tStatement.setString(8, dto.getIpAddress());
			} else {
				tStatement.setString(8, null);
			}

			/*
			* Set the attribute(s) of the primary key for this DAO
			*/ 		
		    tStatement.setLong(9, pkdto.getId().longValue());
        	   	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in update() DownloadsDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in update() DownloadsDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }
   		   close(connection);   		     
   		}
	} //end update



	/**
	 * delete - Deletes a DownloadsDTO from the database.
	 * @param the dto to delete
	 * @param user the userid performing the delete
	 * @throws ApplicationException
	 */
	public void delete(DownloadsDTO dto, String user) throws ApplicationException {
		String sqlStatement = "delete from" + getTABLENAME() + 
			" where  id = ? ";
		Connection connection =  null; 
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/


		   if (dto.getId() != null) {
				tStatement.setLong(1, dto.getId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}

        	tStatement.execute();
      } catch (SQLException e) {
	   log.error("Exception in delete() DownloadsDAO" + e.getMessage(), e);
   	   throw new ApplicationException("Exception in delete() DownloadsDAO" + e.getMessage());
      } finally {
   		 try {
   		    tStatement.close();
   		 } catch(Exception e) {
   		    //let it go.
   		 }      
         close(connection);
      }
      
	}


	/**
	 * create a dto representing the the current row in the result set.
	 * @param tResultSet the current result set
	 * @return a populated dto object
	 * @throws SQLException
	 */
  private DownloadsDTO populateDTO(ResultSet tResultSet) throws SQLException {
		long l;
		DownloadsDTO dto = new DownloadsDTO();
		l = tResultSet.getLong("id");
		if (!tResultSet.wasNull()){
			dto.setId(new Long(l));
		}
		dto.setName(tResultSet.getString("name"));
		dto.setEmail(tResultSet.getString("email"));
		dto.setDownloadId(tResultSet.getString("download_id"));
		dto.setItemTitle(tResultSet.getString("item_title"));
		if (tResultSet.getTimestamp("timestamp") != null){
			dto.setTimestamp(DateConverter.convertTimestampToSqlDate(tResultSet.getTimestamp("timestamp")));
		}	
		dto.setTimeRequested(tResultSet.getString("time_requested"));
		dto.setIpAddress(tResultSet.getString("ip_address"));
		return dto;
	}	
	
	public static String getTABLENAME() {
		
		// account for some DBs that allow spaces in table names
		String tableName = TABLENAME;
		if (TABLENAME.trim().indexOf(" ") > -1) {
			String innerStr = TABLENAME.trim();
			tableName = (char)34 + innerStr + (char)34;
		} else {
			tableName = TABLENAME;
		}
		return tableName;
		
	}	
	
}	