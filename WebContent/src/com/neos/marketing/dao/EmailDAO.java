package com.neos.marketing.dao;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.dto.*;
import com.neos.marketing.util.exception.*;


/**
 * Data access object for EmailDTO.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */
public class EmailDAO extends BaseDAO implements DataAccess {

 	private static final String TABLENAME = " email ";
	private Log log = LogFactory.getLog(this.getClass().getName());
    public static Boolean isDuplicate = false;
	//All Columns to select from the database.
	private static String ALLCOLUMNS = " email_id,  email_address,  person_id ";
	private static String PKCOLUMNS = " email_id ";

	/** returns a count of the columsn **/
	public int count(EmailDTO dto) throws ApplicationException {
		String sqlStatement = "SELECT COUNT(*) AS cnt FROM" + getTABLENAME();		

		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getEmailId() != null) {
					whereBuffer.append("and email_id = ? ");
		}
		if (dto.getEmailAddress() != null) {
					whereBuffer.append("and email_address like ? ");
		}
		if (dto.getPersonId() != null) {
					whereBuffer.append("and person_id = ? ");
		}

		int results = 0;
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			}
			
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	int count = 1;
         	
			if (dto.getEmailId() != null) {	
				tStatement.setLong(count++, dto.getEmailId().longValue());	 		
			}
			if (dto.getEmailAddress() != null) {
				tStatement.setString(count++, dto.getEmailAddress());
			}
			if (dto.getPersonId() != null) {	
				tStatement.setLong(count++, dto.getPersonId().longValue());	 		
			}

        	tResultSet = tStatement.executeQuery();
        	tResultSet.next();
        	
         	results = tResultSet.getInt("cnt");


   		} catch (SQLException e) {
			log.error("Exception in select() UsersDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in count() UsersDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a collection of all persisted dtos matching the search criteria supplied in
	 * the given dto.
	 * @param the dto containing the fields to select on using an AND operation
	 * @throws Exception
	 */		
	public Collection select(EmailDTO dto) throws ApplicationException {
		return (select(dto, 0, -1) ); 
	}

		
	/**
	 * returns a limited collection of persisted dtos matching the search criteria supplied in
	 * the given dto.  This is primarily used when the front end uses page breaks to display the result set.
	 * @param EmailDTO the dto containing the fields to select on using an AND operation
	 * @param startRecord the first record in the result set to return - use 0 for the first record
	 * @param returnSetSize the maximum number of records to return - use -1 to return all records
	 * @throws Exception
	 */		
	public Collection select(EmailDTO dto, int startRecord, int returnSetSize) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getEmailId() != null) {
												whereBuffer.append("and email_id = ? ");
									}
		if (dto.getEmailAddress() != null) {
												if (dto.getEmailAddress().length() > 0) {
						whereBuffer.append("and email_address like ? ");
					}
									}
		if (dto.getPersonId() != null) {
												whereBuffer.append("and person_id = ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by email_address asc");
			} else {
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by email_address asc");
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getEmailId() != null) {	
				tStatement.setLong(count++, dto.getEmailId().longValue());	 		
			}
			if (dto.getEmailAddress() != null && dto.getEmailAddress().length() > 0) {
				tStatement.setString(count++, dto.getEmailAddress());
			}
			if (dto.getPersonId() != null) {	
				tStatement.setLong(count++, dto.getPersonId().longValue());	 		
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() EmailDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() EmailDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}
	
	public Collection select(EmailDTO dto, int startRecord, int returnSetSize, String sort_col, String sort_dir) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getEmailId() != null) {
												whereBuffer.append("and email_id = ? ");
									}
		if (dto.getEmailAddress() != null) {
												whereBuffer.append("and email_address like ? ");
									}
		if (dto.getPersonId() != null) {
												whereBuffer.append("and person_id = ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3)+" order by email_address asc");
			} else {
			   if (sort_col != null) {
			      sqlStatement += ("order by " + sort_col + " " + sort_dir);
			   } else {
   			   	if (" ".equals(PKCOLUMNS))
   			      	sqlStatement += ("order by 1");
   			   	else
   			      	sqlStatement += (" order by email_address asc");
			   }
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getEmailId() != null) {	
				tStatement.setLong(count++, dto.getEmailId().longValue());	 		
			}
			if (dto.getEmailAddress() != null) {
				tStatement.setString(count++, dto.getEmailAddress());
			}
			if (dto.getPersonId() != null) {	
				tStatement.setLong(count++, dto.getPersonId().longValue());	 		
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() EmailDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() EmailDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a String of the next available PrimaryKey 
	 * @throws Exception
	 */		
	public String getNewPK() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select (max( email_id )+1) as PK from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);	
			tResultSet = tStatement.executeQuery();
			tResultSet.next();
			returnVal = tResultSet.getString("PK");
			if (returnVal == null) {
				returnVal = "1";
			}						
   		} catch (SQLException e) {
			log.error("Exception in getNewPK() EmailDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in getNewPK() EmailDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
	     }
   		 return returnVal;				
	}
	
	//Export to CSV	
	public void exporttoCSV() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select email.email_id as id, email.email_address as ed, person.first_name as fn, person.last_name as ln from email inner join person on person.person_id = email.person_id");	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;
		
		HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("text/csv;charset=UTF-8");
		res.setHeader("Content-disposition",  "attachment; filename=Email.csv"); //attachment ensures it saved by browser
		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);
			tResultSet = tStatement.executeQuery();
			try{	
					PrintWriter fw = res.getWriter();
					fw.append("EMAIL ID");
					fw.append(',');
					fw.append("EMAIL ADDRESS");
					fw.append(',');
					fw.append("FIRST NAME");
					fw.append(',');
					fw.append("LAST NAME");
					fw.append('\n');
					while(tResultSet.next()){
						fw.append(tResultSet.getString("id"));
						fw.append(',');
						fw.append(tResultSet.getString("ed"));
						fw.append(',');
						fw.append(tResultSet.getString("fn"));
						fw.append(',');
						fw.append(tResultSet.getString("ln"));
						fw.append('\n');
					}
					fw.flush();
					fw.close();
			}
			catch(IOException e){
				System.out.println("Trouble writing to file");
			}
					
   		} 
		catch (SQLException e) {
			log.error("Exception in exporttoCSV: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in exporttoCSV: " + e.getMessage());
      	} finally {     	
   			try {
   		    	tStatement.close();
			} 
			catch(Exception e) {
   		 	}      
         	close(connection);
	     }			
	}


	/**
	 * insert - Inserts a EmailDTO into the database.
	 * @param dto a dto populated with the values to insert.
	 * @param user the id of the user doing the inserting. 
	 * @throws ApplicationException
	 */
	public void insert(EmailDTO dto, String user) throws ApplicationException {		
		String sqlStatement = "insert into " + getTABLENAME() +" values (  ? ,  ? ,  ?  )";
		String queryStatement = ("select email_address from " + getTABLENAME());
		
		Connection connection = null;
		PreparedStatement tStatement = null;
		PreparedStatement tStatement2 = null;
		ResultSet tResultSet = null;
		try {
			connection = this.getConnection(user);
            tStatement =  connection.prepareStatement(sqlStatement);
			tStatement2 =  connection.prepareStatement(queryStatement);	
			tResultSet = tStatement2.executeQuery();
			while(tResultSet.next()){				
				if (tResultSet.getString("email_address").equalsIgnoreCase(dto.getEmailAddress())){
					isDuplicate = true;
					setIsDuplicate(true);
				}
			}
            /*
            * Set the attributes in the query.
            */
			if (isDuplicate == false){
				if (dto.getEmailId() != null) {
					tStatement.setLong(1, dto.getEmailId().longValue());	 
				} else {
				tStatement.setString(1, null);
				}
				if (dto.getEmailAddress() != null) {
					tStatement.setString(2, dto.getEmailAddress());
				} else {
					tStatement.setString(2, null);
				}
				if (dto.getPersonId() != null) {
					tStatement.setLong(3, dto.getPersonId().longValue());	 
				} else {
					tStatement.setString(3, null);
				}
				tStatement.execute();
			}
   		} catch (SQLException e) {
			log.error("Exception in insert() EmailDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in insert() EmailDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }   		
   		   close(connection);
   		}
	}

	public Boolean getIsDuplicate(){
		return isDuplicate;
	}
	public void setIsDuplicate(Boolean isDuplicate){
		this.isDuplicate = isDuplicate;
	}


	/**
	 * update - Updates a EmailDTO in the database.
	 * @param dto the dto containing the updated information
	 * @param user the userid performing the update
	 * @throws ApplicationException
	 */
	public void update(EmailDTO pkdto, EmailDTO dto, String user) throws ApplicationException {
		String sqlStatement = "update " + getTABLENAME() +
			" SET  email_id = ?,  email_address = ?,  person_id = ? " +
			" where email_id = ?   ";

		Connection connection = null;
		PreparedStatement tStatement = null;		
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/
		 	if (dto.getEmailId() != null) {
				tStatement.setLong(1, dto.getEmailId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getEmailAddress() != null) {
				tStatement.setString(2, dto.getEmailAddress());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getPersonId() != null) {
				tStatement.setLong(3, dto.getPersonId().longValue());	 		
			} else {
				tStatement.setString(3, null);
			}

			/*
			* Set the attribute(s) of the primary key for this DAO
			*/ 		
		    tStatement.setLong(4, pkdto.getEmailId().longValue());
        	   	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in update() EmailDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in update() EmailDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }
   		   close(connection);   		     
   		}
	} //end update



	/**
	 * delete - Deletes a EmailDTO from the database.
	 * @param the dto to delete
	 * @param user the userid performing the delete
	 * @throws ApplicationException
	 */
	public void delete(EmailDTO dto, String user) throws ApplicationException {
		String sqlStatement = "delete from" + getTABLENAME() + 
			" where  email_id = ? ";
		Connection connection =  null; 
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/


		   if (dto.getEmailId() != null) {
				tStatement.setLong(1, dto.getEmailId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}

        	tStatement.execute();
      } catch (SQLException e) {
	   log.error("Exception in delete() EmailDAO" + e.getMessage(), e);
   	   throw new ApplicationException("Exception in delete() EmailDAO" + e.getMessage());
      } finally {
   		 try {
   		    tStatement.close();
   		 } catch(Exception e) {
   		    //let it go.
   		 }      
         close(connection);
      }
      
	}


	/**
	 * create a dto representing the the current row in the result set.
	 * @param tResultSet the current result set
	 * @return a populated dto object
	 * @throws SQLException
	 */
  private EmailDTO populateDTO(ResultSet tResultSet) throws SQLException {
		long l;
		EmailDTO dto = new EmailDTO();
		l = tResultSet.getLong("email_id");
		if (!tResultSet.wasNull()){
			dto.setEmailId(new Long(l));
		}
		dto.setEmailAddress(tResultSet.getString("email_address"));
		l = tResultSet.getLong("person_id");
		if (!tResultSet.wasNull()){
			dto.setPersonId(new Long(l));
		}
		return dto;
	}	
	
	public static String getTABLENAME() {
		
		// account for some DBs that allow spaces in table names
		String tableName = TABLENAME;
		if (TABLENAME.trim().indexOf(" ") > -1) {
			String innerStr = TABLENAME.trim();
			tableName = (char)34 + innerStr + (char)34;
		} else {
			tableName = TABLENAME;
		}
		return tableName;
		
	}	
	
}	