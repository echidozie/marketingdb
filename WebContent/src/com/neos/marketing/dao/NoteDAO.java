package com.neos.marketing.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.dto.*;
import com.neos.marketing.util.exception.*;


/**
 * Data access object for NoteDTO.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */
public class NoteDAO extends BaseDAO implements DataAccess {

 	private static final String TABLENAME = " note ";
	private Log log = LogFactory.getLog(this.getClass().getName());
    
	//All Columns to select from the database.
	private static String ALLCOLUMNS = " note_id,  object_type,  object_id,  note ";
	private static String PKCOLUMNS = " note_id ";

	/** returns a count of the columsn **/
	public int count(NoteDTO dto) throws ApplicationException {
		String sqlStatement = "SELECT COUNT(*) AS cnt FROM" + getTABLENAME();		

		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getNoteId() != null) {
					whereBuffer.append("and note_id = ? ");
		}
		if (dto.getObjectType() != null) {
					whereBuffer.append("and object_type like ? ");
		}
		if (dto.getObjectId() != null) {
					whereBuffer.append("and object_id = ? ");
		}
		if (dto.getNote() != null) {
					whereBuffer.append("and note like ? ");
		}

		int results = 0;
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			}
			
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	int count = 1;
         	
			if (dto.getNoteId() != null) {	
				tStatement.setLong(count++, dto.getNoteId().longValue());	 		
			}
			if (dto.getObjectType() != null) {
				tStatement.setString(count++, dto.getObjectType());
			}
			if (dto.getObjectId() != null) {	
				tStatement.setLong(count++, dto.getObjectId().longValue());	 		
			}
			if (dto.getNote() != null) {
				tStatement.setString(count++, dto.getNote());
			}

        	tResultSet = tStatement.executeQuery();
        	tResultSet.next();
        	
         	results = tResultSet.getInt("cnt");


   		} catch (SQLException e) {
			log.error("Exception in select() UsersDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in count() UsersDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a collection of all persisted dtos matching the search criteria supplied in
	 * the given dto.
	 * @param the dto containing the fields to select on using an AND operation
	 * @throws Exception
	 */		
	public Collection select(NoteDTO dto) throws ApplicationException {
		return (select(dto, 0, -1) ); 
	}

		
	/**
	 * returns a limited collection of persisted dtos matching the search criteria supplied in
	 * the given dto.  This is primarily used when the front end uses page breaks to display the result set.
	 * @param NoteDTO the dto containing the fields to select on using an AND operation
	 * @param startRecord the first record in the result set to return - use 0 for the first record
	 * @param returnSetSize the maximum number of records to return - use -1 to return all records
	 * @throws Exception
	 */		
	public Collection select(NoteDTO dto, int startRecord, int returnSetSize) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getNoteId() != null) {
												whereBuffer.append("and note_id = ? ");
									}
		if (dto.getObjectType() != null) {
												if (dto.getObjectType().length() > 0) {
						whereBuffer.append("and object_type like ? ");
					}
									}
		if (dto.getObjectId() != null) {
												whereBuffer.append("and object_id = ? ");
									}
		if (dto.getNote() != null) {
												if (dto.getNote().length() > 0) {
						whereBuffer.append("and note like ? ");
					}
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by "+PKCOLUMNS);
			} else {
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by "+PKCOLUMNS);
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getNoteId() != null) {	
				tStatement.setLong(count++, dto.getNoteId().longValue());	 		
			}
			if (dto.getObjectType() != null && dto.getObjectType().length() > 0) {
				tStatement.setString(count++, dto.getObjectType());
			}
			if (dto.getObjectId() != null) {	
				tStatement.setLong(count++, dto.getObjectId().longValue());	 		
			}
			if (dto.getNote() != null && dto.getNote().length() > 0) {
				tStatement.setString(count++, dto.getNote());
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() NoteDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() NoteDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}
	
	public Collection select(NoteDTO dto, int startRecord, int returnSetSize, String sort_col, String sort_dir) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getNoteId() != null) {
												whereBuffer.append("and note_id = ? ");
									}
		if (dto.getObjectType() != null) {
												whereBuffer.append("and object_type like ? ");
									}
		if (dto.getObjectId() != null) {
												whereBuffer.append("and object_id = ? ");
									}
		if (dto.getNote() != null) {
												whereBuffer.append("and note like ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3)+" order by "+PKCOLUMNS);
			} else {
			   if (sort_col != null) {
			      sqlStatement += ("order by " + sort_col + " " + sort_dir);
			   } else {
   			   	if (" ".equals(PKCOLUMNS))
   			      	sqlStatement += ("order by 1");
   			   	else
   			      	sqlStatement += (" order by "+PKCOLUMNS);
			   }
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getNoteId() != null) {	
				tStatement.setLong(count++, dto.getNoteId().longValue());	 		
			}
			if (dto.getObjectType() != null) {
				tStatement.setString(count++, dto.getObjectType());
			}
			if (dto.getObjectId() != null) {	
				tStatement.setLong(count++, dto.getObjectId().longValue());	 		
			}
			if (dto.getNote() != null) {
				tStatement.setString(count++, dto.getNote());
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() NoteDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() NoteDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a String of the next available PrimaryKey 
	 * @throws Exception
	 */		
	public String getNewPK() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select (max( note_id )+1) as PK from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);	
			tResultSet = tStatement.executeQuery();
			tResultSet.next();
			returnVal = tResultSet.getString("PK");
			if (returnVal == null) {
				returnVal = "1";
			}						
   		} catch (SQLException e) {
			log.error("Exception in getNewPK() NoteDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in getNewPK() NoteDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
	     }
   		 return returnVal;				
	}
	


	/**
	 * insert - Inserts a NoteDTO into the database.
	 * @param dto a dto populated with the values to insert.
	 * @param user the id of the user doing the inserting. 
	 * @throws ApplicationException
	 */
	public void insert(NoteDTO dto, String user) throws ApplicationException {		
		String sqlStatement = "insert into " + getTABLENAME() +" values (  ? ,  ? ,  ? ,  ?  )";
 
		Connection connection = null;
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
            tStatement =  connection.prepareStatement(sqlStatement);
            /*
            * Set the attributes in the query.
            */
		 	if (dto.getNoteId() != null) {
				tStatement.setLong(1, dto.getNoteId().longValue());	 
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getObjectType() != null) {
				tStatement.setString(2, dto.getObjectType());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getObjectId() != null) {
				tStatement.setLong(3, dto.getObjectId().longValue());	 
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getNote() != null) {
				tStatement.setString(4, dto.getNote());
			} else {
				tStatement.setString(4, null);
			}
        	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in insert() NoteDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in insert() NoteDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }   		
   		   close(connection);
   		}
	}




	/**
	 * update - Updates a NoteDTO in the database.
	 * @param dto the dto containing the updated information
	 * @param user the userid performing the update
	 * @throws ApplicationException
	 */
	public void update(NoteDTO pkdto, NoteDTO dto, String user) throws ApplicationException {
		String sqlStatement = "update " + getTABLENAME() +
			" SET  note_id = ?,  object_type = ?,  object_id = ?,  note = ? " +
			" where note_id = ?   ";

		Connection connection = null;
		PreparedStatement tStatement = null;		
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/
		 	if (dto.getNoteId() != null) {
				tStatement.setLong(1, dto.getNoteId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getObjectType() != null) {
				tStatement.setString(2, dto.getObjectType());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getObjectId() != null) {
				tStatement.setLong(3, dto.getObjectId().longValue());	 		
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getNote() != null) {
				tStatement.setString(4, dto.getNote());
			} else {
				tStatement.setString(4, null);
			}

			/*
			* Set the attribute(s) of the primary key for this DAO
			*/ 		
		    tStatement.setLong(5, pkdto.getNoteId().longValue());
        	   	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in update() NoteDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in update() NoteDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }
   		   close(connection);   		     
   		}
	} //end update



	/**
	 * delete - Deletes a NoteDTO from the database.
	 * @param the dto to delete
	 * @param user the userid performing the delete
	 * @throws ApplicationException
	 */
	public void delete(NoteDTO dto, String user) throws ApplicationException {
		String sqlStatement = "delete from" + getTABLENAME() + 
			" where  note_id = ? ";
		Connection connection =  null; 
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/


		   if (dto.getNoteId() != null) {
				tStatement.setLong(1, dto.getNoteId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}

        	tStatement.execute();
      } catch (SQLException e) {
	   log.error("Exception in delete() NoteDAO" + e.getMessage(), e);
   	   throw new ApplicationException("Exception in delete() NoteDAO" + e.getMessage());
      } finally {
   		 try {
   		    tStatement.close();
   		 } catch(Exception e) {
   		    //let it go.
   		 }      
         close(connection);
      }
      
	}


	/**
	 * create a dto representing the the current row in the result set.
	 * @param tResultSet the current result set
	 * @return a populated dto object
	 * @throws SQLException
	 */
  private NoteDTO populateDTO(ResultSet tResultSet) throws SQLException {
		long l;
		NoteDTO dto = new NoteDTO();
		l = tResultSet.getLong("note_id");
		if (!tResultSet.wasNull()){
			dto.setNoteId(new Long(l));
		}
		dto.setObjectType(tResultSet.getString("object_type"));
		l = tResultSet.getLong("object_id");
		if (!tResultSet.wasNull()){
			dto.setObjectId(new Long(l));
		}
		dto.setNote(tResultSet.getString("note"));
		return dto;
	}	
	
	public static String getTABLENAME() {
		
		// account for some DBs that allow spaces in table names
		String tableName = TABLENAME;
		if (TABLENAME.trim().indexOf(" ") > -1) {
			String innerStr = TABLENAME.trim();
			tableName = (char)34 + innerStr + (char)34;
		} else {
			tableName = TABLENAME;
		}
		return tableName;
		
	}	
	
}	