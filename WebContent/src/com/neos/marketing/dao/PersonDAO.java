package com.neos.marketing.dao;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.dto.*;
import com.neos.marketing.util.exception.*;


/**
 * Data access object for PersonDTO.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */
public class PersonDAO extends BaseDAO implements DataAccess {
 	private static final String TABLENAME = " person ";
	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public static Long personID = null;
    
	//All Columns to select from the database.
	private static String ALLCOLUMNS = " person_id,  first_name,  last_name,  title,  company_id ";
	private static String PKCOLUMNS = " person_id ";

	/** returns a count of the columsn **/
	public int count(PersonDTO dto) throws ApplicationException {
		String sqlStatement = "SELECT COUNT(*) AS cnt FROM" + getTABLENAME();		

		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getPersonId() != null) {
					whereBuffer.append("and person_id = ? ");
		}
		if (dto.getFirstName() != null) {
					whereBuffer.append("and first_name like ? ");
		}
		if (dto.getLastName() != null) {
					whereBuffer.append("and last_name like ? ");
		}
		if (dto.getTitle() != null) {
					whereBuffer.append("and title like ? ");
		}
		if (dto.getCompanyId() != null) {
					whereBuffer.append("and company_id = ? ");
		}

		int results = 0;
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			}
			
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	int count = 1;
         	
			if (dto.getPersonId() != null) {	
				tStatement.setLong(count++, dto.getPersonId().longValue());	 		
			}
			if (dto.getFirstName() != null) {
				tStatement.setString(count++, dto.getFirstName());
			}
			if (dto.getLastName() != null) {
				tStatement.setString(count++, dto.getLastName());
			}
			if (dto.getTitle() != null) {
				tStatement.setString(count++, dto.getTitle());
			}
			if (dto.getCompanyId() != null) {	
				tStatement.setLong(count++, dto.getCompanyId().longValue());	 		
			}

        	tResultSet = tStatement.executeQuery();
        	tResultSet.next();
        	
         	results = tResultSet.getInt("cnt");


   		} catch (SQLException e) {
			log.error("Exception in select() UsersDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in count() UsersDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a collection of all persisted dtos matching the search criteria supplied in
	 * the given dto.
	 * @param the dto containing the fields to select on using an AND operation
	 * @throws Exception
	 */		
	public Collection select(PersonDTO dto) throws ApplicationException {
		return (select(dto, 0, -1) ); 
	}

		
	/**
	 * returns a limited collection of persisted dtos matching the search criteria supplied in
	 * the given dto.  This is primarily used when the front end uses page breaks to display the result set.
	 * @param PersonDTO the dto containing the fields to select on using an AND operation
	 * @param startRecord the first record in the result set to return - use 0 for the first record
	 * @param returnSetSize the maximum number of records to return - use -1 to return all records
	 * @throws Exception
	 */		
	public Collection select(PersonDTO dto, int startRecord, int returnSetSize) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getPersonId() != null) {
												whereBuffer.append("and person_id = ? ");
									}
		if (dto.getFirstName() != null) {
												if (dto.getFirstName().length() > 0) {
						whereBuffer.append("and first_name like ? ");
					}
									}
		if (dto.getLastName() != null) {
												if (dto.getLastName().length() > 0) {
						whereBuffer.append("and last_name like ? ");
					}
									}
		if (dto.getTitle() != null) {
												if (dto.getTitle().length() > 0) {
						whereBuffer.append("and title like ? ");
					}
									}
		if (dto.getCompanyId() != null) {
												whereBuffer.append("and company_id = ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by last_name asc");
			} else {
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by last_name asc");
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getPersonId() != null) {	
				tStatement.setLong(count++, dto.getPersonId().longValue());	 		
			}
			if (dto.getFirstName() != null && dto.getFirstName().length() > 0) {
				tStatement.setString(count++, dto.getFirstName());
			}
			if (dto.getLastName() != null && dto.getLastName().length() > 0) {
				tStatement.setString(count++, dto.getLastName());
			}
			if (dto.getTitle() != null && dto.getTitle().length() > 0) {
				tStatement.setString(count++, dto.getTitle());
			}
			if (dto.getCompanyId() != null) {	
				tStatement.setLong(count++, dto.getCompanyId().longValue());	 		
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() PersonDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() PersonDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}
	
	public Collection select(PersonDTO dto, int startRecord, int returnSetSize, String sort_col, String sort_dir) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getPersonId() != null) {
												whereBuffer.append("and person_id = ? ");
									}
		if (dto.getFirstName() != null) {
												whereBuffer.append("and first_name like ? ");
									}
		if (dto.getLastName() != null) {
												whereBuffer.append("and last_name like ? ");
									}
		if (dto.getTitle() != null) {
												whereBuffer.append("and title like ? ");
									}
		if (dto.getCompanyId() != null) {
												whereBuffer.append("and company_id = ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3)+" order by last_name asc");
			} else {
			   if (sort_col != null) {
			      sqlStatement += ("order by " + sort_col + " " + sort_dir);
			   } else {
   			   	if (" ".equals(PKCOLUMNS))
   			      	sqlStatement += ("order by 1");
   			   	else
   			      	sqlStatement += (" order by last_name asc");
			   }
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getPersonId() != null) {	
				tStatement.setLong(count++, dto.getPersonId().longValue());	 		
			}
			if (dto.getFirstName() != null) {
				tStatement.setString(count++, dto.getFirstName());
			}
			if (dto.getLastName() != null) {
				tStatement.setString(count++, dto.getLastName());
			}
			if (dto.getTitle() != null) {
				tStatement.setString(count++, dto.getTitle());
			}
			if (dto.getCompanyId() != null) {	
				tStatement.setLong(count++, dto.getCompanyId().longValue());	 		
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() PersonDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() PersonDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a String of the next available PrimaryKey 
	 * @throws Exception
	 */		
	public String getNewPK() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select (max( person_id )+1) as PK from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);	
			tResultSet = tStatement.executeQuery();
			tResultSet.next();
			returnVal = tResultSet.getString("PK");
			personID = Long.parseLong(returnVal); //This would be used to generate person ID for entity and email tables
			if (returnVal == null) {
				returnVal = "1";
			}
				
   		} catch (SQLException e) {
			log.error("Exception in getNewPK() PersonDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in getNewPK() PersonDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
	     }
   		 return returnVal;				
	}
	
	//Export to CSV	
	public void exporttoCSV() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select person_id as pd, first_name as fn, last_name as ln, title as tl, company_id as cd from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;
		
		HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("text/csv;charset=UTF-8");
		res.setHeader("Content-disposition",  "attachment; filename=Person.csv"); //attachment ensures it saved by browser
		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);
			tResultSet = tStatement.executeQuery();
			try{	
					PrintWriter fw = res.getWriter();
					fw.append("PERSON ID");
					fw.append(',');
					fw.append("FIRST NAME");
					fw.append(',');
					fw.append("LAST NAME");
					fw.append(',');
					fw.append("TITLE");
					fw.append(',');
					fw.append("COMPANY ID");
					fw.append('\n');
					while(tResultSet.next()){
						fw.append(tResultSet.getString("pd"));
						fw.append(',');
						fw.append(tResultSet.getString("fn"));
						fw.append(',');
						fw.append(tResultSet.getString("ln"));
						fw.append(',');
						fw.append(tResultSet.getString("tl"));
						fw.append(',');
						fw.append(tResultSet.getString("cd"));
						fw.append('\n');
					}
					fw.flush();
					fw.close();
			}
			catch(IOException e){
				System.out.println("Trouble writing to file");
			}
					
   		} 
		catch (SQLException e) {
			log.error("Exception in exporttoCSV: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in exporttoCSV: " + e.getMessage());
      	} finally {     	
   			try {
   		    	tStatement.close();
			} 
			catch(Exception e) {
   		 	}      
         	close(connection);
	     }			
	}
	


	/**
	 * insert - Inserts a PersonDTO into the database.
	 * @param dto a dto populated with the values to insert.
	 * @param user the id of the user doing the inserting. 
	 * @throws ApplicationException
	 */
	public void insert(PersonDTO dto, String user) throws ApplicationException {		
		String sqlStatement = "insert into " + getTABLENAME() +" values (  ? ,  ? ,  ? ,  ? ,  ?  )";
 
		Connection connection = null;
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
            tStatement =  connection.prepareStatement(sqlStatement);
            /*
            * Set the attributes in the query.
            */
		 	if (dto.getPersonId() != null) {
				tStatement.setLong(1, dto.getPersonId().longValue());	 
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getFirstName() != null) {
				tStatement.setString(2, dto.getFirstName());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getLastName() != null) {
				tStatement.setString(3, dto.getLastName());
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getTitle() != null) {
				tStatement.setString(4, dto.getTitle());
			} else {
				tStatement.setString(4, null);
			}
		 	if (dto.getCompanyId() != null) {
				tStatement.setLong(5, dto.getCompanyId().longValue());	 
			} else {
				tStatement.setString(5, null);
			}
        	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in insert() PersonDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in insert() PersonDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }   		
   		   close(connection);
   		}
	}

    


	/**
	 * update - Updates a PersonDTO in the database.
	 * @param dto the dto containing the updated information
	 * @param user the userid performing the update
	 * @throws ApplicationException
	 */
	public void update(PersonDTO pkdto, PersonDTO dto, String user) throws ApplicationException {
		String sqlStatement = "update " + getTABLENAME() +
			" SET  person_id = ?,  first_name = ?,  last_name = ?,  title = ?,  company_id = ? " +
			" where person_id = ?   ";

		Connection connection = null;
		PreparedStatement tStatement = null;		
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/
		 	if (dto.getPersonId() != null) {
				tStatement.setLong(1, dto.getPersonId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getFirstName() != null) {
				tStatement.setString(2, dto.getFirstName());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getLastName() != null) {
				tStatement.setString(3, dto.getLastName());
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getTitle() != null) {
				tStatement.setString(4, dto.getTitle());
			} else {
				tStatement.setString(4, null);
			}
		 	if (dto.getCompanyId() != null) {
				tStatement.setLong(5, dto.getCompanyId().longValue());	 		
			} else {
				tStatement.setString(5, null);
			}

			/*
			* Set the attribute(s) of the primary key for this DAO
			*/ 		
		    tStatement.setLong(6, pkdto.getPersonId().longValue());
        	   	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in update() PersonDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in update() PersonDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }
   		   close(connection);   		     
   		}
	} //end update



	/**
	 * delete - Deletes a PersonDTO from the database.
	 * @param the dto to delete
	 * @param user the userid performing the delete
	 * @throws ApplicationException
	 */
	public void delete(PersonDTO dto, String user) throws ApplicationException {
		String sqlStatement = "delete from" + getTABLENAME() + 
			" where  person_id = ? ";
		Connection connection =  null; 
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/


		   if (dto.getPersonId() != null) {
				tStatement.setLong(1, dto.getPersonId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}

        	tStatement.execute();
      } catch (SQLException e) {
	   log.error("Exception in delete() PersonDAO" + e.getMessage(), e);
   	   throw new ApplicationException("Exception in delete() PersonDAO" + e.getMessage());
      } finally {
   		 try {
   		    tStatement.close();
   		 } catch(Exception e) {
   		    //let it go.
   		 }      
         close(connection);
      }
      
	}


	/**
	 * create a dto representing the the current row in the result set.
	 * @param tResultSet the current result set
	 * @return a populated dto object
	 * @throws SQLException
	 */
  private PersonDTO populateDTO(ResultSet tResultSet) throws SQLException {
		long l;
		PersonDTO dto = new PersonDTO();
		l = tResultSet.getLong("person_id");
		if (!tResultSet.wasNull()){
			dto.setPersonId(new Long(l));
		}
		dto.setFirstName(tResultSet.getString("first_name"));
		dto.setLastName(tResultSet.getString("last_name"));
		dto.setTitle(tResultSet.getString("title"));
		l = tResultSet.getLong("company_id");
		if (!tResultSet.wasNull()){
			dto.setCompanyId(new Long(l));
		}
		return dto;
	}	
	
	public static String getTABLENAME() {
		
		// account for some DBs that allow spaces in table names
		String tableName = TABLENAME;
		if (TABLENAME.trim().indexOf(" ") > -1) {
			String innerStr = TABLENAME.trim();
			tableName = (char)34 + innerStr + (char)34;
		} else {
			tableName = TABLENAME;
		}
		return tableName;
		
	}	
	
}	