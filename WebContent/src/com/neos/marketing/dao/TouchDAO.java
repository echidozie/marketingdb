package com.neos.marketing.dao;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.dto.*;
import com.neos.marketing.util.DateConverter;
import com.neos.marketing.util.exception.*;


/**
 * Data access object for TouchDTO.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */
public class TouchDAO extends BaseDAO implements DataAccess {

 	private static final String TABLENAME = " touch ";
	private Log log = LogFactory.getLog(this.getClass().getName());
    
	//All Columns to select from the database.
	private static String ALLCOLUMNS = " touch_id,  entity_id,  touch_date,  source_id,  detail_id ";
	private static String PKCOLUMNS = " touch_id ";

	/** returns a count of the columsn **/
	public int count(TouchDTO dto) throws ApplicationException {
		String sqlStatement = "SELECT COUNT(*) AS cnt FROM" + getTABLENAME();		

		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getTouchId() != null) {
					whereBuffer.append("and touch_id = ? ");
		}
		if (dto.getEntityId() != null) {
					whereBuffer.append("and entity_id = ? ");
		}
		if (dto.getTouchDate() != null) {
					whereBuffer.append("and touch_date = ? ");
		}
		if (dto.getSourceId() != null) {
					whereBuffer.append("and source_id = ? ");
		}
		if (dto.getDetailId() != null) {
					whereBuffer.append("and detail_id = ? ");
		}

		int results = 0;
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			}
			
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	int count = 1;
         	
			if (dto.getTouchId() != null) {	
				tStatement.setLong(count++, dto.getTouchId().longValue());	 		
			}
			if (dto.getEntityId() != null) {	
				tStatement.setLong(count++, dto.getEntityId().longValue());	 		
			}
			if (dto.getTouchDate() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTouchDate()));	  				
			}
			if (dto.getSourceId() != null) {	
				tStatement.setLong(count++, dto.getSourceId().longValue());	 		
			}
			if (dto.getDetailId() != null) {	
				tStatement.setLong(count++, dto.getDetailId().longValue());	 		
			}

        	tResultSet = tStatement.executeQuery();
        	tResultSet.next();
        	
         	results = tResultSet.getInt("cnt");


   		} catch (SQLException e) {
			log.error("Exception in select() UsersDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in count() UsersDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a collection of all persisted dtos matching the search criteria supplied in
	 * the given dto.
	 * @param the dto containing the fields to select on using an AND operation
	 * @throws Exception
	 */		
	public Collection select(TouchDTO dto) throws ApplicationException {
		return (select(dto, 0, -1) ); 
	}

		
	/**
	 * returns a limited collection of persisted dtos matching the search criteria supplied in
	 * the given dto.  This is primarily used when the front end uses page breaks to display the result set.
	 * @param TouchDTO the dto containing the fields to select on using an AND operation
	 * @param startRecord the first record in the result set to return - use 0 for the first record
	 * @param returnSetSize the maximum number of records to return - use -1 to return all records
	 * @throws Exception
	 */		
	public Collection select(TouchDTO dto, int startRecord, int returnSetSize) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getTouchId() != null) {
												whereBuffer.append("and touch_id = ? ");
									}
		if (dto.getEntityId() != null) {
												whereBuffer.append("and entity_id = ? ");
									}
		if (dto.getTouchDate() != null) {
												whereBuffer.append("and touch_date = ? ");
									}
		if (dto.getSourceId() != null) {
												whereBuffer.append("and source_id = ? ");
									}
		if (dto.getDetailId() != null) {
												whereBuffer.append("and detail_id = ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by "+PKCOLUMNS);
			} else {
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by "+PKCOLUMNS);
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getTouchId() != null) {	
				tStatement.setLong(count++, dto.getTouchId().longValue());	 		
			}
			if (dto.getEntityId() != null) {	
				tStatement.setLong(count++, dto.getEntityId().longValue());	 		
			}
			if (dto.getTouchDate() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTouchDate()));	  				
			}
			if (dto.getSourceId() != null) {	
				tStatement.setLong(count++, dto.getSourceId().longValue());	 		
			}
			if (dto.getDetailId() != null) {	
				tStatement.setLong(count++, dto.getDetailId().longValue());	 		
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() TouchDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() TouchDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}
	
	public Collection select(TouchDTO dto, int startRecord, int returnSetSize, String sort_col, String sort_dir) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getTouchId() != null) {
												whereBuffer.append("and touch_id = ? ");
									}
		if (dto.getEntityId() != null) {
												whereBuffer.append("and entity_id = ? ");
									}
		if (dto.getTouchDate() != null) {
												whereBuffer.append("and touch_date = ? ");
									}
		if (dto.getSourceId() != null) {
												whereBuffer.append("and source_id = ? ");
									}
		if (dto.getDetailId() != null) {
												whereBuffer.append("and detail_id = ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3)+" order by "+PKCOLUMNS);
			} else {
			   if (sort_col != null) {
			      sqlStatement += ("order by " + sort_col + " " + sort_dir);
			   } else {
   			   	if (" ".equals(PKCOLUMNS))
   			      	sqlStatement += ("order by 1");
   			   	else
   			      	sqlStatement += (" order by "+PKCOLUMNS);
			   }
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getTouchId() != null) {	
				tStatement.setLong(count++, dto.getTouchId().longValue());	 		
			}
			if (dto.getEntityId() != null) {	
				tStatement.setLong(count++, dto.getEntityId().longValue());	 		
			}
			if (dto.getTouchDate() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTouchDate()));	  				
			}
			if (dto.getSourceId() != null) {	
				tStatement.setLong(count++, dto.getSourceId().longValue());	 		
			}
			if (dto.getDetailId() != null) {	
				tStatement.setLong(count++, dto.getDetailId().longValue());	 		
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() TouchDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() TouchDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a String of the next available PrimaryKey 
	 * @throws Exception
	 */		
	public String getNewPK() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select (max( touch_id )+1) as PK from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);	
			tResultSet = tStatement.executeQuery();
			tResultSet.next();
			returnVal = tResultSet.getString("PK");
			if (returnVal == null) {
				returnVal = "1";
			}						
   		} catch (SQLException e) {
			log.error("Exception in getNewPK() TouchDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in getNewPK() TouchDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
	     }
   		 return returnVal;				
	}
	
	//Export to CSV
	public void exporttoCSV() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("SELECT touch.touch_id AS id, person.first_name AS fn, person.last_name AS ln, touch.touch_date AS td, source.source AS sd, detail.detail AS dd FROM touch INNER JOIN entity on touch.entity_id = entity.entity_id INNER JOIN person on entity.person_id = person.person_id INNER JOIN source on touch.source_id = source.source_id INNER JOIN detail on touch.detail_id = detail.detail_id");	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;
		
		HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("text/csv;charset=UTF-8");
		res.setHeader("Content-disposition",  "attachment; filename=Touch.csv"); //attachment ensures it saved by browser
		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);
			tResultSet = tStatement.executeQuery();
			try{	
					PrintWriter fw = res.getWriter();
					fw.append("TOUCH ID");
					fw.append(',');
					fw.append("FIRST NAME");
					fw.append(',');
					fw.append("LAST NAME");
					fw.append(',');
					fw.append("TOUCH DATE");
					fw.append(',');
					fw.append("SOURCE");
					fw.append(',');
					fw.append("DETAIL");
					fw.append('\n');
					while(tResultSet.next()){
						fw.append(tResultSet.getString("id"));
						fw.append(',');
						fw.append(tResultSet.getString("fn"));
						fw.append(',');
						fw.append(tResultSet.getString("ln"));
						fw.append(',');
						fw.append(tResultSet.getString("td"));
						fw.append(',');
						fw.append(tResultSet.getString("sd"));
						fw.append(',');
						fw.append(tResultSet.getString("dd"));
						fw.append('\n');
					}
					fw.flush();
					fw.close();
			}
			catch(IOException e){
				System.out.println("Trouble writing to file");
			}
					
   		} 
		catch (SQLException e) {
			log.error("Exception in exporttoCSV: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in exporttoCSV: " + e.getMessage());
      	} finally {     	
   			try {
   		    	tStatement.close();
			} 
			catch(Exception e) {
   		 	}      
         	close(connection);
	     }			
	}

	/**
	 * insert - Inserts a TouchDTO into the database.
	 * @param dto a dto populated with the values to insert.
	 * @param user the id of the user doing the inserting. 
	 * @throws ApplicationException
	 */
	public void insert(TouchDTO dto, String user) throws ApplicationException {		
		String sqlStatement = "insert into " + getTABLENAME() +" values (  ? ,  ? ,  ? ,  ? ,  ?  )";
 
		Connection connection = null;
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
            tStatement =  connection.prepareStatement(sqlStatement);
            /*
            * Set the attributes in the query.
            */
		 	if (dto.getTouchId() != null) {
				tStatement.setLong(1, dto.getTouchId().longValue());	 
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getEntityId() != null) {
				tStatement.setLong(2, dto.getEntityId().longValue());	 
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getTouchDate() != null) {
				tStatement.setDate(3, DateConverter.convertDateToSqlDate(dto.getTouchDate()));
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getSourceId() != null) {
				tStatement.setLong(4, dto.getSourceId().longValue());	 
			} else {
				tStatement.setString(4, null);
			}
		 	if (dto.getDetailId() != null) {
				tStatement.setLong(5, dto.getDetailId().longValue());	 
			} else {
				tStatement.setString(5, null);
			}
        	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in insert() TouchDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in insert() TouchDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }   		
   		   close(connection);
   		}
	}




	/**
	 * update - Updates a TouchDTO in the database.
	 * @param dto the dto containing the updated information
	 * @param user the userid performing the update
	 * @throws ApplicationException
	 */
	public void update(TouchDTO pkdto, TouchDTO dto, String user) throws ApplicationException {
		String sqlStatement = "update " + getTABLENAME() +
			" SET  touch_id = ?,  entity_id = ?,  touch_date = ?,  source_id = ?,  detail_id = ? " +
			" where touch_id = ?   ";

		Connection connection = null;
		PreparedStatement tStatement = null;		
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/
		 	if (dto.getTouchId() != null) {
				tStatement.setLong(1, dto.getTouchId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getEntityId() != null) {
				tStatement.setLong(2, dto.getEntityId().longValue());	 		
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getTouchDate() != null) {
				tStatement.setDate(3, DateConverter.convertDateToSqlDate(dto.getTouchDate()));
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getSourceId() != null) {
				tStatement.setLong(4, dto.getSourceId().longValue());	 		
			} else {
				tStatement.setString(4, null);
			}
		 	if (dto.getDetailId() != null) {
				tStatement.setLong(5, dto.getDetailId().longValue());	 		
			} else {
				tStatement.setString(5, null);
			}

			/*
			* Set the attribute(s) of the primary key for this DAO
			*/ 		
		    tStatement.setLong(6, pkdto.getTouchId().longValue());
        	   	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in update() TouchDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in update() TouchDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }
   		   close(connection);   		     
   		}
	} //end update



	/**
	 * delete - Deletes a TouchDTO from the database.
	 * @param the dto to delete
	 * @param user the userid performing the delete
	 * @throws ApplicationException
	 */
	public void delete(TouchDTO dto, String user) throws ApplicationException {
		String sqlStatement = "delete from" + getTABLENAME() + 
			" where  touch_id = ? ";
		Connection connection =  null; 
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/


		   if (dto.getTouchId() != null) {
				tStatement.setLong(1, dto.getTouchId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}

        	tStatement.execute();
      } catch (SQLException e) {
	   log.error("Exception in delete() TouchDAO" + e.getMessage(), e);
   	   throw new ApplicationException("Exception in delete() TouchDAO" + e.getMessage());
      } finally {
   		 try {
   		    tStatement.close();
   		 } catch(Exception e) {
   		    //let it go.
   		 }      
         close(connection);
      }
      
	}


	/**
	 * create a dto representing the the current row in the result set.
	 * @param tResultSet the current result set
	 * @return a populated dto object
	 * @throws SQLException
	 */
  private TouchDTO populateDTO(ResultSet tResultSet) throws SQLException {
		long l;
		TouchDTO dto = new TouchDTO();
		l = tResultSet.getLong("touch_id");
		if (!tResultSet.wasNull()){
			dto.setTouchId(new Long(l));
		}
		l = tResultSet.getLong("entity_id");
		if (!tResultSet.wasNull()){
			dto.setEntityId(new Long(l));
		}
		if (tResultSet.getTimestamp("touch_date") != null){
			dto.setTouchDate(DateConverter.convertTimestampToSqlDate(tResultSet.getTimestamp("touch_date")));
		}	
		l = tResultSet.getLong("source_id");
		if (!tResultSet.wasNull()){
			dto.setSourceId(new Long(l));
		}
		l = tResultSet.getLong("detail_id");
		if (!tResultSet.wasNull()){
			dto.setDetailId(new Long(l));
		}
		return dto;
	}	
	
	public static String getTABLENAME() {
		
		// account for some DBs that allow spaces in table names
		String tableName = TABLENAME;
		if (TABLENAME.trim().indexOf(" ") > -1) {
			String innerStr = TABLENAME.trim();
			tableName = (char)34 + innerStr + (char)34;
		} else {
			tableName = TABLENAME;
		}
		return tableName;
		
	}	
	
}	