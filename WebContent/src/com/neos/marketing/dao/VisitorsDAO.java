package com.neos.marketing.dao;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.dto.*;
import com.neos.marketing.util.DateConverter;
import com.neos.marketing.util.exception.*;


/**
 * Data access object for VisitorsDTO.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */
public class VisitorsDAO extends BaseDAO implements DataAccess {

 	private static final String TABLENAME = " visitors ";
	private Log log = LogFactory.getLog(this.getClass().getName());
    
	//All Columns to select from the database.
	private static String ALLCOLUMNS = " id,  ip_address,  hostname,  page_visited,  referrer,  time_requested,  timestamp,  os,  browser ";
	private static String PKCOLUMNS = " id ";

	/** returns a count of the columsn **/
	public int count(VisitorsDTO dto) throws ApplicationException {
		String sqlStatement = "SELECT COUNT(*) AS cnt FROM" + getTABLENAME();		

		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getId() != null) {
					whereBuffer.append("and id = ? ");
		}
		if (dto.getIpAddress() != null) {
					whereBuffer.append("and ip_address like ? ");
		}
		if (dto.getHostname() != null) {
					whereBuffer.append("and hostname like ? ");
		}
		if (dto.getPageVisited() != null) {
					whereBuffer.append("and page_visited like ? ");
		}
		if (dto.getReferrer() != null) {
					whereBuffer.append("and referrer like ? ");
		}
		if (dto.getTimeRequested() != null) {
					whereBuffer.append("and time_requested like ? ");
		}
		if (dto.getTimestamp() != null) {
					whereBuffer.append("and timestamp = ? ");
		}
		if (dto.getOs() != null) {
					whereBuffer.append("and os like ? ");
		}
		if (dto.getBrowser() != null) {
					whereBuffer.append("and browser like ? ");
		}

		int results = 0;
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			}
			
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	int count = 1;
         	
			if (dto.getId() != null) {	
				tStatement.setLong(count++, dto.getId().longValue());	 		
			}
			if (dto.getIpAddress() != null) {
				tStatement.setString(count++, dto.getIpAddress());
			}
			if (dto.getHostname() != null) {
				tStatement.setString(count++, dto.getHostname());
			}
			if (dto.getPageVisited() != null) {
				tStatement.setString(count++, dto.getPageVisited());
			}
			if (dto.getReferrer() != null) {
				tStatement.setString(count++, dto.getReferrer());
			}
			if (dto.getTimeRequested() != null) {
				tStatement.setString(count++, dto.getTimeRequested());
			}
			if (dto.getTimestamp() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTimestamp()));	  				
			}
			if (dto.getOs() != null) {
				tStatement.setString(count++, dto.getOs());
			}
			if (dto.getBrowser() != null) {
				tStatement.setString(count++, dto.getBrowser());
			}

        	tResultSet = tStatement.executeQuery();
        	tResultSet.next();
        	
         	results = tResultSet.getInt("cnt");


   		} catch (SQLException e) {
			log.error("Exception in select() UsersDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in count() UsersDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a collection of all persisted dtos matching the search criteria supplied in
	 * the given dto.
	 * @param the dto containing the fields to select on using an AND operation
	 * @throws Exception
	 */		
	public Collection select(VisitorsDTO dto) throws ApplicationException {
		return (select(dto, 0, -1) ); 
	}

		
	/**
	 * returns a limited collection of persisted dtos matching the search criteria supplied in
	 * the given dto.  This is primarily used when the front end uses page breaks to display the result set.
	 * @param VisitorsDTO the dto containing the fields to select on using an AND operation
	 * @param startRecord the first record in the result set to return - use 0 for the first record
	 * @param returnSetSize the maximum number of records to return - use -1 to return all records
	 * @throws Exception
	 */		
	public Collection select(VisitorsDTO dto, int startRecord, int returnSetSize) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getId() != null) {
												whereBuffer.append("and id = ? ");
									}
		if (dto.getIpAddress() != null) {
												if (dto.getIpAddress().length() > 0) {
						whereBuffer.append("and ip_address like ? ");
					}
									}
		if (dto.getHostname() != null) {
												if (dto.getHostname().length() > 0) {
						whereBuffer.append("and hostname like ? ");
					}
									}
		if (dto.getPageVisited() != null) {
												if (dto.getPageVisited().length() > 0) {
						whereBuffer.append("and page_visited like ? ");
					}
									}
		if (dto.getReferrer() != null) {
												if (dto.getReferrer().length() > 0) {
						whereBuffer.append("and referrer like ? ");
					}
									}
		if (dto.getTimeRequested() != null) {
												if (dto.getTimeRequested().length() > 0) {
						whereBuffer.append("and time_requested like ? ");
					}
									}
		if (dto.getTimestamp() != null) {
												whereBuffer.append("and timestamp = ? ");
									}
		if (dto.getOs() != null) {
												if (dto.getOs().length() > 0) {
						whereBuffer.append("and os like ? ");
					}
									}
		if (dto.getBrowser() != null) {
												if (dto.getBrowser().length() > 0) {
						whereBuffer.append("and browser like ? ");
					}
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3));
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by timestamp desc");
			} else {
			   if (" ".equals(PKCOLUMNS))
			      sqlStatement += ("order by 1");
			   else
			      sqlStatement += (" order by timestamp desc");
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getId() != null) {	
				tStatement.setLong(count++, dto.getId().longValue());	 		
			}
			if (dto.getIpAddress() != null && dto.getIpAddress().length() > 0) {
				tStatement.setString(count++, dto.getIpAddress());
			}
			if (dto.getHostname() != null && dto.getHostname().length() > 0) {
				tStatement.setString(count++, dto.getHostname());
			}
			if (dto.getPageVisited() != null && dto.getPageVisited().length() > 0) {
				tStatement.setString(count++, dto.getPageVisited());
			}
			if (dto.getReferrer() != null && dto.getReferrer().length() > 0) {
				tStatement.setString(count++, dto.getReferrer());
			}
			if (dto.getTimeRequested() != null && dto.getTimeRequested().length() > 0) {
				tStatement.setString(count++, dto.getTimeRequested());
			}
			if (dto.getTimestamp() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTimestamp()));	  				
			}
			if (dto.getOs() != null && dto.getOs().length() > 0) {
				tStatement.setString(count++, dto.getOs());
			}
			if (dto.getBrowser() != null && dto.getBrowser().length() > 0) {
				tStatement.setString(count++, dto.getBrowser());
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() VisitorsDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() VisitorsDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}
	
	public Collection select(VisitorsDTO dto, int startRecord, int returnSetSize, String sort_col, String sort_dir) throws ApplicationException {
		String sqlStatement = "select "+ALLCOLUMNS+" from " + getTABLENAME();		
		StringBuffer whereBuffer = new StringBuffer("");
		Connection connection = null;		
		if (dto.getId() != null) {
												whereBuffer.append("and id = ? ");
									}
		if (dto.getIpAddress() != null) {
												whereBuffer.append("and ip_address like ? ");
									}
		if (dto.getHostname() != null) {
												whereBuffer.append("and hostname like ? ");
									}
		if (dto.getPageVisited() != null) {
												whereBuffer.append("and page_visited like ? ");
									}
		if (dto.getReferrer() != null) {
												whereBuffer.append("and referrer like ? ");
									}
		if (dto.getTimeRequested() != null) {
												whereBuffer.append("and time_requested like ? ");
									}
		if (dto.getTimestamp() != null) {
												whereBuffer.append("and timestamp = ? ");
									}
		if (dto.getOs() != null) {
												whereBuffer.append("and os like ? ");
									}
		if (dto.getBrowser() != null) {
												whereBuffer.append("and browser like ? ");
									}
		Collection results = new ArrayList();
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			if (whereBuffer.length() > 0) {
			   sqlStatement += ("where "+whereBuffer.toString().substring(3)+" order by timestamp desc");
			} else {
			   if (sort_col != null) {
			      sqlStatement += ("order by " + sort_col + " " + sort_dir);
			   } else {
   			   	if (" ".equals(PKCOLUMNS))
   			      	sqlStatement += ("order by 1");
   			   	else
   			      	sqlStatement += (" order by timestamp desc");
			   }
			}
        	tStatement =  connection.prepareStatement(sqlStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
         	/*
         	* Set the attributes in the query. 
         	*/
         	int count = 1;
			if (dto.getId() != null) {	
				tStatement.setLong(count++, dto.getId().longValue());	 		
			}
			if (dto.getIpAddress() != null) {
				tStatement.setString(count++, dto.getIpAddress());
			}
			if (dto.getHostname() != null) {
				tStatement.setString(count++, dto.getHostname());
			}
			if (dto.getPageVisited() != null) {
				tStatement.setString(count++, dto.getPageVisited());
			}
			if (dto.getReferrer() != null) {
				tStatement.setString(count++, dto.getReferrer());
			}
			if (dto.getTimeRequested() != null) {
				tStatement.setString(count++, dto.getTimeRequested());
			}
			if (dto.getTimestamp() != null) {	
				tStatement.setDate(count++, DateConverter.convertDateToSqlDate(dto.getTimestamp()));	  				
			}
			if (dto.getOs() != null) {
				tStatement.setString(count++, dto.getOs());
			}
			if (dto.getBrowser() != null) {
				tStatement.setString(count++, dto.getBrowser());
			}

         	tResultSet = tStatement.executeQuery();   
         	if (returnSetSize == -1) { 						//return all records
     			while (tResultSet.next() ) {
            		results.add(populateDTO(tResultSet));
         		}
         	} else { 										//return a block of records
         		if (startRecord == 0) {
         			tResultSet.beforeFirst();
         		} else  {
         			tResultSet.absolute(startRecord);
         		}    		
   				for (int i=0; (i < returnSetSize && tResultSet.next()) ; i++) {
	            	results.add(populateDTO(tResultSet));
	         	}
	         }

   		} catch (SQLException e) {
			log.error("Exception in select() VisitorsDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in select() VisitorsDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
      }
   		return results;							
	}

	/**
	 * returns a String of the next available PrimaryKey 
	 * @throws Exception
	 */		
	public String getNewPK() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select (max( id )+1) as PK from " + getTABLENAME());	
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);	
			tResultSet = tStatement.executeQuery();
			tResultSet.next();
			returnVal = tResultSet.getString("PK");
			if (returnVal == null) {
				returnVal = "1";
			}						
   		} catch (SQLException e) {
			log.error("Exception in getNewPK() VisitorsDAO: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in getNewPK() VisitorsDAO: " + e.getMessage());
      	} finally {
   			try {
   		    	tResultSet.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      	
   			try {
   		    	tStatement.close();
			} catch(Exception e) {
   		    	//let it go.
   		 	}      
         	close(connection);
	     }
   		 return returnVal;				
	}
	

	//Export to CSV
	public void exporttoCSV() throws ApplicationException {
		String returnVal = "";
		String sqlStatement = "";
		sqlStatement = ("select id as id, ip_address as ip, hostname as hn, page_visited as pv, referrer as rr, timestamp as tp, os as os from " + getTABLENAME());		
		Connection connection = null;	
		PreparedStatement tStatement = null;
		ResultSet tResultSet = null;
		
		HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("text/csv;charset=UTF-8");
		res.setHeader("Content-disposition",  "attachment; filename=Visitors.csv"); //attachment ensures it saved by browser
		
		try {
			connection = this.getConnection();
			tStatement =  connection.prepareStatement(sqlStatement);
			tResultSet = tStatement.executeQuery();
			try{	
					PrintWriter fw = res.getWriter();
					fw.append("ID");
					fw.append(',');
					fw.append("IP ADDRESS");
					fw.append(',');
					fw.append("HOSTNAME");
					fw.append(',');
					fw.append("PAGE VISITED");
					fw.append(',');
					fw.append("REFERRER");
					fw.append(',');
					fw.append("TIMESTAMP");
					fw.append(',');
					fw.append("OS");
					fw.append('\n');
					while(tResultSet.next()){
						fw.append(tResultSet.getString("id"));
						fw.append(',');
						fw.append(tResultSet.getString("ip"));
						fw.append(',');
						fw.append(tResultSet.getString("hn"));
						fw.append(',');
						fw.append(tResultSet.getString("pv"));
						fw.append(',');
						fw.append(tResultSet.getString("rr"));
						fw.append(',');
						fw.append(tResultSet.getString("tp"));
						fw.append(',');
						fw.append(tResultSet.getString("os"));
						fw.append('\n');
					}
					fw.flush();
					fw.close();
			}
			catch(IOException e){
				System.out.println("Trouble writing to file");
			}
					
   		} 
		catch (SQLException e) {
			log.error("Exception in exporttoCSV: " + e.getMessage(), e);
   			throw new ApplicationException("Exception in exporttoCSV: " + e.getMessage());
      	} finally {     	
   			try {
   		    	tStatement.close();
			} 
			catch(Exception e) {
   		 	}      
         	close(connection);
	     }			
	}


	/**
	 * insert - Inserts a VisitorsDTO into the database.
	 * @param dto a dto populated with the values to insert.
	 * @param user the id of the user doing the inserting. 
	 * @throws ApplicationException
	 */
	public void insert(VisitorsDTO dto, String user) throws ApplicationException {		
		String sqlStatement = "insert into " + getTABLENAME() +" values (  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ?  )";
 
		Connection connection = null;
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
            tStatement =  connection.prepareStatement(sqlStatement);
            /*
            * Set the attributes in the query.
            */
		 	if (dto.getId() != null) {
				tStatement.setLong(1, dto.getId().longValue());	 
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getIpAddress() != null) {
				tStatement.setString(2, dto.getIpAddress());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getHostname() != null) {
				tStatement.setString(3, dto.getHostname());
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getPageVisited() != null) {
				tStatement.setString(4, dto.getPageVisited());
			} else {
				tStatement.setString(4, null);
			}
		 	if (dto.getReferrer() != null) {
				tStatement.setString(5, dto.getReferrer());
			} else {
				tStatement.setString(5, null);
			}
		 	if (dto.getTimeRequested() != null) {
				tStatement.setString(6, dto.getTimeRequested());
			} else {
				tStatement.setString(6, null);
			}
		 	if (dto.getTimestamp() != null) {
				tStatement.setDate(7, DateConverter.convertDateToSqlDate(dto.getTimestamp()));
			} else {
				tStatement.setString(7, null);
			}
		 	if (dto.getOs() != null) {
				tStatement.setString(8, dto.getOs());
			} else {
				tStatement.setString(8, null);
			}
		 	if (dto.getBrowser() != null) {
				tStatement.setString(9, dto.getBrowser());
			} else {
				tStatement.setString(9, null);
			}
        	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in insert() VisitorsDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in insert() VisitorsDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }   		
   		   close(connection);
   		}
	}




	/**
	 * update - Updates a VisitorsDTO in the database.
	 * @param dto the dto containing the updated information
	 * @param user the userid performing the update
	 * @throws ApplicationException
	 */
	public void update(VisitorsDTO pkdto, VisitorsDTO dto, String user) throws ApplicationException {
		String sqlStatement = "update " + getTABLENAME() +
			" SET  id = ?,  ip_address = ?,  hostname = ?,  page_visited = ?,  referrer = ?,  time_requested = ?,  timestamp = ?,  os = ?,  browser = ? " +
			" where id = ?   ";

		Connection connection = null;
		PreparedStatement tStatement = null;		
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/
		 	if (dto.getId() != null) {
				tStatement.setLong(1, dto.getId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}
		 	if (dto.getIpAddress() != null) {
				tStatement.setString(2, dto.getIpAddress());
			} else {
				tStatement.setString(2, null);
			}
		 	if (dto.getHostname() != null) {
				tStatement.setString(3, dto.getHostname());
			} else {
				tStatement.setString(3, null);
			}
		 	if (dto.getPageVisited() != null) {
				tStatement.setString(4, dto.getPageVisited());
			} else {
				tStatement.setString(4, null);
			}
		 	if (dto.getReferrer() != null) {
				tStatement.setString(5, dto.getReferrer());
			} else {
				tStatement.setString(5, null);
			}
		 	if (dto.getTimeRequested() != null) {
				tStatement.setString(6, dto.getTimeRequested());
			} else {
				tStatement.setString(6, null);
			}
		 	if (dto.getTimestamp() != null) {
				tStatement.setDate(7, DateConverter.convertDateToSqlDate(dto.getTimestamp()));
			} else {
				tStatement.setString(7, null);
			}
		 	if (dto.getOs() != null) {
				tStatement.setString(8, dto.getOs());
			} else {
				tStatement.setString(8, null);
			}
		 	if (dto.getBrowser() != null) {
				tStatement.setString(9, dto.getBrowser());
			} else {
				tStatement.setString(9, null);
			}

			/*
			* Set the attribute(s) of the primary key for this DAO
			*/ 		
		    tStatement.setLong(10, pkdto.getId().longValue());
        	   	tStatement.execute();
   		} catch (SQLException e) {
			log.error("Exception in update() VisitorsDAO" + e.getMessage(), e);
   			throw new ApplicationException("Exception in update() VisitorsDAO" + e.getMessage());
   		} finally {
   		   try {
   		      tStatement.close();
   		   } catch(Exception e) {
   		      //let it go.
   		   }
   		   close(connection);   		     
   		}
	} //end update



	/**
	 * delete - Deletes a VisitorsDTO from the database.
	 * @param the dto to delete
	 * @param user the userid performing the delete
	 * @throws ApplicationException
	 */
	public void delete(VisitorsDTO dto, String user) throws ApplicationException {
		String sqlStatement = "delete from" + getTABLENAME() + 
			" where  id = ? ";
		Connection connection =  null; 
		PreparedStatement tStatement = null;
		try {
			connection = this.getConnection(user);
         	tStatement =  connection.prepareStatement(sqlStatement);
         	/*
         	* Set the attributes in the query.
         	*/


		   if (dto.getId() != null) {
				tStatement.setLong(1, dto.getId().longValue());	 		
			} else {
				tStatement.setString(1, null);
			}

        	tStatement.execute();
      } catch (SQLException e) {
	   log.error("Exception in delete() VisitorsDAO" + e.getMessage(), e);
   	   throw new ApplicationException("Exception in delete() VisitorsDAO" + e.getMessage());
      } finally {
   		 try {
   		    tStatement.close();
   		 } catch(Exception e) {
   		    //let it go.
   		 }      
         close(connection);
      }
      
	}


	/**
	 * create a dto representing the the current row in the result set.
	 * @param tResultSet the current result set
	 * @return a populated dto object
	 * @throws SQLException
	 */
  private VisitorsDTO populateDTO(ResultSet tResultSet) throws SQLException {
		long l;
		VisitorsDTO dto = new VisitorsDTO();
		l = tResultSet.getLong("id");
		if (!tResultSet.wasNull()){
			dto.setId(new Long(l));
		}
		dto.setIpAddress(tResultSet.getString("ip_address"));
		dto.setHostname(tResultSet.getString("hostname"));
		dto.setPageVisited(tResultSet.getString("page_visited"));
		dto.setReferrer(tResultSet.getString("referrer"));
		dto.setTimeRequested(tResultSet.getString("time_requested"));
		if (tResultSet.getTimestamp("timestamp") != null){
			dto.setTimestamp(DateConverter.convertTimestampToSqlDate(tResultSet.getTimestamp("timestamp")));
		}	
		dto.setOs(tResultSet.getString("os"));
		dto.setBrowser(tResultSet.getString("browser"));
		return dto;
	}	
	
	public static String getTABLENAME() {
		
		// account for some DBs that allow spaces in table names
		String tableName = TABLENAME;
		if (TABLENAME.trim().indexOf(" ") > -1) {
			String innerStr = TABLENAME.trim();
			tableName = (char)34 + innerStr + (char)34;
		} else {
			tableName = TABLENAME;
		}
		return tableName;
		
	}	
	
}	