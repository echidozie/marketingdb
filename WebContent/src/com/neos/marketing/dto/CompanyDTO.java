package com.neos.marketing.dto;

/**
 * Container for .Company data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class CompanyDTO implements java.io.Serializable {

	private Long companyId  = null;  
	private String name  = null;  


	/**
	 * a simple constructor
	 */
	public CompanyDTO() {
	}

	
	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a |Long|
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
    	this.companyId = new Long(companyId);
	}
	/**
	 * get the Name attribute
	 * @return the value of the Name attribute
	 */	
	public String getName() {
    	return this.name;
   	}

	/**
	 * set the Name attribute to a value defined by a String
	 * @param name the long value for the attribute
	 */
	public void setName(String name) {
    	this.name = name;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a CompanyDTO with only the Primary Key Values set
	 */ 	
   public CompanyDTO getPKDTO() {
		CompanyDTO pkDTO = new CompanyDTO();
		pkDTO.setCompanyId(this.getCompanyId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this CompanyDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this CompanyDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}