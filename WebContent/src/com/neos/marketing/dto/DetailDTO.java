package com.neos.marketing.dto;

/**
 * Container for .Detail data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class DetailDTO implements java.io.Serializable {

	private Long detailId  = null;  
	private Long sourceId  = null;  
	private String detail  = null;  


	/**
	 * a simple constructor
	 */
	public DetailDTO() {
	}

	
	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a |Long|
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
    	this.detailId = new Long(detailId);
	}
	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a |Long|
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
    	this.sourceId = new Long(sourceId);
	}
	/**
	 * get the Detail attribute
	 * @return the value of the Detail attribute
	 */	
	public String getDetail() {
    	return this.detail;
   	}

	/**
	 * set the Detail attribute to a value defined by a String
	 * @param detail the long value for the attribute
	 */
	public void setDetail(String detail) {
    	this.detail = detail;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a DetailDTO with only the Primary Key Values set
	 */ 	
   public DetailDTO getPKDTO() {
		DetailDTO pkDTO = new DetailDTO();
		pkDTO.setDetailId(this.getDetailId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this DetailDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this DetailDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}