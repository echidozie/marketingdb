package com.neos.marketing.dto;

import com.neos.marketing.util.DateConverter;

import java.util.Date;
/**
 * Container for .Downloads data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class DownloadsDTO implements java.io.Serializable {

	private Long id  = null;  
	private String name  = null;  
	private String email  = null;  
	private String downloadId  = null;  
	private String itemTitle  = null;  
	private java.util.Date timestamp  = null;  
	private String timeRequested  = null;  
	private String ipAddress  = null;  


	/**
	 * a simple constructor
	 */
	public DownloadsDTO() {
	}

	
	/**
	 * get the Id attribute
	 * @return the value of the Id attribute
	 */	
	public Long getId() {
    	return this.id;
   	}

	/**
	 * set the Id attribute to a value defined by a Long
	 * @param id the long value for the attribute
	 */
	public void setId(Long id) {
    	this.id = id;
	}
	/**
	 * overloaded method to set the Id attribute to a value defined by a |Long|
	 * @param id the String value for the attribute
	 */
	public void setId(String id) {
    	this.id = new Long(id);
	}
	/**
	 * get the Name attribute
	 * @return the value of the Name attribute
	 */	
	public String getName() {
    	return this.name;
   	}

	/**
	 * set the Name attribute to a value defined by a String
	 * @param name the long value for the attribute
	 */
	public void setName(String name) {
    	this.name = name;
	}
	/**
	 * get the Email attribute
	 * @return the value of the Email attribute
	 */	
	public String getEmail() {
    	return this.email;
   	}

	/**
	 * set the Email attribute to a value defined by a String
	 * @param email the long value for the attribute
	 */
	public void setEmail(String email) {
    	this.email = email;
	}
	/**
	 * get the DownloadId attribute
	 * @return the value of the DownloadId attribute
	 */	
	public String getDownloadId() {
    	return this.downloadId;
   	}

	/**
	 * set the DownloadId attribute to a value defined by a String
	 * @param downloadId the long value for the attribute
	 */
	public void setDownloadId(String downloadId) {
    	this.downloadId = downloadId;
	}
	/**
	 * get the ItemTitle attribute
	 * @return the value of the ItemTitle attribute
	 */	
	public String getItemTitle() {
    	return this.itemTitle;
   	}

	/**
	 * set the ItemTitle attribute to a value defined by a String
	 * @param itemTitle the long value for the attribute
	 */
	public void setItemTitle(String itemTitle) {
    	this.itemTitle = itemTitle;
	}
	/**
	 * get the Timestamp attribute
	 * @return the value of the Timestamp attribute
	 */	
	public java.util.Date getTimestamp() {
    	return this.timestamp;
   	}

	/**
	 * set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the long value for the attribute
	 */
	public void setTimestamp(java.util.Date timestamp) {
    	this.timestamp = timestamp;
	}
	/**
	 * overloaded method to set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the String value for the attribute
	 */
	public void setTimestamp(String timestamp) {
    	if (timestamp != null && !timestamp.equals("")) {
    		this.timestamp = DateConverter.convertStringToDate(timestamp);
		}
	}		

	/**
	 * get the TimeRequested attribute
	 * @return the value of the TimeRequested attribute
	 */	
	public String getTimeRequested() {
    	return this.timeRequested;
   	}

	/**
	 * set the TimeRequested attribute to a value defined by a String
	 * @param timeRequested the long value for the attribute
	 */
	public void setTimeRequested(String timeRequested) {
    	this.timeRequested = timeRequested;
	}
	/**
	 * get the IpAddress attribute
	 * @return the value of the IpAddress attribute
	 */	
	public String getIpAddress() {
    	return this.ipAddress;
   	}

	/**
	 * set the IpAddress attribute to a value defined by a String
	 * @param ipAddress the long value for the attribute
	 */
	public void setIpAddress(String ipAddress) {
    	this.ipAddress = ipAddress;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a DownloadsDTO with only the Primary Key Values set
	 */ 	
   public DownloadsDTO getPKDTO() {
		DownloadsDTO pkDTO = new DownloadsDTO();
		pkDTO.setId(this.getId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this DownloadsDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this DownloadsDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}