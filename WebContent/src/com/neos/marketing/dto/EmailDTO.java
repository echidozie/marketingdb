package com.neos.marketing.dto;

/**
 * Container for .Email data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class EmailDTO implements java.io.Serializable {

	private Long emailId  = null;  
	private String emailAddress  = null;  
	private Long personId  = null;  


	/**
	 * a simple constructor
	 */
	public EmailDTO() {
	}

	
	/**
	 * get the EmailId attribute
	 * @return the value of the EmailId attribute
	 */	
	public Long getEmailId() {
    	return this.emailId;
   	}

	/**
	 * set the EmailId attribute to a value defined by a Long
	 * @param emailId the long value for the attribute
	 */
	public void setEmailId(Long emailId) {
    	this.emailId = emailId;
	}
	/**
	 * overloaded method to set the EmailId attribute to a value defined by a |Long|
	 * @param emailId the String value for the attribute
	 */
	public void setEmailId(String emailId) {
    	this.emailId = new Long(emailId);
	}
	/**
	 * get the EmailAddress attribute
	 * @return the value of the EmailAddress attribute
	 */	
	public String getEmailAddress() {
    	return this.emailAddress;
   	}

	/**
	 * set the EmailAddress attribute to a value defined by a String
	 * @param emailAddress the long value for the attribute
	 */
	public void setEmailAddress(String emailAddress) {
    	this.emailAddress = emailAddress;
	}
	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a |Long|
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
    	this.personId = new Long(personId);
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a EmailDTO with only the Primary Key Values set
	 */ 	
   public EmailDTO getPKDTO() {
		EmailDTO pkDTO = new EmailDTO();
		pkDTO.setEmailId(this.getEmailId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this EmailDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this EmailDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}