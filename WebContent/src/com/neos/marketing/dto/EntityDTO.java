package com.neos.marketing.dto;

/**
 * Container for .Entity data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class EntityDTO implements java.io.Serializable {

	private Long entityId  = null;  
	private Long companyId  = null;  
	private Long personId  = null;  
	private String companyName = null;
	private String firstName = null;
	private String lastName = null;


	/**
	 * a simple constructor
	 */
	public EntityDTO() {
	}

	
	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a |Long|
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
    	this.entityId = new Long(entityId);
	}
	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a |Long|
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
    	this.companyId = new Long(companyId);
	}
	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a |Long|
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
    	this.personId = new Long(personId);
	}

	
	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a EntityDTO with only the Primary Key Values set
	 */ 	
   public EntityDTO getPKDTO() {
		EntityDTO pkDTO = new EntityDTO();
		pkDTO.setEntityId(this.getEntityId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this EntityDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this EntityDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}