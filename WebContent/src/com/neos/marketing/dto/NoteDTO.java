package com.neos.marketing.dto;

/**
 * Container for .Note data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class NoteDTO implements java.io.Serializable {

	private Long noteId  = null;  
	private String objectType  = null;  
	private Long objectId  = null;  
	private String note  = null;  


	/**
	 * a simple constructor
	 */
	public NoteDTO() {
	}

	
	/**
	 * get the NoteId attribute
	 * @return the value of the NoteId attribute
	 */	
	public Long getNoteId() {
    	return this.noteId;
   	}

	/**
	 * set the NoteId attribute to a value defined by a Long
	 * @param noteId the long value for the attribute
	 */
	public void setNoteId(Long noteId) {
    	this.noteId = noteId;
	}
	/**
	 * overloaded method to set the NoteId attribute to a value defined by a |Long|
	 * @param noteId the String value for the attribute
	 */
	public void setNoteId(String noteId) {
    	this.noteId = new Long(noteId);
	}
	/**
	 * get the ObjectType attribute
	 * @return the value of the ObjectType attribute
	 */	
	public String getObjectType() {
    	return this.objectType;
   	}

	/**
	 * set the ObjectType attribute to a value defined by a String
	 * @param objectType the long value for the attribute
	 */
	public void setObjectType(String objectType) {
    	this.objectType = objectType;
	}
	/**
	 * get the ObjectId attribute
	 * @return the value of the ObjectId attribute
	 */	
	public Long getObjectId() {
    	return this.objectId;
   	}

	/**
	 * set the ObjectId attribute to a value defined by a Long
	 * @param objectId the long value for the attribute
	 */
	public void setObjectId(Long objectId) {
    	this.objectId = objectId;
	}
	/**
	 * overloaded method to set the ObjectId attribute to a value defined by a |Long|
	 * @param objectId the String value for the attribute
	 */
	public void setObjectId(String objectId) {
    	this.objectId = new Long(objectId);
	}
	/**
	 * get the Note attribute
	 * @return the value of the Note attribute
	 */	
	public String getNote() {
    	return this.note;
   	}

	/**
	 * set the Note attribute to a value defined by a String
	 * @param note the long value for the attribute
	 */
	public void setNote(String note) {
    	this.note = note;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a NoteDTO with only the Primary Key Values set
	 */ 	
   public NoteDTO getPKDTO() {
		NoteDTO pkDTO = new NoteDTO();
		pkDTO.setNoteId(this.getNoteId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this NoteDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this NoteDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}