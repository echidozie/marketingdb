package com.neos.marketing.dto;

/**
 * Container for .Person data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class PersonDTO implements java.io.Serializable {

	private Long personId  = null;  
	private String firstName  = null;  
	private String lastName  = null;  
	private String title  = null;  
	private Long companyId  = null;  


	/**
	 * a simple constructor
	 */
	public PersonDTO() {
	}

	
	/**
	 * get the PersonId attribute
	 * @return the value of the PersonId attribute
	 */	
	public Long getPersonId() {
    	return this.personId;
   	}

	/**
	 * set the PersonId attribute to a value defined by a Long
	 * @param personId the long value for the attribute
	 */
	public void setPersonId(Long personId) {
    	this.personId = personId;
	}
	/**
	 * overloaded method to set the PersonId attribute to a value defined by a |Long|
	 * @param personId the String value for the attribute
	 */
	public void setPersonId(String personId) {
    	this.personId = new Long(personId);
	}
	/**
	 * get the FirstName attribute
	 * @return the value of the FirstName attribute
	 */	
	public String getFirstName() {
    	return this.firstName;
   	}

	/**
	 * set the FirstName attribute to a value defined by a String
	 * @param firstName the long value for the attribute
	 */
	public void setFirstName(String firstName) {
    	this.firstName = firstName;
	}
	/**
	 * get the LastName attribute
	 * @return the value of the LastName attribute
	 */	
	public String getLastName() {
    	return this.lastName;
   	}

	/**
	 * set the LastName attribute to a value defined by a String
	 * @param lastName the long value for the attribute
	 */
	public void setLastName(String lastName) {
    	this.lastName = lastName;
	}
	/**
	 * get the Title attribute
	 * @return the value of the Title attribute
	 */	
	public String getTitle() {
    	return this.title;
   	}

	/**
	 * set the Title attribute to a value defined by a String
	 * @param title the long value for the attribute
	 */
	public void setTitle(String title) {
    	this.title = title;
	}
	/**
	 * get the CompanyId attribute
	 * @return the value of the CompanyId attribute
	 */	
	public Long getCompanyId() {
    	return this.companyId;
   	}

	/**
	 * set the CompanyId attribute to a value defined by a Long
	 * @param companyId the long value for the attribute
	 */
	public void setCompanyId(Long companyId) {
    	this.companyId = companyId;
	}
	/**
	 * overloaded method to set the CompanyId attribute to a value defined by a |Long|
	 * @param companyId the String value for the attribute
	 */
	public void setCompanyId(String companyId) {
    	this.companyId = new Long(companyId);
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a PersonDTO with only the Primary Key Values set
	 */ 	
   public PersonDTO getPKDTO() {
		PersonDTO pkDTO = new PersonDTO();
		pkDTO.setPersonId(this.getPersonId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this PersonDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this PersonDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}