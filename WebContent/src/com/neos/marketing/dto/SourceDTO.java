package com.neos.marketing.dto;

/**
 * Container for .Source data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class SourceDTO implements java.io.Serializable {

	private Long sourceId  = null;  
	private String source  = null;  


	/**
	 * a simple constructor
	 */
	public SourceDTO() {
	}

	
	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a |Long|
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
    	this.sourceId = new Long(sourceId);
	}
	/**
	 * get the Source attribute
	 * @return the value of the Source attribute
	 */	
	public String getSource() {
    	return this.source;
   	}

	/**
	 * set the Source attribute to a value defined by a String
	 * @param source the long value for the attribute
	 */
	public void setSource(String source) {
    	this.source = source;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a SourceDTO with only the Primary Key Values set
	 */ 	
   public SourceDTO getPKDTO() {
		SourceDTO pkDTO = new SourceDTO();
		pkDTO.setSourceId(this.getSourceId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this SourceDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this SourceDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}