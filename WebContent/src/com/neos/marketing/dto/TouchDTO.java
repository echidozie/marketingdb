package com.neos.marketing.dto;

import com.neos.marketing.util.DateConverter;

import java.util.Date;
/**
 * Container for .Touch data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class TouchDTO implements java.io.Serializable {

	private Long touchId  = null;  
	private Long entityId  = null;  
	private java.util.Date touchDate  = null;  
	private Long sourceId  = null;  
	private Long detailId  = null;  


	/**
	 * a simple constructor
	 */
	public TouchDTO() {
	}

	
	/**
	 * get the TouchId attribute
	 * @return the value of the TouchId attribute
	 */	
	public Long getTouchId() {
    	return this.touchId;
   	}

	/**
	 * set the TouchId attribute to a value defined by a Long
	 * @param touchId the long value for the attribute
	 */
	public void setTouchId(Long touchId) {
    	this.touchId = touchId;
	}
	/**
	 * overloaded method to set the TouchId attribute to a value defined by a |Long|
	 * @param touchId the String value for the attribute
	 */
	public void setTouchId(String touchId) {
    	this.touchId = new Long(touchId);
	}
	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a |Long|
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
    	this.entityId = new Long(entityId);
	}
	/**
	 * get the TouchDate attribute
	 * @return the value of the TouchDate attribute
	 */	
	public java.util.Date getTouchDate() {
    	return this.touchDate;
   	}

	/**
	 * set the TouchDate attribute to a value defined by a java.util.Date
	 * @param touchDate the long value for the attribute
	 */
	public void setTouchDate(java.util.Date touchDate) {
    	this.touchDate = touchDate;
	}
	/**
	 * overloaded method to set the TouchDate attribute to a value defined by a java.util.Date
	 * @param touchDate the String value for the attribute
	 */
	public void setTouchDate(String touchDate) {
    	if (touchDate != null && !touchDate.equals("")) {
    		this.touchDate = DateConverter.convertStringToDate(touchDate);
		}
	}		

	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a |Long|
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
    	this.sourceId = new Long(sourceId);
	}
	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a |Long|
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
    	this.detailId = new Long(detailId);
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a TouchDTO with only the Primary Key Values set
	 */ 	
   public TouchDTO getPKDTO() {
		TouchDTO pkDTO = new TouchDTO();
		pkDTO.setTouchId(this.getTouchId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this TouchDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this TouchDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}