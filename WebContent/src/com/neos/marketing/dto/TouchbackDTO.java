package com.neos.marketing.dto;

import com.neos.marketing.util.DateConverter;

import java.util.Date;
/**
 * Container for .Touchback data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class TouchbackDTO implements java.io.Serializable {

	private Long touchbackId  = null;  
	private Long entityId  = null;  
	private java.util.Date touchbackDate  = null;  
	private Long sourceId  = null;  
	private Long detailId  = null;  


	/**
	 * a simple constructor
	 */
	public TouchbackDTO() {
	}

	
	/**
	 * get the TouchbackId attribute
	 * @return the value of the TouchbackId attribute
	 */	
	public Long getTouchbackId() {
    	return this.touchbackId;
   	}

	/**
	 * set the TouchbackId attribute to a value defined by a Long
	 * @param touchbackId the long value for the attribute
	 */
	public void setTouchbackId(Long touchbackId) {
    	this.touchbackId = touchbackId;
	}
	/**
	 * overloaded method to set the TouchbackId attribute to a value defined by a |Long|
	 * @param touchbackId the String value for the attribute
	 */
	public void setTouchbackId(String touchbackId) {
    	this.touchbackId = new Long(touchbackId);
	}
	/**
	 * get the EntityId attribute
	 * @return the value of the EntityId attribute
	 */	
	public Long getEntityId() {
    	return this.entityId;
   	}

	/**
	 * set the EntityId attribute to a value defined by a Long
	 * @param entityId the long value for the attribute
	 */
	public void setEntityId(Long entityId) {
    	this.entityId = entityId;
	}
	/**
	 * overloaded method to set the EntityId attribute to a value defined by a |Long|
	 * @param entityId the String value for the attribute
	 */
	public void setEntityId(String entityId) {
    	this.entityId = new Long(entityId);
	}
	/**
	 * get the TouchbackDate attribute
	 * @return the value of the TouchbackDate attribute
	 */	
	public java.util.Date getTouchbackDate() {
    	return this.touchbackDate;
   	}

	/**
	 * set the TouchbackDate attribute to a value defined by a java.util.Date
	 * @param touchbackDate the long value for the attribute
	 */
	public void setTouchbackDate(java.util.Date touchbackDate) {
    	this.touchbackDate = touchbackDate;
	}
	/**
	 * overloaded method to set the TouchbackDate attribute to a value defined by a java.util.Date
	 * @param touchbackDate the String value for the attribute
	 */
	public void setTouchbackDate(String touchbackDate) {
    	if (touchbackDate != null && !touchbackDate.equals("")) {
    		this.touchbackDate = DateConverter.convertStringToDate(touchbackDate);
		}
	}		

	/**
	 * get the SourceId attribute
	 * @return the value of the SourceId attribute
	 */	
	public Long getSourceId() {
    	return this.sourceId;
   	}

	/**
	 * set the SourceId attribute to a value defined by a Long
	 * @param sourceId the long value for the attribute
	 */
	public void setSourceId(Long sourceId) {
    	this.sourceId = sourceId;
	}
	/**
	 * overloaded method to set the SourceId attribute to a value defined by a |Long|
	 * @param sourceId the String value for the attribute
	 */
	public void setSourceId(String sourceId) {
    	this.sourceId = new Long(sourceId);
	}
	/**
	 * get the DetailId attribute
	 * @return the value of the DetailId attribute
	 */	
	public Long getDetailId() {
    	return this.detailId;
   	}

	/**
	 * set the DetailId attribute to a value defined by a Long
	 * @param detailId the long value for the attribute
	 */
	public void setDetailId(Long detailId) {
    	this.detailId = detailId;
	}
	/**
	 * overloaded method to set the DetailId attribute to a value defined by a |Long|
	 * @param detailId the String value for the attribute
	 */
	public void setDetailId(String detailId) {
    	this.detailId = new Long(detailId);
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a TouchbackDTO with only the Primary Key Values set
	 */ 	
   public TouchbackDTO getPKDTO() {
		TouchbackDTO pkDTO = new TouchbackDTO();
		pkDTO.setTouchbackId(this.getTouchbackId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this TouchbackDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this TouchbackDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}