package com.neos.marketing.dto;

import com.neos.marketing.util.DateConverter;

import java.util.Date;
/**
 * Container for .Visitors data.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author Vgo
 * @version 1.2
 */
public class VisitorsDTO implements java.io.Serializable {

	private Long id  = null;  
	private String ipAddress  = null;  
	private String hostname  = null;  
	private String pageVisited  = null;  
	private String referrer  = null;  
	private String timeRequested  = null;  
	private java.util.Date timestamp  = null;  
	private String os  = null;  
	private String browser  = null;  


	/**
	 * a simple constructor
	 */
	public VisitorsDTO() {
	}

	
	/**
	 * get the Id attribute
	 * @return the value of the Id attribute
	 */	
	public Long getId() {
    	return this.id;
   	}

	/**
	 * set the Id attribute to a value defined by a Long
	 * @param id the long value for the attribute
	 */
	public void setId(Long id) {
    	this.id = id;
	}
	/**
	 * overloaded method to set the Id attribute to a value defined by a |Long|
	 * @param id the String value for the attribute
	 */
	public void setId(String id) {
    	this.id = new Long(id);
	}
	/**
	 * get the IpAddress attribute
	 * @return the value of the IpAddress attribute
	 */	
	public String getIpAddress() {
    	return this.ipAddress;
   	}

	/**
	 * set the IpAddress attribute to a value defined by a String
	 * @param ipAddress the long value for the attribute
	 */
	public void setIpAddress(String ipAddress) {
    	this.ipAddress = ipAddress;
	}
	/**
	 * get the Hostname attribute
	 * @return the value of the Hostname attribute
	 */	
	public String getHostname() {
    	return this.hostname;
   	}

	/**
	 * set the Hostname attribute to a value defined by a String
	 * @param hostname the long value for the attribute
	 */
	public void setHostname(String hostname) {
    	this.hostname = hostname;
	}
	/**
	 * get the PageVisited attribute
	 * @return the value of the PageVisited attribute
	 */	
	public String getPageVisited() {
    	return this.pageVisited;
   	}

	/**
	 * set the PageVisited attribute to a value defined by a String
	 * @param pageVisited the long value for the attribute
	 */
	public void setPageVisited(String pageVisited) {
    	this.pageVisited = pageVisited;
	}
	/**
	 * get the Referrer attribute
	 * @return the value of the Referrer attribute
	 */	
	public String getReferrer() {
    	return this.referrer;
   	}

	/**
	 * set the Referrer attribute to a value defined by a String
	 * @param referrer the long value for the attribute
	 */
	public void setReferrer(String referrer) {
    	this.referrer = referrer;
	}
	/**
	 * get the TimeRequested attribute
	 * @return the value of the TimeRequested attribute
	 */	
	public String getTimeRequested() {
    	return this.timeRequested;
   	}

	/**
	 * set the TimeRequested attribute to a value defined by a String
	 * @param timeRequested the long value for the attribute
	 */
	public void setTimeRequested(String timeRequested) {
    	this.timeRequested = timeRequested;
	}
	/**
	 * get the Timestamp attribute
	 * @return the value of the Timestamp attribute
	 */	
	public java.util.Date getTimestamp() {
    	return this.timestamp;
   	}

	/**
	 * set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the long value for the attribute
	 */
	public void setTimestamp(java.util.Date timestamp) {
    	this.timestamp = timestamp;
	}
	/**
	 * overloaded method to set the Timestamp attribute to a value defined by a java.util.Date
	 * @param timestamp the String value for the attribute
	 */
	public void setTimestamp(String timestamp) {
    	if (timestamp != null && !timestamp.equals("")) {
    		this.timestamp = DateConverter.convertStringToDate(timestamp);
		}
	}		

	/**
	 * get the Os attribute
	 * @return the value of the Os attribute
	 */	
	public String getOs() {
    	return this.os;
   	}

	/**
	 * set the Os attribute to a value defined by a String
	 * @param os the long value for the attribute
	 */
	public void setOs(String os) {
    	this.os = os;
	}
	/**
	 * get the Browser attribute
	 * @return the value of the Browser attribute
	 */	
	public String getBrowser() {
    	return this.browser;
   	}

	/**
	 * set the Browser attribute to a value defined by a String
	 * @param browser the long value for the attribute
	 */
	public void setBrowser(String browser) {
    	this.browser = browser;
	}

	/**
	 * returns a DTO object that is just the PrimaryKey values of this DTO
	 * @return a VisitorsDTO with only the Primary Key Values set
	 */ 	
   public VisitorsDTO getPKDTO() {
		VisitorsDTO pkDTO = new VisitorsDTO();
		pkDTO.setId(this.getId());
		return(pkDTO);
	}// end getPKDTO

	/**
	 * returns a String that is a description of an instance of this VisitorsDTO object.  This is
	 * used in all list displays and should be overwritten to change those views.
	 * @return a String that is a description of an instance of this VisitorsDTO object.
	 */   
	public String getTableDescription() {
      //Use for debugging and logging
      StringBuffer description = new StringBuffer("");
      return description.toString();
   }

}