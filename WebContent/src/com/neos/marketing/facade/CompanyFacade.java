  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for CompanyDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class CompanyFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public CompanyFacade() {
   	}

	/**
	 * Retrieve the complete list of CompanyBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new CompanyBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new CompanyBO());
	}

	/**
	 * Retrieve the complete list of CompanyBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(CompanyBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(CompanyBO bo) throws ApplicationException {
		int cnt = 0;
    	CompanyDAO dao = (CompanyDAO) DataAccessFactory.getDAO("CompanyDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Companyfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of CompanyBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(CompanyBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	CompanyDAO dao = (CompanyDAO) DataAccessFactory.getDAO("CompanyDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					CompanyDTO baseDTO = (CompanyDTO) i.next();
					CompanyBO classBO = new CompanyBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Companyfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Companyfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findCompany returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of CompanyBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(CompanyBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		CompanyDAO dao = (CompanyDAO) DataAccessFactory.getDAO("CompanyDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					CompanyDTO baseDTO = (CompanyDTO) i.next();
					CompanyBO classBO = new CompanyBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Company 1 orCollection
			for (Iterator i1 = orCollection.iterator(); i1.hasNext();) {
				CompanyBO temp1 = (CompanyBO) i1.next();
				EntityBO a1EntityBO = new EntityBO();
				a1EntityBO.setCompanyId(temp1.getCompanyId());
				EntityFacade aEntityFacade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");				
				Collection entitycompanyfklistL = aEntityFacade.retrieve(a1EntityBO);
				if (entitycompanyfklistL.size() > 0) {
					Collection entitycompanyfklistNL = new ArrayList();
					for (Iterator i = entitycompanyfklistL.iterator(); i.hasNext();) {
						EntityBO temp = (EntityBO) i.next();
						entitycompanyfklistNL.add(temp);	
					}
					temp1.setEntityCompanyFkList(entitycompanyfklistNL);
//retrieveMaster BaseObject: EntityCompanyFkList Entity 1 entitycompanyfklistNL
//retrieveMaster BaseObject: EntityCompanyFkList Entity 1 entitycompanyfklistNL
//retrieveDetail EntityCompanyFkList 1 entitycompanyfklistNL
//retrieveDetail EntityCompanyFkList 1 entitycompanyfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Company 1 orCollection
			for (Iterator i2 = orCollection.iterator(); i2.hasNext();) {
				CompanyBO temp2 = (CompanyBO) i2.next();
				PersonBO a2PersonBO = new PersonBO();
				a2PersonBO.setCompanyId(temp2.getCompanyId());
				PersonFacade aPersonFacade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");				
				Collection companyfklistL = aPersonFacade.retrieve(a2PersonBO);
				if (companyfklistL.size() > 0) {
					Collection companyfklistNL = new ArrayList();
					for (Iterator i = companyfklistL.iterator(); i.hasNext();) {
						PersonBO temp = (PersonBO) i.next();
						companyfklistNL.add(temp);	
					}
					temp2.setCompanyFkList(companyfklistNL);
//retrieveMaster BaseObject: CompanyFkList Person 1 companyfklistNL
//retrieveDetail CompanyFkList 1 companyfklistNL
//retrieveDetail CompanyFkList 1 companyfklistNL
				}
			}			
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Companyfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Companyfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findCompany returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of CompanyBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(CompanyBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		CompanyDAO dao = (CompanyDAO) DataAccessFactory.getDAO("CompanyDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					CompanyDTO baseDTO = (CompanyDTO) i.next();
					CompanyBO classBO = new CompanyBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Company 1 orCollection
			for (Iterator i3 = orCollection.iterator(); i3.hasNext();) {
				CompanyBO temp3 = (CompanyBO) i3.next();
				EntityBO a3EntityBO = new EntityBO();
				a3EntityBO.setCompanyId(temp3.getCompanyId());
				EntityFacade aEntityFacade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");				
				Collection entitycompanyfklistL = aEntityFacade.retrieve(a3EntityBO);
				if (entitycompanyfklistL.size() > 0) {
					Collection entitycompanyfklistNL = new ArrayList();
					for (Iterator i = entitycompanyfklistL.iterator(); i.hasNext();) {
						EntityBO temp = (EntityBO) i.next();
						entitycompanyfklistNL.add(temp);	
					}
					temp3.setEntityCompanyFkList(entitycompanyfklistNL);
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Company 1 orCollection
			for (Iterator i4 = orCollection.iterator(); i4.hasNext();) {
				CompanyBO temp4 = (CompanyBO) i4.next();
				PersonBO a4PersonBO = new PersonBO();
				a4PersonBO.setCompanyId(temp4.getCompanyId());
				PersonFacade aPersonFacade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");				
				Collection companyfklistL = aPersonFacade.retrieve(a4PersonBO);
				if (companyfklistL.size() > 0) {
					Collection companyfklistNL = new ArrayList();
					for (Iterator i = companyfklistL.iterator(); i.hasNext();) {
						PersonBO temp = (PersonBO) i.next();
						companyfklistNL.add(temp);	
					}
					temp4.setCompanyFkList(companyfklistNL);
//retrieveMaster BaseObject: CompanyFkList Person 1 companyfklistNL
//retrieveDetail CompanyFkList 1 companyfklistNL
//retrieveDetail CompanyFkList 1 companyfklistNL
				}
			}			
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Companyfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Companyfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findCompany returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a CompanyDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a CompanyDTO providing the primary keys specifying which DTO to update.
	 * @param dto a CompanyDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(CompanyBO pkbo, CompanyBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("CompanyBO to save is null");
            return;
		}     
        try  {
			CompanyDAO dao = (CompanyDAO) DataAccessFactory.getDAO("CompanyDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save CompanyDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Companyfacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Companyfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new CompanyBO using the values in the given bo.
	 * 
	 * @param bo a CompanyBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(CompanyBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("CompanyBO to create is null");
            return;
		}
        try  {
        	CompanyDAO dao = (CompanyDAO) DataAccessFactory.getDAO("CompanyDAO");       	
			CompanyDTO baseDTO = bo.getDTO();	
			baseDTO.setCompanyId(dao.getNewPK());
			dao.insert(baseDTO, user);
            log.debug("Successful Create CompanyBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Companyfacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Companyfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a CompanyBO using the bo provided as the lookup values.
	 * 
	 * @param bo a CompanyBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(CompanyBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("CompanyBO to delete is null");
            return;
		}		       
        try  {
			CompanyDAO dao = (CompanyDAO) DataAccessFactory.getDAO("CompanyDAO");	
			CompanyDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in CompanyCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in CompanyCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}