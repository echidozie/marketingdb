  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for DetailDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class DetailFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public DetailFacade() {
   	}

	/**
	 * Retrieve the complete list of DetailBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new DetailBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new DetailBO());
	}

	/**
	 * Retrieve the complete list of DetailBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(DetailBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(DetailBO bo) throws ApplicationException {
		int cnt = 0;
    	DetailDAO dao = (DetailDAO) DataAccessFactory.getDAO("DetailDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Detailfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of DetailBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(DetailBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	DetailDAO dao = (DetailDAO) DataAccessFactory.getDAO("DetailDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					DetailDTO baseDTO = (DetailDTO) i.next();
					DetailBO classBO = new DetailBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Detailfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Detailfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findDetail returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of DetailBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(DetailBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		DetailDAO dao = (DetailDAO) DataAccessFactory.getDAO("DetailDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					DetailDTO baseDTO = (DetailDTO) i.next();
					DetailBO classBO = new DetailBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Detail 1 orCollection
			for (Iterator i1 = orCollection.iterator(); i1.hasNext();) {
				DetailBO temp1 = (DetailBO) i1.next();
				TouchbackBO a1TouchbackBO = new TouchbackBO();
				a1TouchbackBO.setDetailId(temp1.getDetailId());
				TouchbackFacade aTouchbackFacade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");				
				Collection touchbackdetailfklistL = aTouchbackFacade.retrieve(a1TouchbackBO);
				if (touchbackdetailfklistL.size() > 0) {
					Collection touchbackdetailfklistNL = new ArrayList();
					for (Iterator i = touchbackdetailfklistL.iterator(); i.hasNext();) {
						TouchbackBO temp = (TouchbackBO) i.next();
						touchbackdetailfklistNL.add(temp);	
					}
					temp1.setTouchbackDetailFkList(touchbackdetailfklistNL);
//retrieveMaster BaseObject: TouchbackDetailFkList Touchback 1 touchbackdetailfklistNL
//retrieveMaster BaseObject: TouchbackDetailFkList Touchback 1 touchbackdetailfklistNL
//retrieveMaster BaseObject: TouchbackDetailFkList Touchback 1 touchbackdetailfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Detail 1 orCollection
			for (Iterator i2 = orCollection.iterator(); i2.hasNext();) {
				DetailBO temp2 = (DetailBO) i2.next();
				TouchBO a2TouchBO = new TouchBO();
				a2TouchBO.setDetailId(temp2.getDetailId());
				TouchFacade aTouchFacade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");				
				Collection touchdetailfklistL = aTouchFacade.retrieve(a2TouchBO);
				if (touchdetailfklistL.size() > 0) {
					Collection touchdetailfklistNL = new ArrayList();
					for (Iterator i = touchdetailfklistL.iterator(); i.hasNext();) {
						TouchBO temp = (TouchBO) i.next();
						touchdetailfklistNL.add(temp);	
					}
					temp2.setTouchDetailFkList(touchdetailfklistNL);
//retrieveMaster BaseObject: TouchDetailFkList Touch 1 touchdetailfklistNL
//retrieveMaster BaseObject: TouchDetailFkList Touch 1 touchdetailfklistNL
//retrieveMaster BaseObject: TouchDetailFkList Touch 1 touchdetailfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Detail Detail 1 orCollection
			for (Iterator i3 = orCollection.iterator(); i3.hasNext();) {
				DetailBO temp3 = (DetailBO) i3.next();					
				SourceBO a3SourceBO = new SourceBO();
				a3SourceBO.setSourceId(temp3.getSourceId()); 
				SourceFacade a3SourceFacade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");				
				Collection detailsourcefk = a3SourceFacade.retrieve(a3SourceBO);	
				if (detailsourcefk.size() > 0) {	
					temp3.setDetailSourceFkBO((SourceBO) detailsourcefk.toArray()[0]);
//retrieveDetail DetailSourceFk 1 detailsourcefk
//retrieveDetail DetailSourceFk 1 detailsourcefk
//retrieveDetail DetailSourceFk 1 detailsourcefk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Detailfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Detailfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findDetail returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of DetailBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(DetailBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		DetailDAO dao = (DetailDAO) DataAccessFactory.getDAO("DetailDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					DetailDTO baseDTO = (DetailDTO) i.next();
					DetailBO classBO = new DetailBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Detail 1 orCollection
			for (Iterator i4 = orCollection.iterator(); i4.hasNext();) {
				DetailBO temp4 = (DetailBO) i4.next();
				TouchbackBO a4TouchbackBO = new TouchbackBO();
				a4TouchbackBO.setDetailId(temp4.getDetailId());
				TouchbackFacade aTouchbackFacade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");				
				Collection touchbackdetailfklistL = aTouchbackFacade.retrieve(a4TouchbackBO);
				if (touchbackdetailfklistL.size() > 0) {
					Collection touchbackdetailfklistNL = new ArrayList();
					for (Iterator i = touchbackdetailfklistL.iterator(); i.hasNext();) {
						TouchbackBO temp = (TouchbackBO) i.next();
						touchbackdetailfklistNL.add(temp);	
					}
					temp4.setTouchbackDetailFkList(touchbackdetailfklistNL);
//retrieveMaster BaseObject: TouchbackDetailFkList Touchback 1 touchbackdetailfklistNL
//retrieveMaster BaseObject: TouchbackDetailFkList Touchback 1 touchbackdetailfklistNL
//retrieveMaster BaseObject: TouchbackDetailFkList Touchback 1 touchbackdetailfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Detail 1 orCollection
			for (Iterator i5 = orCollection.iterator(); i5.hasNext();) {
				DetailBO temp5 = (DetailBO) i5.next();
				TouchBO a5TouchBO = new TouchBO();
				a5TouchBO.setDetailId(temp5.getDetailId());
				TouchFacade aTouchFacade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");				
				Collection touchdetailfklistL = aTouchFacade.retrieve(a5TouchBO);
				if (touchdetailfklistL.size() > 0) {
					Collection touchdetailfklistNL = new ArrayList();
					for (Iterator i = touchdetailfklistL.iterator(); i.hasNext();) {
						TouchBO temp = (TouchBO) i.next();
						touchdetailfklistNL.add(temp);	
					}
					temp5.setTouchDetailFkList(touchdetailfklistNL);
//retrieveMaster BaseObject: TouchDetailFkList Touch 1 touchdetailfklistNL
//retrieveMaster BaseObject: TouchDetailFkList Touch 1 touchdetailfklistNL
//retrieveMaster BaseObject: TouchDetailFkList Touch 1 touchdetailfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Detail Detail 1 orCollection
			for (Iterator i6 = orCollection.iterator(); i6.hasNext();) {
				DetailBO temp6 = (DetailBO) i6.next();					
				SourceBO a6SourceBO = new SourceBO();
				a6SourceBO.setSourceId(temp6.getSourceId()); 
				SourceFacade a6SourceFacade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");				
				Collection detailsourcefk = a6SourceFacade.retrieve(a6SourceBO);	
				if (detailsourcefk.size() > 0) {	
					temp6.setDetailSourceFkBO((SourceBO) detailsourcefk.toArray()[0]);
//retrieveDetail DetailSourceFk 1 detailsourcefk
//retrieveDetail DetailSourceFk 1 detailsourcefk
//retrieveDetail DetailSourceFk 1 detailsourcefk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Detailfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Detailfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findDetail returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a DetailDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a DetailDTO providing the primary keys specifying which DTO to update.
	 * @param dto a DetailDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(DetailBO pkbo, DetailBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("DetailBO to save is null");
            return;
		}     
        try  {
			DetailDAO dao = (DetailDAO) DataAccessFactory.getDAO("DetailDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save DetailDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Detailfacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Detailfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new DetailBO using the values in the given bo.
	 * 
	 * @param bo a DetailBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(DetailBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("DetailBO to create is null");
            return;
		}
        try  {
        	DetailDAO dao = (DetailDAO) DataAccessFactory.getDAO("DetailDAO");       	
			DetailDTO baseDTO = bo.getDTO();	
			baseDTO.setDetailId(dao.getNewPK());
			dao.insert(baseDTO, user);
            log.debug("Successful Create DetailBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Detailfacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Detailfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a DetailBO using the bo provided as the lookup values.
	 * 
	 * @param bo a DetailBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(DetailBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("DetailBO to delete is null");
            return;
		}		       
        try  {
			DetailDAO dao = (DetailDAO) DataAccessFactory.getDAO("DetailDAO");	
			DetailDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in DetailCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in DetailCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}