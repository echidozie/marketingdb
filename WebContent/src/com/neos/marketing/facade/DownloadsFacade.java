  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for DownloadsDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class DownloadsFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public DownloadsFacade() {
   	}

	/**
	 * Retrieve the complete list of DownloadsBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new DownloadsBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new DownloadsBO());
	}

	/**
	 * Retrieve the complete list of DownloadsBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(DownloadsBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(DownloadsBO bo) throws ApplicationException {
		int cnt = 0;
    	DownloadsDAO dao = (DownloadsDAO) DataAccessFactory.getDAO("DownloadsDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Downloadsfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of DownloadsBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(DownloadsBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	DownloadsDAO dao = (DownloadsDAO) DataAccessFactory.getDAO("DownloadsDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					DownloadsDTO baseDTO = (DownloadsDTO) i.next();
					DownloadsBO classBO = new DownloadsBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Downloadsfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Downloadsfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findDownloads returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of DownloadsBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(DownloadsBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		DownloadsDAO dao = (DownloadsDAO) DataAccessFactory.getDAO("DownloadsDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					DownloadsDTO baseDTO = (DownloadsDTO) i.next();
					DownloadsBO classBO = new DownloadsBO(baseDTO);
					orCollection.add(classBO);
				}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Downloadsfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Downloadsfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findDownloads returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of DownloadsBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(DownloadsBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		DownloadsDAO dao = (DownloadsDAO) DataAccessFactory.getDAO("DownloadsDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					DownloadsDTO baseDTO = (DownloadsDTO) i.next();
					DownloadsBO classBO = new DownloadsBO(baseDTO);
					orCollection.add(classBO);
				}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Downloadsfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Downloadsfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findDownloads returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a DownloadsDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a DownloadsDTO providing the primary keys specifying which DTO to update.
	 * @param dto a DownloadsDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(DownloadsBO pkbo, DownloadsBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("DownloadsBO to save is null");
            return;
		}     
        try  {
			DownloadsDAO dao = (DownloadsDAO) DataAccessFactory.getDAO("DownloadsDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save DownloadsDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Downloadsfacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Downloadsfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new DownloadsBO using the values in the given bo.
	 * 
	 * @param bo a DownloadsBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(DownloadsBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("DownloadsBO to create is null");
            return;
		}
        try  {
        	DownloadsDAO dao = (DownloadsDAO) DataAccessFactory.getDAO("DownloadsDAO");       	
			DownloadsDTO baseDTO = bo.getDTO();	
			baseDTO.setId(dao.getNewPK());
			dao.insert(baseDTO, user);
            log.debug("Successful Create DownloadsBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Downloadsfacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Downloadsfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a DownloadsBO using the bo provided as the lookup values.
	 * 
	 * @param bo a DownloadsBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(DownloadsBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("DownloadsBO to delete is null");
            return;
		}		       
        try  {
			DownloadsDAO dao = (DownloadsDAO) DataAccessFactory.getDAO("DownloadsDAO");	
			DownloadsDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in DownloadsCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in DownloadsCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}