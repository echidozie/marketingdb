  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for EmailDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class EmailFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public EmailFacade() {
   	}

	/**
	 * Retrieve the complete list of EmailBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new EmailBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new EmailBO());
	}

	/**
	 * Retrieve the complete list of EmailBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(EmailBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(EmailBO bo) throws ApplicationException {
		int cnt = 0;
    	EmailDAO dao = (EmailDAO) DataAccessFactory.getDAO("EmailDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Emailfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of EmailBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(EmailBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	EmailDAO dao = (EmailDAO) DataAccessFactory.getDAO("EmailDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					EmailDTO baseDTO = (EmailDTO) i.next();
					EmailBO classBO = new EmailBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Emailfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Emailfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findEmail returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of EmailBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(EmailBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		EmailDAO dao = (EmailDAO) DataAccessFactory.getDAO("EmailDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					EmailDTO baseDTO = (EmailDTO) i.next();
					EmailBO classBO = new EmailBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Email Email 1 orCollection
			for (Iterator i1 = orCollection.iterator(); i1.hasNext();) {
				EmailBO temp1 = (EmailBO) i1.next();					
				PersonBO a1PersonBO = new PersonBO();
				a1PersonBO.setPersonId(temp1.getPersonId()); 
				PersonFacade a1PersonFacade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");				
				Collection emailpersonfk = a1PersonFacade.retrieve(a1PersonBO);	
				if (emailpersonfk.size() > 0) {	
					temp1.setEmailPersonFkBO((PersonBO) emailpersonfk.toArray()[0]);
//retrieveDetail EmailPersonFk 1 emailpersonfk
//retrieveDetail EmailPersonFk 1 emailpersonfk
//retrieveMaster BaseObject: EmailPersonFk Person 1 emailpersonfk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Emailfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Emailfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findEmail returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of EmailBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(EmailBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		EmailDAO dao = (EmailDAO) DataAccessFactory.getDAO("EmailDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					EmailDTO baseDTO = (EmailDTO) i.next();
					EmailBO classBO = new EmailBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Email Email 1 orCollection
			for (Iterator i2 = orCollection.iterator(); i2.hasNext();) {
				EmailBO temp2 = (EmailBO) i2.next();					
				PersonBO a2PersonBO = new PersonBO();
				a2PersonBO.setPersonId(temp2.getPersonId()); 
				PersonFacade a2PersonFacade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");				
				Collection emailpersonfk = a2PersonFacade.retrieve(a2PersonBO);	
				if (emailpersonfk.size() > 0) {	
					temp2.setEmailPersonFkBO((PersonBO) emailpersonfk.toArray()[0]);
//retrieveDetail EmailPersonFk 1 emailpersonfk
//retrieveDetail EmailPersonFk 1 emailpersonfk
//retrieveMaster BaseObject: EmailPersonFk Person 1 emailpersonfk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Emailfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Emailfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findEmail returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a EmailDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a EmailDTO providing the primary keys specifying which DTO to update.
	 * @param dto a EmailDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(EmailBO pkbo, EmailBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("EmailBO to save is null");
            return;
		}     
        try  {
			EmailDAO dao = (EmailDAO) DataAccessFactory.getDAO("EmailDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save EmailDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Emailfacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Emailfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new EmailBO using the values in the given bo.
	 * 
	 * @param bo a EmailBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(EmailBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("EmailBO to create is null");
            return;
		}
        try  {
        	EmailDAO dao = (EmailDAO) DataAccessFactory.getDAO("EmailDAO");       	
			EmailDTO baseDTO = bo.getDTO();	
			EmailBean bean = new EmailBean();
			baseDTO.setEmailId(dao.getNewPK());
			dao.insert(baseDTO, user);
            log.debug("Successful Create EmailBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Emailfacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Emailfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a EmailBO using the bo provided as the lookup values.
	 * 
	 * @param bo a EmailBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(EmailBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("EmailBO to delete is null");
            return;
		}		       
        try  {
			EmailDAO dao = (EmailDAO) DataAccessFactory.getDAO("EmailDAO");	
			EmailDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in EmailCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in EmailCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}