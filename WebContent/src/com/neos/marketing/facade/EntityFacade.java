  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for EntityDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class EntityFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public EntityFacade() {
   	}

	/**
	 * Retrieve the complete list of EntityBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new EntityBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new EntityBO());
	}

	/**
	 * Retrieve the complete list of EntityBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(EntityBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(EntityBO bo) throws ApplicationException {
		int cnt = 0;
    	EntityDAO dao = (EntityDAO) DataAccessFactory.getDAO("EntityDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Entityfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of EntityBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(EntityBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	EntityDAO dao = (EntityDAO) DataAccessFactory.getDAO("EntityDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					EntityDTO baseDTO = (EntityDTO) i.next();
					EntityBO classBO = new EntityBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Entityfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Entityfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findEntity returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of EntityBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(EntityBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		EntityDAO dao = (EntityDAO) DataAccessFactory.getDAO("EntityDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					EntityDTO baseDTO = (EntityDTO) i.next();
					EntityBO classBO = new EntityBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Entity 1 orCollection
			for (Iterator i1 = orCollection.iterator(); i1.hasNext();) {
				EntityBO temp1 = (EntityBO) i1.next();
				TouchbackBO a1TouchbackBO = new TouchbackBO();
				a1TouchbackBO.setEntityId(temp1.getEntityId());
				TouchbackFacade aTouchbackFacade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");				
				Collection touchbackentityfklistL = aTouchbackFacade.retrieve(a1TouchbackBO);
				if (touchbackentityfklistL.size() > 0) {
					Collection touchbackentityfklistNL = new ArrayList();
					for (Iterator i = touchbackentityfklistL.iterator(); i.hasNext();) {
						TouchbackBO temp = (TouchbackBO) i.next();
						touchbackentityfklistNL.add(temp);	
					}
					temp1.setTouchbackEntityFkList(touchbackentityfklistNL);
//retrieveMaster BaseObject: TouchbackEntityFkList Touchback 1 touchbackentityfklistNL
//retrieveMaster BaseObject: TouchbackEntityFkList Touchback 1 touchbackentityfklistNL
//retrieveMaster BaseObject: TouchbackEntityFkList Touchback 1 touchbackentityfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Entity 1 orCollection
			for (Iterator i2 = orCollection.iterator(); i2.hasNext();) {
				EntityBO temp2 = (EntityBO) i2.next();
				TouchBO a2TouchBO = new TouchBO();
				a2TouchBO.setEntityId(temp2.getEntityId());
				TouchFacade aTouchFacade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");				
				Collection touchentityfklistL = aTouchFacade.retrieve(a2TouchBO);
				if (touchentityfklistL.size() > 0) {
					Collection touchentityfklistNL = new ArrayList();
					for (Iterator i = touchentityfklistL.iterator(); i.hasNext();) {
						TouchBO temp = (TouchBO) i.next();
						touchentityfklistNL.add(temp);	
					}
					temp2.setTouchEntityFkList(touchentityfklistNL);
//retrieveMaster BaseObject: TouchEntityFkList Touch 1 touchentityfklistNL
//retrieveMaster BaseObject: TouchEntityFkList Touch 1 touchentityfklistNL
//retrieveMaster BaseObject: TouchEntityFkList Touch 1 touchentityfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Entity Entity 1 orCollection
			for (Iterator i3 = orCollection.iterator(); i3.hasNext();) {
				EntityBO temp3 = (EntityBO) i3.next();					
				CompanyBO a3CompanyBO = new CompanyBO();
				a3CompanyBO.setCompanyId(temp3.getCompanyId()); 
				CompanyFacade a3CompanyFacade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");				
				Collection entitycompanyfk = a3CompanyFacade.retrieve(a3CompanyBO);	
				if (entitycompanyfk.size() > 0) {	
					temp3.setEntityCompanyFkBO((CompanyBO) entitycompanyfk.toArray()[0]);
//retrieveDetail EntityCompanyFk 1 entitycompanyfk
//retrieveDetail EntityCompanyFk 1 entitycompanyfk
    			for (Iterator i4 = entitycompanyfk.iterator(); i4.hasNext();) {
    				CompanyBO temp4 = (CompanyBO) i4.next();
    				PersonBO a4PersonBO = new PersonBO();
    				a4PersonBO.setCompanyId(temp4.getCompanyId());
    				PersonFacade aPersonFacade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");				
    				Collection companyfklistL = aPersonFacade.retrieve(a4PersonBO);
    				if (companyfklistL.size() > 0) {
    					Collection companyfklistNL = new ArrayList();
    					for (Iterator i = companyfklistL.iterator(); i.hasNext();) {
    						PersonBO temp = (PersonBO) i.next();
    						companyfklistNL.add(temp);	
    					}
    					temp4.setCompanyFkList(companyfklistNL);
//retrieveMaster BaseObject: CompanyFkList Person 1 companyfklistNL
//retrieveDetail CompanyFkList 1 companyfklistNL
//retrieveDetail CompanyFkList 1 companyfklistNL
    				}
    			}			
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Entity Entity 1 orCollection
			for (Iterator i5 = orCollection.iterator(); i5.hasNext();) {
				EntityBO temp5 = (EntityBO) i5.next();					
				PersonBO a5PersonBO = new PersonBO();
				a5PersonBO.setPersonId(temp5.getPersonId()); 
				PersonFacade a5PersonFacade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");				
				Collection entitypersonfk = a5PersonFacade.retrieve(a5PersonBO);	
				if (entitypersonfk.size() > 0) {	
					temp5.setEntityPersonFkBO((PersonBO) entitypersonfk.toArray()[0]);
//retrieveDetail EntityPersonFk 1 entitypersonfk
    			for (Iterator i6 = entitypersonfk.iterator(); i6.hasNext();) {
    				PersonBO temp6 = (PersonBO) i6.next();
    				EmailBO a6EmailBO = new EmailBO();
    				a6EmailBO.setPersonId(temp6.getPersonId());
    				EmailFacade aEmailFacade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");				
    				Collection emailpersonfklistL = aEmailFacade.retrieve(a6EmailBO);
    				if (emailpersonfklistL.size() > 0) {
    					Collection emailpersonfklistNL = new ArrayList();
    					for (Iterator i = emailpersonfklistL.iterator(); i.hasNext();) {
    						EmailBO temp = (EmailBO) i.next();
    						emailpersonfklistNL.add(temp);	
    					}
    					temp6.setEmailPersonFkList(emailpersonfklistNL);
//retrieveMaster BaseObject: EmailPersonFkList Email 1 emailpersonfklistNL
    				}
    			}			
//retrieveDetail EntityPersonFk 1 entitypersonfk
//retrieveMaster BaseObject: EntityPersonFk Person 1 entitypersonfk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Entityfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Entityfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findEntity returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of EntityBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(EntityBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		EntityDAO dao = (EntityDAO) DataAccessFactory.getDAO("EntityDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					EntityDTO baseDTO = (EntityDTO) i.next();
					EntityBO classBO = new EntityBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Entity 1 orCollection
			for (Iterator i7 = orCollection.iterator(); i7.hasNext();) {
				EntityBO temp7 = (EntityBO) i7.next();
				TouchbackBO a7TouchbackBO = new TouchbackBO();
				a7TouchbackBO.setEntityId(temp7.getEntityId());
				TouchbackFacade aTouchbackFacade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");				
				Collection touchbackentityfklistL = aTouchbackFacade.retrieve(a7TouchbackBO);
				if (touchbackentityfklistL.size() > 0) {
					Collection touchbackentityfklistNL = new ArrayList();
					for (Iterator i = touchbackentityfklistL.iterator(); i.hasNext();) {
						TouchbackBO temp = (TouchbackBO) i.next();
						touchbackentityfklistNL.add(temp);	
					}
					temp7.setTouchbackEntityFkList(touchbackentityfklistNL);
//retrieveMaster BaseObject: TouchbackEntityFkList Touchback 1 touchbackentityfklistNL
//retrieveMaster BaseObject: TouchbackEntityFkList Touchback 1 touchbackentityfklistNL
//retrieveMaster BaseObject: TouchbackEntityFkList Touchback 1 touchbackentityfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Entity 1 orCollection
			for (Iterator i8 = orCollection.iterator(); i8.hasNext();) {
				EntityBO temp8 = (EntityBO) i8.next();
				TouchBO a8TouchBO = new TouchBO();
				a8TouchBO.setEntityId(temp8.getEntityId());
				TouchFacade aTouchFacade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");				
				Collection touchentityfklistL = aTouchFacade.retrieve(a8TouchBO);
				if (touchentityfklistL.size() > 0) {
					Collection touchentityfklistNL = new ArrayList();
					for (Iterator i = touchentityfklistL.iterator(); i.hasNext();) {
						TouchBO temp = (TouchBO) i.next();
						touchentityfklistNL.add(temp);	
					}
					temp8.setTouchEntityFkList(touchentityfklistNL);
//retrieveMaster BaseObject: TouchEntityFkList Touch 1 touchentityfklistNL
//retrieveMaster BaseObject: TouchEntityFkList Touch 1 touchentityfklistNL
//retrieveMaster BaseObject: TouchEntityFkList Touch 1 touchentityfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Entity Entity 1 orCollection
			for (Iterator i9 = orCollection.iterator(); i9.hasNext();) {
				EntityBO temp9 = (EntityBO) i9.next();					
				CompanyBO a9CompanyBO = new CompanyBO();
				a9CompanyBO.setCompanyId(temp9.getCompanyId()); 
				CompanyFacade a9CompanyFacade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");				
				Collection entitycompanyfk = a9CompanyFacade.retrieve(a9CompanyBO);	
				if (entitycompanyfk.size() > 0) {	
					temp9.setEntityCompanyFkBO((CompanyBO) entitycompanyfk.toArray()[0]);
//retrieveDetail EntityCompanyFk 1 entitycompanyfk
//retrieveDetail EntityCompanyFk 1 entitycompanyfk
    			for (Iterator i10 = entitycompanyfk.iterator(); i10.hasNext();) {
    				CompanyBO temp10 = (CompanyBO) i10.next();
    				PersonBO a10PersonBO = new PersonBO();
    				a10PersonBO.setCompanyId(temp10.getCompanyId());
    				PersonFacade aPersonFacade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");				
    				Collection companyfklistL = aPersonFacade.retrieve(a10PersonBO);
    				if (companyfklistL.size() > 0) {
    					Collection companyfklistNL = new ArrayList();
    					for (Iterator i = companyfklistL.iterator(); i.hasNext();) {
    						PersonBO temp = (PersonBO) i.next();
    						companyfklistNL.add(temp);	
    					}
    					temp10.setCompanyFkList(companyfklistNL);
//retrieveMaster BaseObject: CompanyFkList Person 1 companyfklistNL
//retrieveDetail CompanyFkList 1 companyfklistNL
//retrieveDetail CompanyFkList 1 companyfklistNL
    				}
    			}			
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Entity Entity 1 orCollection
			for (Iterator i11 = orCollection.iterator(); i11.hasNext();) {
				EntityBO temp11 = (EntityBO) i11.next();					
				PersonBO a11PersonBO = new PersonBO();
				a11PersonBO.setPersonId(temp11.getPersonId()); 
				PersonFacade a11PersonFacade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");				
				Collection entitypersonfk = a11PersonFacade.retrieve(a11PersonBO);	
				if (entitypersonfk.size() > 0) {	
					temp11.setEntityPersonFkBO((PersonBO) entitypersonfk.toArray()[0]);
//retrieveDetail EntityPersonFk 1 entitypersonfk
    			for (Iterator i12 = entitypersonfk.iterator(); i12.hasNext();) {
    				PersonBO temp12 = (PersonBO) i12.next();
    				EmailBO a12EmailBO = new EmailBO();
    				a12EmailBO.setPersonId(temp12.getPersonId());
    				EmailFacade aEmailFacade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");				
    				Collection emailpersonfklistL = aEmailFacade.retrieve(a12EmailBO);
    				if (emailpersonfklistL.size() > 0) {
    					Collection emailpersonfklistNL = new ArrayList();
    					for (Iterator i = emailpersonfklistL.iterator(); i.hasNext();) {
    						EmailBO temp = (EmailBO) i.next();
    						emailpersonfklistNL.add(temp);	
    					}
    					temp12.setEmailPersonFkList(emailpersonfklistNL);
//retrieveMaster BaseObject: EmailPersonFkList Email 1 emailpersonfklistNL
    				}
    			}			
//retrieveDetail EntityPersonFk 1 entitypersonfk
//retrieveMaster BaseObject: EntityPersonFk Person 1 entitypersonfk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Entityfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Entityfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findEntity returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a EntityDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a EntityDTO providing the primary keys specifying which DTO to update.
	 * @param dto a EntityDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(EntityBO pkbo, EntityBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("EntityBO to save is null");
            return;
		}     
        try  {
			EntityDAO dao = (EntityDAO) DataAccessFactory.getDAO("EntityDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save EntityDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Entityfacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Entityfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new EntityBO using the values in the given bo.
	 * 
	 * @param bo a EntityBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(EntityBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("EntityBO to create is null");
            return;
		}
        try  {
        	EntityDAO dao = (EntityDAO) DataAccessFactory.getDAO("EntityDAO");       	
			EntityDTO baseDTO = bo.getDTO();	
			baseDTO.setEntityId(dao.getNewPK()); //creates the primary key id value
			dao.insert(baseDTO, user);
            log.debug("Successful Create EntityBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Entityfacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Entityfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a EntityBO using the bo provided as the lookup values.
	 * 
	 * @param bo a EntityBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(EntityBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("EntityBO to delete is null");
            return;
		}		       
        try  {
			EntityDAO dao = (EntityDAO) DataAccessFactory.getDAO("EntityDAO");	
			EntityDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in EntityCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in EntityCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}