package com.neos.marketing.facade;

/**
 * Facade is an for interface for Facades.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    December 1, 2003
 * @version    1.0
 */
public interface Facade {
	
}