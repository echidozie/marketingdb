package com.neos.marketing.facade;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *  FacadeFactory is a factory for Facade Implementors.
 *
 *@author     NEOS Software, Inc.
 *@created    December 1, 2003
 *@version    1.0
 */
public class FacadeFactory {
	private static Log log = LogFactory.getLog("com.neos.marketing.facade.FacadeFactory");
	private static Map facadeHash = new HashMap();
	
	static {
		try {
			facadeHash.put("EntityFacade","com.neos.marketing.facade.EntityFacade"); 
			facadeHash.put("TouchbackFacade","com.neos.marketing.facade.TouchbackFacade"); 
			facadeHash.put("CompanyFacade","com.neos.marketing.facade.CompanyFacade"); 
			facadeHash.put("EmailFacade","com.neos.marketing.facade.EmailFacade"); 
			facadeHash.put("NoteFacade","com.neos.marketing.facade.NoteFacade"); 
			facadeHash.put("DownloadsFacade","com.neos.marketing.facade.DownloadsFacade"); 
			facadeHash.put("VisitorsFacade","com.neos.marketing.facade.VisitorsFacade"); 
			facadeHash.put("TouchFacade","com.neos.marketing.facade.TouchFacade"); 
			facadeHash.put("PersonFacade","com.neos.marketing.facade.PersonFacade"); 
			facadeHash.put("DetailFacade","com.neos.marketing.facade.DetailFacade"); 
			facadeHash.put("SourceFacade","com.neos.marketing.facade.SourceFacade"); 
			java.util.ResourceBundle props =null; //= java.util.ResourceBundle.getBundle("com.neos.marketing.facade.facade");
			if (props != null) {
				for (Enumeration en = props.getKeys(); en.hasMoreElements();) {
					String key = (String) en.nextElement();
					facadeHash.put(key, props.getString(key));
				}
			}
		} catch (Exception e) {
			//Don't care about exception.
		}
	}
		
	/**
	 * return an instance of the specified facade.  The actual class to
	 * return is determined by looking for the key in a properties file.  If
	 * the key exists, that is the facade that will be returned, otherwise
	 * the default facade for the specified object will be returned.
	 * @param key the key of the desired facade.
	 * @return facade the desired facade.
	 */
	public static Facade getFacade(String key) {
		Facade facade = null;
		try {		
			String facadeName = (String) facadeHash.get(key);
			facade = (Facade) Class.forName(facadeName).newInstance();
		} catch (InstantiationException ie) {
			log.error(ie.getMessage());
		} catch (IllegalAccessException iae) {
			log.error(iae.getMessage());
		} catch (ClassNotFoundException cnfe) {
			log.error(cnfe.getMessage());
		}
		return facade;
	}
	
}
