  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for PersonDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class PersonFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public PersonFacade() {
   	}

	/**
	 * Retrieve the complete list of PersonBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new PersonBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new PersonBO());
	}

	/**
	 * Retrieve the complete list of PersonBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(PersonBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(PersonBO bo) throws ApplicationException {
		int cnt = 0;
    	PersonDAO dao = (PersonDAO) DataAccessFactory.getDAO("PersonDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Personfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of PersonBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(PersonBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	PersonDAO dao = (PersonDAO) DataAccessFactory.getDAO("PersonDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					PersonDTO baseDTO = (PersonDTO) i.next();
					PersonBO classBO = new PersonBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Personfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Personfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findPerson returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of PersonBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(PersonBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		PersonDAO dao = (PersonDAO) DataAccessFactory.getDAO("PersonDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					PersonDTO baseDTO = (PersonDTO) i.next();
					PersonBO classBO = new PersonBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Person 1 orCollection
			for (Iterator i1 = orCollection.iterator(); i1.hasNext();) {
				PersonBO temp1 = (PersonBO) i1.next();
				EmailBO a1EmailBO = new EmailBO();
				a1EmailBO.setPersonId(temp1.getPersonId());
				EmailFacade aEmailFacade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");				
				Collection emailpersonfklistL = aEmailFacade.retrieve(a1EmailBO);
				if (emailpersonfklistL.size() > 0) {
					Collection emailpersonfklistNL = new ArrayList();
					for (Iterator i = emailpersonfklistL.iterator(); i.hasNext();) {
						EmailBO temp = (EmailBO) i.next();
						emailpersonfklistNL.add(temp);	
					}
					temp1.setEmailPersonFkList(emailpersonfklistNL);
//retrieveMaster BaseObject: EmailPersonFkList Email 1 emailpersonfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Person 1 orCollection
			for (Iterator i2 = orCollection.iterator(); i2.hasNext();) {
				PersonBO temp2 = (PersonBO) i2.next();
				EntityBO a2EntityBO = new EntityBO();
				a2EntityBO.setPersonId(temp2.getPersonId());
				EntityFacade aEntityFacade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");				
				Collection entitypersonfklistL = aEntityFacade.retrieve(a2EntityBO);
				if (entitypersonfklistL.size() > 0) {
					Collection entitypersonfklistNL = new ArrayList();
					for (Iterator i = entitypersonfklistL.iterator(); i.hasNext();) {
						EntityBO temp = (EntityBO) i.next();
						entitypersonfklistNL.add(temp);	
					}
					temp2.setEntityPersonFkList(entitypersonfklistNL);
//retrieveMaster BaseObject: EntityPersonFkList Entity 1 entitypersonfklistNL
//retrieveMaster BaseObject: EntityPersonFkList Entity 1 entitypersonfklistNL
//retrieveDetail EntityPersonFkList 1 entitypersonfklistNL
//retrieveDetail EntityPersonFkList 1 entitypersonfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Person Person 1 orCollection
			for (Iterator i3 = orCollection.iterator(); i3.hasNext();) {
				PersonBO temp3 = (PersonBO) i3.next();					
				CompanyBO a3CompanyBO = new CompanyBO();
				a3CompanyBO.setCompanyId(temp3.getCompanyId()); 
				CompanyFacade a3CompanyFacade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");				
				Collection companyfk = a3CompanyFacade.retrieve(a3CompanyBO);	
				if (companyfk.size() > 0) {	
					temp3.setCompanyFkBO((CompanyBO) companyfk.toArray()[0]);
//retrieveDetail CompanyFk 1 companyfk
//retrieveDetail CompanyFk 1 companyfk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Personfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Personfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findPerson returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of PersonBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(PersonBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		PersonDAO dao = (PersonDAO) DataAccessFactory.getDAO("PersonDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					PersonDTO baseDTO = (PersonDTO) i.next();
					PersonBO classBO = new PersonBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Person 1 orCollection
			for (Iterator i4 = orCollection.iterator(); i4.hasNext();) {
				PersonBO temp4 = (PersonBO) i4.next();
				EmailBO a4EmailBO = new EmailBO();
				a4EmailBO.setPersonId(temp4.getPersonId());
				EmailFacade aEmailFacade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");				
				Collection emailpersonfklistL = aEmailFacade.retrieve(a4EmailBO);
				if (emailpersonfklistL.size() > 0) {
					Collection emailpersonfklistNL = new ArrayList();
					for (Iterator i = emailpersonfklistL.iterator(); i.hasNext();) {
						EmailBO temp = (EmailBO) i.next();
						emailpersonfklistNL.add(temp);	
					}
					temp4.setEmailPersonFkList(emailpersonfklistNL);
//retrieveMaster BaseObject: EmailPersonFkList Email 1 emailpersonfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Person 1 orCollection
			for (Iterator i5 = orCollection.iterator(); i5.hasNext();) {
				PersonBO temp5 = (PersonBO) i5.next();
				EntityBO a5EntityBO = new EntityBO();
				a5EntityBO.setPersonId(temp5.getPersonId());
				EntityFacade aEntityFacade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");				
				Collection entitypersonfklistL = aEntityFacade.retrieve(a5EntityBO);
				if (entitypersonfklistL.size() > 0) {
					Collection entitypersonfklistNL = new ArrayList();
					for (Iterator i = entitypersonfklistL.iterator(); i.hasNext();) {
						EntityBO temp = (EntityBO) i.next();
						entitypersonfklistNL.add(temp);	
					}
					temp5.setEntityPersonFkList(entitypersonfklistNL);
//retrieveMaster BaseObject: EntityPersonFkList Entity 1 entitypersonfklistNL
//retrieveMaster BaseObject: EntityPersonFkList Entity 1 entitypersonfklistNL
//retrieveDetail EntityPersonFkList 1 entitypersonfklistNL
//retrieveDetail EntityPersonFkList 1 entitypersonfklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Person Person 1 orCollection
			for (Iterator i6 = orCollection.iterator(); i6.hasNext();) {
				PersonBO temp6 = (PersonBO) i6.next();					
				CompanyBO a6CompanyBO = new CompanyBO();
				a6CompanyBO.setCompanyId(temp6.getCompanyId()); 
				CompanyFacade a6CompanyFacade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");				
				Collection companyfk = a6CompanyFacade.retrieve(a6CompanyBO);	
				if (companyfk.size() > 0) {	
					temp6.setCompanyFkBO((CompanyBO) companyfk.toArray()[0]);
//retrieveDetail CompanyFk 1 companyfk
//retrieveDetail CompanyFk 1 companyfk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Personfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Personfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findPerson returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a PersonDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a PersonDTO providing the primary keys specifying which DTO to update.
	 * @param dto a PersonDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(PersonBO pkbo, PersonBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("PersonBO to save is null");
            return;
		}     
        try  {
			PersonDAO dao = (PersonDAO) DataAccessFactory.getDAO("PersonDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save PersonDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Personfacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Personfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new PersonBO using the values in the given bo.
	 * 
	 * @param bo a PersonBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(PersonBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("PersonBO to create is null");
            return;
		}
        try  {
        	PersonDAO dao = (PersonDAO) DataAccessFactory.getDAO("PersonDAO");       	
			PersonDTO baseDTO = bo.getDTO();	
			baseDTO.setPersonId(dao.getNewPK());
			dao.insert(baseDTO, user);
            log.debug("Successful Create PersonBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Personfacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Personfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a PersonBO using the bo provided as the lookup values.
	 * 
	 * @param bo a PersonBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(PersonBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("PersonBO to delete is null");
            return;
		}		       
        try  {
			PersonDAO dao = (PersonDAO) DataAccessFactory.getDAO("PersonDAO");	
			PersonDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in PersonCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in PersonCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}