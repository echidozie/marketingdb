  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for SourceDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class SourceFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public SourceFacade() {
   	}

	/**
	 * Retrieve the complete list of SourceBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new SourceBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new SourceBO());
	}

	/**
	 * Retrieve the complete list of SourceBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(SourceBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(SourceBO bo) throws ApplicationException {
		int cnt = 0;
    	SourceDAO dao = (SourceDAO) DataAccessFactory.getDAO("SourceDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Sourcefacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of SourceBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(SourceBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	SourceDAO dao = (SourceDAO) DataAccessFactory.getDAO("SourceDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					SourceDTO baseDTO = (SourceDTO) i.next();
					SourceBO classBO = new SourceBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Sourcefacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Sourcefacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findSource returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of SourceBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(SourceBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		SourceDAO dao = (SourceDAO) DataAccessFactory.getDAO("SourceDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					SourceDTO baseDTO = (SourceDTO) i.next();
					SourceBO classBO = new SourceBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Source 1 orCollection
			for (Iterator i1 = orCollection.iterator(); i1.hasNext();) {
				SourceBO temp1 = (SourceBO) i1.next();
				DetailBO a1DetailBO = new DetailBO();
				a1DetailBO.setSourceId(temp1.getSourceId());
				DetailFacade aDetailFacade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");				
				Collection detailsourcefklistL = aDetailFacade.retrieve(a1DetailBO);
				if (detailsourcefklistL.size() > 0) {
					Collection detailsourcefklistNL = new ArrayList();
					for (Iterator i = detailsourcefklistL.iterator(); i.hasNext();) {
						DetailBO temp = (DetailBO) i.next();
						detailsourcefklistNL.add(temp);	
					}
					temp1.setDetailSourceFkList(detailsourcefklistNL);
//retrieveMaster BaseObject: DetailSourceFkList Detail 1 detailsourcefklistNL
//retrieveDetail DetailSourceFkList 1 detailsourcefklistNL
//retrieveDetail DetailSourceFkList 1 detailsourcefklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Source 1 orCollection
			for (Iterator i2 = orCollection.iterator(); i2.hasNext();) {
				SourceBO temp2 = (SourceBO) i2.next();
				TouchBO a2TouchBO = new TouchBO();
				a2TouchBO.setSourceId(temp2.getSourceId());
				TouchFacade aTouchFacade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");				
				Collection touchsourcefklistL = aTouchFacade.retrieve(a2TouchBO);
				if (touchsourcefklistL.size() > 0) {
					Collection touchsourcefklistNL = new ArrayList();
					for (Iterator i = touchsourcefklistL.iterator(); i.hasNext();) {
						TouchBO temp = (TouchBO) i.next();
						touchsourcefklistNL.add(temp);	
					}
					temp2.setTouchSourceFkList(touchsourcefklistNL);
//retrieveMaster BaseObject: TouchSourceFkList Touch 1 touchsourcefklistNL
//retrieveMaster BaseObject: TouchSourceFkList Touch 1 touchsourcefklistNL
//retrieveMaster BaseObject: TouchSourceFkList Touch 1 touchsourcefklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Source 1 orCollection
			for (Iterator i3 = orCollection.iterator(); i3.hasNext();) {
				SourceBO temp3 = (SourceBO) i3.next();
				TouchbackBO a3TouchbackBO = new TouchbackBO();
				a3TouchbackBO.setSourceId(temp3.getSourceId());
				TouchbackFacade aTouchbackFacade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");				
				Collection touchbacksourcefklistL = aTouchbackFacade.retrieve(a3TouchbackBO);
				if (touchbacksourcefklistL.size() > 0) {
					Collection touchbacksourcefklistNL = new ArrayList();
					for (Iterator i = touchbacksourcefklistL.iterator(); i.hasNext();) {
						TouchbackBO temp = (TouchbackBO) i.next();
						touchbacksourcefklistNL.add(temp);	
					}
					temp3.setTouchbackSourceFkList(touchbacksourcefklistNL);
//retrieveMaster BaseObject: TouchbackSourceFkList Touchback 1 touchbacksourcefklistNL
//retrieveMaster BaseObject: TouchbackSourceFkList Touchback 1 touchbacksourcefklistNL
//retrieveMaster BaseObject: TouchbackSourceFkList Touchback 1 touchbacksourcefklistNL
				}
			}			
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Sourcefacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Sourcefacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findSource returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of SourceBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(SourceBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		SourceDAO dao = (SourceDAO) DataAccessFactory.getDAO("SourceDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					SourceDTO baseDTO = (SourceDTO) i.next();
					SourceBO classBO = new SourceBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveDetail Source 1 orCollection
			for (Iterator i4 = orCollection.iterator(); i4.hasNext();) {
				SourceBO temp4 = (SourceBO) i4.next();
				DetailBO a4DetailBO = new DetailBO();
				a4DetailBO.setSourceId(temp4.getSourceId());
				DetailFacade aDetailFacade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");				
				Collection detailsourcefklistL = aDetailFacade.retrieve(a4DetailBO);
				if (detailsourcefklistL.size() > 0) {
					Collection detailsourcefklistNL = new ArrayList();
					for (Iterator i = detailsourcefklistL.iterator(); i.hasNext();) {
						DetailBO temp = (DetailBO) i.next();
						detailsourcefklistNL.add(temp);	
					}
					temp4.setDetailSourceFkList(detailsourcefklistNL);
//retrieveMaster BaseObject: DetailSourceFkList Detail 1 detailsourcefklistNL
//retrieveDetail DetailSourceFkList 1 detailsourcefklistNL
//retrieveDetail DetailSourceFkList 1 detailsourcefklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Source 1 orCollection
			for (Iterator i5 = orCollection.iterator(); i5.hasNext();) {
				SourceBO temp5 = (SourceBO) i5.next();
				TouchBO a5TouchBO = new TouchBO();
				a5TouchBO.setSourceId(temp5.getSourceId());
				TouchFacade aTouchFacade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");				
				Collection touchsourcefklistL = aTouchFacade.retrieve(a5TouchBO);
				if (touchsourcefklistL.size() > 0) {
					Collection touchsourcefklistNL = new ArrayList();
					for (Iterator i = touchsourcefklistL.iterator(); i.hasNext();) {
						TouchBO temp = (TouchBO) i.next();
						touchsourcefklistNL.add(temp);	
					}
					temp5.setTouchSourceFkList(touchsourcefklistNL);
//retrieveMaster BaseObject: TouchSourceFkList Touch 1 touchsourcefklistNL
//retrieveMaster BaseObject: TouchSourceFkList Touch 1 touchsourcefklistNL
//retrieveMaster BaseObject: TouchSourceFkList Touch 1 touchsourcefklistNL
				}
			}			
//
//Set cname to orCollection - oho
//retrieveDetail Source 1 orCollection
			for (Iterator i6 = orCollection.iterator(); i6.hasNext();) {
				SourceBO temp6 = (SourceBO) i6.next();
				TouchbackBO a6TouchbackBO = new TouchbackBO();
				a6TouchbackBO.setSourceId(temp6.getSourceId());
				TouchbackFacade aTouchbackFacade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");				
				Collection touchbacksourcefklistL = aTouchbackFacade.retrieve(a6TouchbackBO);
				if (touchbacksourcefklistL.size() > 0) {
					Collection touchbacksourcefklistNL = new ArrayList();
					for (Iterator i = touchbacksourcefklistL.iterator(); i.hasNext();) {
						TouchbackBO temp = (TouchbackBO) i.next();
						touchbacksourcefklistNL.add(temp);	
					}
					temp6.setTouchbackSourceFkList(touchbacksourcefklistNL);
//retrieveMaster BaseObject: TouchbackSourceFkList Touchback 1 touchbacksourcefklistNL
//retrieveMaster BaseObject: TouchbackSourceFkList Touchback 1 touchbacksourcefklistNL
//retrieveMaster BaseObject: TouchbackSourceFkList Touchback 1 touchbacksourcefklistNL
				}
			}			
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Sourcefacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Sourcefacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findSource returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a SourceDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a SourceDTO providing the primary keys specifying which DTO to update.
	 * @param dto a SourceDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(SourceBO pkbo, SourceBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("SourceBO to save is null");
            return;
		}     
        try  {
			SourceDAO dao = (SourceDAO) DataAccessFactory.getDAO("SourceDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save SourceDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Sourcefacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Sourcefacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new SourceBO using the values in the given bo.
	 * 
	 * @param bo a SourceBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(SourceBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("SourceBO to create is null");
            return;
		}
        try  {
        	SourceDAO dao = (SourceDAO) DataAccessFactory.getDAO("SourceDAO");       	
			SourceDTO baseDTO = bo.getDTO();	
			baseDTO.setSourceId(dao.getNewPK());
			dao.insert(baseDTO, user);
            log.debug("Successful Create SourceBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Sourcefacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Sourcefacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a SourceBO using the bo provided as the lookup values.
	 * 
	 * @param bo a SourceBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(SourceBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("SourceBO to delete is null");
            return;
		}		       
        try  {
			SourceDAO dao = (SourceDAO) DataAccessFactory.getDAO("SourceDAO");	
			SourceDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in SourceCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in SourceCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}