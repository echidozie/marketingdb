  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for TouchDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class TouchFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public TouchFacade() {
   	}

	/**
	 * Retrieve the complete list of TouchBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new TouchBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new TouchBO());
	}

	/**
	 * Retrieve the complete list of TouchBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(TouchBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(TouchBO bo) throws ApplicationException {
		int cnt = 0;
    	TouchDAO dao = (TouchDAO) DataAccessFactory.getDAO("TouchDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Touchfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of TouchBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(TouchBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	TouchDAO dao = (TouchDAO) DataAccessFactory.getDAO("TouchDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					TouchDTO baseDTO = (TouchDTO) i.next();
					TouchBO classBO = new TouchBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Touchfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Touchfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findTouch returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of TouchBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(TouchBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		TouchDAO dao = (TouchDAO) DataAccessFactory.getDAO("TouchDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					TouchDTO baseDTO = (TouchDTO) i.next();
					TouchBO classBO = new TouchBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touch Touch 1 orCollection
			for (Iterator i1 = orCollection.iterator(); i1.hasNext();) {
				TouchBO temp1 = (TouchBO) i1.next();					
				SourceBO a1SourceBO = new SourceBO();
				a1SourceBO.setSourceId(temp1.getSourceId()); 
				SourceFacade a1SourceFacade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");				
				Collection touchsourcefk = a1SourceFacade.retrieve(a1SourceBO);	
				if (touchsourcefk.size() > 0) {	
					temp1.setTouchSourceFkBO((SourceBO) touchsourcefk.toArray()[0]);
//retrieveDetail TouchSourceFk 1 touchsourcefk
//retrieveDetail TouchSourceFk 1 touchsourcefk
//retrieveDetail TouchSourceFk 1 touchsourcefk
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touch Touch 1 orCollection
			for (Iterator i2 = orCollection.iterator(); i2.hasNext();) {
				TouchBO temp2 = (TouchBO) i2.next();					
				EntityBO a2EntityBO = new EntityBO();
				a2EntityBO.setEntityId(temp2.getEntityId()); 
				EntityFacade a2EntityFacade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");				
				Collection touchentityfk = a2EntityFacade.retrieve(a2EntityBO);	
				if (touchentityfk.size() > 0) {	
					temp2.setTouchEntityFkBO((EntityBO) touchentityfk.toArray()[0]);
//retrieveDetail TouchEntityFk 1 touchentityfk
//retrieveDetail TouchEntityFk 1 touchentityfk
//retrieveMaster BaseObject: TouchEntityFk Entity 1 touchentityfk
//retrieveMaster BaseObject: TouchEntityFk Entity 1 touchentityfk
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touch Touch 1 orCollection
			for (Iterator i3 = orCollection.iterator(); i3.hasNext();) {
				TouchBO temp3 = (TouchBO) i3.next();					
				DetailBO a3DetailBO = new DetailBO();
				a3DetailBO.setDetailId(temp3.getDetailId()); 
				DetailFacade a3DetailFacade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");				
				Collection touchdetailfk = a3DetailFacade.retrieve(a3DetailBO);	
				if (touchdetailfk.size() > 0) {	
					temp3.setTouchDetailFkBO((DetailBO) touchdetailfk.toArray()[0]);
//retrieveDetail TouchDetailFk 1 touchdetailfk
//retrieveDetail TouchDetailFk 1 touchdetailfk
//retrieveMaster BaseObject: TouchDetailFk Detail 1 touchdetailfk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Touchfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Touchfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findTouch returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of TouchBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(TouchBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		TouchDAO dao = (TouchDAO) DataAccessFactory.getDAO("TouchDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					TouchDTO baseDTO = (TouchDTO) i.next();
					TouchBO classBO = new TouchBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touch Touch 1 orCollection
			for (Iterator i4 = orCollection.iterator(); i4.hasNext();) {
				TouchBO temp4 = (TouchBO) i4.next();					
				SourceBO a4SourceBO = new SourceBO();
				a4SourceBO.setSourceId(temp4.getSourceId()); 
				SourceFacade a4SourceFacade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");				
				Collection touchsourcefk = a4SourceFacade.retrieve(a4SourceBO);	
				if (touchsourcefk.size() > 0) {	
					temp4.setTouchSourceFkBO((SourceBO) touchsourcefk.toArray()[0]);
//retrieveDetail TouchSourceFk 1 touchsourcefk
//retrieveDetail TouchSourceFk 1 touchsourcefk
//retrieveDetail TouchSourceFk 1 touchsourcefk
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touch Touch 1 orCollection
			for (Iterator i5 = orCollection.iterator(); i5.hasNext();) {
				TouchBO temp5 = (TouchBO) i5.next();					
				EntityBO a5EntityBO = new EntityBO();
				a5EntityBO.setEntityId(temp5.getEntityId()); 
				EntityFacade a5EntityFacade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");				
				Collection touchentityfk = a5EntityFacade.retrieve(a5EntityBO);	
				if (touchentityfk.size() > 0) {	
					temp5.setTouchEntityFkBO((EntityBO) touchentityfk.toArray()[0]);
//retrieveDetail TouchEntityFk 1 touchentityfk
//retrieveDetail TouchEntityFk 1 touchentityfk
//retrieveMaster BaseObject: TouchEntityFk Entity 1 touchentityfk
//retrieveMaster BaseObject: TouchEntityFk Entity 1 touchentityfk
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touch Touch 1 orCollection
			for (Iterator i6 = orCollection.iterator(); i6.hasNext();) {
				TouchBO temp6 = (TouchBO) i6.next();					
				DetailBO a6DetailBO = new DetailBO();
				a6DetailBO.setDetailId(temp6.getDetailId()); 
				DetailFacade a6DetailFacade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");				
				Collection touchdetailfk = a6DetailFacade.retrieve(a6DetailBO);	
				if (touchdetailfk.size() > 0) {	
					temp6.setTouchDetailFkBO((DetailBO) touchdetailfk.toArray()[0]);
//retrieveDetail TouchDetailFk 1 touchdetailfk
//retrieveDetail TouchDetailFk 1 touchdetailfk
//retrieveMaster BaseObject: TouchDetailFk Detail 1 touchdetailfk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Touchfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Touchfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findTouch returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a TouchDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a TouchDTO providing the primary keys specifying which DTO to update.
	 * @param dto a TouchDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(TouchBO pkbo, TouchBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("TouchBO to save is null");
            return;
		}     
        try  {
			TouchDAO dao = (TouchDAO) DataAccessFactory.getDAO("TouchDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save TouchDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Touchfacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Touchfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new TouchBO using the values in the given bo.
	 * 
	 * @param bo a TouchBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(TouchBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("TouchBO to create is null");
            return;
		}
        try  {
        	TouchDAO dao = (TouchDAO) DataAccessFactory.getDAO("TouchDAO");       	
			TouchDTO baseDTO = bo.getDTO();	
			baseDTO.setTouchId(dao.getNewPK());
			dao.insert(baseDTO, user);
            log.debug("Successful Create TouchBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Touchfacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Touchfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a TouchBO using the bo provided as the lookup values.
	 * 
	 * @param bo a TouchBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(TouchBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("TouchBO to delete is null");
            return;
		}		       
        try  {
			TouchDAO dao = (TouchDAO) DataAccessFactory.getDAO("TouchDAO");	
			TouchDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in TouchCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in TouchCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}