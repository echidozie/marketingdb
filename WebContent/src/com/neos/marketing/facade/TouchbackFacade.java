  package com.neos.marketing.facade;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neos.marketing.bo.*;
import com.neos.marketing.dto.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.util.exception.*;


/**
 * Facade for TouchbackDTO data.  Used for persisting the dto using
 * the DAO layer.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author NEOS Software, Inc.
 * @version 1.0
 */

public class TouchbackFacade implements Facade {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public TouchbackFacade() {
   	}

	/**
	 * Retrieve the complete list of TouchbackBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection list() throws ApplicationException {
		return retrieve(new TouchbackBO(), 0, -1, null, null);
	}

	/** Get a count of number of rows in table **/
	public int getCount() throws ApplicationException {
		return getCount(new TouchbackBO());
	}

	/**
	 * Retrieve the complete list of TouchbackBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(TouchbackBO bo) throws ApplicationException {
		return (retrieve(bo, 0, -1, null, null) ); 
	}
	/** return an int representing the number of rows in da table **/
	public int getCount(TouchbackBO bo) throws ApplicationException {
		int cnt = 0;
    	TouchbackDAO dao = (TouchbackDAO) DataAccessFactory.getDAO("TouchbackDAO");
        try {
			cnt = dao.count(bo.getDTO()); 
			
			
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Usersfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Touchbackfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("Count returned: "+ cnt);
        }        
        return cnt;
    }
	
	/**
	 * Retrieve the complete list of TouchbackBOs.
	 *
	 * @return a collection containing the results.
	 * @exception an exception in processing.
	 */
	public Collection retrieve(TouchbackBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException {
        Collection orCollection = new ArrayList();
    	TouchbackDAO dao = (TouchbackDAO) DataAccessFactory.getDAO("TouchbackDAO");
        try {
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			}
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					TouchbackDTO baseDTO = (TouchbackDTO) i.next();
					TouchbackBO classBO = new TouchbackBO(baseDTO);				
					orCollection.add(classBO);	
				}
			}
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Touchbackfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Touchbackfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findTouchback returned: "+orCollection.size()+" objects");
        }        
        return orCollection;
    }


	/**
	 * Retrieve the complete list of TouchbackBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(TouchbackBO bo) throws ApplicationException{
        Collection orCollection = new ArrayList();
		TouchbackDAO dao = (TouchbackDAO) DataAccessFactory.getDAO("TouchbackDAO");
        try {	
			Collection returnCollection = dao.select(bo.getDTO()); 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					TouchbackDTO baseDTO = (TouchbackDTO) i.next();
					TouchbackBO classBO = new TouchbackBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touchback Touchback 1 orCollection
			for (Iterator i1 = orCollection.iterator(); i1.hasNext();) {
				TouchbackBO temp1 = (TouchbackBO) i1.next();					
				DetailBO a1DetailBO = new DetailBO();
				a1DetailBO.setDetailId(temp1.getDetailId()); 
				DetailFacade a1DetailFacade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");				
				Collection touchbackdetailfk = a1DetailFacade.retrieve(a1DetailBO);	
				if (touchbackdetailfk.size() > 0) {	
					temp1.setTouchbackDetailFkBO((DetailBO) touchbackdetailfk.toArray()[0]);
//retrieveDetail TouchbackDetailFk 1 touchbackdetailfk
//retrieveDetail TouchbackDetailFk 1 touchbackdetailfk
//retrieveMaster BaseObject: TouchbackDetailFk Detail 1 touchbackdetailfk
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touchback Touchback 1 orCollection
			for (Iterator i2 = orCollection.iterator(); i2.hasNext();) {
				TouchbackBO temp2 = (TouchbackBO) i2.next();					
				EntityBO a2EntityBO = new EntityBO();
				a2EntityBO.setEntityId(temp2.getEntityId()); 
				EntityFacade a2EntityFacade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");				
				Collection touchbackentityfk = a2EntityFacade.retrieve(a2EntityBO);	
				if (touchbackentityfk.size() > 0) {	
					temp2.setTouchbackEntityFkBO((EntityBO) touchbackentityfk.toArray()[0]);
//retrieveDetail TouchbackEntityFk 1 touchbackentityfk
//retrieveDetail TouchbackEntityFk 1 touchbackentityfk
//retrieveMaster BaseObject: TouchbackEntityFk Entity 1 touchbackentityfk
//retrieveMaster BaseObject: TouchbackEntityFk Entity 1 touchbackentityfk
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touchback Touchback 1 orCollection
			for (Iterator i3 = orCollection.iterator(); i3.hasNext();) {
				TouchbackBO temp3 = (TouchbackBO) i3.next();					
				SourceBO a3SourceBO = new SourceBO();
				a3SourceBO.setSourceId(temp3.getSourceId()); 
				SourceFacade a3SourceFacade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");				
				Collection touchbacksourcefk = a3SourceFacade.retrieve(a3SourceBO);	
				if (touchbacksourcefk.size() > 0) {	
					temp3.setTouchbackSourceFkBO((SourceBO) touchbacksourcefk.toArray()[0]);
//retrieveDetail TouchbackSourceFk 1 touchbacksourcefk
//retrieveDetail TouchbackSourceFk 1 touchbacksourcefk
//retrieveDetail TouchbackSourceFk 1 touchbacksourcefk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Touchbackfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Touchbackfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findTouchback returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * Retrieve a subset list of TouchbackBOs and all of their 
	 * associated objects.
	 *
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public Collection retrieveWithNested(TouchbackBO bo, int startRecord, int returnSetSize, String sort_cols, String sort_dir) throws ApplicationException{
        Collection orCollection = new ArrayList();
		TouchbackDAO dao = (TouchbackDAO) DataAccessFactory.getDAO("TouchbackDAO");
        try {	
			Collection returnCollection = null;
			if (sort_cols != null && sort_dir != null && sort_cols.length() > 0 && sort_dir.length() > 0) {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize, sort_cols, sort_dir); 
			} else {
				returnCollection = dao.select(bo.getDTO(), startRecord, returnSetSize); 
			} 		
			if (returnCollection != null) {
				for (Iterator i = returnCollection.iterator(); i.hasNext();) {
					TouchbackDTO baseDTO = (TouchbackDTO) i.next();
					TouchbackBO classBO = new TouchbackBO(baseDTO);
					orCollection.add(classBO);
				}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touchback Touchback 1 orCollection
			for (Iterator i4 = orCollection.iterator(); i4.hasNext();) {
				TouchbackBO temp4 = (TouchbackBO) i4.next();					
				DetailBO a4DetailBO = new DetailBO();
				a4DetailBO.setDetailId(temp4.getDetailId()); 
				DetailFacade a4DetailFacade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");				
				Collection touchbackdetailfk = a4DetailFacade.retrieve(a4DetailBO);	
				if (touchbackdetailfk.size() > 0) {	
					temp4.setTouchbackDetailFkBO((DetailBO) touchbackdetailfk.toArray()[0]);
//retrieveDetail TouchbackDetailFk 1 touchbackdetailfk
//retrieveDetail TouchbackDetailFk 1 touchbackdetailfk
//retrieveMaster BaseObject: TouchbackDetailFk Detail 1 touchbackdetailfk
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touchback Touchback 1 orCollection
			for (Iterator i5 = orCollection.iterator(); i5.hasNext();) {
				TouchbackBO temp5 = (TouchbackBO) i5.next();					
				EntityBO a5EntityBO = new EntityBO();
				a5EntityBO.setEntityId(temp5.getEntityId()); 
				EntityFacade a5EntityFacade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");				
				Collection touchbackentityfk = a5EntityFacade.retrieve(a5EntityBO);	
				if (touchbackentityfk.size() > 0) {	
					temp5.setTouchbackEntityFkBO((EntityBO) touchbackentityfk.toArray()[0]);
//retrieveDetail TouchbackEntityFk 1 touchbackentityfk
//retrieveDetail TouchbackEntityFk 1 touchbackentityfk
//retrieveMaster BaseObject: TouchbackEntityFk Entity 1 touchbackentityfk
//retrieveMaster BaseObject: TouchbackEntityFk Entity 1 touchbackentityfk
				}
			}		
//
//Set cname to orCollection - oho
//retrieveMaster BaseObject: Touchback Touchback 1 orCollection
			for (Iterator i6 = orCollection.iterator(); i6.hasNext();) {
				TouchbackBO temp6 = (TouchbackBO) i6.next();					
				SourceBO a6SourceBO = new SourceBO();
				a6SourceBO.setSourceId(temp6.getSourceId()); 
				SourceFacade a6SourceFacade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");				
				Collection touchbacksourcefk = a6SourceFacade.retrieve(a6SourceBO);	
				if (touchbacksourcefk.size() > 0) {	
					temp6.setTouchbackSourceFkBO((SourceBO) touchbacksourcefk.toArray()[0]);
//retrieveDetail TouchbackSourceFk 1 touchbacksourcefk
//retrieveDetail TouchbackSourceFk 1 touchbacksourcefk
//retrieveDetail TouchbackSourceFk 1 touchbacksourcefk
				}
			}		
			}	
        } catch( Exception ae ) {
 			log.error("Error retrieving dto collection in Touchbackfacade:"+ae.getMessage());
 			ae.printStackTrace();
      		throw new ApplicationException("Error retrieving dto collection in Touchbackfacade:"+ae.getMessage(), ae);
        } finally {
            log.debug("findTouchback returned: "+orCollection.size()+" objects");
        }               
        return orCollection;
    }

	/**
	 * update the values for a TouchbackDTO.  The pkdto provides the primary key of the
	 * the object to be updated, the dto provides the new values.  All fields will be updated
	 * that are not null in the dto.  Note that the primary key can, in fact, be updated through
	 * this method if the database will allow it.
	 * 
	 * @param pkdto a TouchbackDTO providing the primary keys specifying which DTO to update.
	 * @param dto a TouchbackDTO providing the values to be used for updating.
	 * @return a collection containing the results.
	 * @throws Exception
	 */	
	public void update(TouchbackBO pkbo, TouchbackBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("TouchbackBO to save is null");
            return;
		}     
        try  {
			TouchbackDAO dao = (TouchbackDAO) DataAccessFactory.getDAO("TouchbackDAO");
			dao.update(pkbo.getPKDTO(), bo.getDTO(), user); 
            log.debug("Successful Save TouchbackDTO");
        } catch(Exception ae ) {
 			log.error("Error saving dto in Touchbackfacade:"+ae.getMessage());
      		throw new ApplicationException("Error saving dto collection in Touchbackfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                                   
        return;
        
    } 

	/**
	 * persist a new TouchbackBO using the values in the given bo.
	 * 
	 * @param bo a TouchbackBO providing the values to be used for inserting.
	 * @return a collection containing the results.
	 * @throws Exception
	 */
	public void insert(TouchbackBO bo, String user) throws ApplicationException {
        if ( bo == null ) {
            log.error("TouchbackBO to create is null");
            return;
		}
        try  {
        	TouchbackDAO dao = (TouchbackDAO) DataAccessFactory.getDAO("TouchbackDAO");       	
			TouchbackDTO baseDTO = bo.getDTO();	
			baseDTO.setTouchbackId(dao.getNewPK());
			dao.insert(baseDTO, user);
            log.debug("Successful Create TouchbackBO");
        } catch( Exception ae ) {
 			log.error("Error creating dto in Touchbackfacade:"+ae.getMessage());
      		throw new ApplicationException("Error creating dto collection in Touchbackfacade:"+ae.getMessage(), ae);
        } finally {
            //
        }                               
        return;
    } 


	/**
	 * delete a TouchbackBO using the bo provided as the lookup values.
	 * 
	 * @param bo a TouchbackBO providing the values to be used for lookup.
	 * @throws Exception
	 */
	public void delete(TouchbackBO bo, String user) throws ApplicationException {
         if ( bo == null ) {
            log.error("TouchbackBO to delete is null");
            return;
		}		       
        try  {
			TouchbackDAO dao = (TouchbackDAO) DataAccessFactory.getDAO("TouchbackDAO");	
			TouchbackDTO pkDTO = bo.getPKDTO();
			dao.delete(pkDTO, user);
        } catch( Exception ae ) {
 			log.error("Error deleting dto in TouchbackCollection:"+ae.getMessage());
      		throw new ApplicationException("Error deleting dto collection in TouchbackCollection:"+ae.getMessage(), ae);
        } finally {
            //
        }              
    } 
	
}