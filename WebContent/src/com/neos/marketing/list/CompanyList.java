package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class CompanyList {
	
	public ArrayList companyList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public CompanyList(){}
	
	public void setCompanyList(ArrayList theList){
		this.companyList = theList; 
	}
	
	public ArrayList getCompanyList(){
		return this.companyList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");    	    
			Collection list = facade.retrieveWithNested(new CompanyBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.companyList.add(o);
	}
	
	public void clear(){
		this.companyList = new ArrayList();
	}

}