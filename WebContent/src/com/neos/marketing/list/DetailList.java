package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class DetailList {
	
	private ArrayList detailList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public DetailList(){}
	
	public void setDetailList(ArrayList theList){
		this.detailList = theList; 
	}
	
	public ArrayList getDetailList(){
		return this.detailList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
DetailFacade facade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");    	    
			Collection list = facade.retrieveWithNested(new DetailBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.detailList.add(o);
	}
	
	public void clear(){
		this.detailList = new ArrayList();
	}

}