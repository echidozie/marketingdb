package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class DownloadsList {
	
	private ArrayList downloadsList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public DownloadsList(){}
	
	public void setDownloadsList(ArrayList theList){
		this.downloadsList = theList; 
	}
	
	public ArrayList getDownloadsList(){
		return this.downloadsList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
DownloadsFacade facade = (DownloadsFacade) FacadeFactory.getFacade("DownloadsFacade");    	    
			Collection list = facade.retrieveWithNested(new DownloadsBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.downloadsList.add(o);
	}
	
	public void clear(){
		this.downloadsList = new ArrayList();
	}

}