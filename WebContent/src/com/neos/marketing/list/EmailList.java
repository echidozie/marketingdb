package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class EmailList {
	
	private ArrayList emailList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public EmailList(){}
	
	public void setEmailList(ArrayList theList){
		this.emailList = theList; 
	}
	
	public ArrayList getEmailList(){
		return this.emailList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
EmailFacade facade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");    	    
			Collection list = facade.retrieveWithNested(new EmailBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.emailList.add(o);
	}
	
	public void clear(){
		this.emailList = new ArrayList();
	}

}