package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class EntityList {
	
	private ArrayList entityList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public EntityList(){}
	
	public void setEntityList(ArrayList theList){
		this.entityList = theList; 
	}
	
	public ArrayList getEntityList(){
		return this.entityList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
EntityFacade facade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");    	    
			Collection list = facade.retrieveWithNested(new EntityBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.entityList.add(o);
	}
	
	public void clear(){
		this.entityList = new ArrayList();
	}

}