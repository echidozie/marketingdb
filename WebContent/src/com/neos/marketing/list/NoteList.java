package com.neos.marketing.list;

import java.util.ArrayList; 
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class NoteList {
	
	private ArrayList noteList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public NoteList(){}
	
	public void setNoteList(ArrayList theList){
		this.noteList = theList; 
	}
	
	public ArrayList getNoteList(){
		return this.noteList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");    	    
			Collection list = facade.retrieveWithNested(new NoteBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.noteList.add(o);
	}
	
	public void clear(){
		this.noteList = new ArrayList();
	}
	
}