package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class PersonList {
	
	private ArrayList personList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public PersonList(){}
	
	public void setPersonList(ArrayList theList){
		this.personList = theList; 
	}
	
	public ArrayList getPersonList(){
		return this.personList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
PersonFacade facade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");    	    
			Collection list = facade.retrieveWithNested(new PersonBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.personList.add(o);
	}
	
	public void clear(){
		this.personList = new ArrayList();
	}

}