package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class SourceList {
	
	private ArrayList sourceList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public SourceList(){}
	
	public void setSourceList(ArrayList theList){
		this.sourceList = theList; 
	}
	
	public ArrayList getSourceList(){
		return this.sourceList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
SourceFacade facade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");    	    
			Collection list = facade.retrieveWithNested(new SourceBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.sourceList.add(o);
	}
	
	public void clear(){
		this.sourceList = new ArrayList();
	}

}