package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class TouchList {
	
	private ArrayList touchList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public TouchList(){}
	
	public void setTouchList(ArrayList theList){
		this.touchList = theList; 
	}
	
	public ArrayList getTouchList(){
		return this.touchList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
TouchFacade facade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");    	    
			Collection list = facade.retrieveWithNested(new TouchBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.touchList.add(o);
	}
	
	public void clear(){
		this.touchList = new ArrayList();
	}

}