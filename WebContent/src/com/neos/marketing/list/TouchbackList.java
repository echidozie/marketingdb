package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class TouchbackList {
	
	private ArrayList touchbackList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public TouchbackList(){}
	
	public void setTouchbackList(ArrayList theList){
		this.touchbackList = theList; 
	}
	
	public ArrayList getTouchbackList(){
		return this.touchbackList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
TouchbackFacade facade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");    	    
			Collection list = facade.retrieveWithNested(new TouchbackBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.touchbackList.add(o);
	}
	
	public void clear(){
		this.touchbackList = new ArrayList();
	}

}