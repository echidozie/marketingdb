package com.neos.marketing.list;

import java.util.ArrayList;
import java.util.Collection;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.manage.*;
import com.neos.marketing.list.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;

public class VisitorsList {
	
	private ArrayList visitorsList = new ArrayList();
	
	private boolean editDisabled = true;
	
	public VisitorsList(){}
	
	public void setVisitorsList(ArrayList theList){
		this.visitorsList = theList; 
	}
	
	public ArrayList getVisitorsList(){
		return this.visitorsList;
	}
	
	public boolean getEditDisabled() {
		return this.editDisabled;
	}
	
	public void setEditDisabled(boolean isActivated) {
		this.editDisabled = isActivated;
	}

	public int getTotalSize(){
		try{
VisitorsFacade facade = (VisitorsFacade) FacadeFactory.getFacade("VisitorsFacade");    	    
			Collection list = facade.retrieveWithNested(new VisitorsBO());
			return list.size();
		}catch(Exception e){
			MessageBean msg = (MessageBean)JSFUtil.getSessionBean("MessageBean");
			msg.display(e.getMessage());
			return 0;
		}
	}
	
	public void add(Object o){
		this.visitorsList.add(o);
	}
	
	public void clear(){
		this.visitorsList = new ArrayList();
	}

}