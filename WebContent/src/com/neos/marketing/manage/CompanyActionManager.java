package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;


public class CompanyActionManager {
	public CompanyActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");
			CompanyBean bean = new CompanyBean();
			CompanyList list = (CompanyList)JSFUtil.getSessionBean("CompanyList");
			ArrayList results = list.getCompanyList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (CompanyBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			CompanyBO BO = new CompanyBO();
					BO.setCompanyId(bean.getCompanyId());
					BO.setName(bean.getName());
					BO.setEntityCompanyFkList(bean.getEntityCompanyFkList());
					BO.setCompanyFkList(bean.getCompanyFkList());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(CompanyBO editBO, CompanyBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(CompanyBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	CompanyBO companyBO = null;
        
        try {      	
      	    CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");      	    
         	Collection list = facade.retrieve(new CompanyBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		CompanyBO temp = (CompanyBO) i.next();
         		results.add(temp);
         		companyBO = temp;
         	}         	    	
			return "Companyedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");     	    
         	Collection list = facade.retrieve(new CompanyBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		CompanyBO temp = (CompanyBO) i.next();
         		results.add(temp);
         	}  
			listNavigate(150,0); 	
			return "Companycreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");    	    
         	Collection list = facade.retrieve(new CompanyBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		CompanyBO temp = (CompanyBO) i.next();
         		results.add(temp);
         	}
         	return "Companydelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		CompanyList companyList = (CompanyList) JSFUtil.getSessionBean("CompanyList");
		companyList.clear();
		CompanyBO companySearchBO = (CompanyBO) JSFUtil.getFromSession("CompanySearchBO");
        try {      	
      	    CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(companySearchBO == null){
	         	list = facade.retrieveWithNested(new CompanyBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(companySearchBO, startingPoint, maxRows, null, null);
	        }
         	CompanyBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (CompanyBO) i.next();
				CompanyBean tempBean = new CompanyBean(temp);
       			results.add(tempBean);
         	}
         	CompanyList theList = (CompanyList)JSFUtil.getSessionBean("CompanyList");
         	theList.setCompanyList(results);         	
         	return "Companylist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
	
    /*
     * Function to populate the company list with all current companies
     * */
	public String populateCompanyList() {
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		CompanyList companyList = (CompanyList) JSFUtil.getSessionBean("CompanyList");
		companyList.clear();
		CompanyBO companySearchBO = (CompanyBO) JSFUtil.getFromSession("CompanySearchBO");
        try {      	
      	    CompanyFacade facade = (CompanyFacade) FacadeFactory.getFacade("CompanyFacade");    	    
         	int startingPoint = 10000000; // ALLL OF THEM AHHHHHH 
         	int maxRows = 10000000;
         	Collection list = null;
         	if(companySearchBO == null){
	         	list = facade.retrieveWithNested(new CompanyBO());
	        }else{
	        	list = facade.retrieveWithNested(companySearchBO);
	        }
         	CompanyBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (CompanyBO) i.next();
				CompanyBean tempBean = new CompanyBean(temp);
       			results.add(tempBean);
         	}
         	CompanyList theList = (CompanyList)JSFUtil.getSessionBean("CompanyList");
         	theList.setCompanyList(results);         	
         	return "Companylist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
	
	
    public String searchNavigate(){
		return "Companysearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}