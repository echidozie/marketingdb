package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;


public class DetailActionManager {
	public DetailActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		DetailFacade facade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");
			DetailBean bean = new DetailBean();
			DetailList list = (DetailList)JSFUtil.getSessionBean("DetailList");
			ArrayList results = list.getDetailList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (DetailBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			DetailBO BO = new DetailBO();
					BO.setDetailId(bean.getDetailId());
					BO.setSourceId(bean.getSourceId());
					BO.setDetail(bean.getDetail());
					BO.setTouchDetailFkList(bean.getTouchDetailFkList());
					BO.setTouchbackDetailFkList(bean.getTouchbackDetailFkList());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(DetailBO editBO, DetailBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		DetailFacade facade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(DetailBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		DetailFacade facade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	DetailBO detailBO = null;
        
        try {      	
      	    DetailFacade facade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");      	    
         	Collection list = facade.retrieve(new DetailBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		DetailBO temp = (DetailBO) i.next();
         		results.add(temp);
         		detailBO = temp;
         	}         	    	
			return "Detailedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    DetailFacade facade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");     	    
         	Collection list = facade.retrieve(new DetailBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		DetailBO temp = (DetailBO) i.next();
         		results.add(temp);
         	}    
			listNavigate(150,0); 	
			return "Detailcreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    DetailFacade facade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");    	    
         	Collection list = facade.retrieve(new DetailBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		DetailBO temp = (DetailBO) i.next();
         		results.add(temp);
         	}
         	return "Detaildelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		DetailList detailList = (DetailList) JSFUtil.getSessionBean("DetailList");
		detailList.clear();
		DetailBO detailSearchBO = (DetailBO) JSFUtil.getFromSession("DetailSearchBO");
        try {      	
      	    DetailFacade facade = (DetailFacade) FacadeFactory.getFacade("DetailFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(detailSearchBO == null){
	         	list = facade.retrieveWithNested(new DetailBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(detailSearchBO, startingPoint, maxRows, null, null);
	        }
         	DetailBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (DetailBO) i.next();
				DetailBean tempBean = new DetailBean(temp);
       			results.add(tempBean);
         	}
         	DetailList theList = (DetailList)JSFUtil.getSessionBean("DetailList");
         	theList.setDetailList(results);         	
         	return "Detaillist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Detailsearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}