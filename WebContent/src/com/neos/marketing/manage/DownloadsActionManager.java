package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.DateConverter;


public class DownloadsActionManager {
	public DownloadsActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		DownloadsFacade facade = (DownloadsFacade) FacadeFactory.getFacade("DownloadsFacade");
			DownloadsBean bean = new DownloadsBean();
			DownloadsList list = (DownloadsList)JSFUtil.getSessionBean("DownloadsList");
			ArrayList results = list.getDownloadsList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (DownloadsBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			DownloadsBO BO = new DownloadsBO();
					BO.setId(bean.getId());
					BO.setName(bean.getName());
					BO.setEmail(bean.getEmail());
					BO.setDownloadId(bean.getDownloadId());
					BO.setItemTitle(bean.getItemTitle());
					BO.setTimestamp(bean.getTimestamp());
					BO.setTimeRequested(bean.getTimeRequested());
					BO.setIpAddress(bean.getIpAddress());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(DownloadsBO editBO, DownloadsBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		DownloadsFacade facade = (DownloadsFacade) FacadeFactory.getFacade("DownloadsFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(DownloadsBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		DownloadsFacade facade = (DownloadsFacade) FacadeFactory.getFacade("DownloadsFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	DownloadsBO downloadsBO = null;
        
        try {      	
      	    DownloadsFacade facade = (DownloadsFacade) FacadeFactory.getFacade("DownloadsFacade");      	    
         	Collection list = facade.retrieve(new DownloadsBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		DownloadsBO temp = (DownloadsBO) i.next();
         		results.add(temp);
         		downloadsBO = temp;
         	}         	    	
			return "Downloadsedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    DownloadsFacade facade = (DownloadsFacade) FacadeFactory.getFacade("DownloadsFacade");     	    
         	Collection list = facade.retrieve(new DownloadsBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		DownloadsBO temp = (DownloadsBO) i.next();
         		results.add(temp);
         	}     	
			return "Downloadscreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    DownloadsFacade facade = (DownloadsFacade) FacadeFactory.getFacade("DownloadsFacade");    	    
         	Collection list = facade.retrieve(new DownloadsBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		DownloadsBO temp = (DownloadsBO) i.next();
         		results.add(temp);
         	}
         	return "Downloadsdelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		DownloadsList downloadsList = (DownloadsList) JSFUtil.getSessionBean("DownloadsList");
		downloadsList.clear();
		DownloadsBO downloadsSearchBO = (DownloadsBO) JSFUtil.getFromSession("DownloadsSearchBO");
        try {      	
      	    DownloadsFacade facade = (DownloadsFacade) FacadeFactory.getFacade("DownloadsFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(downloadsSearchBO == null){
	         	list = facade.retrieveWithNested(new DownloadsBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(downloadsSearchBO, startingPoint, maxRows, null, null);
	        }
         	DownloadsBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (DownloadsBO) i.next();
				DownloadsBean tempBean = new DownloadsBean(temp);
       			results.add(tempBean);
         	}
         	DownloadsList theList = (DownloadsList)JSFUtil.getSessionBean("DownloadsList");
         	theList.setDownloadsList(results);         	
         	return "Downloadslist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Downloadssearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}