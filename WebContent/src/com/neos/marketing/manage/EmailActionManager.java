package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;


public class EmailActionManager {
	public EmailActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		EmailFacade facade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");
			EmailBean bean = new EmailBean();
			EmailList list = (EmailList)JSFUtil.getSessionBean("EmailList");
			ArrayList results = list.getEmailList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (EmailBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			EmailBO BO = new EmailBO();
					BO.setEmailId(bean.getEmailId());
					BO.setEmailAddress(bean.getEmailAddress());
					BO.setPersonId(bean.getPersonId());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(EmailBO editBO, EmailBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		EmailFacade facade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(EmailBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		EmailFacade facade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	EmailBO emailBO = null;
        
        try {      	
      	    EmailFacade facade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");      	    
         	Collection list = facade.retrieve(new EmailBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		EmailBO temp = (EmailBO) i.next();
         		results.add(temp);
         		emailBO = temp;
         	}         	    	
			return "Emailedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    EmailFacade facade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");     	    
         	Collection list = facade.retrieve(new EmailBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		EmailBO temp = (EmailBO) i.next();
         		results.add(temp);
         	}
			return "Emailcreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    EmailFacade facade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");    	    
         	Collection list = facade.retrieve(new EmailBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		EmailBO temp = (EmailBO) i.next();
         		results.add(temp);
         	}
         	return "Emaildelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		EmailList emailList = (EmailList) JSFUtil.getSessionBean("EmailList");
		emailList.clear();
		EmailBO emailSearchBO = (EmailBO) JSFUtil.getFromSession("EmailSearchBO");
        try {      	
      	    EmailFacade facade = (EmailFacade) FacadeFactory.getFacade("EmailFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(emailSearchBO == null){
	         	list = facade.retrieveWithNested(new EmailBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(emailSearchBO, startingPoint, maxRows, null, null);
	        }
         	EmailBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (EmailBO) i.next();
				EmailBean tempBean = new EmailBean(temp);
       			results.add(tempBean);
         	}
         	EmailList theList = (EmailList)JSFUtil.getSessionBean("EmailList");
         	theList.setEmailList(results);         	
         	return "Emaillist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Emailsearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}