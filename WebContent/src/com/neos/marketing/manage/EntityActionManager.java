package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;


public class EntityActionManager {
	public EntityActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		EntityFacade facade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");
			EntityBean bean = new EntityBean();
			EntityList list = (EntityList)JSFUtil.getSessionBean("EntityList");
			ArrayList results = list.getEntityList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (EntityBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			EntityBO BO = new EntityBO();
					BO.setEntityId(bean.getEntityId());
					BO.setCompanyId(bean.getCompanyId());
					BO.setPersonId(bean.getPersonId());
					BO.setTouchEntityFkList(bean.getTouchEntityFkList());
					BO.setTouchbackEntityFkList(bean.getTouchbackEntityFkList());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(EntityBO editBO, EntityBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		EntityFacade facade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(EntityBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		EntityFacade facade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	EntityBO entityBO = null;
        
        try {      	
      	    EntityFacade facade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");      	    
         	Collection list = facade.retrieve(new EntityBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		EntityBO temp = (EntityBO) i.next();
         		results.add(temp);
         		entityBO = temp;
         	}         	    	
			return "Entityedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    EntityFacade facade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");     	    
         	Collection list = facade.retrieve(new EntityBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		EntityBO temp = (EntityBO) i.next();
         		results.add(temp);
         	}     	
			return "Entitycreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    EntityFacade facade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");    	    
         	Collection list = facade.retrieve(new EntityBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		EntityBO temp = (EntityBO) i.next();
         		results.add(temp);
         	}
         	return "Entitydelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		EntityList entityList = (EntityList) JSFUtil.getSessionBean("EntityList");
		entityList.clear();
		EntityBO entitySearchBO = (EntityBO) JSFUtil.getFromSession("EntitySearchBO");
        try {      	
      	    EntityFacade facade = (EntityFacade) FacadeFactory.getFacade("EntityFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(entitySearchBO == null){
	         	list = facade.retrieveWithNested(new EntityBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(entitySearchBO, startingPoint, maxRows, null, null);
	        }
         	EntityBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (EntityBO) i.next();
				EntityBean tempBean = new EntityBean(temp);
       			results.add(tempBean);
         	}
         	EntityList theList = (EntityList)JSFUtil.getSessionBean("EntityList");
         	theList.setEntityList(results);         	
         	return "Entitylist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Entitysearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}