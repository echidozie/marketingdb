package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;


public class NoteActionManager {
	public NoteActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");
			NoteBean bean = new NoteBean();
			NoteList list = (NoteList)JSFUtil.getSessionBean("NoteList");
			ArrayList results = list.getNoteList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (NoteBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			NoteBO BO = new NoteBO();
					BO.setNoteId(bean.getNoteId());
					BO.setObjectType(bean.getObjectType());
					BO.setObjectId(bean.getObjectId());
					BO.setNote(bean.getNote());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String deleteNoteAction(Long noteId, String objectType, Long objectId, String note) {
  		MessageBean messageBean = null;
  	try {
  		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
  		messageBean.clear();
  		NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");
		NoteBO BO = new NoteBO();
		BO.setNoteId(noteId);
		BO.setObjectType(objectType);
		BO.setObjectId(objectId);
		BO.setNote(note);
     	facade.delete(BO, null);
    	System.out.println("Deleted");
		return "success";
  	} catch (Exception e) {
  		messageBean.display("There was an error editing record: "+e.getMessage());
		return "error";
  	}     	
}
	
	public String editAction(NoteBO editBO, NoteBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }
	
	public String editNoteAction(Long noteId, String objectType, Long objectId, String note) {
  		MessageBean messageBean = null;
  		try {
  			messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
  			messageBean.clear();
  			NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");
    		NoteBO pkBO = new NoteBO();
    		pkBO.setNoteId(noteId);
    		pkBO.setObjectType(objectType);
    		pkBO.setObjectId(objectId);
    		
    		NoteBO editBO = new NoteBO();
    		editBO.setNoteId(noteId);
    		editBO.setObjectType(objectType);
    		editBO.setObjectId(objectId);
    		editBO.setNote(note);
 		
  			facade.update(pkBO, editBO, null);
  			System.out.println("Edited!");
  			return "success";
  		} catch (Exception e) {
  			messageBean.display("There was an error editing record: "+e.getMessage());
  			return "error";
  		}     	
	}
    
	public String createAction(NoteBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	

	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	NoteBO noteBO = null;
        
        try {      	
      	    NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");      	    
         	Collection list = facade.retrieve(new NoteBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		NoteBO temp = (NoteBO) i.next();
         		results.add(temp);
         		noteBO = temp;
         	}         	    	
			return "Noteedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");     	    
         	Collection list = facade.retrieve(new NoteBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		NoteBO temp = (NoteBO) i.next();
         		results.add(temp);
         	}     	
			return "Notecreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");    	    
         	Collection list = facade.retrieve(new NoteBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		NoteBO temp = (NoteBO) i.next();
         		results.add(temp);
         	}
         	return "Notedelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		NoteList noteList = (NoteList) JSFUtil.getSessionBean("NoteList");
		noteList.clear();
		NoteBO noteSearchBO = (NoteBO) JSFUtil.getFromSession("NoteSearchBO");
        try {      	
      	    NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(noteSearchBO == null){
	         	list = facade.retrieveWithNested(new NoteBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(noteSearchBO, startingPoint, maxRows, null, null);
	        }
         	NoteBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (NoteBO) i.next();
				NoteBean tempBean = new NoteBean(temp);
       			results.add(tempBean);
         	}
         	NoteList theList = (NoteList)JSFUtil.getSessionBean("NoteList");
         	theList.setNoteList(results);         	
         	return "Notelist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
	/* Populates the Notes List with a filtered list based on input
	 * */
	public void populateFilter(String objectType, Long objectId) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		NoteList noteList = (NoteList) JSFUtil.getSessionBean("NoteList");
		noteList.clear();
        try {      	
      	    NoteFacade facade = (NoteFacade) FacadeFactory.getFacade("NoteFacade");    	    
         	Collection list = null;
         	NoteBO filteredNoteBO = new NoteBO();
         	filteredNoteBO.setObjectId(objectId);
         	filteredNoteBO.setObjectType(objectType);
	        list = facade.retrieveWithNested(filteredNoteBO);
         	NoteBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (NoteBO) i.next();
				NoteBean tempBean = new NoteBean(temp);
       			results.add(tempBean);
         	}
         	NoteList theList = (NoteList)JSFUtil.getSessionBean("NoteList");
         	System.out.println(theList);
         	theList.setNoteList(results);         	
         	return;
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return;
	    }
    }
	
	
    public String searchNavigate(){
		return "Notesearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}