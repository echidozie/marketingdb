package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.dao.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;


public class PersonActionManager {
	public PersonActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		PersonFacade facade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");
			PersonBean bean = new PersonBean();
			PersonList list = (PersonList)JSFUtil.getSessionBean("PersonList");
			ArrayList results = list.getPersonList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (PersonBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			PersonBO BO = new PersonBO();
					BO.setPersonId(bean.getPersonId());
					BO.setFirstName(bean.getFirstName());
					BO.setLastName(bean.getLastName());
					BO.setTitle(bean.getTitle());
					BO.setCompanyId(bean.getCompanyId());
					BO.setEmailPersonFkList(bean.getEmailPersonFkList());
					BO.setEntityPersonFkList(bean.getEntityPersonFkList());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(PersonBO editBO, PersonBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		PersonFacade facade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(PersonBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		PersonFacade facade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	PersonBO personBO = null;
        
        try {      	
      	    PersonFacade facade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");      	    
         	Collection list = facade.retrieve(new PersonBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		PersonBO temp = (PersonBO) i.next();
         		results.add(temp);
         		personBO = temp;
         	}         	    	
			return "Personedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    PersonFacade facade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");     	    
         	Collection list = facade.retrieve(new PersonBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		PersonBO temp = (PersonBO) i.next();
         		results.add(temp);
         	}
			EmailActionManager emailManager = new EmailActionManager();
			emailManager.listNavigate(250,0);
			return "Personcreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    PersonFacade facade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");    	    
         	Collection list = facade.retrieve(new PersonBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		PersonBO temp = (PersonBO) i.next();
         		results.add(temp);
         	}
         	return "Persondelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		PersonList personList = (PersonList) JSFUtil.getSessionBean("PersonList");
		personList.clear();
		PersonBO personSearchBO = (PersonBO) JSFUtil.getFromSession("PersonSearchBO");
        try {      	
      	    PersonFacade facade = (PersonFacade) FacadeFactory.getFacade("PersonFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(personSearchBO == null){
	         	list = facade.retrieveWithNested(new PersonBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(personSearchBO, startingPoint, maxRows, null, null);
	        }
         	PersonBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (PersonBO) i.next();
				PersonBean tempBean = new PersonBean(temp);
       			results.add(tempBean);
         	}
         	PersonList theList = (PersonList)JSFUtil.getSessionBean("PersonList");
         	theList.setPersonList(results);
         	return "Personlist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Personsearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}