package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;


public class SourceActionManager {
	public SourceActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		SourceFacade facade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
			SourceBean bean = new SourceBean();
			SourceList list = (SourceList)JSFUtil.getSessionBean("SourceList");
			ArrayList results = list.getSourceList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (SourceBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			SourceBO BO = new SourceBO();
					BO.setSourceId(bean.getSourceId());
					BO.setSource(bean.getSource());
					BO.setDetailSourceFkList(bean.getDetailSourceFkList());
					BO.setTouchSourceFkList(bean.getTouchSourceFkList());
					BO.setTouchbackSourceFkList(bean.getTouchbackSourceFkList());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(SourceBO editBO, SourceBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		SourceFacade facade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(SourceBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		SourceFacade facade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	SourceBO sourceBO = null;
        
        try {      	
      	    SourceFacade facade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");      	    
         	Collection list = facade.retrieve(new SourceBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		SourceBO temp = (SourceBO) i.next();
         		results.add(temp);
         		sourceBO = temp;
         	}         	    	
			return "Sourceedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    SourceFacade facade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");     	    
         	Collection list = facade.retrieve(new SourceBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		SourceBO temp = (SourceBO) i.next();
         		results.add(temp);
         	} 
			listNavigate(150,0);
			return "Sourcecreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    SourceFacade facade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");    	    
         	Collection list = facade.retrieve(new SourceBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		SourceBO temp = (SourceBO) i.next();
         		results.add(temp);
         	}
         	return "Sourcedelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		SourceList sourceList = (SourceList) JSFUtil.getSessionBean("SourceList");
		sourceList.clear();
		SourceBO sourceSearchBO = (SourceBO) JSFUtil.getFromSession("SourceSearchBO");
        try {      	
      	    SourceFacade facade = (SourceFacade) FacadeFactory.getFacade("SourceFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(sourceSearchBO == null){
	         	list = facade.retrieveWithNested(new SourceBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(sourceSearchBO, startingPoint, maxRows, null, null);
	        }
         	SourceBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (SourceBO) i.next();
				SourceBean tempBean = new SourceBean(temp);
       			results.add(tempBean);
         	}
         	SourceList theList = (SourceList)JSFUtil.getSessionBean("SourceList");
         	theList.setSourceList(results);         	
         	return "Sourcelist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Sourcesearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}