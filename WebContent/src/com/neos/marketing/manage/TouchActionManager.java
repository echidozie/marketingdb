package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.DateConverter;


public class TouchActionManager {
	public TouchActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		TouchFacade facade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");
			TouchBean bean = new TouchBean();
			TouchList list = (TouchList)JSFUtil.getSessionBean("TouchList");
			ArrayList results = list.getTouchList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (TouchBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			TouchBO BO = new TouchBO();
					BO.setTouchId(bean.getTouchId());
					BO.setEntityId(bean.getEntityId());
					BO.setTouchDate(bean.getTouchDate());
					BO.setSourceId(bean.getSourceId());
					BO.setDetailId(bean.getDetailId());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(TouchBO editBO, TouchBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		TouchFacade facade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(TouchBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		TouchFacade facade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	TouchBO touchBO = null;
        
        try {      	
      	    TouchFacade facade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");      	    
         	Collection list = facade.retrieve(new TouchBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		TouchBO temp = (TouchBO) i.next();
         		results.add(temp);
         		touchBO = temp;
         	}         	    	
			return "Touchedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    TouchFacade facade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");     	    
         	Collection list = facade.retrieve(new TouchBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		TouchBO temp = (TouchBO) i.next();
         		results.add(temp);
         	}     	
			return "Touchcreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    TouchFacade facade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");    	    
         	Collection list = facade.retrieve(new TouchBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		TouchBO temp = (TouchBO) i.next();
         		results.add(temp);
         	}
         	return "Touchdelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		TouchList touchList = (TouchList) JSFUtil.getSessionBean("TouchList");
		touchList.clear();
		TouchBO touchSearchBO = (TouchBO) JSFUtil.getFromSession("TouchSearchBO");
        try {      	
      	    TouchFacade facade = (TouchFacade) FacadeFactory.getFacade("TouchFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(touchSearchBO == null){
	         	list = facade.retrieveWithNested(new TouchBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(touchSearchBO, startingPoint, maxRows, null, null);
	        }
         	TouchBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (TouchBO) i.next();
				TouchBean tempBean = new TouchBean(temp);
       			results.add(tempBean);
         	}
         	TouchList theList = (TouchList)JSFUtil.getSessionBean("TouchList");
         	theList.setTouchList(results);         	
         	return "Touchlist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Touchsearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}