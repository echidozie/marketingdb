package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.DateConverter;


public class TouchbackActionManager {
	public TouchbackActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		TouchbackFacade facade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");
			TouchbackBean bean = new TouchbackBean();
			TouchbackList list = (TouchbackList)JSFUtil.getSessionBean("TouchbackList");
			ArrayList results = list.getTouchbackList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (TouchbackBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			TouchbackBO BO = new TouchbackBO();
					BO.setTouchbackId(bean.getTouchbackId());
					BO.setEntityId(bean.getEntityId());
					BO.setTouchbackDate(bean.getTouchbackDate());
					BO.setSourceId(bean.getSourceId());
					BO.setDetailId(bean.getDetailId());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(TouchbackBO editBO, TouchbackBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		TouchbackFacade facade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(TouchbackBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		TouchbackFacade facade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	TouchbackBO touchbackBO = null;
        
        try {      	
      	    TouchbackFacade facade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");      	    
         	Collection list = facade.retrieve(new TouchbackBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		TouchbackBO temp = (TouchbackBO) i.next();
         		results.add(temp);
         		touchbackBO = temp;
         	}         	    	
			return "Touchbackedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    TouchbackFacade facade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");     	    
         	Collection list = facade.retrieve(new TouchbackBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		TouchbackBO temp = (TouchbackBO) i.next();
         		results.add(temp);
         	}     	
			return "Touchbackcreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    TouchbackFacade facade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");    	    
         	Collection list = facade.retrieve(new TouchbackBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		TouchbackBO temp = (TouchbackBO) i.next();
         		results.add(temp);
         	}
         	return "Touchbackdelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		TouchbackList touchbackList = (TouchbackList) JSFUtil.getSessionBean("TouchbackList");
		touchbackList.clear();
		TouchbackBO touchbackSearchBO = (TouchbackBO) JSFUtil.getFromSession("TouchbackSearchBO");
        try {      	
      	    TouchbackFacade facade = (TouchbackFacade) FacadeFactory.getFacade("TouchbackFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(touchbackSearchBO == null){
	         	list = facade.retrieveWithNested(new TouchbackBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(touchbackSearchBO, startingPoint, maxRows, null, null);
	        }
         	TouchbackBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (TouchbackBO) i.next();
				TouchbackBean tempBean = new TouchbackBean(temp);
       			results.add(tempBean);
         	}
         	TouchbackList theList = (TouchbackList)JSFUtil.getSessionBean("TouchbackList");
         	theList.setTouchbackList(results);         	
         	return "Touchbacklist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Touchbacksearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}