package com.neos.marketing.manage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.*;
import javax.faces.model.SelectItem;

import com.neos.marketing.bo.*;
import com.neos.marketing.facade.*;
import com.neos.marketing.list.*;
import com.neos.marketing.bean.*;
import com.neos.marketing.util.MessageBean;
import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.util.DateConverter;


public class VisitorsActionManager {
	public VisitorsActionManager() {
		super();
	}
	private boolean selected = false;

    private ArrayList results = null;

	public String deleteAction() {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		VisitorsFacade facade = (VisitorsFacade) FacadeFactory.getFacade("VisitorsFacade");
			VisitorsBean bean = new VisitorsBean();
			VisitorsList list = (VisitorsList)JSFUtil.getSessionBean("VisitorsList");
			ArrayList results = list.getVisitorsList();
	     	for (Iterator i = results.iterator(); i.hasNext();) {
	     		bean = (VisitorsBean) i.next();
	     		if(bean.isSelected()){
	     			//bean to bo
	     			VisitorsBO BO = new VisitorsBO();
					BO.setId(bean.getId());
					BO.setIpAddress(bean.getIpAddress());
					BO.setHostname(bean.getHostname());
					BO.setPageVisited(bean.getPageVisited());
					BO.setReferrer(bean.getReferrer());
					BO.setTimeRequested(bean.getTimeRequested());
					BO.setTimestamp(bean.getTimestamp());
					BO.setOs(bean.getOs());
					BO.setBrowser(bean.getBrowser());
	     			facade.delete(BO, null);
	    			System.out.println("Deleted");
	     		}
	     	}
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	
    }

	public String editAction(VisitorsBO editBO, VisitorsBO pkBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		VisitorsFacade facade = (VisitorsFacade) FacadeFactory.getFacade("VisitorsFacade");
	     	facade.update(pkBO, editBO, null);
			System.out.println("Edited!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error editing record: "+e.getMessage());
			return "error";
      	}     	

    }
    
	public String createAction(VisitorsBO createBO) {
      		MessageBean messageBean = null;
      	try {
      		messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      		messageBean.clear();
      		VisitorsFacade facade = (VisitorsFacade) FacadeFactory.getFacade("VisitorsFacade");
	     	facade.insert(createBO, null);
			System.out.println("Inserted!");
			return "success";
      	} catch (Exception e) {
      		messageBean.display("There was an error creating record: "+e.getMessage());
			return "error";
      	}     	
    }	
	public String editNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
     	VisitorsBO visitorsBO = null;
        
        try {      	
      	    VisitorsFacade facade = (VisitorsFacade) FacadeFactory.getFacade("VisitorsFacade");      	    
         	Collection list = facade.retrieve(new VisitorsBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		VisitorsBO temp = (VisitorsBO) i.next();
         		results.add(temp);
         		visitorsBO = temp;
         	}         	    	
			return "Visitorsedit";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }     
	}
	
	public String createNavigate() {
     	String action = null;
     	results = new ArrayList();  
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();
        try {      	
      	    VisitorsFacade facade = (VisitorsFacade) FacadeFactory.getFacade("VisitorsFacade");     	    
         	Collection list = facade.retrieve(new VisitorsBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		VisitorsBO temp = (VisitorsBO) i.next();
         		results.add(temp);
         	}     	
			return "Visitorscreate";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String deleteNavigate() {
     	String action = null;
     	results = new ArrayList();
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	
        try {      	
      	    VisitorsFacade facade = (VisitorsFacade) FacadeFactory.getFacade("VisitorsFacade");    	    
         	Collection list = facade.retrieve(new VisitorsBO());
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		VisitorsBO temp = (VisitorsBO) i.next();
         		results.add(temp);
         	}
         	return "Visitorsdelete";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
			messageBean.display(e.getMessage());
			return "error";
	    }
	}
	
	public String listNavigate(){
		return listNavigate(15, 0);
	}
	
	public String listNavigate(int maxRows, int pageNum) {
     	String action = null;
     	ArrayList results = new ArrayList();   
     	MessageBean messageBean = (MessageBean) JSFUtil.getSessionBean("MessageBean");
      	messageBean.clear();     	  
		VisitorsList visitorsList = (VisitorsList) JSFUtil.getSessionBean("VisitorsList");
		visitorsList.clear();
		VisitorsBO visitorsSearchBO = (VisitorsBO) JSFUtil.getFromSession("VisitorsSearchBO");
        try {      	
      	    VisitorsFacade facade = (VisitorsFacade) FacadeFactory.getFacade("VisitorsFacade");    	    
         	int startingPoint = maxRows * pageNum;
         	Collection list = null;
         	if(visitorsSearchBO == null){
	         	list = facade.retrieveWithNested(new VisitorsBO(), startingPoint, maxRows, null, null);
	        }else{
	        	list = facade.retrieveWithNested(visitorsSearchBO, startingPoint, maxRows, null, null);
	        }
         	VisitorsBO temp = null;
         	for (Iterator i = list.iterator(); i.hasNext();) {
         		temp = (VisitorsBO) i.next();
				VisitorsBean tempBean = new VisitorsBean(temp);
       			results.add(tempBean);
         	}
         	VisitorsList theList = (VisitorsList)JSFUtil.getSessionBean("VisitorsList");
         	theList.setVisitorsList(results);         	
         	return "Visitorslist";
      	} catch (Exception e) {
      		// Log errors and send bind them to a message
      		messageBean.display(e.getMessage());
      		return "error";
	    }
    }
    
    public String searchNavigate(){
		return "Visitorssearch";
    }
    
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public ArrayList getResults() {
		return results;
	}

	public void setResults(ArrayList results) {
		this.results = results;
	}
}