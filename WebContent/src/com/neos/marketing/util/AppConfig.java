package com.neos.marketing.util;

/**
 * AppConfig
 * reads properties from the config.properties file and makes those
 * accessible to the utility classes.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    June 21, 2001
 * @version    1.0
 */
public class AppConfig {

	private static String APP_DATASOURCE = null;
	private static String DRIVERURL = null;
	private static String DRIVER = null;
	private static String USER = null;
	private static String PASSWORD = null;
	private static String RESOURCE_REFERENCE = null;
	private static String DB_CONNECTION_TYPE = null;
	private static String initialContextFactory=null;
	private static String providerUrl=null;

	static {
		try {
			java.util.ResourceBundle prop = java.util.ResourceBundle.getBundle("com.neos.marketing.util.config");
			APP_DATASOURCE = prop.getString("DATASOURCE");
			DRIVERURL = prop.getString("DRIVERURL");
			DRIVER = prop.getString("DRIVER");
			USER = prop.getString("USER");
			PASSWORD = prop.getString("PASSWORD");
			RESOURCE_REFERENCE = prop.getString("RESOURCE_REFERENCE");
			DB_CONNECTION_TYPE = prop.getString("DB_CONNECTION_TYPE");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Return the DatabaseConnection type to use.  Valid types are:
	 * container = implements a ContainerManagedConnection.
 	 *		For this type a ResourceReference must be specified.
 	 *	driver = implements a JDBCConnection
	 * 		For this type the DriverUrl and Driver class must be specified.
	 * For both types the user name and password must be specified.
	 * @return Database connection type to use.
	 */
	public static String getDbConnectionType() {
		return DB_CONNECTION_TYPE;
	}

	/**
	 * @return JNDI Lookup name for the dababase connection
	 */
	public static String getResourceReference() {
		return RESOURCE_REFERENCE;
	}

	/**
	 * @return the class name of the database driver.
	 */
	public static String getDriver() {
		return DRIVER;
	}

	/**
	 * @return the required password for the user/database combination.
	 */
	public static String getPassword() {
		return PASSWORD;
	}

	/**
	 * @return the url for a connection to the database.
	 */
	public static String getDriverUrl() {
		return DRIVERURL;
	}

	/**
	 * @return the user name to use for the database connection.
	 */
	public static String getUser() {
		return USER;
	}

	/**
	 * @return the initial conectext factory for the application server providing the datasource.
	 */
	public static String getInitialContextFactory() {
		return initialContextFactory;
	}

	/**
	 * @return the url for the application server providing the datasource.
 	 */
	public static String getProviderUrl() {
		return providerUrl;
	}

	/**
	 * @param string set the name of the database driver class.
	 */
	public static void setDriver(String string) {
		DRIVER = string;
	}

	/**
 	 * @param string set the name of the datasource to use for this connection.
 	 */
	public static void setAPP_DATASOURCE(String string) {
		APP_DATASOURCE = string;
	}

	/**
 	 * @param string set the password for the database connection.
 	 */
	public static void setPasswword(String string) {
		PASSWORD = string;
	}

	/**
	 * @param string set the url for the database connection.
 	 */
	public static void setDriverUrl(String string) {
		DRIVERURL = string;
	}

	/**
 	 * @param string set the user name for the database connnection.
 	 */
	public static void setUser(String string) {
		USER = string;
	}

	/**
 	 * @param string set the initial context factory of the application server providing
 	 * the datasource.
	 */
	public static void setInitialContextFactory(String string) {
		initialContextFactory = string;
	}

	/**
 	 * @param string set the provider url of the application server providing
 	 * the datasource.
 	 */
	public static void setProviderUrl(String string) {
		providerUrl = string;
	}

}
