package com.neos.marketing.util;

import java.util.ArrayList; 

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.bean.CompanyBean;
import com.neos.marketing.list.CompanyList;

@FacesValidator("companyNameValidator")
public class FormValidator implements Validator{

	@Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        // Check Component
		
		if (value.equals("")) {
            throw new ValidatorException(new FacesMessage("Exmpty Response"));
        }
		
		boolean valueIsInvalid = false;
		ArrayList companyList = ((CompanyList)JSFUtil.getSessionBean("CompanyList")).getCompanyList();
		for(int index=0; index < companyList.size(); index++ ){
			if( ((CompanyBean) companyList.get(index) ).getName().equals(value)){
				valueIsInvalid = true;
			}
		}
		if (valueIsInvalid) {
            throw new ValidatorException(new FacesMessage("Invalid Response. Company Already exists"));
        }
	}
}


