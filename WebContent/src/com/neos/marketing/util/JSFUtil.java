package com.neos.marketing.util;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.context.ExternalContext;

/**
 * Title: JSFUtil Description: Helper class for common pieces of JSF
 * functionality. 
 * <pre>
 * Copyright: 2006 
 * Company: Oracle Retail
 * </pre>
 *@author     Rev
 *@created    07/20/2006
 *@version    1.0
 */
/**
 * @author Robert Nocera
 * 
 */
public class JSFUtil {

    /**
     * Retrieve a session-scoped bean from the context
     * 
     * @return BaseBean the request BaseBean object.
     */
    public static Object getSessionBean(String beanName) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getApplication().getVariableResolver().resolveVariable(facesContext, beanName);
    }

    /**
     * Store a value in the user's parameter space.
     * 
     * @param key the String that identifies the stored value
     * @param value the stored object
     */
    public static void addToRequest(String key, Object value) {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        Map requestMap = ec.getRequestMap();
        requestMap.put(key, value);
    }

    /**
     * retrieve a value from the user's parameter space.
     * 
     * @param key the String that identifies the stored value
     */
    public static Object getFromRequest(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        Map requestMap = ec.getRequestMap();
        return requestMap.get(key);
    }

    /**
     * remove a value from the user's parameter space.
     * 
     * @param key the String that identifies the stored value
     */
    public static void removeFromRequest(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        Map requestMap = ec.getRequestMap();
        requestMap.remove(key);
    }

    /**
     * Store a value in the user's parameter space.
     * 
     * @param key the String that identifies the stored value
     * @param value the stored object
     */
    public static void addToSession(String key, Object value) {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        Map sessionMap = ec.getSessionMap();
        sessionMap.put(key, value);
    }

    /**
     * retrieve a value from the user's parameter space.
     * 
     * @param key the String that identifies the stored value
     */
    public static Object getFromSession(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        Map sessionMap = ec.getSessionMap();
        Object returnObj = sessionMap.get(key);
        if (returnObj == null) {
            Map requestMap = context.getExternalContext().getRequestMap();
            returnObj = requestMap.get(key);
        }
        return returnObj;
    }

    /**
     * remove a value from the user's parameter space.
     * 
     * @param key the String that identifies the stored value
     */
    public static Object removeFromSession(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        Map sessionMap = ec.getSessionMap();
        return sessionMap.remove(key);
    }
}