package com.neos.marketing.util;

/**
 * MessageBean
 * makes various messages from the back end available to the GUI
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    June 21, 2001
 * @version    1.0
 */
public class MessageBean {

	private String msg = null;
	
	public void setMsg(String msg){
		this.msg = msg;
	}
	
	public String getMsg(){
		return this.msg;
	}
	
	public void clear(){
		this.msg = null;
	}
	
	public void display(String msg){
		this.setMsg(msg);
	}
}