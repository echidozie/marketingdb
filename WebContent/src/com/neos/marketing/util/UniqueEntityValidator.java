package com.neos.marketing.util;

import java.util.ArrayList;   
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIOutput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.neos.marketing.util.JSFUtil;
import com.neos.marketing.bean.CompanyBean;
import com.neos.marketing.bean.EntityBean;
import com.neos.marketing.list.EntityList;

@FacesValidator("uniqueEntityValidator")
public class UniqueEntityValidator implements Validator{

	@Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

//		String butValue  = request.getParameter("hidBt");
		
		ArrayList entityList = ((EntityList)JSFUtil.getSessionBean("EntityList")).getEntityList();
		boolean valueIsInvalid = false;

		Long userCompanyId =  Long.parseLong((String) ((UIInput) component.findComponent("companyId")).getSubmittedValue(), 10);
		Long userPersonId =  Long.parseLong((String) ((UIInput) component.findComponent("personId")).getSubmittedValue(), 10);
				
		for(int index=0; index < entityList.size(); index++ ){
			if( ((EntityBean) entityList.get(index) ).getCompanyId().equals(userCompanyId) && 
					((EntityBean) entityList.get(index) ).getPersonId().equals(userPersonId)){
				valueIsInvalid = true;
			}
		}
		if (valueIsInvalid) {
            throw new ValidatorException(new FacesMessage("Invalid Input. Entity Already exists"));
        }
		
	}
}


