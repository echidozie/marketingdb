package com.neos.marketing.util.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.neos.marketing.util.exception.ApplicationException;
import com.neos.marketing.util.AppConfig;

/**
 *  ContainerManagedConnection is a pooled database connection from an
 *  application server. 
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    April 8, 2002
 * @version    1.0
 */

public class ContainerManagedConnection implements DatabaseConnection {


	/**
	 *  Constructor for the ContainerManagedConnection object
	 */
	public ContainerManagedConnection() {
	}


	/**
	 *  gets a connection from the pool.
	 *
	 *@return                        a Silverstream pooled database connection.
	 *@throws  ApplicationException  wrapped SQLException
	 */
	public Connection getConnection() throws ApplicationException {
		DataSource ds = null;
		Connection conn = null;
		try {
         InitialContext ic = new InitialContext();
			ds = (DataSource) ic.lookup(AppConfig.getResourceReference());
			conn = ds.getConnection();
		}
		catch (NamingException ne) {
			throw new ApplicationException(ne.toString());
		}
		catch (SQLException se) {
			throw new ApplicationException(se.toString());
		}
      if (conn == null)
         throw new ApplicationException("No Datasource found for "+AppConfig.getResourceReference());
		return conn;
	}
	
	public void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch(SQLException sqle) {
				// don't sweat the small stuff.
			}
		}
	}

}
