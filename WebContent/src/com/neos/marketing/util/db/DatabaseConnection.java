package com.neos.marketing.util.db;

import java.sql.Connection;

import com.neos.marketing.util.exception.ApplicationException;

/**
 * DatabaseConnection
 * is an interface that defines a database connection. It should be implemented by applications 
 * when accessing a database instead of using a database-specific package.  Specific 
 * connection types that implement this interface will be interchangeable in applications. 
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    June 21, 2001
 * @version    1.0
 */

public interface DatabaseConnection {

	/**
	 * Signifies an Oracle constraint violation SQL Error Code.
	 */
	public final static int ORACLECONSTRAINTVIOLATION = 2292;

	/**
	 *  gets a new Connection either from a pool or by creating a new one.
	 *
	 *@return                           a new Connection.
	 *@exception  ApplicationException  wrapped SQLException.
	 */
	public Connection getConnection() throws ApplicationException;
	

}
