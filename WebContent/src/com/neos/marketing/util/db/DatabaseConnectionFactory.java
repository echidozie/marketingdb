package com.neos.marketing.util.db;


import com.neos.marketing.util.exception.ApplicationException;
import com.neos.marketing.util.*;

/**
 * DatabaseConnectionFactory
 * Used to get a connection to the database.
 * <pre>
 * Copyright: ${option.get("copyright")}
 * Company: ${option.get("company")}
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    June 21, 2001
 * @version    1.0
 */
public class DatabaseConnectionFactory {

	/* (non-Javadoc)
	 * @see com.hig.eap.util.DatabaseConnection#getConnection()
	 */
	public static DatabaseConnection getDatbaseConnection() throws ApplicationException {
		DatabaseConnection dbconn = null;
		if ("container".equals(AppConfig.getDbConnectionType()))
			dbconn =  new ContainerManagedConnection();
		else if ("driver".equals(AppConfig.getDbConnectionType()))
			dbconn =  new JDBCConnection();
		else
			throw new ApplicationException("No Resource Type defined for the database connection.");
		return dbconn;			
	}

}
