package com.neos.marketing.util.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.neos.marketing.util.exception.ApplicationException;
import com.neos.marketing.util.AppConfig;

/**
 * DatabaseConnection implementation for normal connections to jdbc
 * without pooling. Not recommended for use in a production environment.
 * <pre>
 * Copyright: 2015
 * Company: NEOS 
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    June 21, 2001
 * @version    1.0
 */

public class JDBCConnection implements DatabaseConnection {
	
	/**
	 *  Constructor for the OracleConnection object
	 */
	public JDBCConnection() {
	}

	/**
	 *  Gets a connection to Oracle for the url, user, and password
    *  that have been previously set.
	 *
	 *@return                           The Connection value
	 *@exception  ApplicationException  Description of Exception
	 */
	public Connection getConnection() throws ApplicationException {
		/*
		 * Register the driver
		 */
		Connection tConnection = null;
		try {
			Class.forName(AppConfig.getDriver());
			tConnection = DriverManager.getConnection(AppConfig.getDriverUrl(), AppConfig.getUser(), AppConfig.getPassword());
		}
		catch (SQLException se) {
			throw new ApplicationException("Error in JDBCConnection@getConnection : " + se.toString());
		}
		catch (ClassNotFoundException cnfe) {
			throw new ApplicationException("Error in JDBCConnection@getConnection : " + cnfe.toString());
		}
		catch (NullPointerException ne) {
			throw new ApplicationException("Error in JDBCConnection@getConnection : " + ne.toString());
		}
		return tConnection;
	}

}
