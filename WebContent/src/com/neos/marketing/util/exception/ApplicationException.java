package com.neos.marketing.util.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ApplicationException
 * extends the exception class to provide opportunity for
 * expansion.
 * <pre>
 * Copyright: 2015
 * Company: NEOS
 * </pre>
 * @author     NEOS Software, Inc.
 * @created    June 21, 2001
 * @version    1.0
 */
public class ApplicationException extends Exception {

	private Log log = LogFactory.getLog(this.getClass().getName());
	
	public ApplicationException(String message) {
		super(message);
	}

	public ApplicationException(String message, Throwable thrown) {
		super(message);
		log.error(message, thrown);
	}
		
}
